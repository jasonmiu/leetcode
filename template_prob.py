#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import glob

def print_usage():
    print("Create a dir with a empty Leetcode problem and submission.")
    print("USAGE: {} <problem id and name>".format(sys.argv[0]))

def main():

    prob_path = os.path.join(os.getcwd(), "problems")

    if not os.path.exists(prob_path):
        print("No problem path:", prob_path)

    help_msg = """
Create a dir with a empty Leetcode problem and submission.
"""

    ap = argparse.ArgumentParser(add_help=help_msg)
    ap.add_argument("-l", dest="lang",
                    type=str,
                    action="store",
                    default="py",
                    help="program lang. Possible values: [py, cpp]. Default: py.")
    ap.add_argument("-f", action="store_true", dest="force", help="Create templates even same problem ID has been found.")
    ap.add_argument("prob_id", type=int, help="Problem ID")
    ap.add_argument("prob_name", type=str, help="Problem name")
    args = ap.parse_args()

    prob = str(args.prob_id) + " " + args.prob_name

    p = prob.translate({ord(".") : None,
                        ord(" ") : "_"})

    lang_suffixs = {"py" : "py",
                    "cpp" : "cpp"}
    if args.lang not in lang_suffixs.keys():
        print("Unknown lang:", args.lang)
        return -1

    print("p", p)

    ps = p.split("_")
    pid = ps[0]

    if not pid.isdigit():
        print("No problem ID")
        return -1

    has_pid_dir = len(glob.glob(prob_path + "/" + str(pid) + "_" + "*"))
    if has_pid_dir and (not args.force):
        print("Found problem with the same id {} in {}. Exit.".format(pid, prob_path));
        return -1

    pname = p.replace(pid + "_", "")

    if not pname:
        print("No problem name")
        return -1

    pdir = os.path.join(prob_path, p)
    if not os.path.exists(pdir):
        os.mkdir(pdir)

    pfile = os.path.join(pdir, p + ".txt")
    sfile = os.path.join(pdir, "{}.{}.{}".format(pid, pname.replace("_", "-"), lang_suffixs[args.lang]))

    if os.path.exists(pfile):
        print("Problem description file exists. Not create again")
    else:
        f = open(pfile, 'w')
        f.close()

    if os.path.exists(sfile):
        print("Problem code file already exists")
        return -1
    else:
        f = open(sfile, 'w')
        f.close()

    return 0


if __name__ == "__main__":
    sys.exit(main())
