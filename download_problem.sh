#!/usr/bin/env bash

## leetcode-cli on
## https://github.com/skygragon/leetcode-cli/releases

LC=""
type leetcode-cli > /dev/null 2>&1
if [ $? -eq 0 ]; then
    LC="leetcode-cli"
else
    type leetcode > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        LC="leetcode"
    else
        echo "Cannot found leetcode CLI bin"
        exit 1
    fi
fi

function help()
{
    d=$1
    echo "Download LeetCode problem with leetcode-cli"
    echo "Usage:"
    echo "$d <problem id> <output dir>"
}

if [ $# -ne 2 ]; then
    help $0
    exit -1
fi

prob=$1
dest=$2
if [ ! -e "$dest" ]; then
    mkdir -p "$dest"
fi

tempfile=`mktemp`

/usr/bin/env $LC show $prob > $tempfile

if [ $? -ne 0 ]; then
   echo "leetcode download failed"
   exit 1
fi
   
title=`head -1 $tempfile | sed -e 's/[[:space:]]*$//' |tr ' ' '_' | tr -d '[]'`
mkdir -p "$dest"/"$title"
mv $tempfile "$dest"/"$title"/"$title".txt
echo "Downloaded to" "$dest"/"$title"/"$title".txt

