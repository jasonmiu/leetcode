#!/usr/bin/env bash

TAG_DIR=./tags
PROB_DIR=./problems

function realpath()
{
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

function usage()
{
    echo "Link the $PROB_DIR/<code dir> to the tag in the $TAG_DIR"
    echo "<code dir> support globbing"
    echo "$0 <code dir> <tag 1> [tag 2] ... [tag n]"
}

if [ $# -lt 2 ]; then
    usage $0
    exit 1
fi

cnt=`ls -d $PROB_DIR/$1 | wc -l | awk '{print $1}'`
if [ $cnt -ne 1 ]; then
    echo "Please check the inputted source problem: $1"
    exit 1
fi
code_dir=`basename $PROB_DIR/$1`
shift

while [ $# -gt 0 ]
do

    t=$1
    if [ ! -e $TAG_DIR/$t ]; then
        echo "No tag '$t'"
    else
        r=../../$PROB_DIR/$code_dir
        b=`basename $code_dir`
        ln -sfh $r "$TAG_DIR/$t/$b"
    fi
    
    shift
done

