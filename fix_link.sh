#!/usr/env bash

find $1 -type l | while read i
do
    link=`readlink $i`
    new="../$link"
    p=`pwd`
    d=`dirname $i`
    f=`basename $i`
    cd $d
    ln -sf $new $f
    cd $p

done
                        
