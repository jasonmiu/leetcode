#!/usr/bin/env bash

## leetcode-cli on
## https://github.com/skygragon/leetcode-cli/releases/download/

LC=""
type leetcode-cli > /dev/null 2>&1
if [ $? -eq 0 ]; then
    LC="leetcode-cli"
else
    type leetcode > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        LC="leetcode"
    else
        echo "Cannot found leetcode CLI bin"
        exit 1
    fi
fi


function help()
{
    d=$1
    echo "Download LeetCode submitted code with leetcode-cli"
    echo "Usage:"
    echo "$d <problem id> <output dir>"
}

if [ $# -ne 2 ]; then
    help $0
    exit -1
fi

prob=$1
dest=$2
if [ ! -e "$dest" ]; then
    mkdir -p "$dest"
fi

tempdir=`mktemp -d`
/usr/bin/env $LC submission -x $prob -o "$tempdir"
if [ $? -ne 0 ]; then
    echo "leetcode download failed"
    exit 1
fi

d=`find "$dest" -maxdepth 1 -type d -name "${prob}_*"`

if [ -z "$d" ]; then
    echo "Cannot find the downloaded problem in the dir $dest. Will place the submission in the current dir."
    mv "$tempdir"/* ./
else
    mv "$tempdir"/* "${d}"
fi

rmdir $tempdir
exit 0





