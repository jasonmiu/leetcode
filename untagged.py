#!/usr/bin/env python3

import sys
import os
import os.path


def main():

    if len(sys.argv) != 3:
        print("Usage: {} <problem dir> <tags dir>".format(sys.argv[0]))
        sys.exit(-1)
    
    prob_dir = sys.argv[1]
    tag_dir = sys.argv[2]

    tagged = set()
    untagged_probs = []

    cwd = os.getcwd()

    for (dirpath, dirnames, files) in os.walk(tag_dir):
        for d in dirnames:
            if os.path.islink(os.path.join(dirpath, d)):
                tagged.add(d)

    os.chdir(prob_dir)
    for entry in os.listdir():
        if os.path.isdir(entry):
            if entry not in tagged:
                untagged_probs.append(entry)
                
    os.chdir(cwd)

    untagged_probs.sort()

    list(map(print, untagged_probs))

if __name__ == "__main__":
    sys.exit(main())
