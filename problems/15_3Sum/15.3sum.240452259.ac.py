#
# @lc app=leetcode id=15 lang=python3
#
# [15] 3Sum
#
# https://leetcode.com/problems/3sum/description/
#
# algorithms
# Medium (25.07%)
# Total Accepted:    683K
# Total Submissions: 2.7M
# Testcase Example:  '[-1,0,1,2,-1,-4]'
#
# Given an array nums of n integers, are there elements a, b, c in nums such
# that a + b + c = 0? Find all unique triplets in the array which gives the sum
# of zero.
# 
# Note:
# 
# The solution set must not contain duplicate triplets.
# 
# Example:
# 
# 
# Given array nums = [-1, 0, 1, 2, -1, -4],
# 
# A solution set is:
# [
# ⁠ [-1, 0, 1],
# ⁠ [-1, -1, 2]
# ]
# 
#
# The idea is assuming we have 1 number (pivot), find another two numbers in the list
# that can be summed to 0. We sort the list first, so we know the right hand side
# is always larger than left hand side. Then loop for the pivot, and find another 2 numbers
# for the current pivot.
# For example:
# [-1, -1, 0, 1, 2, 4]
#  ^i   ^l          ^r
# let the current pivot at index i, we search the numbers from left index l to right index r
# and move two indice inward until they crossed.
# If sum of 3 numbers is larger than 0, means we want a smaller r (l is already the smallest),
# hence move r to left (r = r - 1)
# If sum of 3 numbers is smaller than 0, means we want a bigger l (r is already the biggest)
# hence move l to right (l = l + 1)
# if sum of 3 numbers is 0, it is the answer.
# However, how to deal with duplicated anwser is tricky. For example:
# [-1, -1, 0, 1, 2, 4]
#  ^i     ^l  ^r
# 
# [-1, -1, 0, 1, 2, 4]
#     ^i  ^l  ^r
# are both anwsers. We deal this with two points:
# 1. If we have seen this pivot value before, means we already searched for the anwser. We can skip the
#    repeated pivot. (var seens)
# 2. If we found an anwser, the number around l or r may has the same value, since it is a sorted list. 
# [-2, -2, -2, 0, 0, 2, 2]
#  ^i          ^l       ^r
# When we found this anwser, we move l and r both inward to check if we can found another pair of numbers 
# for the current pivot
# [-2, -2, -2, 0, 0, 2, 2]
#  ^i            ^l  ^r
# The anwser is repeated. We can check this by looking forward of the indices l and r.
# If the next right of l is same as current l, keep moving l to right, until l and r are crossed.
# If the next left of r is same as current r, keep moving r to left, until l and r are crossed.
# Complexity is O(sort) + O(loop of pivot) * O(loop of l r scan)
# = O(nlogn) + O(n^2) = O(n^2)

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        
        sorted_nums = sorted(nums)
        ans = []
        n = len(nums)
        seens = set()
        for i in range(0, n-2):
            l = i+1
            r = n-1
            if sorted_nums[i] in seens:
                continue
            else:
                seens.add(sorted_nums[i])
            
            while l < r:
                #print("i", i, "l", l, "r", r)
                a = sorted_nums[i] + sorted_nums[l] + sorted_nums[r]
                if a == 0:
                    ans.append([sorted_nums[i], sorted_nums[l], sorted_nums[r]])
                    while (l < r):
                        if sorted_nums[l] == sorted_nums[l+1]:
                            l += 1
                        else:
                            break
                    while (l < r):
                        if sorted_nums[r] == sorted_nums[r-1]:
                            r -= 1
                        else:
                            break
                            
                    l += 1
                    r -=1
                    
                elif a > 0:
                    r -= 1
                elif a < 0:
                    l += 1
                    
        return ans
