// Same as the python solution, but use
// set to handle the duplciated ans.
// Time O(n^2), space O(n) for set.

struct vec_hash {
    int operator() (const vector<int>& v) const {
        return v[0] + v[1] * 31 + v[2] * 97;
    }
};

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {

        unordered_set<vector<int>, vec_hash> ans;
        vector<vector<int>> ansv;

        if (nums.size() < 3) {
            return ansv;
        }

        sort(nums.begin(), nums.end());
        for (int i = 0; i < nums.size()-2; i++) {
            int l = i+1;
            int r = nums.size()-1;

            while (l < r) {
                int a = nums[i];
                int b = nums[l];
                int c = nums[r];

                if (a + b + c == 0) {
                    vector<int> v = {a, b, c};
                    ans.insert(v);

                    while (l < r) {
                        if (nums[l+1] == nums[l]) {
                            l++;
                        } else {
                            break;
                        }
                    }

                    while (l < r) {
                        if (nums[r-1] == nums[r]) {
                            r--;
                        } else {
                            break;
                        }
                    }

                    l ++;
                    r --;
                } else if (a + b + c > 0) {
                    r --;
                } else if (a + b + c < 0) {
                    l ++;
                }
            }
        }

        copy(ans.begin(), ans.end(), back_inserter(ansv));

        return ansv;
    }
};
