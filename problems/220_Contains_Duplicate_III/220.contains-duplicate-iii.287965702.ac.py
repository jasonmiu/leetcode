#
# @lc app=leetcode id=220 lang=python3
#
# [220] Contains Duplicate III
#
# https://leetcode.com/problems/contains-duplicate-iii/description/
#
# algorithms
# Medium (20.23%)
# Total Accepted:    107.2K
# Total Submissions: 527K
# Testcase Example:  '[1,2,3,1]\n3\n0'
#
# Given an array of integers, find out whether there are two distinct indices i
# and j in the array such that the absolute difference between nums[i] and
# nums[j] is at most t and the absolute difference between i and j is at most
# k.
# 
# 
# Example 1:
# 
# 
# Input: nums = [1,2,3,1], k = 3, t = 0
# Output: true
# 
# 
# 
# Example 2:
# 
# 
# Input: nums = [1,0,1,1], k = 1, t = 2
# Output: true
# 
# 
# 
# Example 3:
# 
# 
# Input: nums = [1,5,9,1,5,9], k = 2, t = 3
# Output: false
# 
# 
# 
# 
#
# The idea is to use buckets to divide all intergers in buckets, and the bucket size is t,
# for eg, t = 3
# [... -4 -3 -2 -1 0 1 2 3 4 5 .....]
#      `---------' `-----' `---
#         id -1    id 0     id 1 ...
# So an number, v, divided by t + 1, will fall in the same bucket, we call this bucket_id
# numbers in the same bucket has different at most t. 
# If two numbers, v and u, are fallen in two buckets next to each other, they have chance
# to have a different <= t, like abs(-1 - 1) = 2 < t, so if we see two numbers in two
# adjacent buckets, we compute their different.
# We scan the input nums from left to right. If current index, i, is larger than the 
# window size, k, then we remove the numbers before nums[i - k] from buckets as they are
# no longer valid.
# Complexity is O(n)
# Space complexity is O(n) for bucket

class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:
        
        bucket = {}
        if k <= 0 or t < 0 or len(nums) <= 0:
            return False
                
        for (i, v) in enumerate(nums):
            bucket_id = v // (t+1) # To avoid t = 0 divided by zero
            
            if bucket_id in bucket:
                return True
            
            if bucket_id - 1 in bucket and abs(v - bucket[bucket_id-1]) <= t:
                return True
            
            if bucket_id + 1 in bucket and abs(v - bucket[bucket_id+1]) <= t:
                return True
            
            bucket[bucket_id] = v
            
            if i >= k:
                # start delete the previous values that are out of current window
                to_del = nums[i - k] // (t+1)
                del bucket[to_del]
                
        return False
