// The condidtion of a valid subarray is having only unique elements,
// ie. if we extending the subarray and see a new coming element is already
// in the current subarray, we should shrink the subarray from left to
// remove the repeated element.
// Since the question only ask for the single score of the maximum score
// we can get from a subarray, and all elements are positive, so it is indeed asking,
// find a longest subarray that has only unique elements, and return only the sum.
// So instead of having a double loop, can use a sliding window to find that subarray,
// and the loop invariant of the sliding window should contain only unique elements.
// So start from the input beginnig, keep expanding the sliding window, for each new
// coming element, add it to a counter dict. If that element is already in the counter
// dict, which means we saw that before in the current subarray, and has to remove the
// exisiting element. Indeed can use a set for checking the uniqueness, and shrink
// the window directly from the left since the only possible repeating element
// must be the leftmost one if we keep maintaining the subarray element uniqueness.
// Once we find a repeated element, means we need to see the score we can get from this
// subarray. Save it to the max_sum, and expanding the sliding window to the right until
// it hit the end of the input.
//
// Since we test only when we found a repeated element, do not forget to test the last
// subarray that end at the rightmost element of the input.
//
// Another speed up point is not to find the sum of subarray everytime with the whole window.
// Can keep track of the current sum of the subarray as cur_sum, which add the new element to that
// when expending the window, and subtract the removed element when shrinking the window.
//
// Time: scan the input once, O(n). Space: O(n) for the count dict.
//
// Example:
// [4,2,4,5,6]      count dict = {}
//  ^
//  |- e
//  |- s
//
// Start from the beginning. Check if the element pointed by e is already in count dict. Its not
// so add it to the count dict and expend the current window.
//
// [4,2,4,5,6]      count dict = {4:1}
//  ^ ^
//  | |- e
//  |- s
//
// The new coming element, 2, is not in the count dict. Expend the window and add it to the cound dict.
//
// [4,2,4,5,6]      count dict = {4:1, 2:1}
//  ^   ^
//  |   |- e
//  |- s
//
// The new coming element, 4, is already seen in the count dict. Should shrink the window until we have
// all elements being unique. Move the s pointer until the new coming element is not found in the count dict.
// Then add the new coming element back to the count dict.
//
// [4,2,4,5,6]      count dict = {2:1, 4:1}
//    ^ ^
//    | |- e
//    |- s
//
// Keep expending the window, until it hit the end:
//
// [4,2,4,5,6]      count dict = {2:1, 4:1, 5:1, 6:1}
//    ^       ^
//    |       |- e
//    |- s
//
// Now check the max score one last time, for this last subarray.


class Solution {
public:
    int maximumUniqueSubarray(vector<int>& nums) {

        int s = 0;
        int e = 0;
        unordered_map<int, int> cnt_dict;

        int cur_sum = 0;
        int max_sum = INT_MIN;

        while (e < nums.size()) {
            if (cnt_dict.find(nums[e]) == cnt_dict.end()) {

                cnt_dict[nums[e]] ++;
                cur_sum += nums[e];

            } else {

                max_sum = max(max_sum, cur_sum);

                while (true) {
                    cnt_dict[nums[s]] --;
                    if (cnt_dict[nums[s]] == 0) {
                        cnt_dict.erase(nums[s]);
                    }
                    cur_sum -= nums[s];
                    s++;
                    if (cnt_dict.find(nums[e]) == cnt_dict.end()) {
                        break;
                    }
                }
                cnt_dict[nums[e]] ++;
                cur_sum += nums[e];
            }

            e++;
        }

        max_sum = max(max_sum, cur_sum);

        return max_sum;
    }
};
