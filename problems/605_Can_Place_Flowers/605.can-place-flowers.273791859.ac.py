#
# @lc app=leetcode id=605 lang=python3
#
# [605] Can Place Flowers
#
# https://leetcode.com/problems/can-place-flowers/description/
#
# algorithms
# Easy (31.43%)
# Total Accepted:    75.4K
# Total Submissions: 240K
# Testcase Example:  '[1,0,0,0,1]\n1'
#
# Suppose you have a long flowerbed in which some of the plots are planted and
# some are not. However, flowers cannot be planted in adjacent plots - they
# would compete for water and both would die.
# 
# Given a flowerbed (represented as an array containing 0 and 1, where 0 means
# empty and 1 means not empty), and a number n, return if n new flowers can be
# planted in it without violating the no-adjacent-flowers rule.
# 
# Example 1:
# 
# Input: flowerbed = [1,0,0,0,1], n = 1
# Output: True
# 
# 
# 
# Example 2:
# 
# Input: flowerbed = [1,0,0,0,1], n = 2
# Output: False
# 
# 
# 
# Note:
# 
# The input array won't violate no-adjacent-flowers rule.
# The input array size is in the range of [1, 20000].
# n is a non-negative integer which won't exceed the input array size.
# 
# 
#
# Think we can put 1 flower each time on the flowerbed
# If we can find a place to place a flower, then the 
# flowerbed became 1 spot less. And we put the new flowerbed
# back to the function recusively. If any case we can not find 
# a place to place a flower, we return False back upward.
# For finding a place for placing a flower, we use the function
# find_flower_plot() which scan the current flowerbed in linear.
# But to speed up, we do not need to scan whole flowerbed everytime.
# If we can find a place to place flower last time, that prefix of
# flowerbed has been scanned and verified we can place a flower,
# so we only need to scan the flowerbed from the last spot we
# placed a flower.
# The complexity is O(n), 
# Since we scan elements of flowerbed for n times, which only start from last
# found spot.
# The space complexity is O(1) as we do not need store extra info
# but just the indices.

class Solution:
    
    # Nicer logic expression
    # The point is, when facing the boundary case, like i == 0 (begin), or i == len-1 (end)
    # use the OR short cut if the boundary case is always true. Join both boundary cases with AND
    # to check the common elements in the middle of the list
    def find_flower_plot(self, flowerbed):
        for i in range(0, len(flowerbed)):
            if flowerbed[i] == 0:
                if (i == 0 or flowerbed[i-1] == 0) and (i == (len(flowerbed) - 1) or flowerbed[i+1] == 0):
                    flowerbed[i] = 1
                    return (True, i)
                
        return (False, -1)
                
    
    # updated to the upper version for nicer express of the conditions.
    # Should rem as an idiom when we need to check list elements with
    # its previous and next elements status.
    def __depecated_find_flower_plot(self, flowerbed):
        for i in range(0, len(flowerbed)):
            if flowerbed[i] == 0:
                if i > 0 and i < len(flowerbed) - 1:
                    if flowerbed[i-1] == 0 and flowerbed[i+1] == 0:
                        flowerbed[i] = 1
                        return (True, i)
                    
                if i == 0 and i < len(flowerbed) - 1:
                    if flowerbed[i+1] == 0:
                        flowerbed[i] = 1
                        return (True, i)
                    
                if i > 0 and i == len(flowerbed) - 1:
                    if flowerbed[i-1] == 0:
                        flowerbed[i] = 1
                        return (True, i)
                    
                if i == 0 and i == len(flowerbed) - 1:
                    flowerbed[i] = 1
                    return (True, i)
                
        return (False, -1)
                
                
                
    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:
        
        if n == 0:
            return True
        
        ans = True
        
        (r, pos) = self.find_flower_plot(flowerbed)
        if r:
            ans = self.canPlaceFlowers(flowerbed[pos::], n - 1)
            if not ans:
                return False
        else:
            return False
        
        return ans
        

                
        
