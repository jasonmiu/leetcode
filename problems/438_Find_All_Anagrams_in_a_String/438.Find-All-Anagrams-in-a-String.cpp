// For checking anagrams, there are few useful properties:
// 1. Two anagrams are the same if they have same char multiset
// 2. Two anagrams are the same if all their char counts are then same
// For example,
//                   key:  a b c d e f g h .....
// s = "abb" , count map: [1 2 0 0 0 0 0 0 .....
// p = "bab" , count map: [1 2 0 0 0 0 0 0 .....
// x = "bbc",  count map: [0 2 1 0 0 0 0 0 .....
//
// If the possible number of char used in a string is fixed, like all lower case alpha-letters,
// then the count map can be modeled as a vector of int, where the index is the ascii code offset,
// and the elements are the count of that ascii code (char).
//
// Then we can slide a window with size of p, on s, and create such count map with the chars in the
// window. Another trick for build this count map for a window is, for every window sliding, we
// add a new char from right, and remove a char from left. This means we can add a count of the new char,
// and reduce a count of the old char. So we do not need to re-scan the whole window for every slides.
// If the window is big, this can be very time consuming.
//
// We scan the s once, and each comparison for a window is constance as the count map size is constance,
// so the time is O(n), space is constance so O(1).
//
// Another (time limit over) thinking was, have the p and the windows in s as multi-set. And compare
// the multi-sets for each window. But since the set key size is depends on p, to compare the p multi-set
// and the current window multi-set, we have to check all keys see if they are the same on both sets.
// For m = p size, echo window comparing takes O(m). So whole time will be O(n*m). If m is big ~n, it comes
// a O(n^2) algor. The lesson is to think what info is fixed and can be used.

vector<int> build_cnt_map(const string& str, int s, int e)
{
    vector<int> m(26, 0);

    for (int i = s; i <= e; i++) {
        char c = str[i];
        m[c - 'a'] ++;
    }

    return m;
}

class Solution {
public:
    vector<int> findAnagrams(string s, string p) {

        if (s.size() < p.size()) {
            return vector<int>();
        }

        vector<int> p_map = build_cnt_map(p, 0, p.size()-1);
        int i = 0;
        int j = i + p.size() - 1;

        vector<int> ans;
        vector<int> s_map;
        while (j < s.size()) {
            if (s_map.empty()) {
                s_map = build_cnt_map(s, i, j);
            } else {
                char prev = s[i-1];
                s_map[prev - 'a'] --;
                char c = s[j];
                s_map[c - 'a'] ++;
            }

            if (s_map == p_map) {
                ans.push_back(i);
            }

            i++;
            j++;
        }

        return ans;
    }
};
