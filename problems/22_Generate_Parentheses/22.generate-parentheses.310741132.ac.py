#
# @lc app=leetcode id=22 lang=python3
#
# [22] Generate Parentheses
#
# https://leetcode.com/problems/generate-parentheses/description/
#
# algorithms
# Medium (57.80%)
# Total Accepted:    476.8K
# Total Submissions: 795.3K
# Testcase Example:  '3'
#
#
# Given n pairs of parentheses, write a function to generate all combinations
# of well-formed parentheses.
#
#
#
# For example, given n = 3, a solution set is:
#
#
# [
# ⁠ "((()))",
# ⁠ "(()())",
# ⁠ "(())()",
# ⁠ "()(())",
# ⁠ "()()()"
# ]
#
#
def gen(s, cnt, n, ans_set):
    #print("s", s, "cnt", cnt)
    if cnt == n:
        ans_set.add(s)
        return

    gen(s + "()", cnt+1, n, ans_set)
    gen("({})".format(s), cnt+1, n, ans_set)
    for i in range(0, len(s)):
        if s[i] == '(':
            p = s[0:i+1] + "()" + s[i+1::]
            gen(p, cnt+1, n, ans_set)

    return



class Solution:
    def generateParenthesis(self, n: int) -> List[str]:

        if n == 0:
            return []

        ans_set = set()
        gen("()", 1, n, ans_set)

        ans = []
        for a in ans_set:
            ans.append(a)

        return ans
