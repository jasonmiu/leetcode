// Start with a open parenthesis, we can then generate the combinations,
// for each step, we can select the next choice as open or close parenthesis.
// However, we have to generate the well-formed parenthesis, some choices are
// not possible, for example, n = 2:
//
//                          (
//                     /        \
//                    (           )
//                     \         /
// open p. used up -->  )       (  <-- close parenthesis is not possible here, since the close parenthesis count >= open
//                       \       \
//                        )       )
//
// So we need to keep track both counts of parentheses. If we still have open parenthesis quota,
// then we can select the next one as open parenthesis. If we still have close parentesis quota and the current
// used close is lesser than open, which means we do not have a un-open sequence like : "()))", so we can
// select the next one as close parenthesis. Recursively select both possible paths and generate the sequence
// along the selection path. If both parentheses used up, we save the generated sequence to the result.
//
// Time O(4^n / n * sqtr(n)). Space is the same O(4^n / n * sqtr(n)) for storing all results.

void gen(char p, string cur, int o_cnt, int c_cnt, int n, vector<string>& result)
{
    if (p == '(') {
        o_cnt ++;
    } else {
        c_cnt ++;
    }

    cur.append(1, p);

    if (o_cnt == n && c_cnt == n) {
        result.push_back(cur);
        return;
    }

    if (o_cnt < n) {
        gen('(', cur, o_cnt, c_cnt, n, result);
    }

    if (c_cnt < n && c_cnt < o_cnt) {
        gen(')', cur, o_cnt, c_cnt, n, result);
    }

    return;
}

class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> result;
        string cur;

        gen('(', cur, 0, 0, n, result);

        return result;
    }
};
