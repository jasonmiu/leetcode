# The main idea is to build valid sequences from "()"
# If n is 1, then it is "()".
# If n is 2, then we have "()()" and "(())".
# If n is 3, then we have
# "()()()", "((()))", "(())()", "()(())", "(()())"
# So we can think for each n increase, it is the opeations of
# 1. append ()
# 2. wrap a () on the outside
# 3. for each (), put a () inside
# of the sequences n-1.
# The problem of this method is we will have a lot of duplication, like:
# n = 2 , "()()" "(())"
# when n = 3, the operaton (2) warp "()" outside of "(())" input gives:
# "[(())]" , [ ] is the new parentheses
# But the operation (3) put "()" inside for each () of the "(())" input gives:
# "(([ ])), [] is the new parentheses. Both gives ((())) and make a duplications.
# Used set() to remove the duplications, but the complexity is higher since we redo
# the duplications. Time limit will over with n = 10.
# A better ans is to generate the valid parenthesees with correct sequences.
# For n = 3,
# the input should be :
# "((" , and
# "()"
# If we have enought "(", which is N, when we start to close it with ")"
# If we dun have enought "(", we have two choices:
# 1. Add 1 more "(", and recusive it again.
# 2. Close 1 ")" if current right ")" is lesser than left "("
# The complexity depends on how many valid sequences.

class Solution(object):
    def generateParenthesis(self, N):
        ans = []
        def backtrack(S = '', left = 0, right = 0):
            if len(S) == 2 * N:
                ans.append(S)
                return
            if left < N:
                backtrack(S+'(', left+1, right)
            if right < left:
                backtrack(S+')', left, right+1)

        backtrack()
        return ans

"""

def gen(s, cnt, n, ans_set):
    #print("s", s, "cnt", cnt)
    if cnt == n:
        ans_set.add(s)
        return

    gen(s + "()", cnt+1, n, ans_set)
    gen("({})".format(s), cnt+1, n, ans_set)
    for i in range(0, len(s)):
        if s[i] == '(':
            p = s[0:i+1] + "()" + s[i+1::]
            gen(p, cnt+1, n, ans_set)

    return



class Solution:
    def generateParenthesis(self, n: int) -> List[str]:

        if n == 0:
            return []

        ans_set = set()
        gen("()", 1, n, ans_set)

        ans = []
        for a in ans_set:
            ans.append(a)

        return ans

"""
