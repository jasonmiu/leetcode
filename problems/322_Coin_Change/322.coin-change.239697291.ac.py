#
# @lc app=leetcode id=322 lang=python3
#
# [322] Coin Change
#
# https://leetcode.com/problems/coin-change/description/
#
# algorithms
# Medium (32.30%)
# Total Accepted:    261.8K
# Total Submissions: 810.5K
# Testcase Example:  '[1,2,5]\n11'
#
# You are given coins of different denominations and a total amount of money
# amount. Write a function to compute the fewest number of coins that you need
# to make up that amount. If that amount of money cannot be made up by any
# combination of the coins, return -1.
# 
# Example 1:
# 
# 
# Input: coins = [1, 2, 5], amount = 11
# Output: 3 
# Explanation: 11 = 5 + 5 + 1
# 
# Example 2:
# 
# 
# Input: coins = [2], amount = 3
# Output: -1
# 
# 
# Note:
# You may assume that you have an infinite number of each kind of coin.
# 
#
# The idea is for the amount, it can be splitted into the denominations of
# the coins. For example, coins = [1, 2, 5], amount == 11
#                  11
#             1/   2|   5\
#            10   *9     6
#       1/ 2| 5\ 1/2|5\  1/2|5\
#     *9   8   5 ...... 5  4   1
#   1/2|5\
# It can be viewed as a tree, each node is the amount and the number of branches
# is the number of coins. The children of a node is the amount - the denomination
# of that branch.
# We do a DSF to find the shortest path to the leave of a node (amount). For each
# node, the shortest path is the shortest path of its children, plus 1 which its 
# the number of change of itself.
# If all denominations are larger that the current amount, means it is not possible
# to change so it is not possible to reach a leaf. So the path length (number of change)
# is math.inf.
# The DP trick is that, many subtrees are repeated. From the example above, the subtree 9
# is repeated so the min number of changes is known after the first occurrence. If we saw 
# it before, we can use the saved min number of this amount.


import math
class Solution:
        
    def coinChange(self, coins: List[int], amount: int) -> int:
        
        cache = {}
        
        if amount == 0:
            return 0
        
        ans = self._coinChange(coins, amount, cache)        
        if ans == math.inf:
            return -1
        else:
            return ans
            
        
    def _coinChange(self, coins: List[int], amount: int, cache: dict) -> int:
        #print(amount)
        
        if amount in cache:
            return cache[amount]
        
        min_change = math.inf
        for c in coins:
            #print("c" , c)
            if c > amount:
                continue
            elif c == amount:
                return 1
            else:
                r = amount - c
                m = self._coinChange(coins, r, cache) + 1
                #print("m", m)
                if m <= min_change:
                    min_change = m
                    
        cache[amount] = min_change
        
        return min_change
            
            
        
