// Since we can think the coin combinations are a tree:
// For example, coins = [1, 2, 5], amount == 11
//                  11
//             1/   2|   5\
//            10   *9     6
//       1/ 2| 5\ 1/2|5\  1/2|5\
//     *9   8   5 ...... 5  4   1
//   1/2|5\
//
// The number of coins we need is the height of the tree, and the
// min number we need is the min height of the tree.
// While we can explore all paths to find the min height, but since
// we can see there are a lot of repeating sub-solutions, like,
// for amount of 10, how many coins we need? For amount of 9,
// how many coins we need? And for each sub-solution, assuming
// we know its min number of coins for that amount already,
// can we add the available coins on top of it to hit the final
// amount we want? If possible, then the min number of coin we need for
// the final amount will be that sub-amount + 1. From this, we
// can assume if we know the sub-amount solution, and if plus 1 coin
// can hit the final amount, we know the optimal solution of this final
// amount too. Then if we know the sub-sub-amount solution of the sub-amount,
// and sub-sub-sub-amount solution ... this keep going. Until the amount
// is one of those available coins, as the number we need for that amount
// must be 1. Those will be our root cases.
//
// From this, we can make an array with the size amount. The indices
// are the sub-amounts, while the values are the number of coins we know
// we need for that sub-amount. From beginning, all amounts need MAX coins,
// while the amounts with available coins are initialized to 1.
// Start from amount 1, try add all available coins on this amount.
// If the current amount needed coin + 1 is lesser than the coins
// needed for summed amount, we find a shorter combination, so place the
// summed amount value as the number of coins needed for the current amount + 1.
// Do this until the final amount. The value at final amount is the answer we need.
// If there is no way to sum up as final amount, the value at final amount will stay
// as MAX. In this case we return -1. For c++, since the int on the platform
// is 32bits so sum on that will overflow. Make the size of the coin larger for that.
//
// Exmaple:
//
// coins = [1, 2, 5], amount == 11
//
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 X X 1 X X X X  X  X]
// X is the MAX here. Start with current amount = 1, add all available coins on
// the top of it, so the summed amount is 2 (1+1), 3 (1+2), 6 (1+5)
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 X 1 2 X X X  X  X]
//   i ^ ^     ^
//     | |     | the current needed coin + 1 is smaller than coins needed for 6, update
//     | the current needed coin + 1 is smaller than coins needed for 3, update
//    current needed coin is 1, plus 1 coins is 2, larger than the value at amount 2.
//    so do not update the coins needed for amount 2
//
// current amount = 2
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 2 1 2 2 X X  X  X]
//     i ^ ^     ^
//
// current amount = 3
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 2 1 2 2 3 X  X  X]
//       i ^ ^     ^
//
// current amount = 4
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 2 1 2 2 3 3  X  X]
//         i ^ ^     ^
//
// current amount = 5
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 2 1 2 2 3 3  2  X]
//           i ^ ^      ^
//
// current amount = 6
// 0 1 2 3 4 5 6 7 8 9 10 11
//[0 1 1 2 2 1 2 2 3 3  2  3]
//             i ^ ^       ^
//
// So the coins we need for amount 11 is 3.
//
// Time: O(m*n) for m = amount, n = number of coins. Space O(m) for the array.



class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {

        if (amount == 0) {
            return 0;
        }

        vector<int> dp(amount+1, 65535);
        for (int c : coins) {
            if (c <= amount) {
                dp[c] = 1;
            }
        }
        dp[0] = 0;

        for (int i = 1; i <= amount; i++) {
            for (long long int c : coins) {
                long long int sum = i + c;
                if (sum > amount) {
                    continue;
                }
                if (dp[i] + 1 <= dp[sum]) {
                    dp[sum] = dp[i] + 1;
                }
            }
        }

        if (dp[amount] == 65535) {
            return -1;
        } else {
            return dp[amount];
        }
    }
};
