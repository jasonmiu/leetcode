// This question can be solved by simulating the walk process.
// 1. Start moving to right
// 2. If hit boundary, or a cell visited, turn down
// 3. Keep moving in the same direction, if hit boundary, or a cell visited, turn left
// 4. Keep moving in the same direction, if hit boundary, or a cell visited, turn up
// 5. Keep moving in the same direction, if hit boundary, or a cell visited, turn right
// 6. If visited all cell, stop
//
// So how to save the visited cells? The cell value range is -100 - 100 so can put a value out of
// this range as a marker. For each visited cell, mark it with marker.
// Wrote some helper functions for easier abstract the concepts.
//
// Time: O(m*n), Space: O(1).


enum direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
};

direction change_dir(direction d)
{
    switch (d) {
        case RIGHT:
            return DOWN;
        case DOWN:
            return LEFT;
        case LEFT:
            return UP;
        case UP:
            return RIGHT;
    }

    return d;
}

pair<int,int> update_pos(pair<int,int> pos, direction d)
{
    switch (d) {
        case RIGHT:
            return {pos.first, pos.second+1};
        case DOWN:
            return {pos.first+1, pos.second};
        case LEFT:
            return {pos.first, pos.second-1};
        case UP:
            return {pos.first-1, pos.second};
    }

    return pos;
}

void spiral(vector<vector<int>>& a, pair<int,int> pos, direction d, vector<int>& path)
{
    int m = a.size();
    int n = a[0].size();

    int asize = m * n;
    if (path.size() == asize) {
        return;
    }

    path.push_back(a[pos.first][pos.second]);
    a[pos.first][pos.second] = INT_MAX;

    pair<int,int> new_pos = update_pos(pos, d);
    if (new_pos.first < 0 || new_pos.first >= m || new_pos.second < 0 || new_pos.second >= n ||
        a[new_pos.first][new_pos.second] == INT_MAX) {
        direction new_dir = change_dir(d);
        new_pos = update_pos(pos, new_dir);
        d = new_dir;
    }

    spiral(a, new_pos, d, path);

    return;
}

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        vector<int> path;

        spiral(matrix, {0,0}, RIGHT, path);

        return path;
    }
};
