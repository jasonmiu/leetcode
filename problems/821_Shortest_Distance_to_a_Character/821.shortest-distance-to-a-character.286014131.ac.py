#
# @lc app=leetcode id=821 lang=python3
#
# [821] Shortest Distance to a Character
#
# https://leetcode.com/problems/shortest-distance-to-a-character/description/
#
# algorithms
# Easy (64.56%)
# Total Accepted:    49.4K
# Total Submissions: 75.9K
# Testcase Example:  '"loveleetcode"\n"e"'
#
# Given a string S and a character C, return an array of integers representing
# the shortest distance from the character C in the string.
# 
# Example 1:
# 
# 
# Input: S = "loveleetcode", C = 'e'
# Output: [3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0]
# 
# 
# 
# 
# Note:
# 
# 
# S string length is in [1, 10000].
# C is a single character, and guaranteed to be in string S.
# All letters in S and C are lowercase.
# 
# 
#
# The idea is do two passes, first from right to left, and then
# left to right. In the first pass we know the distance between
# a char S[i] and the target C on the left of S[i], example:
# lovelee, C = 'e'
# 7770100 <-- The init distance is n, when we see C, S[i] must be
# 0. Mark the last index of C, the chars on its right will update the
# distance between this C index
# Then in the 2nd pass, we know the distance between S[i] and the target C
# on the right of S[i]. Compare the current distance to the distance in 
# the previous pass, update with the smaller one.
# lovelee, C = 'e'
# 7770100 <-- pass 1
# 3210100 <-- pass 2
# Complexity is O(n)
# Space complexity is O(n) for the result


class Solution:
    def shortestToChar(self, S: str, C: str) -> List[int]:
        
        n = len(S)
        
        # Max distance between each char must be < len
        ans = [n] * n
        idx = None
        for i in range(0, n):
            c = S[i]
            if c == C:
                ans[i] = 0
                idx = i
                
            if idx != None:
                ans[i] = abs(i - idx)
                
        idx = None
        for i in range(n-1, -1, -1):
            c = S[i]
            if c == C:
                idx = i
                
            if idx != None:
                ans[i] = min(abs(i-idx), ans[i])
                
        return ans
            
        
# Here is another solution, but slower. The idea is also two passes.
# First pass to use a dict to save all index of target C.
# The 2nd pass will compute the distances between S[i] and each index in dict[C]
# The result is the min of those distances.
# Wrose case can be O(n^2), if S has all chars of C, like:
# S = aaa, C = a
# dict['a'] = [0, 1, 2]
# Then for each S[i], it need to access n of items in dict[C], hence worse O(n^2)

# class Solution:
#    def shortestToChar(self, S: str, C: str) -> List[int]:
#        
#        cindex = collections.defaultdict(list)
#        
#        for i in range(0, len(S)):
#            c = S[i]
#            if c == C:
#                cindex[c].append(i)
#           
#        ans = []
#        for i in range(0, len(S)):
#            s = S[i]
#            
#            a = min(map(lambda j: abs(i-j), cindex[C]))
#           
#        return ans
