#
# @lc app=leetcode id=966 lang=python3
#
# [966] Vowel Spellchecker
#
# https://leetcode.com/problems/vowel-spellchecker/description/
#
# algorithms
# Medium (45.54%)
# Total Accepted:    8.4K
# Total Submissions: 18.4K
# Testcase Example:  '["KiTe","kite","hare","Hare"]\n["kite","Kite","KiTe","Hare","HARE","Hear","hear","keti","keet","keto"]'
#
# Given a wordlist, we want to implement a spellchecker that converts a query
# word into a correct word.
# 
# For a given query word, the spell checker handles two categories of spelling
# mistakes:
# 
# 
# Capitalization: If the query matches a word in the wordlist
# (case-insensitive), then the query word is returned with the same case as the
# case in the wordlist.
# 
# 
# Example: wordlist = ["yellow"], query = "YellOw": correct = "yellow"
# Example: wordlist = ["Yellow"], query = "yellow": correct = "Yellow"
# Example: wordlist = ["yellow"], query = "yellow": correct =
# "yellow"
# 
# 
# Vowel Errors: If after replacing the vowels ('a', 'e', 'i', 'o', 'u') of the
# query word with any vowel individually, it matches a word in the wordlist
# (case-insensitive), then the query word is returned with the same case as the
# match in the wordlist.
# 
# Example: wordlist = ["YellOw"], query = "yollow": correct = "YellOw"
# Example: wordlist = ["YellOw"], query = "yeellow": correct = "" (no
# match)
# Example: wordlist = ["YellOw"], query = "yllw": correct = "" (no
# match)
# 
# 
# 
# 
# In addition, the spell checker operates under the following precedence
# rules:
# 
# 
# When the query exactly matches a word in the wordlist (case-sensitive), you
# should return the same word back.
# When the query matches a word up to capitlization, you should return the
# first such match in the wordlist.
# When the query matches a word up to vowel errors, you should return the first
# such match in the wordlist.
# If the query has no matches in the wordlist, you should return the empty
# string.
# 
# 
# Given some queries, return a list of words answer, where answer[i] is the
# correct word for query = queries[i].
# 
# 
# 
# Example 1:
# 
# 
# Input: wordlist = ["KiTe","kite","hare","Hare"], queries =
# ["kite","Kite","KiTe","Hare","HARE","Hear","hear","keti","keet","keto"]
# Output: ["kite","KiTe","KiTe","Hare","hare","","","KiTe","","KiTe"]
# 
# 
# 
# Note:
# 
# 
# 1 <= wordlist.length <= 5000
# 1 <= queries.length <= 5000
# 1 <= wordlist[i].length <= 7
# 1 <= queries[i].length <= 7
# All strings in wordlist and queries consist only of english letters.
# 
# 
#
def lower_no_vowel(w):
    w = w.lower()
    n= []
    
    for i in range(0, len(w)):
        c = w[i]
        if c in ('a', 'e', 'i', 'o', 'u'):
            c = '-'
        n.append(c)
        
    return ''.join(n)


# The idea is generate different hash table for different precedences.
# So we do not need to handle if two words in wordlist hash into the same key.
# One trick is the translate the no vowel.
# Question says:
# If after replacing the vowels ('a', 'e', 'i', 'o', 'u') of the query word
# with any vowel individually, it matches a word in the wordlist
# If wordlist has "leet, lite", query "liit" match "leet", and "late" match lite.
# query "liiit" has no match (too many vowel), and "aite" has no match too 
# (wrong vowel posistion).
# So we can generalize the word to replace the vowel char and any non-alphabet, like
# leet -> l--t, then liit will also be translated to 'l--t' hence can be caught by hash
# Complexity is O(n), n is the number of queries. space is also O(n) for hash tables.



class Solution:
    def spellchecker(self, wordlist: List[str], queries: List[str]) -> List[str]:
        
        d_match = set(wordlist)
        d_lower = dict()
        d_no_vowel = dict()
        
        for w in wordlist:
            lower_w = w.lower()
            
            if lower_w not in d_lower:
                d_lower[lower_w] = w
                
            lower_no_vow_w = lower_no_vowel(w)
            if lower_no_vow_w not in d_no_vowel:
                d_no_vowel[lower_no_vow_w] = w
                
        ans = []
        for q in queries:
            if q in d_match:
                ans.append(q)
                continue
                
            if q.lower() in d_lower:
                ans.append(d_lower[q.lower()])
                continue
                
            if lower_no_vowel(q) in d_no_vowel:
                ans.append(d_no_vowel[lower_no_vowel(q)])
                continue
                
            ans.append("")
            
        return ans
            
        
