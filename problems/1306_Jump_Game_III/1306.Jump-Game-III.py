# The main idea is for each step, at index i, 
# we can choice move forward A[i] or backward A[i] steps.
# And it will land to a new index i+A[i] or i-A[i], hence is
# is a recursive relation and can be searched by DFS.
# For the case has multiple possible path to the goal, A[i] == 0,
# Some indices will be visited multiple times, for example:
# arr = [4,2,3,0,3,1,2], start = 5
#                  5 (v:1)
#             /               \
#         *4 (v:3)             6 (v:2)
#         /                     \
#       1 (v:2)                 *4 (v:3)
#     /
#    3 (v:0)
# The index 4 will be visited multiple time. So we can cache it like DP.
# The hard thing is when we have no solution, like:
# arr = [1, 1], start = 0
# Seems we do not have a terminal case. But indeed when we search a path,
# if we see the same index appears again on the current path, we are seeing
# a loop and will not reach the goal. So in this case we use a set to rem
# the current path. If we revisit the same index on the same path, 
# return False.
# Complexity is O(n) as we will visit all indices once to build the cache.
# Space is O(n) for the cache.


def dfs(arr, n, i, cache, path):
    
    if arr[i] == 0:
        return True
    
    if i in cache:
        return cache[i]
    
    if i in path:
        return False
    
    path.add(i)

    v = arr[i]
    r1 = r2 = False
    if i - v >= 0:
        r1 = dfs(arr, n, i-v, cache, path)
        
    if i + v < n:
        r2 = dfs(arr, n, i+v, cache, path)
        
    r = r1 or r2
    cache[i] = r
    
    path.remove(i)
    
    return r
    

class Solution:
    def canReach(self, arr: List[int], start: int) -> bool:
        ans = dfs(arr, len(arr), start, dict(), set())
        return ans
