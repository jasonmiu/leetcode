#
# @lc app=leetcode id=126 lang=python3
#
# [126] Word Ladder II
#
# https://leetcode.com/problems/word-ladder-ii/description/
#
# algorithms
# Hard (19.32%)
# Total Accepted:    164.1K
# Total Submissions: 793K
# Testcase Example:  '"hit"\n"cog"\n["hot","dot","dog","lot","log","cog"]'
#
# Given two words (beginWord and endWord), and a dictionary's word list, find
# all shortest transformation sequence(s) from beginWord to endWord, such
# that:
# 
# 
# Only one letter can be changed at a time
# Each transformed word must exist in the word list. Note that beginWord is not
# a transformed word.
# 
# 
# Note:
# 
# 
# Return an empty list if there is no such transformation sequence.
# All words have the same length.
# All words contain only lowercase alphabetic characters.
# You may assume no duplicates in the word list.
# You may assume beginWord and endWord are non-empty and are not the same.
# 
# 
# Example 1:
# 
# 
# Input:
# beginWord = "hit",
# endWord = "cog",
# wordList = ["hot","dot","dog","lot","log","cog"]
# 
# Output:
# [
# ⁠ ["hit","hot","dot","dog","cog"],
# ["hit","hot","lot","log","cog"]
# ]
# 
# 
# Example 2:
# 
# 
# Input:
# beginWord = "hit"
# endWord = "cog"
# wordList = ["hot","dot","dog","lot","log"]
# 
# Output: []
# 
# Explanation: The endWord "cog" is not in wordList, therefore no possible
# transformation.
# 
# 
# 
# 
# 
#
# In the question, the
# "Only one letter can be changed at a time"
# means, for two same length words, only 1 char at only 1 posistion can be
# different, like:
# hit <-> hot
# tot and tto are not transformable as two chars (ot -> to) were changed.
# For example, ["hot","dot","dog","lot","log","cog"] from "hit" -> "cog", 
# For exach transform, we can see 1 word can move to another word, so:
#
# hit ---> hot ---> dot ---> dog ---> cog
#               +        +           /
#               \-> lot  \-> log --/ 
#
# We want to find the shortest path from "hit" to "cog", hence this is a 
# graph problem. To find the shortest path of a unweighted graph, we can
# use BFS.
# First, we need to build the graph. Since we can have only 1 char change 
# of each word, for each word we can replace 1 char 1 by 1 to a special
# char, which means "don't care" char and make a transform pattern. And hash those
# possible transform patterns to a dict. For 2 words that are transformable
# to each others, they have same transform pattern. For example:
# word  | transform pattern
# hit   | *it, h*t, hi*
# hot   | *ot, h*t, ho*
# dot   | *ot, d*t, do*
# So "hit" can transform to "hot" with "h*t" transform pattern matching, 
# "hot" can transform to "dot" with "*ot" transform pattern matching.
# We can then make a adjacency matrix:
# [hit] : [hot]
# [hot] : [hit, dot, lot]
# [dot] : [hot, dog, lot]
# [dog] : [dot, log, cog]
# [cog] : [dog, log]
# [lot] : [hot, dot, log]
# [log] : [lot, dog, cog]
# Then we can run the BFS and it will get the shortest path to target by layer to layer.
# One implementation point is this graph is bi-directional. So for a node it can
# go back to the previous one. So we need to save the visited nodes. If we save the
# visited nodes as the current path with List, then for each checking we need to go thru
# O(n) nodes which n is the number of nodes (words). So we need to use a Set for visited nodes.
# We do not need to restrict it check only the current path as if we are going visit any
# visited node, that will only increase the path length hence can be eliminated.
# Complexity O(n) for building the graph, and the BFS it can visit all nodes once for worse case, 
# hence total complexity is O(n).
# Space is O(n) for the graph.


import collections

def build_words_map(words):
    h = collections.defaultdict(list)
    for w in words:
        for i in range(0, len(w)):
            p = w[0:i] + '*' + w[i+1::]
            h[p].append(w)
            
    return h

def next_tran(w, words_map):
    nexts = []
    for i in range(0, len(w)):
        p = w[0:i] + '*' + w[i+1::]
        ws = words_map[p]
        for w2 in ws:
            if w2 != w:
                nexts.append(w2)
    return nexts
            

def build_graph(words):
    g = dict()
    
    h = build_words_map(words)
    
    for w in words:
        nexts = next_tran(w, h)
        g[w] = nexts
        
    return g

def bfs(g, root, end_word, ans):
    q = [([], root)]
    visited = set()
    stop = False
    while len(q) > 0 and not stop:
        qlen = len(q)
        
        for i in range(0, qlen):
            (p, n) = q.pop(0)
            visited.add(n)
        
            for m in g[n]:
                if m not in visited:
                    q.append((p + [n], m))
            
            if n == end_word:
                ans.append(p + [n])
                stop = True
                
    return           
            

class Solution:
    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:
        
        if endWord not in wordList:
            return []
        
        g = build_graph([beginWord] + wordList)
        ans = []
        bfs(g, beginWord, endWord, ans)

        return ans

        
