#
# @lc app=leetcode id=304 lang=python3
#
# [304] Range Sum Query 2D - Immutable
#
# https://leetcode.com/problems/range-sum-query-2d-immutable/description/
#
# algorithms
# Medium (34.79%)
# Total Accepted:    89K
# Total Submissions: 252.7K
# Testcase Example:  '["NumMatrix","sumRegion","sumRegion","sumRegion"]\n[[[[3,0,1,4,2],[5,6,3,2,1],[1,2,0,1,5],[4,1,0,1,7],[1,0,3,0,5]]],[2,1,4,3],[1,1,2,2],[1,2,2,4]]'
#
# Given a 2D matrix matrix, find the sum of the elements inside the rectangle
# defined by its upper left corner (row1, col1) and lower right corner (row2,
# col2).
# 
# 
# 
# The above rectangle (with the red border) is defined by (row1, col1) = (2, 1)
# and (row2, col2) = (4, 3), which contains sum = 8.
# 
# 
# Example:
# 
# Given matrix = [
# ⁠ [3, 0, 1, 4, 2],
# ⁠ [5, 6, 3, 2, 1],
# ⁠ [1, 2, 0, 1, 5],
# ⁠ [4, 1, 0, 1, 7],
# ⁠ [1, 0, 3, 0, 5]
# ]
# 
# sumRegion(2, 1, 4, 3) -> 8
# sumRegion(1, 1, 2, 2) -> 11
# sumRegion(1, 2, 2, 4) -> 12
# 
# 
# 
# Note:
# 
# You may assume that the matrix does not change.
# There are many calls to sumRegion function.
# You may assume that row1 ≤ row2 and col1 ≤ col2.
# 
# 
#
# This is asking the algorithm of Summed Area Table (SAT),
# ref: https://blog.demofox.org/2018/04/16/prefix-sums-and-summed-area-tables/
# The main part is building the SAT with build_sat().
# It is using a DP, for a matrix and its SAT:
# [ 3 0 1 ]     [ 3     3       4     ]
# [ 5 6 3 ]     [ 8    14(C)   18(B) ]
# [ 1 2 0 ]     [ 9    17(A)   X    ]
# We are at position "X", the left of X (point A, 17) is the sum of
# the position (0, 0) to A, the matrix area:
# [3 0]
# [5 6]
# [1 2]
# The top of X, point B (18) is the sum of the position of (0,0) to B, the matrix area:
# [3 0 1]
# [5 6 3]
# Added A and B, we have the sum of area of matrix of the left of A, and top of B, but the 
# area of C, is added twice. So we substract C, and we get X.
# The sumRegion() is applying the SAT algorithm. The point is when we operate the SAT table,
# we need to exclude the input positions, for example, this SAT:
# [ 3     3    4 ]
# [ 8    14   18 ]
# [ 9    17   21 ]
# the input positions are (1, 1), (2, 2). We need to do 21 - 4 - 9 + 3.
# The complexity is O(M*N), where M*N is the dimension of the matrix, for building the SAT.
# Each subRegion() call takes O(1).
# The space complexity is O(M*N) for storing SAT

class NumMatrix:

    def __init__(self, matrix: List[List[int]]):
        self.M = len(matrix)
        if self.M > 0:
            self.N = len(matrix[0])
        else:
            self.N = 0
        
        self.matrix = matrix
        
        self.sat = []
        for r in range(0, self.M):
            self.sat.append([0] * self.N)
            
        self.build_sat()
        
    def build_sat(self):
        for r in range(0, self.M):
            for c in range(0, self.N):
                #  LU | RU
                # ----+----
                #  LB | RB (here)
                
                if r > 0:
                    ru = self.sat[r-1][c]
                else:
                    ru = 0
                
                if c > 0:
                    lb = self.sat[r][c-1]
                else:
                    lb = 0
                    
                if r > 0 and c > 0:
                    lu = self.sat[r-1][c-1]
                else:
                    lu = 0
                    
                self.sat[r][c] = self.matrix[r][c] + ru + lb - lu
                
        

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:
        a = self.sat[row1 - 1][col1 - 1] if (row1 > 0 and col1 > 0) else 0        
        b = self.sat[row1 - 1][col2] if (row1 > 0) else 0
        c = self.sat[row2][col1 - 1] if (col1 > 0) else 0
        d = self.sat[row2][col2]
        
        return (d - b - c + a)
            
        


# Your NumMatrix object will be instantiated and called as such:
# obj = NumMatrix(matrix)
# param_1 = obj.sumRegion(row1,col1,row2,col2)
