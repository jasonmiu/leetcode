# TO EXPLAIN
#
# @lc app=leetcode id=387 lang=python3
#
# [387] First Unique Character in a String
#
# https://leetcode.com/problems/first-unique-character-in-a-string/description/
#
# algorithms
# Easy (50.86%)
# Total Accepted:    337.3K
# Total Submissions: 663.2K
# Testcase Example:  '"leetcode"'
#
# 
# Given a string, find the first non-repeating character in it and return it's
# index. If it doesn't exist, return -1.
# 
# Examples:
# 
# s = "leetcode"
# return 0.
# 
# s = "loveleetcode",
# return 2.
# 
# 
# 
# 
# Note: You may assume the string contain only lowercase letters.
# 
#
from collections import defaultdict
import math

class Solution:
    def firstUniqChar(self, s: str) -> int:
        seen = defaultdict(list)
        
        for (i, c) in enumerate(s):
            seen[c].append(i)
        
        min_idx = math.inf
        for (k, v) in seen.items():
            if len(v) == 1:
                if v[0] <= min_idx:
                    min_idx = v[0]
                    
        if min_idx == math.inf:
            return -1
        else:
            return min_idx
            

