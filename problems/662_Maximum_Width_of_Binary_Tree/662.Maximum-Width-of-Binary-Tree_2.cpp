/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void fill_level(TreeNode* root, unsigned long long cur_idx, unsigned int h, unordered_map<int, unsigned long long>& levels, int& max_w)
{
    if (!root) {
        return;
    }

    int w = 1;

    if (levels.find(h) == levels.end()) {
        levels[h] = cur_idx;
    } else {
        w = cur_idx - levels[h] + 1;
    }

    max_w = max(max_w, w);

    fill_level(root->left, cur_idx*2+1, h+1, levels, max_w);
    fill_level(root->right, cur_idx*2+2, h+1, levels, max_w);

    return;
}

class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {

        if (!root) {
            return 0;
        }

        int max_w = 0;
        unordered_map<int, unsigned long long> levels;
        fill_level(root, 0, 0, levels, max_w);

        return max_w;
    }

};
