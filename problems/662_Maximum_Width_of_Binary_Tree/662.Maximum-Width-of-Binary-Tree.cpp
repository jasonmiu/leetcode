// The question is not very clear, but the width of a level is indeed meaning
// the empty node spaces between two nodes should be filled from root as a
// full tree. Example:
//
//               1
//            /     \
//           3       2
//          / \     / \
//         5   N   N   9
//        / \  /| |\  / \
//       6   N NN NN N   7  <-- at this level, the width is 8, instead of 4.
//
//
//             1
//           /   \
//          3     2
//         / \    / \
//        5   N  N   N <-- has no empty node space *in between*, so width is 1, not 4.
//
// So instead of counting the actual nodes of each level, we are asked to find the width
// with possible empty node space in-between.
//
// The idea is, assuming we have a full binary tree, and counting it from top and left to right,
// then the width is the different between left and right node order in each level. For example:
//
//              0
//           /    \
//          1      2
//         / \    / \
//        3   4  5   6
//
// So if the current node is a left child, its order is its parent order * 2 + 1,
// if it is a right child, its order is its parent order * 2 + 2. This is similar to the heap in an
// array representation.
// If we can write the order value to the node value, and then walk each level to find the left and right
// most order value, we can know each level width. So for the order filling, we can use DFS to find the order
// of each node, with the parent and left/right child info. For level walking, do a BFS to get the left and right
// most node order value.
// While this algorithm works, for C++ there is a problem. The TreeNode.val is a int type, and the order value of
// a node is depending on its height, which is 2^h. If a tree is very deep, for example a singal side linear tree,
// the order value of signed int can be overflow and signed int overflow is a UDB in C++. And we cannot change the
// TreeNode struct. To get over the overflow, use a unsigned int, and have a hash map, which the key is the height,
// the value is a array of all the node order values on that height as unsigned int. While unsigned int can be
// overflow too if order value is big, but it can wrap back and if all values are wrapped back the differents
// between them are the same just like the non-overflow values. This works like a table and the level walking
// part is to do down the table and find the largest different between first and last value of each level.
// Time: O(n), as accessing all nodes once. Space is O(n) for the table. If can use tree itself, the space can be O(1),
// which is the python version, which support big number without overflow issue.
//
// The table for C++ can be smaller as O(logn), which only need to store the starting order value of each level.
// It is because when DFS access the level, it will access the same level multiple times, and if we do a
// pre-order walk, ie. current node -> left -> right, then when DFS access the same level again, it must accessing
// the right-most node on the level at that moment. So save the very first node order value for each level,
// and if we see the same level again, we are at the right-most node, so current width of that level can only
// be increasing. Find the current width, if it is the global max, save it. Finally we will have the biggest
// width. The implementation is in c++ solution version 2.


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void fill_level(TreeNode* root, unsigned long long cur_idx, unsigned int h, unordered_map<int, vector<unsigned long long>>& levels)
{
    if (!root) {
        return;
    }

    levels[h].push_back(cur_idx);

    fill_level(root->left, cur_idx*2+1, h+1, levels);
    fill_level(root->right, cur_idx*2+2, h+1, levels);

    return;
}

class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {

        if (!root) {
            return 0;
        }

        int max_w = 0;
        unordered_map<int, vector<unsigned long long>> levels;
        fill_level(root, 0, 0, levels);

        for (const auto& p : levels) {

            const auto& v = p.second;

            int front_val = v.front();
            int last_val = v.back();
            int w = last_val - front_val + 1;
            max_w = max(w, max_w);
        }

        return max_w;
    }
};

// signed int overflow with root->val
/****
 **** void fill_order(TreeNode* root, int parent_idx, bool is_left)
 **** {
 ****     if (root == nullptr) {
 ****         return;
 ****     }
 ****
 ****     int i = parent_idx * 2;
 ****     if (is_left) {
 ****         i = i + 1;
 ****     } else {
 ****         i = i + 2;
 ****     }
 ****
 ****     root->val = i;
 ****
 ****     fill_order(root->left, root->val, true);
 ****     fill_order(root->right, root->val, false);
 ****
 ****     return;
 **** }
 ****
 **** class Solution {
 **** public:
 ****     int widthOfBinaryTree(TreeNode* root) {
 ****
 ****         if (root) {
 ****             root->val = 0;
 ****             fill_order(root->left, 0, true);
 ****             fill_order(root->right, 0, false);
 ****         }
 ****
 ****         int max_w = INT_MIN;
 ****         queue<TreeNode*> q;
 ****         q.push(root);
 ****
 ****         while (q.size() != 0) {
 ****             TreeNode* node;
 ****             int qsize = q.size();
 ****
 ****             int w = 0;
 ****             if (q.front() && q.back()) {
 ****                 int first_val = q.front()->val;
 ****                 int last_val = q.back()->val;
 ****                 w = last_val - first_val + 1;
 ****             }
 ****             max_w = max(w, max_w);
 ****
 ****             for (int i = 0; i < qsize; i++) {
 ****                 node = q.front();
 ****                 q.pop();
 ****                 if (node->left) {
 ****                     q.push(node->left);
 ****                 }
 ****
 ****                 if (node->right) {
 ****                     q.push(node->right);
 ****                 }
 ****             }
 ****         }
 ****
 ****         return max_w;
 ****     }
 **** };
 ****/
