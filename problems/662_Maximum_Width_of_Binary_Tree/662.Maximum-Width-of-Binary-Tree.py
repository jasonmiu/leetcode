# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def fill_order(root, parent_idx, is_left):
    if root == None :
        return

    i = parent_idx * 2
    if is_left:
        i = i + 1
    else:
        i = i + 2

    root.val = i

    fill_order(root.left, root.val, True)
    fill_order(root.right, root.val, False)

    return


import queue

class Solution:
    def widthOfBinaryTree(self, root: Optional[TreeNode]) -> int:

        if root:
            root.val = 0
            fill_order(root.left, 0, True)
            fill_order(root.right, 0, False)

        max_w = 0
        q = queue.Queue()
        q.put(root)

        while q.qsize() != 0 :
            qsize = q.qsize()

            w = 0
            first_node = None
            last_node = None
            node = None

            for i in range(0, qsize):
                node = q.get()
                if first_node == None:
                    first_node = node

                if node.left:
                    q.put(node.left)
                if node.right:
                    q.put(node.right)

            last_node = node

            w = last_node.val - first_node.val + 1
            max_w = max(w, max_w)

        return max_w
