# This is the binary insert. 
# works like bisect.bisect_left but return index if hit.
# Have to be careful, the s, e is the start and end indices,
# which should never go outside of the nums array range.
# complexity: O(logn)
# Space: O(1)

def bin_insert(nums, t, s, e):
    n = (e - s) + 1
    m = (n//2) + s
    
    if nums[m] == t:
        return m
    
    if n == 1:
        if t < nums[m]:
            return m
        elif t > nums[m]:
            return m + 1
    
    if t < nums[m]:
        return bin_insert(nums, t, s, m-1)
    elif t > nums[m]:
        return bin_insert(nums, t, m, e)


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        ans = bin_insert(nums, target, 0, len(nums) - 1)
        
        return ans

