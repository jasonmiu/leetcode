//  A binary search insert.
//  the point is the two indices, s and e, which are the starting and ending
//  indices of the search window, can be overlapped, which test for the last
//  element.
//   0 1 2 3
//  [1 2 3 4] , target = 0
//   s     e  | m = (3 - 0) / 2 + 0 = 1, [m] = 2 > target
//   se       | m = 0, [m] = 1 > target
// e s        | e = -1
//
//  the point is the "e" pointer is pointing to the left of the target position.
//  Time = O(logn)

class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {

        int s = 0;
        int e = nums.size() - 1;
        int m = 0;
        while (s <= e) {
            m = s + ((e - s) / 2); // prevent overflow

            if (nums[m] == target) {
                return m;
            } else if (nums[m] < target) {
                s = m + 1;
            } else if (nums[m] > target) {
                e = m - 1;
            }
        }

        return e + 1;
    }
};
