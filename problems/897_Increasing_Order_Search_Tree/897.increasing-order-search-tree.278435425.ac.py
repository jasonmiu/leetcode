#
# @lc app=leetcode id=897 lang=python3
#
# [897] Increasing Order Search Tree
#
# https://leetcode.com/problems/increasing-order-search-tree/description/
#
# algorithms
# Easy (66.18%)
# Likes:    383
# Dislikes: 377
# Total Accepted:    45.5K
# Total Submissions: 68.6K
# Testcase Example:  '[5,3,6,2,4,null,8,1,null,null,null,7,9]'
#
# Given a binary search tree, rearrange the tree in in-order so that the
# leftmost node in the tree is now the root of the tree, and every node has no
# left child and only 1 right child.
# 
# 
# Example 1:
# Input: [5,3,6,2,4,null,8,1,null,null,null,7,9]
# 
# ⁠      5
# ⁠     / \
# ⁠   3    6
# ⁠  / \    \
# ⁠ 2   4    8
# /        / \ 
# 1        7   9
# 
# Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
# 
# ⁠1
# \
# 2
# \
# 3
# \
# 4
# \
# 5
# \
# 6
# \
# 7
# \
# 8
# \
# ⁠                9  
# 
# Note:
# 
# 
# The number of nodes in the given tree will be between 1 and 100.
# Each node will have a unique integer value from 0 to 1000.
# 
# 
#

# @lc code=start
#
# @lc app=leetcode id=897 lang=python3
#
# [897] Increasing Order Search Tree
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# By definition, a BST all nodes in left subtree are smaller
# than current node, all nodes in right subtree are smaller than
# current node. So walk the tree in the order of
# left + current + right will give the sorted result. This is
# in-order walk.
# Complexity is O(n) as walk all node once.
# Space complexity is O(n) for a tree copy.

def tree_sort(root):

    if root == None:
        return [None]

    n = TreeNode(root.val)
    left = []
    right = []
    if root.left:
        left = tree_sort(root.left)
        left[-1].right = n
        
    if root.right:
        right = tree_sort(root.right)
        n.right = right[0]

    return left + [n] + right
    

class Solution:
    def increasingBST(self, root: TreeNode) -> TreeNode:

        r = tree_sort(root)
        return r[0]

        
# @lc code=end

# @lc code=end
