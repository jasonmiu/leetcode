#
# @lc app=leetcode id=143 lang=python3
#
# [143] Reorder List
#
# https://leetcode.com/problems/reorder-list/description/
#
# algorithms
# Medium (32.89%)
# Total Accepted:    201.8K
# Total Submissions: 583.4K
# Testcase Example:  '[1,2,3,4]'
#
# Given a singly linked list L: L0→L1→…→Ln-1→Ln,
# reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…
# 
# You may not modify the values in the list's nodes, only nodes itself may be
# changed.
# 
# Example 1:
# 
# 
# Given 1->2->3->4, reorder it to 1->4->2->3.
# 
# Example 2:
# 
# 
# Given 1->2->3->4->5, reorder it to 1->5->2->4->3.
# 
# 
#
# For a list, 
# N = 4 (last index)
# node_0 -> node_1 -> node_2 -> node_3 -> node_4 -> null
# Change to 
# node_0 -> node_4 -> node_1 -> node_3 -> node_2 -> null
# So for node_i -> node_i+1, we change it to node_i -> node_N-i
# And node_N-i -> node_i+1
# And node_N-i-1 -> null
# The first step is like, for i = 0:
# node_0 -> node_1 -> node_2 -> node_3 -> node_4 -> null
# node_0 -> node_4 -> node_1 -> node_2 -> node_3 -> null
# We can see the last node of the new list will be the N / 2,
# It does not need to update, so we assign static indices to the
# nodes, and stop when we need the index N / 2.
# Complexity is O(n) for scanning the list,
# Space is O(n) for the index array.

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        if not head:
            return
        
        node = head
        cnt = 0
        array = []
        while node:
            cnt += 1
            array.append(node)
            node = node.next
            
        n = cnt - 1
        mid = n // 2
        i = 0
        while i < mid:
            array[i].next = array[n-i]
            array[n-i].next = array[i+1]
            array[n-i-1].next = None
            i += 1
            
        return
