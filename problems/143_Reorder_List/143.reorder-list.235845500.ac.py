# TO EXPLAIN
#
# @lc app=leetcode id=143 lang=python3
#
# [143] Reorder List
#
# https://leetcode.com/problems/reorder-list/description/
#
# algorithms
# Medium (32.88%)
# Total Accepted:    179K
# Total Submissions: 544.4K
# Testcase Example:  '[1,2,3,4]'
#
# Given a singly linked list L: L0→L1→…→Ln-1→Ln,
# reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…
# 
# You may not modify the values in the list's nodes, only nodes itself may be
# changed.
# 
# Example 1:
# 
# 
# Given 1->2->3->4, reorder it to 1->4->2->3.
# 
# Example 2:
# 
# 
# Given 1->2->3->4->5, reorder it to 1->5->2->4->3.
# 
# 
#
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        
        
        
        idx = []
        node = head
        while (not node == None):
            idx.append(node)
            node = node.next
            
        
        n = len(idx)
        if n == 1:
            return
        if n == 0:
            return

        mid = int(n / 2)
        
        for i in range(0, mid):
            node = idx[i]
            node2 = idx[n-i-1]
            node.next = node2
            
            #print("a {}: {} -> {}", i, node.val, node2.val)
            
        for i in range(n-1, mid-1, -1):
            node = idx[i]
            node2 = idx[n-i]
            
            if n-i >= i:
                node.next = None
            else:
                node.next = node2
                
            #print("b {}: {} -> {}", i, node.val, node2.val)

            
            
