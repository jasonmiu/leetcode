// Using a stack to store all nodes, and dail it backward,
// while having a forward pointer start from head, count the
// forward and backward pointer.
// The hard part is to find the end case, since the end node need
// to be have null next. Two special cases are, if two pointers
// meets, means the list size is odd, and this is the middle node
// which will be the last node.
// If two pointers are now next to each other, means, they are the
// last two nodes of the even size list, the backward pointer
// will be the last node.
// Time: O(n), Space: (n)

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode* head) {

        stack<ListNode*> s;
        int list_len = 0;

        ListNode* n = head;
        while (n != nullptr) {
            list_len++;
            s.push(n);
            n = n->next;
        }

        int i = 0;
        int j = list_len - 1;

        ListNode* h = head;
        ListNode* t = s.top();
        s.pop();

        while (i <= j) {

            if (i == j) {
                h->next = nullptr;
                break;
            } else if (i + 1 == j) {
                t->next = nullptr;
                break;
            }

            n = h->next;
            h->next = t;
            t->next = n;

            h = n;
            t = s.top();
            s.pop();

            i++;
            j--;
        }

        return;
    }
};
