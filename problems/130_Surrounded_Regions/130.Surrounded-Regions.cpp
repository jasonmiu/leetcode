// The question provided a great hint, which is:
// <quote>Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'.</quote>
// So for a surrounded region, it means it cannot be reached from any border cell. If a cell is a border cell,
// or it can be reached from a border cell, then it is NOT a surrounded region.
// We can find all regions that are not surrounded, ie those cells can be reached from border cells. To find them,
// just access each border cell, and start a DFS fashion flood fill from it. Mark a cell not surrounded if it can be
// reached in that DFS search.
// Since the final result is to flip those surrounded cells, we cannot just mark those non-surrounded cells as 'X',
// as if doing so they will be "mixed" with the original 'X's but indeed we want to preseve them as 'O'. So just
// mark them with other marker, for example '!', then after all DFS searches, we have 3 types of cells, original X,
// non-surrounded cells '!', and the surrounded cells 'O'. Visit the matrix again to change all 'O' to X, and
// restore those "!" back to "O".
// Time: access all cell once, O(mn). Space: O(1).


void dfs(vector<vector<char>>& g, int i, int j)
{
    if (g[i][j] != 'O') {
        return;
    }

    g[i][j] = '!';

    vector<pair<int,int>> dirs = {
        {i + 1, j},
        {i, j + 1},
        {i - 1, j},
        {i, j - 1}
    };

    for (auto d : dirs) {
        if (d.first >= 0 && d.first < g.size() &&
            d.second >= 0 && d.second < g[0].size()) {
            dfs(g, d.first, d.second);
        }
    }

    return;
}

class Solution {
public:
    void solve(vector<vector<char>>& board) {
        for (int i = 0; i < board.size(); i++) {
            dfs(board, i, 0);
            dfs(board, i, board[0].size()-1);
        }

        for (int j = 0; j < board[0].size(); j++) {
            dfs(board, 0, j);
            dfs(board, board.size()-1, j);
        }

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if (board[i][j] == '!') {
                    board[i][j] = 'O';
                } else if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }

        return;
    }
};
