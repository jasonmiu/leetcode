# For a cell in a connected segment of "O", if we run a flooding algor, two cases
# will happen:
# 1. visited all connected "O"s, until we hit "X"s at its boundary
# 2. visited all connected "O"s, until we hit the board boundary
# If case 1, we update all visited cells in that segment to "X".
# If case 2, we do not need to update the board. But we do not need to check any
# cells in this visited segment again, we can save the coordinations in this segment
# to a global visited set "not_surrounded".
# We start with visiting each cell one by one. If this cell is "O", we apply the 
# flooding algor above. One trick is during the recursive flooding algor, if the
# current visiting cell is on the board boundary, we return a 0x01, else 0x00,
# and bitwise OR all the recursive results. If any recursive results is 0x01, 
# ie. hit board boundary, the overall flooding function will return 0x01. 
# So we know this the connected segment containing this cell hit the board 
# boundary, which is case 2.
# Complexity O(n) while n is the number of the cells. If a cell is case 1, 
# the other cells become "X" and will not be reran with flooding. If a cell
# is case 2, the other cells will be added to the global visited set,
# and will not be reran with flooding too.
# Space is O(n) for the global visited since it can be all "O"s.

def flood(board, i, j, visited, h, w):
    if (i, j) in visited:
        return 0
    
    visited.add((i, j))
    
    hit_bound = 0
    if i == 0 or i == (h-1) or j == 0 or j == (w-1):
        hit_bound = 1
        
    offsets = [(-1, 0),
               (1, 0),
               (0, -1),
               (0, 1)]
    
    for (off_i, off_j) in offsets:
        r = i + off_i
        c = j + off_j
        if r >= 0 and r < h and c >= 0 and c < w and board[r][c] == "O":
            b = flood(board, r, c, visited, h, w)
            hit_bound = hit_bound | b
            
    return hit_bound

class Solution:
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        
        h = len(board)
        if h == 0:
            return
        
        w = len(board[0])
        if w == 0:
            return
        
        not_surrounded = set()
        
        for i in range(0, h):
            for j in range(0, w):
                if board[i][j] == "O" and (not (i, j) in not_surrounded):
                    visited = set()
                    hit_bound = flood(board, i, j, visited, h, w)
                    if hit_bound == 0:
                        for (r, c) in visited:
                            board[r][c] = "X"
                    else:
                        not_surrounded = not_surrounded.union(visited)
                        
        return

