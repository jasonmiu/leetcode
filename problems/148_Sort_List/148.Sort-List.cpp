// Easiest way to do the sort, convert all node pointers to vector,
// and sort the vector, by checking the pointer pointing values.
// Remember, for the comparator of the sort() function,
// *always* return false if two values are equal.
// Time: O(nlogn), space O(n)

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* sortList(ListNode* head) {

        vector<ListNode*> v;
        ListNode* n = head;

        if (head == nullptr) {
            return nullptr;
        }

        while (n != nullptr) {
            v.push_back(n);
            n = n->next;
        }

        sort(v.begin(), v.end(), [](ListNode* a, ListNode* b) {
            if (a->val < b->val) {
                return true;
            } else {
                return false;
            }
        });

        for (int i = 0; i < v.size() - 1; i++) {
            v[i]->next = v[i+1];
        }

        v.back()->next = nullptr;

        return v[0];
    }
};
