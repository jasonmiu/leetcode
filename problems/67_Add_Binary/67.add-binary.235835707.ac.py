# TO EXPLAIN
#
# @lc app=leetcode id=67 lang=python3
#
# [67] Add Binary
#
# https://leetcode.com/problems/add-binary/description/
#
# algorithms
# Easy (41.08%)
# Total Accepted:    353.2K
# Total Submissions: 859.7K
# Testcase Example:  '"11"\n"1"'
#
# Given two binary strings, return their sum (also a binary string).
# 
# The input strings are both non-empty and contains only characters 1 or 0.
# 
# Example 1:
# 
# 
# Input: a = "11", b = "1"
# Output: "100"
# 
# Example 2:
# 
# 
# Input: a = "1010", b = "1011"
# Output: "10101"
# 
#
class Solution:
    def addBinary(self, a: str, b: str) -> str:
        
        a_len = len(a)
        b_len = len(b)
        
        # align two strings
        if a_len >= b_len:
            ab_diff = a_len - b_len
            p = "0" * ab_diff
            b = p + b
        else:
            ab_diff = b_len - a_len
            p = "0" * ab_diff
            a = p + a
            
        c = 0
        r = []
        for i in range(len(a)-1, -1, -1):
            d_a = int(a[i])
            d_b = int(b[i])
            
            d = 0
            if d_a + d_b + c == 2:
                d = 0
                c = 1
            elif d_a + d_b + c == 3:
                d = 1
                c = 1
            else:
                d = d_a + d_b + c
                c = 0
                
            r.append(str(d))
            
        if c:
            r.append("1")
            
        r.reverse()
        return "".join(r)
        
        
            
                
            
        
            
            
