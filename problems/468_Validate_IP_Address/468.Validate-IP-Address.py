# This has not strick but checking all invaild cases for IPv4 and IPv6.
# Some special cases:
# IPv4:
# 01.01.01.01 : No leading 0
# 192.0.0.1 : single 0 is not leading 0
# abc.12.14.15 : non-convertable to int
# -10.9.8.7 : convertable to int, but invalid char (- or +)
# IPv6:
# 2001:GGZZ:85a3:0000:0000:8a2e:0370:7334 : each field is hex, so char is Aa-Ff.
# Complexity: O(n) checking all chars
# Time: O(1) no extra space

def check_ipv4(ip):
    tokens = ip.split(".")
    if len(tokens) != 4:
        return False
    
    for t in tokens:
        
        if (len(t) > 1 and t.startswith("0")) or t.startswith("-") or t.startswith("+"):
            return False
        
        try:
            i = int(t)
        except:
            return False
        
        if not (i >= 0 and i <= 255):
            return False
        
    return True


def check_ipv6(ip):
    tokens = ip.split(':')
    if len(tokens) != 8:
        return False
    
    for t in tokens:
        if len(t) > 4 or len(t) == 0:
            return False
        for c in t:
            if not c.isalnum():
                return False
            if c.isalpha():
                if c not in "abcdefABCDEF":
                    return False
            
    return True


class Solution:
    def validIPAddress(self, IP: str) -> str:
        is_ipv4 = False
        is_ipv6 = False
        
        if '.' in IP:
            is_ipv4 = check_ipv4(IP)
        elif ':' in IP:
            is_ipv6 = check_ipv6(IP)
        else:
            return "Neither"
        
        if is_ipv4 and not is_ipv6:
            return "IPv4"
        
        if is_ipv6 and not is_ipv4:
            return "IPv6"
        
        if not is_ipv6 and not is_ipv4:
            return "Neither"
