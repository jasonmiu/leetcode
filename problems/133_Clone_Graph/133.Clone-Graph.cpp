// Since the graph can have cycles, also it is undirected,
// means if two nodes connected their neighbors will contain each other.
// So the hard part is to avoid copying the same node again.
// Instead of copying the structure during traversal, build a standard
// adjacency list with a map, like:
//
// Index:    1     2     3     4
// ----------------------------------
// input : [[2,4],[1,3],[2,4],[1,3]]
//
// val | node
// 1   | Node(1) -> [2, 4]
// 2   | Node(2) -> [1, 3]
// 3   | Node(3) -> [2, 4]
// 4   | Node(4) -> [1, 2]
//
// As the node values are unique, use it as the key for referencing nodes.
// Create a copy of such table, with the same key value, but new node pointers:
// val | node
// 1   | Node(1) -> []
// 2   | Node(2) -> []
// 3   | Node(3) -> []
// 4   | Node(4) -> []
//
// Traverse the original table, check the adjacency of the each node,
// for each neighbor node in the adjacency list, take the node value,
// use this value on the cloned table to find the cloned node pointer.
// Then fill the cloned table node's adjaceny list with the
// found cloned node pointer.
//
// Time: O(n), Space: O(n).


/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
*/

void find_nodes(Node* r, unordered_map<int, Node*>& g, unordered_set<int>& visited)
{
    if (r == nullptr) {
        return;
    }

    if (visited.find(r->val) != visited.end()) {
        return;
    } else {
        visited.insert(r->val);
    }

    g[r->val] = r;

    for (Node* n : r->neighbors) {
        find_nodes(n, g, visited);
    }

    return;
}

class Solution {
public:
    Node* cloneGraph(Node* node) {

        if (!node) {
            return nullptr;
        }

        unordered_set<int> visited;
        unordered_map<int, Node*> g1;

        find_nodes(node, g1, visited);

        unordered_map<int, Node*> g2;

        for (auto p : g1) {
            Node* n = new Node(p.first);
            g2[p.first] = n;
        }

        for (auto p : g1) {
            int v = p.first;
            Node* n = p.second;

            for (Node* c : n->neighbors) {
                int cv = c->val;

                g2[v]->neighbors.push_back(g2[cv]);
            }
        }

        return g2[node->val];
    }
};
