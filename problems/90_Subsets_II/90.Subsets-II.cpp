// The main different of this problem with normal power set generation is,
// the elements in the input can be duplicated. For example,
// [ 1 2 2 ]
// The power set contains:
// [1] [2] and the second [2] should be removed, same as,
// [1 2] (index 0, 1) and the second [1 2] from index (0, 2).
// The idea to remove those duplicated subset is to sort them
// in the same order. Then generate a hash which recongize the
// order of the elements in a subset. If two subsets are the same,
// they have same elements also in same order. Use a unordered_set
// to remove them.
// Since we still need to generate all the subsets,
// so the time is O(2^n), also the space to store all subsets is O(2^n)

struct vec_hash {
    size_t operator() (const vector<int>& v) const {
        string s;
        for (int i : v) {
            s = s + to_string(i);
        }
        return hash<string>()(s);
    }
};

void gensub(const vector<int>& a, int start, vector<int> cur_set, unordered_set<vector<int>, vec_hash>& ans)
{
    if (start >= a.size()) {
        sort(cur_set.begin(), cur_set.end());
        ans.insert(cur_set);
        return;
    }

    vector<int> c(cur_set);
    c.push_back(a[start]);

    gensub(a, start + 1, c, ans);
    gensub(a, start + 1, cur_set, ans);

    return;
}


class Solution {
public:
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        unordered_set<vector<int>, vec_hash> ans;

        gensub(nums, 0, vector<int>(), ans);

        vector<vector<int>> ret(ans.begin(), ans.end());

        return ret;

    }
};
