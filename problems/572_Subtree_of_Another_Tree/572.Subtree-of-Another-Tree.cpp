// Following a DFS make a path of nodes of a tree as a array of node values.
// If a tree is a sub-tree of another, the returned array is a sub-array
// of another tree as well, as long as using the same DFS visiting order.
// One case to be careful is, we need to take null node as account as well,
// since
//   4
//  /  \
// 1    2
//
// and
//
//   4
//  /
// 1
//  \
//   2
//
// Both give path: [ 4 1 2 ] if not counting null.
//
// Time: O(n^m) n is the big tree size while m is the small tree size.
// Space: O(n+m).


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void dfs_path(TreeNode* r, vector<int>& path)
{
    if (r == nullptr) {
        path.push_back(INT_MAX);
        return;
    }

    path.push_back(r->val);

    dfs_path(r->left, path);
    dfs_path(r->right, path);

    return;
}

class Solution {
public:
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        vector<int> root_path;
        vector<int> sub_root_path;

        dfs_path(root, root_path);
        dfs_path(subRoot, sub_root_path);

        auto it = find(root_path.begin(), root_path.end(), sub_root_path[0]);
        while (it != root_path.end()) {
            if (equal(it, it+sub_root_path.size(), sub_root_path.begin())) {
                return true;
            }
            it = find(it+1, root_path.end(), sub_root_path[0]);
        }
        return false;
    }
};
