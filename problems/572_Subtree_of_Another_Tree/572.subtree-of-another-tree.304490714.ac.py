#
# @lc app=leetcode id=572 lang=python3
#
# [572] Subtree of Another Tree
#
# https://leetcode.com/problems/subtree-of-another-tree/description/
#
# algorithms
# Easy (42.67%)
# Total Accepted:    173.6K
# Total Submissions: 397.3K
# Testcase Example:  '[3,4,5,1,2]\n[4,1,2]'
#
# 
# Given two non-empty binary trees s and t, check whether tree t has exactly
# the same structure and node values with a subtree of s. A subtree of s is a
# tree consists of a node in s and all of this node's descendants. The tree s
# could also be considered as a subtree of itself.
# 
# 
# Example 1:
# 
# Given tree s:
# 
# ⁠    3
# ⁠   / \
# ⁠  4   5
# ⁠ / \
# ⁠1   2
# 
# Given tree t:
# 
# ⁠  4 
# ⁠ / \
# ⁠1   2
# 
# Return true, because t has the same structure and node values with a subtree
# of s.
# 
# 
# Example 2:
# 
# Given tree s:
# 
# ⁠    3
# ⁠   / \
# ⁠  4   5
# ⁠ / \
# ⁠1   2
# ⁠   /
# ⁠  0
# 
# Given tree t:
# 
# ⁠  4
# ⁠ / \
# ⁠1   2
# 
# Return false.
# 
#
# The idea is the preorder list of a smaller tree is the sub-list of the bigger tree.
# To preserve the tree structure of the tree, we add a place holder for the null leaf nodes. 
# For example:
#  (a)   1      1  (b)
#      /         \
#    2            2
# The trees (a) (b) both give the preorder list [1, 2], but their structures are different.
# So we need to count the null leaf nodes too, (a) becomes [1, 2, NULL] and (b) becomes [1, NULL, 2].
# While we can convert the list to string and use sub-string functions to see if the smaller tree
# is a sub-string of bigger tree, but some tricky node vals like:
# [12] => "12", [1, 2] => "12" make the string comparing method tricky. Instead, a sliding window function
# is_sublist() is implemented to compare the lists.
# Complexity: O(n^2) for comparing the sublist, n is the nodes of bigger tree.
# Space: O(m+n) for tree node lists, m is the nodes of smaller tree.
# This is the same question of CTCI 4.10.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def tree_to_list(root, a):
    if root == None:
        a.append("@")
        return
    a.append(root.val)
    tree_to_list(root.left, a)
    tree_to_list(root.right, a)
    return


def is_sublist(a, b):
    if len(a) > len(b):
        a, b = b, a
        
    i = 0
    while i + len(a) - 1 < len(b):
        if b[i:i+len(a)] == a:
            return True
        else:
            i += 1
            
    return False
        

class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        
        slist = []
        tlist = []
        tree_to_list(s, slist)
        tree_to_list(t, tlist)
        
        return is_sublist(tlist, slist)
