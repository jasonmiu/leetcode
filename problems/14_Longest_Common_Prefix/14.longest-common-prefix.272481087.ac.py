#
# @lc app=leetcode id=14 lang=python3
#
# [14] Longest Common Prefix
#
# https://leetcode.com/problems/longest-common-prefix/description/
#
# algorithms
# Easy (34.29%)
# Total Accepted:    570.7K
# Total Submissions: 1.7M
# Testcase Example:  '["flower","flow","flight"]'
#
# Write a function to find the longest common prefix string amongst an array of
# strings.
# 
# If there is no common prefix, return an empty string "".
# 
# Example 1:
# 
# 
# Input: ["flower","flow","flight"]
# Output: "fl"
# 
# 
# Example 2:
# 
# 
# Input: ["dog","racecar","car"]
# Output: ""
# Explanation: There is no common prefix among the input strings.
# 
# 
# Note:
# 
# All given inputs are in lowercase letters a-z.
# 
#
# The longest possible common prefix must be lesser or equal to the length of the
# shortest string in the input set. So find the shortest_len first and search
# each string until shortest_len.
# We can image to place input strings as rows, and loop thru the chars as columns
# ["ab", "abc", "abb"]
# "ab"
# "abc"
# "abb"
#  ^-0
#   ^-1 (shortest_len)
# Each char in a string must be same as the same columns of the previous row,
# we save it as prev_c. If in the one scan, ie. 1 column, all chars are the same,
# the longest common prefix extends. Otherwise, the prefix searching stops.
# The complexity is O(n), while n is the total chars in the input set 'strs'.
# The space complexity is O(1) since we use only constance space to store prev_c.

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:

        if len(strs) == 0:
            return ""
        
        prefix_idx = None
        
        shortest_len = min(list(map(len, strs)))
        search_stop = False
        
        for i in range(0, shortest_len):
            prev_c = None
            
            for s in strs:
                if prev_c == None:
                    prev_c = s[i]
                else:
                    if prev_c != s[i]:
                        search_stop = True
                        break
                    else:
                        prev_c = s[i]
            
            if search_stop:
                break
            else:
                prefix_idx = i
                
        if prefix_idx != None:
            return strs[0][0:prefix_idx+1]
        else:
            return ""
