// The first issue is, how to deal with the possible duplicated combinations. For example:
// [ 2 3 6 7 ], Target = 7
// start with the 2, it can be 2 2 3
// then try with the next 3, it can be 3 2 2
// Both paths are possible in the selection tree, but they are duplicated and the question asked
// for unique combinations.
// The reason of this duplication is, when we try the next element, we have to try the combinations
// start from 0 again to find all possible combinations. Hence in this case the 3 find 2 again so the
// final combination becomes a duplication. If we can find the combination forward without search
// again from beginning, then the duplication can be solved. So start with a sorted input.
// Sort it so it is starting from smallest element, like:
// [ 2 3 6 7 ]
// Then for the smallest element (index 0), it will find the combination from beginning to the end.
// For the second element, it find the combinations from it's index to the end. It is because those
// elements before it, have searched the combinations covered the current element. So the search
// tree looks like:
//
//                                             root
//                                  /         |          |       \
//                                 2          3          6        7
//                    /   |   |    |      / | |       /  |         \
//                   2    3   6    7     3  6 7      6   7          7
//         /  |  |  /   /||   |\   |    /|| |\ \
//        2   3  6 7   3 67   6 7  7   3 67 6 7 7
//
//
// For each level, the number of selections depends on which element we have picked on this level, ie.,
// the element we added to the current sequence.
// If we picked the first element for the current sequence, the next choices will be the elements from
// first to the end, since we have reuse the current element.
// Similarly, if we picked the 2nd element for the current sequence, the next choice will be the elements
// from 2nd to the end. The righter the element, the next choices will be lesser.
// To stop searching down, we keep track the sum of the current sequence. If the sum hits the target,
// the current sequence is the combination we want. Save it and return.
// If the sum is smaller than the current, we can search down.
// To search down, try all choices, and put the next choice to the current sequence. Recusively go down the
// tree, but with the searching start index to the next choice index, it is because for the next choice
// it does not need to search those elements have smaller index than it.
// After trying the next choice, revert the change on the current sequence, for trying the next choice
// on the current list.
//
// Time: O(n^t), t is the target, it is because the worse case can be a linear tree that reuse a small
// candidate element many times, like
// [ 1 2 ], target = 4
//                  root
//                 /     \
//               1        2
//             /  \
//           1     2
//         /  \
//        1    2
//       /
//      1
// so n selection each level and has t levels. Space is O(^t) in case all candidates can make combinations
// which means the whole tree has to be stored to the result.

void find_sum(const vector<int>& a, vector<int> cur, int start, int sum, int t, vector<vector<int>>& result)
{
    if (sum == t) {
        result.push_back(cur);
        return;
    }

    for (int i = start; i < a.size(); i++) {
        if (sum + a[i] <= t) {
            cur.push_back(a[i]);
            find_sum(a, cur, i, sum + a[i], t, result);
            cur.pop_back();
        }
    }
    return;
}

class Solution {
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());

        vector<vector<int>> result;
        find_sum(candidates, vector<int>(), 0, 0, target, result);

        return result;
    }
};
