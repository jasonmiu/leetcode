#
# @lc app=leetcode id=1043 lang=python3
#
# [1043] Partition Array for Maximum Sum
#
# https://leetcode.com/problems/partition-array-for-maximum-sum/description/
#
# algorithms
# Medium (62.76%)
# Total Accepted:    13.2K
# Total Submissions: 20.8K
# Testcase Example:  '[1,15,7,9,2,5,10]\n3'
#
# Given an integer array A, you partition the array into (contiguous) subarrays
# of length at most K.  After partitioning, each subarray has their values
# changed to become the maximum value of that subarray.
# 
# Return the largest sum of the given array after partitioning.
# 
# 
# 
# Example 1:
# 
# 
# Input: A = [1,15,7,9,2,5,10], K = 3
# Output: 84
# Explanation: A becomes [15,15,15,9,10,10,10]
# 
# 
# 
# Note:
# 
# 
# 1 <= K <= A.length <= 500
# 0 <= A[i] <= 10^6
# 
# 
#
# Array A can be cutted at most K, means we can cut the array
# with slice side 1, 2, ... K. For example:
# [1 2 3 4], K = 2
# we can cut:
#                        [1 2 3 4]
#            /                            \
#        [1][2 3 4]                      [1 2][3 4]
#       /          \                     /            \
# [1][2][3 4]      [1][2 3][4]          [1 2][3][4]   [1 2][3 4]
# So it is a recursive structure we can use DFS to search.
# Lets say we cut at i, call the partition before i prefix, 
# and after i suffix. The current sum will the
# max(prefix) * len(prefix) + max possible sum of suffix
# The suffix is the subproblem we can resolve recursively.
# We return the max sum of best cutting point i. 
# If we encountered the suffix before, we knew its max sum already,
# so we have a cache to memorize them.
# There are N suffixes so complexity is O(N)
# The space complexity is also O(N)

def dfs(A, k, cache):
    
    n = len(A)
    if n == 0:
        return 0
    
    if n == 1:
        return A[0]
    
    t = tuple(A)
    
    if t in cache:
        return cache[t]
    
    parts = []
    i = 0
    while i < k and i < n:
        m = max(A[0:i+1])
        p = m * (i+1)
        r = (p + dfs(A[i+1::], k, cache))
        #print("A", A,  "i", i, A[0:i+1], A[i+1::], "m", m, "p", p, "r", r)
        parts.append(r)
        i += 1
        
    cache[t] = max(parts)
    return cache[t]
        


class Solution:
    def maxSumAfterPartitioning(self, A: List[int], K: int) -> int:
        ans = dfs(A, K, {})
        
        return ans
