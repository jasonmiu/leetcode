# TO EXPLAIN
#
# @lc app=leetcode id=8 lang=python3
#
# [8] String to Integer (atoi)
#
# https://leetcode.com/problems/string-to-integer-atoi/description/
#
# algorithms
# Medium (14.84%)
# Total Accepted:    435K
# Total Submissions: 2.9M
# Testcase Example:  '"42"'
#
# Implement atoi which converts a string to an integer.
# 
# The function first discards as many whitespace characters as necessary until
# the first non-whitespace character is found. Then, starting from this
# character, takes an optional initial plus or minus sign followed by as many
# numerical digits as possible, and interprets them as a numerical value.
# 
# The string can contain additional characters after those that form the
# integral number, which are ignored and have no effect on the behavior of this
# function.
# 
# If the first sequence of non-whitespace characters in str is not a valid
# integral number, or if no such sequence exists because either str is empty or
# it contains only whitespace characters, no conversion is performed.
# 
# If no valid conversion could be performed, a zero value is returned.
# 
# Note:
# 
# 
# Only the space character ' ' is considered as whitespace character.
# Assume we are dealing with an environment which could only store integers
# within the 32-bit signed integer range: [−2^31,  2^31 − 1]. If the numerical
# value is out of the range of representable values, INT_MAX (2^31 − 1) or
# INT_MIN (−2^31) is returned.
# 
# 
# Example 1:
# 
# 
# Input: "42"
# Output: 42
# 
# 
# Example 2:
# 
# 
# Input: "   -42"
# Output: -42
# Explanation: The first non-whitespace character is '-', which is the minus
# sign.
# Then take as many numerical digits as possible, which gets 42.
# 
# 
# Example 3:
# 
# 
# Input: "4193 with words"
# Output: 4193
# Explanation: Conversion stops at digit '3' as the next character is not a
# numerical digit.
# 
# 
# Example 4:
# 
# 
# Input: "words and 987"
# Output: 0
# Explanation: The first non-whitespace character is 'w', which is not a
# numerical 
# digit or a +/- sign. Therefore no valid conversion could be performed.
# 
# Example 5:
# 
# 
# Input: "-91283472332"
# Output: -2147483648
# Explanation: The number "-91283472332" is out of the range of a 32-bit signed
# integer.
# Thefore INT_MIN (−2^31) is returned.
# 
#
class Solution:
    def myAtoi(self, str: str) -> int:
        
        INT_MIN = - 2**31
        INT_MAX = 2**31 - 1
        
        str = str.lstrip().strip()
        if len(str) == 0:
            return 0
        if not str[0].isdigit() and not str[0] == "-" and not str[0] == "+":
            return 0
        
        s = ""
        first_sign = True
        for i in range(0, len(str)):
            c = str[i]
            if c.isdigit():
                first_sign = False
                s = s + c
            elif c == "-" or c == "+":
                if first_sign:
                    s = s + c
                    first_sign = False
                else:
                    break
            else:
                break
        
        if len(s) == 0:
            return 0
        if len(s) == 1:
            if s[0] == "-" or s[0] == "+":
                return 0
        
        num = 0
        fig = 0
        for i in range(len(s)-1, -1, -1):
            c = s[i]
#            print(c)
            if not c == "-" and not c == "+":
                n = int(c)
                num = num + (n * 10**fig)
                fig += 1

        if s[0] == "-":
            num = -num
        
        if num <= INT_MIN:
            num = INT_MIN
        if num >= INT_MAX:
            num = INT_MAX
        
        return num
                
