# The subarray is a cutting the input array in some continues sequences, like:
# [1,4,2,5,3]
# subarrays are: [1 4 2], [4 2 5], [2 5 3] if subarray len is 3.
# First want to find all subarrays with odd len, that is 1, 3, 5, ... <= N, while N is the len of input array
# If subarray len is 1, then the sum of them will be:
# sum([1] [4] [2] [5] [3]), doing this take O(n)
# If subarray len is 3, then sum of them will be:
# sum(sum([1 4 2]), sum([4 2 5]), sum([2 5 3])), brute force will be O(n^2), but can be reduced as:
# [1 4 2 5 3]
#  |     |
#  i     j
# sum[i:j) = 1 + 4 + 2 = 7
# [1 4 2 5 3]
#    |     |
#    i     j
#  i'
# since we have the [4 2] covered in the previous sum, we do not need to sum from start of this subarray again,
# but previous sum - array[i'] + array[j-1]
# so only have to scan once, this overall sum takes O(n)
#
# Since we have to do such O(n) sum array n times, time is O(n^2), space is O(1)


class Solution:
    def sumOddLengthSubarrays(self, arr: List[int]) -> int:
        sub_array_len = 1
        total = 0
        while sub_array_len <= len(arr):
            i = 0
            j = sub_array_len
            prev_head = 0
            s = 0
            while j <= len(arr):
                if i == 0:
                    s = sum(arr[i:j])
                else:
                    s = s - prev_head + arr[j-1]

                prev_head = arr[i]
                total = total + s
                i = i + 1
                j = j + 1

            sub_array_len += 2

        return total
