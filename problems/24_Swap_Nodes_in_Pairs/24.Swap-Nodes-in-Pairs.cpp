// For example, a list:
// 1->2->3->4
//
// We like to swap every 2 nodes, so the list will be:
// 2->1->4->3
//
// And return the new head. Can think for the smallest list we
// can swap, it should be the list with 2 nodes:
//
// 1->2
// L  R
//
// 2->1
// R  L
// And return the R (right) pointer as the new head.
//
// If we are at a node, which we want to swap with its adjacent node,
// then append a list of *already swapped nodes*, then we can solve this problem.
// So recursivly go down the list, for every 2 nodes, until we have only 2 nodes, swap them,
// return the new head, and back to the parent list, swap the current 2 nodes, join
// the sublist, and return the new head back to the parent list again.
// But what if odd size of list, or zero size? If the list has odd length, for moving forward
// every 2 nodes, we will have a single node left. Single node needs not to be swapped, so
// just return it back. If we have a zero size list, just return a null pointer. And indeed
// when we have a even size list, but moving forward 2 nodes everytime, we will get a zero size
// list at the end. So both will be the term cases of the recursion.
//
// Time: O(n) for scanning the list once. Space: O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

ListNode* swap_nodes(ListNode* n)
{
    if (n == nullptr) {
        return nullptr;
    }

    if (n->next == nullptr) {
        return n;
    }

    ListNode* sublist = swap_nodes(n->next->next);

    ListNode* l = n;
    ListNode* r = n->next;

    r->next = l;
    l->next = sublist;

    return r;
}

class Solution {
public:
    ListNode* swapPairs(ListNode* head) {

        ListNode* h = swap_nodes(head);
        return h;
    }
};
