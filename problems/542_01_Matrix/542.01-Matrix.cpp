// Since the shortest path from a point (1 in this case) to
// another point (0 in this case) with a unit distance can be
// found by BFS, the first idea is to have every 1 cell,
// run a BFS until it hit a 0 cell.
// However, Time Limit Exceeded (TLE) happens, as a BFS need
// O(n) if n is the number of cells to run. Then looping all
// n cells and run a BFS on them, costs O(n^2).
//
// I had a hard time to think about how to make the complexity
// smaller. Thought about start from 0 cells, but didn't know
// how to avoid the repeated checking like BFS for each cell.
//
// Indeed, if start from zero, when it find its adjacent 1 cell,
// the distance of that 1 cell becomes 1. For those "inner" 1 cells,
// i.e. not direct connection to any zero cell, we want to propagate
// the checking from outter 1 cells to them, each 1 step propagation
// means distance from 0 cell increase 1. And the shortest distance
// can be compared with all the connected propagation directions.
// Only update the distance if the distance to a 0 cell is shortest.
//
// Example Input
//     0 1 2
//  +--------
// 0| [0,0,0]
// 1| [0,1,0]
// 2| [1,1,1]
//
// Make a distance map:
//
//     0 1 2
//  +--------
// 0| [0,0,0]
// 1| [0,*,0]
// 2| [*,*,*]
//
// * is the max distance. For all 0 cell, their distance is 0.

// Queue all 0 cells:
// queue: (0, 0), (0, 1), (0, 2), (1, 0), (1, 2)
//
// This first queue of 0 cells are at the *level 0*.
// For each queued cell, propagate to all 4 directions, if they are not visited.
// If visited, do not need to check futher, since the idea is to visit each cell,
// and compute their distance to a closest 0 cell. So if visited, the distance map
// had its shortest distance already.
//
// For first queued cell (0, 0), it will check 4 directions, in this case, they are
// (0, 1) and (1, 0), since the UP and LEFT are out of boundary.
// Since they are the 0 cells, we do not have to check their distances and no need
// to push them to queue again, as we know 0 cell to 0 cell has not distance increment.
// Put this cell to the visited map, and pop next from the queue.
//
// Then for the next cell in the queue, (0, 1), same 4 directions checking, which are
// (0, 0), (0, 2), (1, 1). (0, 0) is already in the visited map, so skip.
// (0, 2) is also a 0 cell, do not need to check nor put to queue.
// For (1, 1), it is a 1 cell. Check the distance, where its current distance is inifity.
// Propagate from current cell + 1 step is smaller than the current inifity distance, so
// update it with distance 1.
//
//     0 1 2
//  +--------
// 0| [0,0,0]
// 1| [0,1,0]
// 2| [*,*,*]
//
// Then put this cell to the queue:
// queue: (0, 2), (1, 0), (1, 2), (1, 1)
//
// The current queue keep poping until this level is done:
//     0 1 2
//  +--------
// 0| [0,0,0]
// 1| [0,1,0]
// 2| [1,*,1]
//
// queue: (1, 1), (2, 0), (1, 2)
// Those 1 cells get queued, for level 1. The cell (1, 1) get poped,
// and check the adjacent cell (2, 1). (2, 1) is not visited, and not a 0 cell,
// try update its distance:
//
//     0 1 2
//  +--------
// 0| [0,0,0]
// 1| [0,1,0]
// 2| [1,2,1]
//
// And queue that cell:
// queue: (2, 0), (1, 2), (2, 1)
// Next two cells do the same checking but no shorter distance is made for (2, 1),
// they queue it up:
// queue: (2, 1), (2, 1), (2, 1)
// for the next level.
// While same (2, 1) cell get queued, its ok as we check the visited map when it get poped out.
// This is the last cell this all adjacent cells got visited.
// Time: O(n) as all cell visited once. Space O(n) for visited map and distance map.
//
// Also Thought about use DP. If we know a previous cell shortest distance, then
// the shortest distance of current cell should be min(previous cell distances) + 1
// However, the problem is the shortest distance has direction. For example:
//
// Input:
// [1, 1, 1, 1, 0]
// Assuming the first cell we know the shortest distance already:
//
// Distance map:
//  0  1  2  3  4
// [4, *, *, *, 0]
//
// If using DP, the previous cell of [1] is UP or LEFT cell, so the min(UP, LEFT) is 4 hence
// dist[1] will be 5. However, it should be 3, since the cloest 0 cell is at [4].
//
// If there was a 0 cell at the direction of the previous cell, in above case, UP or LEFT,
// then can that shortest distance of [1] will be correct. However, in this case the cloest
// 0 cell is on the opposit side.
//
// The solution is, do a two passes. One pass from UP LEFT, and one pass from BOTTOM RIGHT.
// A cell compare its shortest distance with its min(dist of previous cells) + 1, if 1 way is
// smaller, then use that distance.
// Do not need to compute the very first upper left and lower right coner too, can let them be
// inifity first, since each pass they will be updated by another direction. All 1 cell is not
// possible as the question assumed at least one 0 cell exists.



#include <queue>

using namespace std;

class Solution {
public:
    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {

        int h = mat.size();
        int w = mat[0].size();

        vector<vector<int>> visited(h, vector<int>(w, 0));
        vector<vector<int>> dist(h, vector<int>(w, INT_MAX));

        queue<pair<int,int>> q;

        for (int i = 0; i < h; i ++) {
            for (int j = 0; j < w; j++) {
                if (mat[i][j] == 0) {
                    q.push(pair<int,int>(i, j));
                    dist[i][j] = 0;
                }
            }
        }

        while (!q.empty()) {
            int qsize = q.size();
            for (int i = 0; i < qsize; i++) {
                pair<int,int> pos = q.front();
                q.pop();

                visited[pos.first][pos.second] = 1;

                vector<pair<int,int>> dirs = {
                    make_pair(pos.first - 1, pos.second),
                    make_pair(pos.first, pos.second + 1),
                    make_pair(pos.first + 1, pos.second),
                    make_pair(pos.first, pos.second - 1)
                };

                for (auto d : dirs) {
                    if (d.first >= 0 && d.first < h && d.second >= 0 && d.second < w &&
                        visited[d.first][d.second] == 0) {

                        if (mat[d.first][d.second] != 0 &&
                            dist[pos.first][pos.second] + 1 <= dist[d.first][d.second]) {
                            dist[d.first][d.second] = dist[pos.first][pos.second] + 1;
                        }

                        if (mat[d.first][d.second] != 0) {
                            q.push(d);
                        }
                    }
                }
            }
        }

        return dist;
    }
};

