// Standard swapping algor but using
// set to eliminate the duplications.
// Since this has to generate all permutation,
// it is not the fastest, but easier to code.
// Time: O(n!), space O(n!) for storing all permutations.

struct vec_hash {
    size_t operator() (const vector<int>& v) const {
        string s;
        for (int i : v) {
            s = s + to_string(i);
        }

        return hash<string>()(s);
    }

};

void permutate(vector<int>& a, int s, unordered_set<vector<int>, vec_hash>& result)
{
    if (s >= a.size()) {
        result.insert(a);
        return;
    }

    for (int i = s; i < a.size(); i++) {
        int t = a[s];
        a[s] = a[i];
        a[i] = t;

        permutate(a, s+1, result);

        t = a[s];
        a[s] = a[i];
        a[i] = t;
    }

    return;
}

class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        unordered_set<vector<int>, vec_hash> result;
        permutate(nums, 0, result);

        vector<vector<int>> ans(result.begin(), result.end());


        return ans;
    }
};
