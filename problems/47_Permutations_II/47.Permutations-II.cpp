// Instead of using swaping the input to generate the permutation, can also
// think as using a sequence generation to make a permutation.
// For example, input:
// [ 1 2 3 ]
// For generating a sequence, the first element of this sequence we can pick any element from
// the input, lets say pick "1".
// cur seq: [ 1 ]
// Since the "1" has been selected, for the next element of the sequence we have only 2 choices from
// input:
// [ 2 3 ]
// and lets pick "2" for the sequence,
// cur seq: [ 1 2 ]
// and the last element in the sequence has only 1 choice, "3", so the sequence is:
// cur seq: [ 1 2 3 ]
// and this is the first permutation.
// If the at the 2 element of the sequence, we pick "3", then the sequence will become:
// cur seq: [ 1 3 2 ]
// which is another permutation. Then going back to the first element of the sequence,
// we can see this is a recusive selection, like using DFS to follow a selection path,
// and once hit the end, reverse the selection and do down to next path.
//
//                            Root
//                     /       |        \
//                    1        2         3
//                  / |      / |       / |
//                 2  3     1  3      1  2
//                 |  |     |  |      |  |
//                 3  2     3  1      2  1
//
// So for each step of the sequence generation, we move down 1 level of the tree.
// and each level we have 1 lesser choices than before since it get used in the
// current path.
// Compare with the swap method, the "reverse" the change path of going backward
// of the tree for next selection is done by backtracking the work we made on the
// current sequence. ie, before select the next choice in the current level,
// undo the change of the current sequence so the next choice can be selected.
// But how do we know how many choices we have on the current level?
// To keep track the used element, a hash table can be used. In the swap algor case,
// the used elements are keep tracked by moving the current swapping element forward.
// If no duplicated element, we only swap the elements after the current element.
// So the selected elements on the current path will not be swapped again.
// But using the generation algor, we have to pick the selection from the input
// everytime, so have to use extra data struct to keep track it.
// How this generation algor help to solve the duplications?
// The hash table can be a counter table, where the key is the element, and the
// value is the count of the element. We keep track how many elements have been used
// on the current sequence. Example:
//
// [ 1 1 2]
//
// Instead of 3 choices, we have only 2 choices in this case, which is "1 2".
// It is because picking the same element in the same index of current sequence
// will generate the same final permutation. Like:
// If pick the first "1" as sequence[0], then remaining will be "1 2",
// If pick the second "1" as sequence[0], then the remaining of course still be "1 2".
// So the tree like:
//
//                        root
//                      /      \
//                     1        2
//                  /     \       \
//                1        2       1
//                |        |       |
//                2        1       1
//
// For each level of tree, we have the number of choices as the number of unique elements.
// But the choice is available only if it still have count in the counting table.
// For example, the left most path of the tree above, the first selection is "1", then the
// remaining count of "1" is 1. Then next selection will be 1 or 2. The generation algor
// above can be treated as the special case of this counting table where all elements
// have count 1 (no duplication).
// The fan out of each node on levels is reduced since no need to consider n choices
// echo level. While the time worse case is still O(n!), and the space is increased to
// O(n+n!)

void permutate(const vector<int>& a, unordered_map<int, int>& count, vector<int>& cur, vector<vector<int>>& result)
{
    if (cur.size() == a.size()) {
        result.push_back(cur);
        return;
    }

    for (auto e : count) {

        int i = e.first;

        if (count[i] == 0) {
            continue;
        }
        cur.push_back(i);
        count[i] --;

        permutate(a, count, cur, result);

        cur.pop_back();
        count[i] ++;
    }

    return;

}

class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {

        vector<vector<int>> result;
        vector<int> cur;

        unordered_map<int, int> count;
        for_each(nums.begin(), nums.end(), [&count](int i) {
           count[i]++;
        });

        permutate(nums, count, cur, result);

        return result;
    }
};
