#
# @lc app=leetcode id=139 lang=python3
#
# [139] Word Break
#
# https://leetcode.com/problems/word-break/description/
#
# algorithms
# Medium (37.07%)
# Total Accepted:    405K
# Total Submissions: 1.1M
# Testcase Example:  '"leetcode"\n["leet","code"]'
#
# Given a non-empty string s and a dictionary wordDict containing a list of
# non-empty words, determine if s can be segmented into a space-separated
# sequence of one or more dictionary words.
# 
# Note:
# 
# 
# The same word in the dictionary may be reused multiple times in the
# segmentation.
# You may assume the dictionary does not contain duplicate words.
# 
# 
# Example 1:
# 
# 
# Input: s = "leetcode", wordDict = ["leet", "code"]
# Output: true
# Explanation: Return true because "leetcode" can be segmented as "leet
# code".
# 
# 
# Example 2:
# 
# 
# Input: s = "applepenapple", wordDict = ["apple", "pen"]
# Output: true
# Explanation: Return true because "applepenapple" can be segmented as "apple
# pen apple".
# Note that you are allowed to reuse a dictionary word.
# 
# 
# Example 3:
# 
# 
# Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
# Output: false
# 
# 
#
# The idea is assuming we are at the portion of the input string, and
# substring in front of this portion can be segmented. If we found the current
# portion in word dictionary, whole string can be segmented. If not, we backtrack
# the last segmented portion, and try another word for it. If we found another word
# in dictionary, when segment it and move to next portion, if not backtrack again.
# Recursively return the results. If all true, then whole string can be segmented.
# The DP trick is, at the current string position, the next portion can be checked 
# many times. For eg:
# s = "ABCD", Dict = ["A", "B", "AB", "XX"]
# we can segment it as:
# A -> B -> CD -> False
# AB -> CD -> False
# In this example, when we are at the string position B, the next portion CD will be tested
# twice. We know the result from the first test already, so we can cache it without
# recursively test the same substring again.
class Solution:
    
    def wb(self, s, wordDict, seen):
        #print("s", s)
        if len(s) == 0:
            return True
        
        # Test all words
        for w in wordDict:
            # check if we can segment at the current position
            if s.startswith(w):
                #print("w", w)
                # The next substring
                s2 = s[len(w)::]
                if s2 in seen:
                    # we cached this substring before, just use the cached result
                    r = seen[s2]
                else:
                    # we see this substring first time, test it recursively
                    r = self.wb(s2, wordDict, seen)
                    seen[s2] = r
                
                # The current position cannot be segmented since the next substring cannot be segmented
                # Try next word in dictionary
                if not r:
                    continue
                else:
                    return True
        
        # we tried all words in the dictionary but no segment can be found
        return False
        
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        seen = {}
        
        return self.wb(s, wordDict, seen)
