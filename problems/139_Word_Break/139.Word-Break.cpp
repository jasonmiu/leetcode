// This solution is same as the one of the python version.
// By scanning the input string char by char, assuming we are at the index i:

// WordDict = {"apple", "pen"}
//
//     i
//     V
// applepenapple
// \___|\______|
//     |       |
// prefix     suffix
//
// If the prefix substring can be found in the wordDict, we want to know if the suffix is word break-able,
// ie. ths suffix can be recusivly broke down to the elements in wordDict. So this work like a DFS to a
// search tree. For each char index, we want to search all possible suffixes. If the current prefix
// cannot find a word-breakable suffix, we try next char for the next prefix. If prefix has no word-breakable
// suffix, then the whole input string is not word-break-able.
// The issue is we can encounter the same suffix multiple time. Like:
//
// WordDict = {"app", "le", "apple", "pen"}
// "applepenapplexx"
// "app" -> "le" -> "penapplexx"
// "apple" -> "penapplexx"
// The suffix penapplexx happended multiple times. And we know this suffix is not breakable from the first
// path. So use a cache to save such result.
// Time is O(n^2), scan the whole input string forward, for every char, create a substring up to n size,
// so need create n times substring. Hence n^2. Space is O(n) for the cache.


bool trace(const string& str, int start, unordered_map<int,bool>& visited, const unordered_set<string>& wd)
{
    int n = str.size();

    if (start >= n) {
        return true;
    }

    if (visited.find(start) != visited.end()) {
        return visited[start];
    } else {
        bool ok = false;
        for (int i = start; i < n; i++) {
            string substr(str.begin()+start, str.begin()+i+1);
            if (wd.find(substr) != wd.end()) {
                bool r = trace(str, i+1, visited, wd);
                ok = ok || r;
            }
        }
        visited[start] = ok;
    }

    return visited[start];
}

class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {

        unordered_set<string> wd(wordDict.begin(), wordDict.end());

        unordered_map<int, bool> visited;

        bool ok = trace(s, 0, visited, wd);
        return visited[0];
    }
};
