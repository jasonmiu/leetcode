# For a house, we can select rob it or not.
# If we rob this house, we cannot rob the next but the next of the next.
# If we do not rob this house, we can rob the next.
# So it becomes a selecting tree for a input [1, 2, 3, 1]
#
#  H0 --R--> H2 --R--> End
#   +-- N -->H1 --R--> H3 --> End
#            +-- N --> H2 --R--> End
#                      +-- N --> H3 --> End
#
# We sum the money along the path of the tree and find the max one.
# Since some of the house will repeat, like H2 above, so we have 
# cache the known max return of a house.
# However, the main difficulty is, for the last house, like the H3,
# the decision of robbing it or not depends on if we have robbed 
# the first house, H0 along the path. If we have robbed that H0, 
# the last house cannot be robbed as they are adjacent. So when 
# we see the last house, we need to know if we have robbed the first
# house which is different from each tree path. And the max return
# of a tree path depends on this varible last house decision
# hence the middle nodes along the tree path cannot be saved to 
# a cache since for a house, H_i, the max return from this house 
# is no longer static but depends on did we robbed the first house.
# We cannot cache for repeating H_i then.
# So we want to make sure for a house H_i, we knew we robbed the 
# first house or not already. That becomes two seperated trees:
# The tree started with first house *robbed*, and the tree started
# with the first house *not robbed*. For each tree they have their 
# own caches.
# The first tree will start from H0, and force to rob this house.
# The second tree will start from H1, so we do not rob the H0.
# We find the max return from each tree, and return the max of two trees.
# Complexity O(n) for scanning all houses and cache if we visited before.
# Space is O(n) for the cache.


def robber(nums, i, start_with_zero, cache):
    n = len(nums)
    if i >= n:
        return 0
    if n > 1 and start_with_zero and i == (n-1):
        return 0
    
    if i in cache:
        return cache[i]
    
    if start_with_zero and i == 0:
        s1 = nums[i] + robber(nums, i+2, start_with_zero, cache)
        s2 = 0
    else:
        s1 = nums[i] + robber(nums, i+2, start_with_zero, cache)
        s2 = robber(nums, i+1, start_with_zero, cache)
        
    s = max(s1, s2)
    cache[i] = s
        
    return s

class Solution:
    def rob(self, nums: List[int]) -> int:
        if len(nums) == 0:
            return 0
        
        c1 = robber(nums, 0, True, dict())
        c2 = robber(nums, 1, False, dict())
        
        return max(c1, c2)

