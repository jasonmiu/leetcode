#
# @lc app=leetcode id=75 lang=python3
#
# [75] Sort Colors
#
# https://leetcode.com/problems/sort-colors/description/
#
# algorithms
# Medium (43.62%)
# Total Accepted:    399.7K
# Total Submissions: 900.9K
# Testcase Example:  '[2,0,2,1,1,0]'
#
# Given an array with n objects colored red, white or blue, sort them in-place
# so that objects of the same color are adjacent, with the colors in the order
# red, white and blue.
# 
# Here, we will use the integers 0, 1, and 2 to represent the color red, white,
# and blue respectively.
# 
# Note: You are not suppose to use the library's sort function for this
# problem.
# 
# Example:
# 
# 
# Input: [2,0,2,1,1,0]
# Output: [0,0,1,1,2,2]
# 
# Follow up:
# 
# 
# A rather straight forward solution is a two-pass algorithm using counting
# sort.
# First, iterate the array counting number of 0's, 1's, and 2's, then overwrite
# array with total number of 0's, then 1's and followed by 2's.
# Could you come up with a one-pass algorithm using only constant space?
# 
# 
#
# This is a Dutch national flag problem. We can think the result array
# is construcuted by 4 partitions:
# 0 -> L : Red (0) partition
# L -> M : White (1) partition
# M -> H : Unknown partition
# H -> n - 1: Blue (2) partition
# We like to keep those invariances and shrink the size of the unknown partition.
# We keep testing the elemenets in the unknown partition, and place it to the 
# correct partitions. When the unknown partition disappeared, ie M > H, we have
# all elements in the correct partitions.
# At the beginning M->H = 0 -> n-1, the whole list is unknown partition
# If A[M] == 0, we swap it with A[L], since the L is the upper of Red parition,
# we increase L and M
# if A[M] == 1, we want it to be larger than L, so increase M
# if A[M] == 2, we know it is for the blue partition, and since blue partition is
# from H -> n-1, we decrease H
# 
# Example
# [ 2 0 2 1 1 0]
#   L         H
#   M             [M] = 2
#
# [ 0 0 2 1 1 2 ]
#   L       H
#   M            [M] = 0
#
# [ 0 0 2 1 1 2 ]
#     L     H
#     M         [M] = 0
# [ 0 0 2 1 1 2 ]
#       L   H
#       M       [M] = 2
# [ 0 0 1 1 2 2 ]
#       L   H
#         M     [M] = 1
# [ 0 0 1 1 2 2 ]
#       L   H
#           M   [M] = 2
# [ 0 0 1 1 2 2 ]
#       L H
#          M     M > H, end
#
# The complexity is O(n) for scanning once of the list. The space complexity is O(1) since no extra space is needed.

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        
        l = 0
        m = 0
        h = len(nums) - 1
        
        while m <= h:
            if nums[m] == 0:
                nums[l], nums[m] = nums[m], nums[l]
                m += 1
                l += 1
            elif nums[m] == 1:
                m += 1
            elif nums[m] == 2:
                nums[h], nums[m] = nums[m], nums[h]
                h -= 1
