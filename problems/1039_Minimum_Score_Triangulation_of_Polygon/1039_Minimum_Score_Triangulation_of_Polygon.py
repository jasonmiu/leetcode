# A good explaination here: 
# https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/286753/C%2B%2B-with-picture
# The idea is to fix two points, i and j first. 
# let i be 0, j be the last vertex, n-1. 
# Move the 3rd point, k, from i+1 to j-1. This will form a triangle, in a clockwise fashsion.
# This also cut the ploygon in 3 parts. The ijk triangle, left and right parts of triangle.
# The left and right parts of triangle is a smaller ploygon, which can be resolved recursively.
# The new i is always the starting vertex of the new ploygon, and the new j is always the last
# vertex of the new ploygon.
# The i and j pair is like a "singature" of a ploygon, since we know by cutting it with all
# k vertex we know the min total score of this ploygon, we can cache the result of (i,j). 
# for example,
#               y     k
#               /-----\
#            x |      |
#              \_____/
#              i     j 
# the current triangle ijk will make a left ploygon ixyk. This ixyk ploygon will
# cut a triangle ixy, which is the same as the previous triangle ixk (k started from i clockwise).
# Hence we can get all triangles recusively and cache result of each ploygon.
# The complexity is O(n^3), since each ploygon we need run n times for k, and in this n times loop
# we create sub-ploygons which each sub-ploygon takes O(n^2) times.
# The space is O(n^2) for the cache.

def tri_value(alist, i, j, k):
    return alist[i] * alist[j] * alist[k]
    

def cut(alist, i, j, memo):
    
    if j == i + 1:
        return 0
    
    if (i, j) in memo:
        return memo[(i, j)]
    
    min_total = 999999999999999
    k = i + 1
    while k < j:
        t = tri_value(alist, i, j, k)
        total = t + cut(alist, i, k, memo) + cut(alist, k, j, memo)
        if total < min_total:
            min_total = total
        k += 1

    memo[(i,j)] = min_total
    return min_total
        
    

class Solution:
    def minScoreTriangulation(self, A: List[int]) -> int:
        ans = cut(A, 0, len(A)-1, dict())
        return ans

