// If the input value x, is an element in the input array, the
// output array with size k, should contains this value x, like:
// [1,2,3,4,5], k = 4, x = 3
// [1,2,3,4]
// If input value x is *not* an element in the input array,
// we like to get the closest value of x first, example:
// [1,2,3,5,6], k = 4, x = 4
// [2, 3, 5, 6]
// So want to find the closest value of x, and since the input array
// is sorted, using binary search lower_bound() to find that place.
// The lower_bound() actually return the first element that is *larger*
// than the x, so if the case:
//  1 2 3 4   5
//-----------------
// [1,2,3,200,300], k = 3, x = 4
// lower_bound will return index 4 (200), which is not the closest value of x (4).
// So the closest will be the lower_bound index - 1, as the lower_bound index is the
// first element larger than x, lower_bound index - 1 will be the last value small than x.
// We want to know which value is closer to x, where we want to test the lower_bound index - 1 value,
// and the lower_bound index value, as they are the last value small than x, and first value bigger
// than x. Call these two indices i, j. If x is an element in the input array, the lower_bound index
// value will be x. It must be the closest, and we put it to the test of i and j for generality.
// Then we keep checking if value at i or j is closer to x, following the instructions provided by
// question:
// An integer a is closer to x than an integer b if:
//
//     |a - x| < |b - x|, or
//     |a - x| == |b - x| and a < b
//
// If value at i is closer, expend it to left. If value at j is closer, expend it to right.
// Keep doing until we have k closest values.
// A trick in implementation is, when adding the values for i side, since it expends to left from right,
// means we putting the values in a reverse sort order. Like:
//
// [1,2,3,5,6], k = 4, x = 4
//      ^
//      i = 2, first_part = [3]
//    ^
//    i = 1, first_part = [3, 2]
//
// Since the question want to have a sorted answer, directly putting values with values at i and j
// will not be sorted since the order of added them is not known beforehead.
// But for i side values, we know they are in reverse order,
// and for j side values, we know they are in sorted order,
// So put them into two seperated arrays, then reverse array i, and join the reversed array i and array j,
// we will get a sorted array in linear time, which is faster than sort the direct inserted result array.
//
// Time: O(logn + k), if k = n, it will be O(n). Space is O(n) for storing the result.


class Solution {
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {

        auto lb_it = lower_bound(arr.begin(), arr.end(), x);
        int lb_idx = lb_it - arr.begin() - 1;

        int n = arr.size();
        int i = lb_idx;
        int j = lb_idx + 1;

        vector<int> ans;
        vector<int> first_part;
        vector<int> second_part;

        while (k != 0) {

            if (i >= 0 && j < n) {

                int iv = abs(arr[i] - x);
                int jv = abs(arr[j] - x);

                if (iv <= jv) {
                    first_part.push_back(arr[i]);
                    i--;
                } else {
                    second_part.push_back(arr[j]);
                    j++;
                }

            } else if (i < 0) {
                second_part.push_back(arr[j]);
                j++;
            } else if (j >= n) {
                first_part.push_back(arr[i]);
                i--;
            }

            k--;
        }

        reverse(first_part.begin(), first_part.end());
        ans.insert(ans.end(), first_part.begin(), first_part.end());
        ans.insert(ans.end(), second_part.begin(), second_part.end());

        return ans;
    }
};
