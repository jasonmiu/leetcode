# There are 3 conditions we want to stop to process next thread:
# 1. have an O but not enough H
# 2. have 2 H, but not enough O
# 3. the number of current threads are not 3.
# For wait a resouces, semaphore is the way. We have 1 semaphore for O,
# 2 semaphore for H. Then a thread start, it acquire a resouces.
# So at instance, 2 H and 1 O can be acquired.
# For the threads acquried semaphore resources, we want sync all 3 threads
# to release H2O. Barrier is designed for syncing a set of fixed number
# of threads. It works like a counter, if the number of the threads
# wait on the barrier hit the counter, it release all of them.


import threading

class H2O:
    def __init__(self):
        self.o_sem = threading.Semaphore(1)
        self.h_sem = threading.Semaphore(2)
        self.b = threading.Barrier(3)


    def hydrogen(self, releaseHydrogen: 'Callable[[], None]') -> None:
        
        # releaseHydrogen() outputs "H". Do not change or remove this line.
        self.h_sem.acquire()
        i = self.b.wait()
        releaseHydrogen()
        self.h_sem.release()
        


    def oxygen(self, releaseOxygen: 'Callable[[], None]') -> None:
    
        # releaseOxygen() outputs "O". Do not change or remove this line.
        
        self.o_sem.acquire()
        i = self.b.wait()
        releaseOxygen()
        self.o_sem.release()
        

