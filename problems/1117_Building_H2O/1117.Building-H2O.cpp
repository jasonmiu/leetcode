class H2O {
private:
    int h_cnt;
    int o_cnt;
    mutex m;
    condition_variable cv;

public:
    H2O() {
        h_cnt = 2;
        o_cnt = 1;
    }

    void hydrogen(function<void()> releaseHydrogen) {

        unique_lock<mutex> lock(m);
        cv.wait(lock, [this]() {
           return h_cnt > 0;
        });

        h_cnt --;

        // releaseHydrogen() outputs "H". Do not change or remove this line.
        releaseHydrogen();

        if (h_cnt == 0 && o_cnt == 0) {
            h_cnt = 2;
            o_cnt = 1;
        }

        lock.unlock();
        cv.notify_one();
    }

    void oxygen(function<void()> releaseOxygen) {

        unique_lock<mutex> lock(m);
        cv.wait(lock, [this]() {
           return o_cnt > 0;
        });

        o_cnt --;

        // releaseOxygen() outputs "O". Do not change or remove this line.
        releaseOxygen();

        if (h_cnt == 0 && o_cnt == 0) {
            h_cnt = 2;
            o_cnt = 1;
        }

        lock.unlock();
        cv.notify_one();
    }
};
