#
# @lc app=leetcode id=959 lang=python3
#
# [959] Regions Cut By Slashes
#
# https://leetcode.com/problems/regions-cut-by-slashes/description/
#
# algorithms
# Medium (64.34%)
# Total Accepted:    13.7K
# Total Submissions: 21.1K
# Testcase Example:  '[" /","/ "]'
#
# In a N x N grid composed of 1 x 1 squares, each 1 x 1 square consists of a /,
# \, or blank space.  These characters divide the square into contiguous
# regions.
# 
# (Note that backslash characters are escaped, so a \ is represented as "\\".)
# 
# Return the number of regions.
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input:
# [
# " /",
# "/ "
# ]
# Output: 2
# Explanation: The 2x2 grid is as follows:
# 
# 
# 
# 
# Example 2:
# 
# 
# Input:
# [
# " /",
# "  "
# ]
# Output: 1
# Explanation: The 2x2 grid is as follows:
# 
# 
# 
# 
# Example 3:
# 
# 
# Input:
# [
# "\\/",
# "/\\"
# ]
# Output: 4
# Explanation: (Recall that because \ characters are escaped, "\\/" refers to
# \/, and "/\\" refers to /\.)
# The 2x2 grid is as follows:
# 
# 
# 
# 
# Example 4:
# 
# 
# Input:
# [
# "/\\",
# "\\/"
# ]
# Output: 5
# Explanation: (Recall that because \ characters are escaped, "/\\" refers to
# /\, and "\\/" refers to \/.)
# The 2x2 grid is as follows:
# 
# 
# 
# 
# Example 5:
# 
# 
# Input:
# [
# "//",
# "/ "
# ]
# Output: 3
# Explanation: The 2x2 grid is as follows:
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= grid.length == grid[0].length <= 30
# grid[i][j] is either '/', '\', or ' '.
# 
# 
# 
# 
# 
# 
#
# The main idea is instead of thinking how to connect the edges
# together and find the partitions, can think in a way that:
# "What creates a partition?"
# "Instead of cutting, can I join pieces together and form a larger partition that we want?
#
# Think the smallest unit, a 1x1 square, we have 3 cases:
# 1. Empty cell
# +-+
# | |
# +-+
#
# 2. "/" cell
# +-+
# |/|
# +-+
#
# 3. "\" cell
# +-+
# |\|
# +-+
#
# From case 2 and 3, we can see the triangles are made from smaller triangles, if we
# divide a cell square in to 4 divisions:
#                 v-- division 0
#                +--+
# division 3 --> |\/| <-- division 1
#                |/\|  
#                +--+
#                 ^-- division 2
#
# A '/' cell can be saw as division 1 + 2 and division 0 + 3
# A '\' cell can be saw as division 0 + 1 and division 3 + 2
# A empty cell can be saw as divison 0+1+2+3
# The joined divisions will form a partition. We can use union find to union the divisions,
# and find the representative of each division. If two divisions are in the same union, ie.
# a partition, they have the same representative we can think this representative as a
# partition id.
# A tricky part is when we have two cells, the adjacent divisions need to be unioned too, like:
# 
#                +--++--+
#                |\/||\/| 
#                |/\||/\|  
#                +--++--+
#  last cell d 1 --^  ^-- current cell d 3
#
# We want to union the current cell division 3 and the division 1 of the cell on the left
# Similarly, we want to union the curent cell division 0 and the division 2 of the cell on the top,
# if we scan the cells from left to right, top to down.
# So the division union representative need to include the cell ID (i, j) as well.
#
# Finally we find all unique union representative of each division. The number of unqiue
# union representative is the number of the partitions.
# Complexity is O(n^2), n is the dimension of the input grid.
# Space is O(n^2) as well, for the unions.
#
# Another way to solve this is to think the whole grid is a raster picture.
# We expend each cell to 9X9, like
# 001
# 010
# 100 
# for a '/' cell. So we "draw" each cell and form a N*N*9 2D array as a picture.
# Then we use DFS/BFS to fill up empty pixels. For each filled up area, we increase the 
# filled area count. The number of the filled area count is the number of partitions.

def find(a, s):
    if a not in s:
        s[a] = a
        return s[a]
    else:
        if s[a] != a:
            return find(s[a], s)
        else:
            return s[a]

def union(a, b, s):
    x = find(a, s)
    y = find(b, s)
    s[x] = y

    return s

class Solution:
    def regionsBySlashes(self, grid: List[str]) -> int:
        
        s = {}
        n = len(grid)
        
        for i in range(0, n):
            for j in range(0, n):
                c = grid[i][j]
                    
                if c == '\\':
                    union((i, j, 3), (i, j, 2), s)
                    union((i, j, 0), (i, j, 1), s)
                    
                if c == '/':
                    union((i, j, 0), (i, j, 3), s)
                    union((i, j, 1), (i, j, 2), s)
                    
                if c == ' ':
                    union((i, j, 0), (i, j, 3), s)
                    union((i, j, 1), (i, j, 2), s)
                    union((i, j, 0), (i, j, 2), s)
                    
                if j > 0:
                    union((i, j-1, 1), (i, j, 3), s)
                if i > 0:
                    union((i-1, j, 2), (i, j, 0), s)

        parents = set()
        for n in s:
            parents.add(find(n, s))
            
        ans = len(parents)
        return ans
            
        
        
        
