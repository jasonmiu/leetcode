# This problem is similar to 31_Next_Permutation
# The main problem of this question is the permutation is in
# lex order. This is the algor to find the next lex permutation:
#
# https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
# 1. Find the largest index i such that a[i] < a[i + 1]. If no such index exists, the permutation is the last permutation.
#   - so from the right, a[i] is the first value smaller then its right hand side. 
# 2. Find the largest index j greater than i such that a[j] > a[i]
#   - from the a[i], search to right to find the largest index that a[j] > a[i]
# 3. Swap the value of a[i] with that of a[j].
# 4. Reverse the sequence from a[i + 1] up to and including the final element a[n-1]
#
# So we start from the smallest permutation, which is always sorted, and find the next lex permutation
# until we find the k-th.
# A good recursive next lex permutation is:
# https://stackoverflow.com/questions/29928236/print-all-permutation-in-lexicographic-order
#
# def permute(done, remaining):
#   if not remaining:
#     print done
#     return
#
#   sorted_rem = sorted(remaining)
#   l = len(sorted_rem)
#
#   for i in xrange(0, l):
#     c = sorted_rem[i]
#
#     # Move to c to done portion.
#     done.append(c)
#     remaining.remove(c)
#
#     # Permute the remaining
#     permute(done, remaining)
#
#     # Put c back.
#     remaining.append(c)
#     # Remove from done.
#     del done[-1]
#
# which is pick up one element at a time, move to "done" array, and sort the remaining array,
# so we always pick the next available smallest permutation.
#
# The lex permutation is O(n), and we need at most n! for main loop, to the complexity is 
# O(nn!).
# It is inplace so space os O(1).
#
# The best solution is:
# https://leetcode.com/problems/permutation-sequence/discuss/22507/%22Explain-like-I'm-five%22-Java-Solution-in-O(n)


def next_permu(a):
    n = len(a)
    # first_val is the first value smaller than the values on its right
    # it is also the reverse point of the remaining substring as we want to keep the 
    # remaining substring in lex acs order
    first_val = -1 
    first_idx = 0
    sec_val = 999999 # is the larger than first_val
    sec_idx = 0
    
    # find the first value smaller than current [i]
    for i in range(n-2, -1, -1):
        if a[i] < a[i+1]:
            first_val = a[i]
            first_idx = i
            break
            
    # find the largest index that has value larger than first_val
    for i in range(first_idx, n):
        if a[i] > first_val:
            sec_val = a[i]
            sec_idx = i
            
    a[first_idx] = sec_val
    a[sec_idx] = first_val
    
    a[first_idx+1::] = reversed(a[first_idx+1::])
    
    return a
            

class Solution:
    def getPermutation(self, n: int, k: int) -> str:
        a = list(range(1, n+1))
        for i in range(0, k-1):
            a = next_permu(a)
            
        a = list(map(str, a))
        return "".join(a)
        
