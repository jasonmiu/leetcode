#
# @lc app=leetcode id=877 lang=python3
#
# [877] Stone Game
#
# https://leetcode.com/problems/stone-game/description/
#
# algorithms
# Medium (62.64%)
# Total Accepted:    43.5K
# Total Submissions: 69.2K
# Testcase Example:  '[5,3,4,5]'
#
# Alex and Lee play a game with piles of stones.  There are an even number of
# piles arranged in a row, and each pile has a positive integer number of
# stones piles[i].
# 
# The objective of the game is to end with the most stones.  The total number
# of stones is odd, so there are no ties.
# 
# Alex and Lee take turns, with Alex starting first.  Each turn, a player takes
# the entire pile of stones from either the beginning or the end of the row.
# This continues until there are no more piles left, at which point the person
# with the most stones wins.
# 
# Assuming Alex and Lee play optimally, return True if and only if Alex wins
# the game.
# 
# 
# 
# Example 1:
# 
# 
# Input: [5,3,4,5]
# Output: true
# Explanation: 
# Alex starts first, and can only take the first 5 or the last 5.
# Say he takes the first 5, so that the row becomes [3, 4, 5].
# If Lee takes 3, then the board is [4, 5], and Alex takes 5 to win with 10
# points.
# If Lee takes the last 5, then the board is [3, 4], and Alex takes 4 to win
# with 9 points.
# This demonstrated that taking the first 5 was a winning move for Alex, so we
# return true.
# 
# 
# 
# 
# Note:
# 
# 
# 2 <= piles.length <= 500
# piles.length is even.
# 1 <= piles[i] <= 500
# sum(piles) is odd.
# 
#
# The idea is to simulate the game turns, we can see for input:
# P = [5 3 4 5]
# Alex (first player) can take P[0] or P[3]
#                         [5 3 4 5]                  (A)
#                      /            \
#                [3 4 5]           [5 3 4]           (L)
#               /      \           /     \
#           [4 5]     [3 4]      [5 3]    [3 4]      (A)
# Each level of tree is a turn of a user. And we can see
# some siturations like [3 4] (A) are repeated.
# By taking each pile of stones (element in the array), 
# it is recursive problem, with a smaller input, and with different player
# We can use index s (start) and e (end) to tell the sub-input of each
# turn (level in the recursion tree), and keep track the scores of each players,
# and if s > e, which is the input is empty, we check if Alex has higher score
# than Lee. If so, return Ture for that game path. We search all game paths,
# if any game path produces True (Alex can win), then propagate the True
# back to the top.
# Since we have repeated siturations, we can memorize the (s e player) for 
# that situration. If we saw this situration before and we know if Alex will
# win or not, just return the result.
# The complexity is O(n^2), since the s can be N and e can be N hence
# we need to memorize N*N siturations. The space is O(n^2) too.


def game(piles, s, e, player, score1, score2, memo):
    if s > e:
        if score1 > score2:
            return True
        else:
            return False
        
    if (s, e, player) in memo:
        return memo[(s, e, player)]

    ns1 = score1
    ns2 = score2
    if player > 0:
        ns1 = piles[s] + score1
    else:
        ns2 = piles[s] + score2
    r1 = game(piles, s+1, e, player*-1, ns1, ns2, memo)
    
    ns1 = score1
    ns2 = score2
    if player > 0:
        ns1 = piles[e] + score1
    else:
        ns2 = piles[e] + score2
    r2 = game(piles, s, e-1, player*-1, ns1, ns2, memo)
    
    
    if r1 or r2:
        r = True
    else:
        r = False
        
    memo[(s, e, player)] = r
    return r

    

class Solution:
    def stoneGame(self, piles: List[int]) -> bool:
        
        ans = game(piles, 0, len(piles)-1, 1, 0, 0, {})
        return ans
