// The idea is using fast-slow pointers.
// The slow pointer, i, is the current element we want to check.
// The fast pointer, j, is the known next non-zero element, which is going to be swapped with the element i.
// Once found the next non-zero element, save it as last_non_zero, (L)
// example:
// [0, 1, 0, 3, 12]
//  i
//  L
//  j <- search j to the next non-zero element
//
// [0, 1, 0, 3, 12]
//  i
//  L
//     j <-- swap [i, j]
//
//
// [1, 0, 0, 3, 12]
//  i
//  L <-- save the last_non_zero as j
//     j
//
//
// [1, 0, 0, 3, 12]
//  i
//     L
//     j
//
//
// [1, 0, 0, 3, 12]
//     i
//     L
//     j <-- start search next non-zero from last_non_zero
//
//
// [1, 0, 0, 3, 12]
//     i
//     L
//           j <-- swap [i,j]
//
//
// [1, 3, 0, 0, 12]
//     i
//           L
//           j
//
//
// [1, 3, 0, 0, 12]
//        i
//           L
//           j <-- search next non-zero from last_non_zero
//
// [1, 3, 0, 0, 12]
//        i
//           L
//               j <-- sawp
//
// [1, 3, 12, 0, 0]
//        i
//               L
//               j
//
//
//
// One important point is the i has always be smaller than or equal to last_non_zero,
// it is because the i should point at the current 0 value element when try to swap.
// If this case:
//
// [9, 0]
//  L  i
//  j
//
// while the last_non_zero is index 0 (value 9), i is pointing to the first zero value,
// but since we want to move all zeros to right, ie. only swap if the 0 is going to right,
// if the to be last_non_zero is <= i, we set it to i, and start searching the next non-zero always
// after i, so when swap we know the current i element will be moved to right (tail).
//
// Time O(n), since i and j only scan the array once each. Space O(1).



class Solution {
public:
    void moveZeroes(vector<int>& nums) {

        int n = nums.size();
        int i = 0;
        int last_non_zero = 0;

        while (i < n) {
            if (nums[i] == 0) {
                if (last_non_zero <= i) {
                    last_non_zero = i;
                }
                for (int j = last_non_zero; j < n; j++) {
                    if (nums[j] != 0) {
                        int t = nums[i];
                        nums[i] = nums[j];
                        nums[j] = t;
                        last_non_zero = j + 1;
                        break;
                    }
                }
            }
            i = i + 1;
        }
    }
};
