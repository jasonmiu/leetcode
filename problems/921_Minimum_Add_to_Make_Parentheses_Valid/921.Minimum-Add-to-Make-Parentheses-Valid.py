# The idea is to find how many parentheses are not balance.
# Use a stack to save the current unbalance parentheses.
# Scan the input string from left, for a pair of balance parentheses,
# The top of stack must be a open parenthese '(' and the current char
# has to be a close parenthese ')'. If they are balance, then
# pop the current stack top. If not balance, then push the current char
# to the stack. The stack always contains unbalance parentheses.
# Since each unbalance parenthese needs 1 parenthese to make it valid,
# so the anwser is the size of the stack.
# Complexity O(n) since scan the input once.
# Space is O(n) for the stack.


def is_balance(c, t):
    if c == "(":
        return False
    elif c == ")" and t == "(":
        return True
    
    return False

class Solution:
    def minAddToMakeValid(self, S: str) -> int:
        
        stack = []
        for i in range(0, len(S)):
            c = S[i]
            
            if len(stack) == 0:
                stack.append(c)
            else:
                t = stack[-1]
                if is_balance(c, t):
                    stack.pop(-1)
                else:
                    stack.append(c)
                
        return len(stack)

