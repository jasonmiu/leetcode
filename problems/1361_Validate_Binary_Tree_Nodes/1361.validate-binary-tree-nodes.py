# For all nodes form *exactly one vaild* binary tree, here are the
# conditions:
# 1. No cycle
# 2. All nodes are accessible from single root (completely connected)
# 3. Only 1 root
# For (1), we can use DFS to detect cycle. But to use DFS, we need to start with
# the root. So we want to find the root.
# For a root, it has only 0 in-degree, ie. no other node is going to it.
# We can scan the left and right child lists, for every value in those lists,
# it is referenced, and we increase its in-degree count. And since the nodes
# are numbered from 0 to n-1, all node values are unique. We can use an array
# to store all nodes in-degrees.
# If we have more than 1 node that has 0 in-degree, means we have multiple roots,
# and condition (2) and (3) failed, we return False.
# If we have no node has 0 in-degree, means we found a self-looped node. Condition (1)
# failed, we return False.
# Then we want to find if an indirect cycle appears in the graph, we build a adjacency list
# and run the DFS on it. We keep track the visited nodes and if we re-visit a node,
# means we find a cycle and condition (1) failed. If all 3 conditions are pass, then we return
# True.
# Complexity is O(n) since we need to visit all nodes once.
# The space complexity is O(n) for adjacency list and visited nodes set.

import collections

def to_alist_degs(n, lc, rc):
    alist = collections.defaultdict(list)
    degs = [0] * n
    for i in range(0, n):
        if lc[i] != -1:
            alist[i].append(lc[i])
            degs[lc[i]] += 1
            
        if rc[i] != -1:
            alist[i].append(rc[i])
            degs[rc[i]] += 1
            
    
    return (alist, degs)        
    
def dfs(alist, root, visited):
    if root in visited:
        return False
    
    visited.add(root)
    ans = True
    for n in alist[root]:
        ans = dfs(alist, n, visited)
        if not ans:
            return False
        
    return ans
               
    

class Solution:
    def validateBinaryTreeNodes(self, n: int, leftChild: List[int], rightChild: List[int]) -> bool:
        
        (alist, degs) = to_alist_degs(n, leftChild, rightChild)
        
        root = -1
        for i in range(0, n):
            if degs[i] == 0 and root == -1:
                root = i
            elif degs[i] == 0 and root >= 0: # we saw a in-deg == 0 node before
                return False
        if root == -1:
            return False
        
        ans = dfs(alist, root, set())
        return ans
