#
# @lc app=leetcode id=969 lang=python3
#
# [969] Pancake Sorting
#
# https://leetcode.com/problems/pancake-sorting/description/
#
# algorithms
# Medium (62.51%)
# Total Accepted:    23.6K
# Total Submissions: 35.9K
# Testcase Example:  '[3,2,4,1]'
#
# Given an array A, we can perform a pancake flip: We choose some positive
# integer k <= A.length, then reverse the order of the first k elements of A.
# We want to perform zero or more pancake flips (doing them one after another
# in succession) to sort the array A.
# 
# Return the k-values corresponding to a sequence of pancake flips that sort
# A.  Any valid answer that sorts the array within 10 * A.length flips will be
# judged as correct.
# 
# 
# 
# Example 1:
# 
# 
# Input: [3,2,4,1]
# Output: [4,2,4,3]
# Explanation: 
# We perform 4 pancake flips, with k values 4, 2, 4, and 3.
# Starting state: A = [3, 2, 4, 1]
# After 1st flip (k=4): A = [1, 4, 2, 3]
# After 2nd flip (k=2): A = [4, 1, 2, 3]
# After 3rd flip (k=4): A = [3, 2, 1, 4]
# After 4th flip (k=3): A = [1, 2, 3, 4], which is sorted. 
# 
# 
# 
# Example 2:
# 
# 
# Input: [1,2,3]
# Output: []
# Explanation: The input is already sorted, so there is no need to flip
# anything.
# Note that other answers, such as [3, 3], would also be accepted.
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= A.length <= 100
# A[i] is a permutation of [1, 2, ..., A.length]
# 
# 
#
# The idea is to move the current largest value to the end of the array.
# The current largest value means, the largest value in the unsorted portion
# of the array. If we can move the largest value to the end of the unsorted portion,
# the unsorted portion will become smaller and smaller. When the unsorted portion
# moves to the head of the array, this array is sorted.
# For example:
#   0 1 2 3 4
# [ 1 4 2 3 ]
#           ^- unsorted portion
# The current unsorted portion is at index 4, ie the whole array is unsorted.
# The largest value in unsorted portion is 4, index 1.
# Reversing index 0 to the index of largest value, we can move the largest value to the head.
# Then do a 0 to unsorted portion index - 1 reversing, we can move the largest value to the
# end of the current unsorted portion.
#   0 1 2 3 4
# [ 1 4 2 3 ]
#     ^     ^- unsorted portion
#     \ reverse 0 to here
#
#   0 1 2 3 4
# [ 4 1 2 3 ]
#         ^ ^- unsorted portion
#         \ reverse 0 to here
#
#   0 1 2 3 4
# [ 3 1 2 4 ]
#         ^- unsorted portion updated
#
# By keep moving the unsorted portion, we can sort the array. For each reverse operation, 
# we save the reversing point and get the answer.
# Complexity O(n^2) as scanning N elements, each element do a reverse which is O(n).
# Space is O(1) since the reverse operation can be in-place.

def is_sorted(a, s, e):
    for i in range(s+1, e+1):
        x = a[i-1]
        y = a[i]
        if y < x:
            return False
    return True

def max_val_idx(a, s, e):
    m = 0
    max_idx = s
    for i in range(s, e+1):
        if a[i] > m:
            max_idx = i
            m = a[i]
            
    return max_idx

class Solution:
    def pancakeSort(self, A: List[int]) -> List[int]:
        sorted_idx = len(A)
        a = A
        ans = []
        while sorted_idx > 0:           
            if is_sorted(a, 0, sorted_idx-1):
                return ans
            
            max_idx = max_val_idx(a, 0, sorted_idx - 1)
            if max_idx > 0:
                a = list(reversed(a[0:max_idx+1])) + a[max_idx+1::]
                ans.append(max_idx+1)
                if is_sorted(a, 0, sorted_idx-1):
                    return ans
            
            a = list(reversed(a[0:sorted_idx])) + a[sorted_idx::]
            ans.append(sorted_idx)
            if is_sorted(a, 0, sorted_idx - 1):
                return ans
            
            sorted_idx -= 1
            
        return ans
