#
# @lc app=leetcode id=1161 lang=python3
#
# [1161] Maximum Level Sum of a Binary Tree
#
# https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree/description/
#
# algorithms
# Medium (70.90%)
# Total Accepted:    25.6K
# Total Submissions: 35.9K
# Testcase Example:  '[1,7,0,7,-8,null,null]'
#
# Given the root of a binary tree, the level of its root is 1, the level of its
# children is 2, and so on.
# 
# Return the smallest level X such that the sum of all the values of nodes at
# level X is maximal.
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: [1,7,0,7,-8,null,null]
# Output: 2
# Explanation: 
# Level 1 sum = 1.
# Level 2 sum = 7 + 0 = 7.
# Level 3 sum = 7 + -8 = -1.
# So we return the level with the maximum sum which is level 2.
# 
# 
# 
# 
# Note:
# 
# 
# The number of nodes in the given tree is between 1 and 10^4.
# -10^5 <= node.val <= 10^5
# 
# 
#
# The main idea to do a BFS to find the each level.
# Using the queue, and read the number of nodes in the 
# queue for for each level instead of reading all nodes
# in queue for counting levels.
# Complexity is O(n) for visiting all nodes.
# Space is O(n) since the leaf node numbers is at max (n-1) / 2
# and this is the largest queue size.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def maxLevelSum(self, root: TreeNode) -> int:
        q = []
        q.append(root)
        cur_max = -pow(10, 7)
        cur_lv = 1
        max_lv = 1
        
        while len(q) > 0:
            qlen = len(q)
            s = 0
            for i in range(0, qlen):
                node = q.pop(0)
                s += node.val
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
                
            if s > cur_max:
                max_lv = cur_lv
                cur_max = s
                
            cur_lv += 1
            
        return max_lv
            
