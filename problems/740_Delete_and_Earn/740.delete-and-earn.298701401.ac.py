#
# @lc app=leetcode id=740 lang=python3
#
# [740] Delete and Earn
#
# https://leetcode.com/problems/delete-and-earn/description/
#
# algorithms
# Medium (47.10%)
# Total Accepted:    30.2K
# Total Submissions: 63.2K
# Testcase Example:  '[3,4,2]'
#
# Given an array nums of integers, you can perform operations on the array.
# 
# In each operation, you pick any nums[i] and delete it to earn nums[i] points.
# After, you must delete every element equal to nums[i] - 1 or nums[i] + 1.
# 
# You start with 0 points. Return the maximum number of points you can earn by
# applying such operations.
# 
# Example 1:
# 
# 
# Input: nums = [3, 4, 2]
# Output: 6
# Explanation: 
# Delete 4 to earn 4 points, consequently 3 is also deleted.
# Then, delete 2 to earn 2 points. 6 total points are earned.
# 
# 
# 
# 
# Example 2:
# 
# 
# Input: nums = [2, 2, 3, 3, 3, 4]
# Output: 9
# Explanation: 
# Delete 3 to earn 3 points, deleting both 2's and the 4.
# Then, delete 3 again to earn 3 points, and 3 again to earn 3 points.
# 9 total points are earned.
# 
# 
# 
# 
# Note:
# 
# 
# The length of nums is at most 20000.
# Each element nums[i] is an integer in the range [1, 10000].
# 
# 
# 
# 
#
# One important thing can be noticed is when picking a number,
# we will take all occurance of that number, for example:
# [2 2 2 3 3 4], if we take 2, we will pick up all three 2s,
# and get 6 points. Then we cannot pick 3, but can pick 4.
# If we pick 3, then we cannot pick 2 and 4.
# If we put all numbers as a line, we see we cannot pick
# the adjacently values. For example:
# 0 1 2 3 4 5 6 7 8 
#     ^ ^
#        ^ ^
#     if we picked 2, the next one will be 4
#       if we picked 3, the next one will be 5
# If we think "What is the max possible value I can get when
# I am now at the number i?", then the current max possible
# means, we pick up the current value i, plus the max value i - 2 
# (like we pick 5 then we also picked 3)
# or we do not pick 5, then the max current value is i - 1
# With the first observation we know we get the sum of the value i
# with its frequency. So we build a dict with the key is the 
# values 1 -> n, and value is its value * frequency.
# Then for each number n, we can pick or not pick, the max current value 
# at n will be:
# max(sums[n] + dp[n-2], dp[n-1])
#     \__ pick i,         \__ not pick i
# One trick is we need to build the DP list from 1 -> max(nums),
# instead of just values in nums. Because we need to look at n - 1 and n - 2,
# if n-1 or n-2 do not exisit in the nums, that will not work, so we need
# to build the DP with the values not exisit in the nums as well.
# Complexity is O(n) for building the DP which as max N size.
# The space is also O(n) for DP list.

import collections
class Solution:
    def deleteAndEarn(self, nums: List[int]) -> int:
        
        if len(nums) == 0:
            return 0
        
        sums = collections.defaultdict(int)
        for n in nums:
            sums[n] += n
            
        dp = [0] * 10001
        max_num = max(nums)
        
        for n in range(1, max_num+1):
            #print("n", n, "dp[n-2]", dp[n-2])
            if n > 1:
                dp[n] = max(sums[n] + dp[n-2], dp[n-1])
            else:
                dp[n] = max(sums[n], dp[n-1])
                
        return max(dp)
