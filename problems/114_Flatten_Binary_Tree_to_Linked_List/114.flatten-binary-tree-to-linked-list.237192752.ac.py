# TO EXPLAIN
#
# @lc app=leetcode id=114 lang=python3
#
# [114] Flatten Binary Tree to Linked List
#
# https://leetcode.com/problems/flatten-binary-tree-to-linked-list/description/
#
# algorithms
# Medium (44.81%)
# Total Accepted:    274.3K
# Total Submissions: 612.1K
# Testcase Example:  '[1,2,5,3,4,null,6]'
#
# Given a binary tree, flatten it to a linked list in-place.
# 
# For example, given the following tree:
# 
# 
# ⁠   1
# ⁠  / \
# ⁠ 2   5
# ⁠/ \   \
# 3   4   6
# 
# 
# The flattened tree should look like:
# 
# 
# 1
# ⁠\
# ⁠ 2
# ⁠  \
# ⁠   3
# ⁠    \
# ⁠     4
# ⁠      \
# ⁠       5
# ⁠        \
# ⁠         6
# 
#
#
# If we like to do this in-place, we need to modify the link between the nodes.
# For 3 base cases:
#      1
#     /
#    2
# We shift the left node to right node
#   1
#    \
#     2
# We do not need to change.
#     1
#    / \
#   2   3
# We frist change the right node of the root to left node,
# and search to the end of the new right node, and attach
# the old right subtree, like:
#
#
#     1
#    / \
#   2   3 <- n
#
#     1
#      \
#       2   3 <- n
#
#      1
#       \
#   r -> 2   3 <- n
#
#       1
#        \
#         2 <- r
#          \
#           3 <- n
#
# Do this operation from the left mode first, as a pre-order visit.
# Complexity O(n) for accessing all nodes.
# Space O(logn) for tree height.
#
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def flatten(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        
        if root == None:
            return
        
        if root.left == None and root.right == None:
            return
        
        if root.left:
            self.flatten(root.left)
        
        if root.right:
            self.flatten(root.right)
                
        if root.left and (root.right == None):
            root.right = root.left
            root.left = None
            return
        
        if root.right and (root.left == None):
            return
        
        if root.right and root.left:
            n = root.right
            root.right = root.left
            ### append right most
            r = root.right
            while(True):
                if r.right == None:
                    break
                else:
                    r = r.right
                    
            r.right = n
            root.left = None
            
        return
