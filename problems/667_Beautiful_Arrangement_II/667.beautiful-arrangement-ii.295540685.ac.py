#
# @lc app=leetcode id=667 lang=python3
#
# [667] Beautiful Arrangement II
#
# https://leetcode.com/problems/beautiful-arrangement-ii/description/
#
# algorithms
# Medium (52.77%)
# Total Accepted:    21.1K
# Total Submissions: 39.7K
# Testcase Example:  '3\n2'
#
# 
# Given two integers n and k, you need to construct a list which contains n
# different positive integers ranging from 1 to n and obeys the following
# requirement: 
# 
# Suppose this list is [a1, a2, a3, ... , an], then the list [|a1 - a2|, |a2 -
# a3|, |a3 - a4|, ... , |an-1 - an|] has exactly k distinct integers.
# 
# 
# 
# If there are multiple answers, print any of them.
# 
# 
# Example 1:
# 
# Input: n = 3, k = 1
# Output: [1, 2, 3]
# Explanation: The [1, 2, 3] has three different positive integers ranging from
# 1 to 3, and the [1, 1] has exactly 1 distinct integer: 1.
# 
# 
# 
# Example 2:
# 
# Input: n = 3, k = 2
# Output: [1, 3, 2]
# Explanation: The [1, 3, 2] has three different positive integers ranging from
# 1 to 3, and the [2, 1] has exactly 2 distinct integers: 1 and 2.
# 
# 
# 
# Note:
# 
# The n and k are in the range 1 
# 
# 
#
# From examples, we see it like to have the differences *decreasing*, like:
# n = 3, k = 2
# [1 3 2]
# differences = [2 1]
# n = 4, k = 2
# [1 3 2 4]
# differences = [2 1 2]
# and want to keep the repeat differnece as monotonic increacing
# So there are two cases:
# 1. We still want to make distinct integers
# 2. We fulfill K already. Make repeating differerences with max
# To make the disinct intergers, we can:
# [ 1 +k -k-1 +k-2 -k+3 ], then each difference will be the decreasing k.
# For case 2, we find the max of the list, and keep increacing it so
# the difference between current number and the previous one will 
# always be 1.
# Complexity O(n), space = O(1)

class Solution:
    def constructArray(self, n: int, k: int) -> List[int]:
        
        A = [1]
        
        if n == 1:
            return A
        
        cur_max = 1
        c = 1
        for i in range(1, n):
            if k > 0:
                a = A[i-1] + (k * c)
                c = -c
                A.append(a)
                k = k - 1
                if a > cur_max:
                    cur_max = a
            else:
                a = cur_max + 1
                cur_max = a
                A.append(a)
                
        return A

# I think the following algor is correct and more intuitive
# but the system does not accept
# I understand why now. As the max number need to be "n"
# **you need to construct a list which contains n different positive integers ranging from 1 to n**
# example:
# 4/3
# [1 4 6 7], expect [1 4 2 3]
# 3/2
# [1 3 4], expect [1 3 2]
# 5/3
# [1 4 6 7 8], expect [1 4 2 3 5]
'''
class Solution:
    def constructArray(self, n: int, k: int) -> List[int]:
        A = [1]
        if n == 1:
            return A
        
        d = 0
        for i in range(1, n):
            if k > 0:
                a = A[i-1] + k
                k -= 1
            else:
                a = A[i-1] + d
                
            A.append(a)
            d = a - A[i-1]
            
        return A
'''
        
                
                
                
                
            
        
