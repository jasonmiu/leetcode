// Think search in the index space. Since we have n + 1 elements,
// and each element is range from 1 to n. Assuming no duplication (only n elements),
// like:
//   0 1 2 3 4 5
// ---------------
// [ 1 2 3 4 5 6 ] , n = 6
//         ^
//         | 4 has 4 elements less or equal to it.
//
// Pick any number, for example, 4. Count the whole array, there should be exactly 4
// elements are less or equal to it. So for a element 'x', there should has
// x elements are less or equal to it. But if we have duplication, like:
//
//   0 1 2 3 4 5 6
// ---------------
// [ 1 2 3 4 5 6 4 ] , n = 6
//         ^
//         | 4 has 5 elements (1,2,3,4,4) that are less or equal to it.
//
// Try pick 4 for an example again. Now it has 4 elements that are less or equal
// to it, means this value *may* has duplication. Why just "may has"? Since
// for the values larger than it, the counts of less or equal are bigger than the value.
// For example, 5 has 6 elements (1,2,3,4,4,5) that are less or equal to it. So seeing
// the count over the value is not enough to say this value is a duplication.
// For all counts of less or equal, it looks like:
//
// n  | 1 2 3 4 5 6
// cnt| 1 2 3 5 6 7
//
// We want the first value (smallest) that shows the over-the-value count. While can do
// scan each element and find its count, and find the smallest value, but count takes
// O(n) and scan each one takes O(n) so it will be O(n^2), which not better then simple
// double loop for scanning duplications.
// But think again the index space search. If we pick a value, and see its less-or-equal
// count is over its value, we do not need to search every element, but search the
// indices left to the current value, since we know the real anwser may be smaller.
// If the less-or-equal is same as the value, we search to right indices. This gives us the
// binary search in the index space.
// Since the max value is n, this is the index space range. Pick the mid index, do a
// less-or-equal count on it. If it over its value, save this index, and search left. If not,
// search right. Do binary search until all indices get processed. This is a O(logn) scanning,
// So the total time will be O(nlogn), while space is O(1).


int count_less_or_equal(const vector<int>& nums, int x)
{
    int cnt = 0;
    for (int a : nums) {
        if (a <= x) {
            cnt ++;
        }
    }

    return cnt;
}

class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        int left = 0;
        int right = nums.size() - 1;

        int dup = -1;

        while (left <= right) {
            int mid = left + ((right - left) / 2);

            int cnt = count_less_or_equal(nums, mid);
            if (cnt > mid) {
                dup = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return dup;
    }
};
