// Simple index as count hash algor, using c++ bitset.
//// The class template bitset represents a fixed-size sequence of N
//// bits. Bitsets can be manipulated by standard logic operators and
//// converted to and from strings and integers.
// The array like access for bit make it a space efficient implementation.
// Time: O(n), Space: O(1)

class Solution {
public:
    int findDuplicate(vector<int>& nums) {

        constexpr int max_n = 10 * 10 * 10 * 10 * 10 + 1;
        bitset<max_n> bs= {0};

        for (int a : nums) {
            if (bs[a] == 1) {
                return a;
            } else {
                bs[a] = 1;
            }
        }

        return 0;

    }
};
