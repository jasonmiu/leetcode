#
# @lc app=leetcode id=349 lang=python3
#
# [349] Intersection of Two Arrays
#
# https://leetcode.com/problems/intersection-of-two-arrays/description/
#
# algorithms
# Easy (57.51%)
# Total Accepted:    275.8K
# Total Submissions: 475.3K
# Testcase Example:  '[1,2,2,1]\n[2,2]'
#
# Given two arrays, write a function to compute their intersection.
# 
# Example 1:
# 
# 
# Input: nums1 = [1,2,2,1], nums2 = [2,2]
# Output: [2]
# 
# 
# 
# Example 2:
# 
# 
# Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
# Output: [9,4]
# 
# 
# Note:
# 
# 
# Each element in the result must be unique.
# The result can be in any order.
# 
# 
# 
# 
#
# The idea is simple, just put two input lists to two hash tables,
# and find their common keys. We can use set() like:
#        a = set(nums1)
#        b = set(nums2)
#        c = a & b
#        return list(c)
# but for exercise I wrote code for hash tables building too.
# We cannot just build 1 table, and check if the input list 2
# has the common elements of table, as the duplications in input list
# will cause the resulted list having duplications as well. We need to 
# remember which element has appered hence need two hash tables.
# The complexity is O(n), n is the total number of the input elements.
# The space complexity is O(n), for the resulted list.

class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        
        a = {}
        b = {}
        ans = []
        for n in nums1:
            a[n] = True
            
        for n in nums2:
            b[n] = True
            
        for k in a.keys():
            if k in b:
                ans.append(k)
                
        return ans
            

        
