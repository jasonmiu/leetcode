#
# @lc app=leetcode id=34 lang=python3
#
# [34] Find First and Last Position of Element in Sorted Array
#
# https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/
#
# algorithms
# Medium (34.46%)
# Total Accepted:    432.9K
# Total Submissions: 1.2M
# Testcase Example:  '[5,7,7,8,8,10]\n8'
#
# Given an array of integers nums sorted in ascending order, find the starting
# and ending position of a given target value.
# 
# Your algorithm's runtime complexity must be in the order of O(log n).
# 
# If the target is not found in the array, return [-1, -1].
# 
# Example 1:
# 
# 
# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]
# 
# Example 2:
# 
# 
# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]
# 
#
# The idea is to think if the target t exists in the sorted list,
# what is its left index and right index. The bisect package can do
# this. Also rewrite my own version to practic.
# For example
#  0  1  2  3
# [1, 2, 3]
#  L  M    H
# The mid-point to be complare is [1] = (L + H) // 2
# To get the LEFT of the target, assume the target is "2", 
# [1] = target = 2.
# We want to move down the H, so the next route the mid-point will
# be 0. 
# If the target is "1", [1] > target, so the answer will be on the
# left, so we want to move down the H too.
# Hence for [M] >= t, we move H = M, else move up L = M + 1
# 
# To get the RIGHT of the target, assume the target is "2",
# [1] == target = 2, since we want to return the L (low index) 
# as the right on the target, so we move up L = M + 1.
# If the target is "3", [M] < target, the answer is on the right, so we 
# move up the L = M + 1. Hence for [M] <= t, we move up L = M + 1,
# else move down H = M
#
# One implemenation point is the starting mid-point is len(input) // 2,
# instead of (len(input) - 1) // 2.
# Complexity is O(logn) since we cut the input half every time 
# and space is O(1).



def bisec_left(a, t):
    l = 0
    h = len(a)
    
    while l < h:
        m = (l + h) // 2
        if a[m] >= t:
            h = m
        else:
            l = m + 1
            
    return l

def bisec_right(a, t):
    l = 0
    h = len(a)
    
    while l < h:
        m = (l + h) // 2
        if a[m] <= t:
            l = m + 1
        else:
            h = m
            
    return l


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        s = bisec_left(nums, target)
        e = bisect_right(nums, target)
                
        if e == s:
            return [-1, -1]
        else:
            return [s, e-1]
                        

'''
import bisect

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        
        s = bisect.bisect_left(nums, target)
        e = bisect.bisect_right(nums, target)
        if e == s:
            return [-1, -1]
        
        return [s, e-1]
'''        
        
