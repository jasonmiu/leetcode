// There are few cases:
// 1. Target not found. Return {-1, -1}
// 2. Target found, and has 2 target elements in the input arrary, like:
//    [1, 2, 2, 3], T=2, Return {1, 2}, while the ending is start + 1
// 3. Target found, and has more than 2 target elements, like:
//    [1, 2, 2, 2, 3], T=2, Return {1, 3}, the ending is num of target element - 1 + start (general case of case 2)
// 4. Target found, only 1 target element, like:
//    [1, 2, 3], T=2, Return {1, 1}, the start and end are the same.
//
// Since it has case 3, we cannot just find the first target element, and count forward,
// as this can lead O(n) for counting all elements.
// As this is a sorted input, and asking for O(logn), so binary search should be used. However,
// binary search does not guarantee it will find the first occurance of the target element, for example:
//  0 1 2 3 4 5 6 7 8
// [0 1 1 2 2 2 2 3 3]
//          ^ hit target, since it is at the middle, but not the first occurance.
//
// By finding the first hit, we know it is always found at the middle, if the target is there.
// For the first occurance, can think it is the occurance at the left most. If the target can be
// found in the left half, then try the left half of the left half, until the target cannot be found.
// The last found target, is at the left most.
// Similar to the last occurance of the target, as it is the right most occurance. So repeatly search
// the right half of right half, until no more target can be found. The last found is at the right most.
// With left most and right most occurances, the starting and ending indices can be found.
//
// Since for every search, we search half, so the time is O(logn). Space is O(1).

int bs(const vector<int>& a, int s, int e, int t)
{
    if (s > e) {
        return -1;
    }

    int m = s + ((e - s) / 2);

    if (a[m] == t) {
        return m;
    } else if (t < a[m]) {
        return bs(a, s, m - 1, t);
    } else if (t > a[m]) {
        return bs(a, m + 1, e, t);
    }

    return -1;
}


class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {

        int first_hit = bs(nums, 0, nums.size() - 1, target);
        if (first_hit == -1) {
            return {-1, -1};
        }

        int left_found = first_hit;
        while (true) {
            int found = bs(nums, 0, left_found - 1, target);
            if (found != -1) {
                left_found = found;
            } else {
                break;
            }
        }

        int right_found  = first_hit;
        while (true) {
            int found = bs(nums, right_found + 1, nums.size() - 1, target);
            if (found != -1) {
                right_found = found;
            } else {
                break;
            }
        }

        return {left_found, right_found};
    }
};
