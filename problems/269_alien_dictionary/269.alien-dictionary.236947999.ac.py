# Use "Topological Sort" to solve this.
# Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering
# of vertices such that for every directed edge uv, vertex u comes before v
# in the ordering. Topological Sorting for a graph is not possible
# if the graph is not a DAG.

# For example, a topological sorting of the following graph is “5 4 2 3 1 0”.
# There can be more than one topological sorting for a graph. For example,
# another topological sorting of the following graph is “4 5 2 3 1 0”.
# The first vertex in topological sorting is always a vertex
# with in-degree as 0 (a vertex with no incoming edges).
# [5] ----> [0] <---- [4]
#  |                   |
#  |                   +-+
#  |                     |
#  |                     V
#  +-> [2] ----> [3] -->[1]

#
# Topological Sorting vs Depth First Traversal (DFS):
#
# In DFS, we print a vertex and then recursively call DFS
# for its adjacent vertices. In topological sorting, we need to print a vertex
# before its adjacent vertices. For example, in the given graph,
# the vertex ‘5’ should be printed before vertex ‘0’, but unlike DFS,
# the vertex ‘4’ should also be printed before vertex ‘0’.
# So Topological sorting is different from DFS. For example,
# a DFS of the shown graph is “5 2 3 1 0 4”, but it is not a topological sorting

# Algorithm to find Topological Sorting:
#
# We recommend to first see implementation of DFS here. We can modify
# DFS to find Topological Sorting of a graph. In DFS, we start from a vertex,
# we first print it and then recursively call DFS for its adjacent vertices.
# In topological sorting, we use a temporary stack. We don’t print the
# vertex immediately, we first recursively call topological sorting for
# all its adjacent vertices, then push it to a stack. Finally,
# print contents of stack. Note that a vertex is pushed to stack only when
# all of its adjacent vertices (and their adjacent vertices and so on)
# are already in stack.

# https://www.geeksforgeeks.org/topological-sorting/

class Solution:

    # Build the topological graph. Can think the 1 alphabet depends on another
    # alphabet with their 'topology'. For eg:
    # "abc"
    # "cd"
    # a -> b -> c
    # c -> d
    # This is the horizontal topology.
    # Also need compare two words as:
    # a
    # z
    # a -> z
    # This is the vertical topology.
    def build_graph(self, words):
        g = {}
        
        for i in range(0, len(words) - 1):
            w = words[i]
            w2 = words[i+1]
            
            # all char should be self-loop
            for c in w:
                if c not in g:
                    g[c] = list(c)

            # Boundary case for verical two words compare.
            # For example:
            # ab
            # cd <- here
            # e
            # We are at 'here' where is the len(words) -2.
            # w is "cd" and w2 is "e". The outter loop
            # will not access "e" as boundary is len(words) -1.
            # So we handle the last two words here.
            if i == len(words) - 2:
                for c in w2:
                    if c not in g:
                        g[c] = list(c)
            
            min_len = min(len(w), len(w2))
            for j in range(0, min_len):
                if not w[j] == w2[j]:
                    if w[j] in g:
                        g[w[j]].append(w2[j])
                    else:
                        g[w[j]] = list(w2[j])
                    
                    break
                        
        return g
    
    def print_graph(self, graph):
        for u in graph:
            for v in graph[u]:
                print("{} -> {}".format(u, v))
            
        return
                

    # Topo sort wirh DFS
    def dfs(self, graph, u, visited, s):
        
        if u in visited:
            return True
        
        visited[u] = 1 # visiting
        
        #print(u)
        
        if u in graph:
            for v in graph[u]:
                if (v in visited) and (visited[v] == 1):
                    if not u == v:  # special case of same char (loop to self)
                        return False
            
                if v not in visited:
                    if not self.dfs(graph, v, visited, s):
                        return False
        
        
        visited[u] = 2 # visited
        s.append(u)
        
        
        return True
        
        
    
    def alienOrder(self, words: List[str]) -> str:
        
        if len(words) == 1:
            return "".join(sorted(words[0]))
        
        graph = self.build_graph(words)
        
        self.print_graph(graph)
        
        visited = {}
        s = []
        for u in graph:
            if not self.dfs(graph, u, visited, s):
                return ""
            
        return "".join(s[::-1])

                    
        
