#
# @lc app=leetcode id=463 lang=python3
#
# [463] Island Perimeter
#
# https://leetcode.com/problems/island-perimeter/description/
#
# algorithms
# Easy (62.02%)
# Total Accepted:    161.8K
# Total Submissions: 258.9K
# Testcase Example:  '[[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]'
#
# You are given a map in form of a two-dimensional integer grid where 1
# represents land and 0 represents water.
# 
# Grid cells are connected horizontally/vertically (not diagonally). The grid
# is completely surrounded by water, and there is exactly one island (i.e., one
# or more connected land cells).
# 
# The island doesn't have "lakes" (water inside that isn't connected to the
# water around the island). One cell is a square with side length 1. The grid
# is rectangular, width and height don't exceed 100. Determine the perimeter of
# the island.
# 
# 
# 
# Example:
# 
# 
# Input:
# [[0,1,0,0],
# ⁠[1,1,1,0],
# ⁠[0,1,0,0],
# ⁠[1,1,0,0]]
# 
# Output: 16
# 
# Explanation: The perimeter is the 16 yellow stripes in the image below:
# 
# 
# 
# 
#
# The Idea is to scan all cells, and check the neighbors. If we found a neighbor with 1, decrease the current cell perimeter
# by 1 as it shared 1 side with neighbor.
# The complexity is O(n), and space complexity is O(1)
# An another submission use a cleverer 'look back' to check only cells on the left and above to save half of operations.
class Solution:
    def islandPerimeter(self, grid: List[List[int]]) -> int:
        
        ans = 0
        d = [(-1, 0),
             (1, 0),
             (0, -1),
             (0, 1)]
        
        for i in range(0, len(grid)):
            for j in range(0, len(grid[0])):
                dim = 4
                cell = grid[i][j]
                if cell == 0:
                    continue
                    
                for offset in d:
                    if i + offset[0] >=0 and i + offset[0] < len(grid) and j + offset[1] >= 0 and j + offset[1] < len(grid[0]):
                        n = grid[i+offset[0]][j+offset[1]]
                        if n == 1:
                            dim -= 1
                            
                ans += dim
                
        return ans
                            
                    
                    
                
                
