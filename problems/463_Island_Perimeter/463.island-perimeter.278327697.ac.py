#
# @lc app=leetcode id=463 lang=python3
#
# [463] Island Perimeter
#
# https://leetcode.com/problems/island-perimeter/description/
#
# algorithms
# Easy (62.01%)
# Total Accepted:    155.2K
# Total Submissions: 249.9K
# Testcase Example:  '[[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]'
#
# You are given a map in form of a two-dimensional integer grid where 1
# represents land and 0 represents water.
# 
# Grid cells are connected horizontally/vertically (not diagonally). The grid
# is completely surrounded by water, and there is exactly one island (i.e., one
# or more connected land cells).
# 
# The island doesn't have "lakes" (water inside that isn't connected to the
# water around the island). One cell is a square with side length 1. The grid
# is rectangular, width and height don't exceed 100. Determine the perimeter of
# the island.
# 
# 
# 
# Example:
# 
# 
# Input:
# [[0,1,0,0],
# ⁠[1,1,1,0],
# ⁠[0,1,0,0],
# ⁠[1,1,0,0]]
# 
# Output: 16
# 
# Explanation: The perimeter is the 16 yellow stripes in the image below:
# 
# 
# 
# 
#
# The idea is to check all 'land' grids. The simplest case is 1 land grid,
# the perimeter is 4. We scan the grids from left to right, top to down.
# For each land grid, we check if we have connected to some lands before. 
# If so, the shared boundary between two land grids will be cancelled,
# so the perimeter will be 4 + 4 - 2. Mark this land grid as visited.
# Here is the first version of checking all 4 directions. 

#class Solution:
#    def islandPerimeter(self, grid: List[List[int]]) -> int:
#        
#        w = len(grid[0])
#        h = len(grid)
#        
#        dirs = ((1, 0), (-1, 0), (0, 1), (0, -1))
#        perimeter = 0
#        for r in range(0, h):
#            for c in range(0, w):
#                g = grid[r][c]
#                if g == 1:
#                    p = 4
#                    
#                    check_dirs = [0, 0, 0, 0]
#                    
#                    if r < h - 1:
#                        check_dirs[0] = 1
#                    if r >= 1:
#                        check_dirs[1] = 1
#                    if c < w - 1:
#                        check_dirs[2] = 1
#                    if c >= 1:
#                        check_dirs[3] = 1
#                        
#                    for i, d in enumerate(dirs):
#                        if check_dirs[i]:                        
#                            n = grid[r + d[0]][c + d[1]]
#                            if n == 2:
#                                p -= 2
#                            
#                    perimeter += p
#                    grid[r][c] = 2
#                    
#        return perimeter

# Here is the optimized version.
# Since we search left to right and top to bottom, we only need to 'look back'
# to check the left and upper grids, since the right and bottom grids will be checked 
# by those grids next to them. This save half of operations.
# The complexity is O(n) for scanning all grids.
# The space complexity is O(1) since we do not introduce new storage.

class Solution:
    def islandPerimeter(self, grid: List[List[int]]) -> int:
        w = len(grid[0])
        h = len(grid)
        
        perimeter = 0
        for r in range(0, h):
            for c in range(0, w):
                g = grid[r][c]
                if g == 0:
                    continue
                p = 4
                if r >= 1 and grid[r-1][c] == 1:
                    p -= 2
                if c >= 1 and grid[r][c-1] == 1:
                    p -= 2
                    
                perimeter += p
                
        return perimeter
                    
            
