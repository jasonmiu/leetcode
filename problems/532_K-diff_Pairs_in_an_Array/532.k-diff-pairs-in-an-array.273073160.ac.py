#
# @lc app=leetcode id=532 lang=python3
#
# [532] K-diff Pairs in an Array
#
# https://leetcode.com/problems/k-diff-pairs-in-an-array/description/
#
# algorithms
# Easy (30.50%)
# Total Accepted:    76.9K
# Total Submissions: 252K
# Testcase Example:  '[3,1,4,1,5]\n2'
#
# 
# Given an array of integers and an integer k, you need to find the number of
# unique k-diff pairs in the array. Here a k-diff pair is defined as an integer
# pair (i, j), where i and j are both numbers in the array and their absolute
# difference is k.
# 
# 
# 
# Example 1:
# 
# Input: [3, 1, 4, 1, 5], k = 2
# Output: 2
# Explanation: There are two 2-diff pairs in the array, (1, 3) and (3,
# 5).Although we have two 1s in the input, we should only return the number of
# unique pairs.
# 
# 
# 
# Example 2:
# 
# Input:[1, 2, 3, 4, 5], k = 1
# Output: 4
# Explanation: There are four 1-diff pairs in the array, (1, 2), (2, 3), (3, 4)
# and (4, 5).
# 
# 
# 
# Example 3:
# 
# Input: [1, 3, 1, 5, 4], k = 0
# Output: 1
# Explanation: There is one 0-diff pair in the array, (1, 1).
# 
# 
# 
# Note:
# 
# The pairs (i, j) and (j, i) count as the same pair.
# The length of the array won't exceed 10,000.
# All the integers in the given input belong to the range: [-1e7, 1e7].
# 
# 
#
# The basic algor is to check every number i in nums, 
# if i + k exist in the nums. If exist, means the 
# i and i + k is a pair with k-diff. 
# We can make a set from nums, and check if the i+k exist in 
# the set.
# However, since each element in set appear once, there is a special case
# [1, 1, 2], k = 0
# If k = 0, the same numbers count as 1 pair, ie. the first 1 and 2nd 1 is a 
# kdiff pair. We make a special case when k == 0, not to use set() but
# a dict(), to check if we have same number mulitple times. If we see a number
# appear more than once, that must be 1 pair of this number. But even we
# see the same number more than twice its still 1 pair as we only count unique pair
# Another special case can be k < 0. for example:
# [3, 4], k = -1
# While I think the diff of 3, 4 can be -1, the 'absolute different' in the question
# seems asking for +ve k only. So we return 0 in this case.
# the complexity is O(n) as we scan numbers once.
# the space complexity is O(n) as we use a set() or dict() to store all numbers

class Solution:
    def findPairs(self, nums: List[int], k: int) -> int:
        
        if len(nums) == 0:
            return 0
        
        # special case: k is -ve
        if k < 0:
            return 0
        
        cnt = 0
        
        # special case: k == 0, use dict()
        if k == 0:
            same_pairs = {}
            for i in nums:
                if not i in same_pairs:
                    same_pairs[i] = 1
                else:
                    if same_pairs[i] == 1:
                        cnt = cnt + 1
                        
                    same_pairs[i] += 1
            return cnt
        
        # gerneral case, use set()
        nums_set = set(nums)       
        for i in nums_set:
            if (i + k) in nums_set:
                cnt = cnt + 1
                
        return cnt
