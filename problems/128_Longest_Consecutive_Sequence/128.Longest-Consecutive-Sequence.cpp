// The consecutive sequence means, find a sequence that
// starting from i, all the values are i, i+1, i+2, i+3....
// in the input array.
// For example, [0,3,7,2,5,8,4,6,0,1], the first idea will be
// sort it, so the whole array will form consective sequences inside,
// like:
// [0,3,7,2,5,8,4,6,0,1] -> sort
// [0, 0, 1, 2, 3, 4, 5, 6, 7, 8]
// Then search from starting point, which is the min element, test
// if the next one is consecutive, if yes, count len and move forward.
// If not, reset the count and start from that element again.
// This will be a O(nlogn) algor.
// But since the question asked for a O(n), feel like can use some
// count hash.
// From the example above, the repeated "0"s are useless, since if
// it is a part of consecutive seq, we only count the len once.
// If we put all elements in a set, and start from the min of the
// set, then check if next consecutive element exist, if so, remove
// that element and goes on.
// However, if we removed a element, can it be a element that can be
// a future potential longest consecutive seq, and we lost the chance
// to count it for the next seq checking?
// For a element that is a part of the current sequence checking, it will
// not be a part of the future longest consecutive sequence. Because
// a element can only be equal or larger than the min of the current consecutive
// sequence. And we pick a min to be a starting point of every consecutive
// seq. If an element is a min of future sequence and also a part of the
// current sequence, the current sequence must be longer, as the current
// sequence is longer as least including the current min. So we can
// check the element only once by removing it from the set.
// If we found the next consecutive element, then the len count increase and go on.
// If not found, then reset the len and start counting a new consecutive sequence.
// Since we check only once for the sequence building, and once for finding the min,
// time is O(n), space is O(n) for the set.


class Solution {
public:
    int longestConsecutive(vector<int>& nums) {

        unordered_set<int> s(nums.begin(), nums.end());

        int max_len = 0;

        while (!s.empty()) {
            int min_ele = *(min_element(s.begin(), s.end()));
            int len = 0;
            int ele = min_ele;

            while (true) {
                s.erase(ele);
                len ++;
                if (len >= max_len) {
                    max_len = len;
                }

                if (s.find(ele+1) == s.end()) {
                    break;
                } else {
                    ele = ele + 1;
                }
            }
        }

        return max_len;
    }
};
