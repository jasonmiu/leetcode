#
# @lc app=leetcode id=1105 lang=python3
#
# [1105] Filling Bookcase Shelves
#
# https://leetcode.com/problems/filling-bookcase-shelves/description/
#
# algorithms
# Medium (57.33%)
# Total Accepted:    10.2K
# Total Submissions: 17.8K
# Testcase Example:  '[[1,1],[2,3],[2,3],[1,1],[1,1],[1,1],[1,2]]\n4'
#
# We have a sequence of books: the i-th book has thickness books[i][0] and
# height books[i][1].
# 
# We want to place these books in order onto bookcase shelves that have total
# width shelf_width.
# 
# We choose some of the books to place on this shelf (such that the sum of
# their thickness is <= shelf_width), then build another level of shelf of the
# bookcase so that the total height of the bookcase has increased by the
# maximum height of the books we just put down.  We repeat this process until
# there are no more books to place.
# 
# Note again that at each step of the above process, the order of the books we
# place is the same order as the given sequence of books.  For example, if we
# have an ordered list of 5 books, we might place the first and second book
# onto the first shelf, the third book on the second shelf, and the fourth and
# fifth book on the last shelf.
# 
# Return the minimum possible height that the total bookshelf can be after
# placing shelves in this manner.
# 
# 
# Example 1:
# 
# 
# Input: books = [[1,1],[2,3],[2,3],[1,1],[1,1],[1,1],[1,2]], shelf_width = 4
# Output: 6
# Explanation:
# The sum of the heights of the 3 shelves are 1 + 3 + 2 = 6.
# Notice that book number 2 does not have to be on the first shelf.
# 
# 
# 
# Constraints:
# 
# 
# 1 <= books.length <= 1000
# 1 <= books[i][0] <= shelf_width <= 1000
# 1 <= books[i][1] <= 1000
# 
# 
#
# The idea is a book can be handled in 2 cases:
# 1. put on the current shelf if have space
# 2. put on the new shelf
# We can use a DFS search for this. For each recursion, 
# we return the min of the current total height. 
# This total height is bubbled up from the last book,
# which return the last level of shelf height.
# There are some difficulties for me in coding this DFS:
# 1. Should we deduce the avaiale space for the first book and 
#    give the first book height as the cur_shelf_h?
#   - It turns out I can implmenet for the first book, given the 
#     0 cur_shelf_h, putting on the current shelf and putting on the 
#     new (empty) shelf are the same.
# 2. How to keep track the tallest book which is the current shelf height?
#   - We should keep track ONLY the cur_shelf_h, and if we put it on the same
#     shelf, we check if the current book is taller than the current shelf height,
#     if so we update the current shelf height. If we put it on a new shelf,
#     then the next current shelf height will be the book height
# 3. What should the terminal case return? 0 or the total height or current shelf height?
#   - While they are all possible depends on the implemenation, but think for each
#     call of recursion put_shelf() is working on *CURRENT SHELF LEVEL* is easier.
#     Hence of the last book, we return the current shelf height, and bubble up and 
#     sum along the returning path to the top to reduce back to the *TOTAL HEIGHT* of the
#     current shelf config
# Another hard thing is what to memoize. Which is asking, what is the repeating 
# shelf config?
# 
#                              1 |
#                  1 2 |                         1 | 2   
#             1 2 | 3                   1 |2 3          1 | 2 | 3
#     1 2 | 3 4    1 2 | 3 |4*      1 | 2 3 |4*      1|2|3 4     1|2|3|4
#
# each '|' is a shelf. For example the:
# 1 2 | 3 | 4 , and
# 1 | 2 3 | 4
# Book 4 is at the first position of the 3rd shelf. At this config, we want to rem the 
# min height of the shelfs. So we will memoize the (book_idx, shelf_remain_space).
# To do this, we need to make sure each call of the put_shelf() means working on 
# the signle shelf.
# Since the DP memo has n * width case, and we will hit the all memo cells at least once,
# so the complexity is O(n*m), and space is O(n*m) too.
# Time: --



class Solution:
    def minHeightShelves(self, books: List[List[int]], shelf_width: int) -> int:
        
        memo = {}
        
        def put_shelf(i, space, cur_shelf_h):
            
            if i >= len(books):
                return cur_shelf_h
            
            if (i, space) in memo:
                return memo[(i, space)]
            
            b = books[i]
            
            if b[0] <= space:
                inc = put_shelf(i+1, space-b[0], max(b[1], cur_shelf_h))
                exc = put_shelf(i+1, shelf_width-b[0], b[1]) + cur_shelf_h
                h = min(inc, exc)
                
            else:
                h = put_shelf(i+1, shelf_width-b[0], b[1]) + cur_shelf_h
                
            memo[(i, space)] = h
            
            return h
                
        
        h = put_shelf(0, shelf_width, 0)
        
        return h
            
