#
# @lc app=leetcode id=242 lang=python3
#
# [242] Valid Anagram
#
# https://leetcode.com/problems/valid-anagram/description/
#
# algorithms
# Easy (54.16%)
# Total Accepted:    440.1K
# Total Submissions: 808.2K
# Testcase Example:  '"anagram"\n"nagaram"'
#
# Given two strings s and t , write a function to determine if t is an anagram
# of s.
# 
# Example 1:
# 
# 
# Input: s = "anagram", t = "nagaram"
# Output: true
# 
# 
# Example 2:
# 
# 
# Input: s = "rat", t = "car"
# Output: false
# 
# 
# Note:
# You may assume the string contains only lowercase alphabets.
# 
# Follow up:
# What if the inputs contain unicode characters? How would you adapt your
# solution to such case?
# 
#
# Anagram means the char in string s can be rearranged to from
# string t. So both strings have the same chars.
# The idea is to build a multi-set for both s and t. If they have the
# same set of chars, the different between two sets is zero.
# If the different set has size > zero, means s and t do not have
# the same set of char, ie, not anagram.
# Compliexity O(n) for building multi-set.
# Space compliexity O(n) for multi-set.

import collections

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        
        if len(s) != len(t):
            return False
        
        sd = collections.Counter(s)
        td = collections.Counter(t)
        
        sub = sd - td
        if len(sub) == 0:
            return True
        else:
            return False
