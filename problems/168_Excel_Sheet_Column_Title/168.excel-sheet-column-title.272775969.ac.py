#
# @lc app=leetcode id=168 lang=python3
#
# [168] Excel Sheet Column Title
#
# https://leetcode.com/problems/excel-sheet-column-title/description/
#
# algorithms
# Easy (29.76%)
# Total Accepted:    189.1K
# Total Submissions: 635.2K
# Testcase Example:  '1'
#
# Given a positive integer, return its corresponding column title as appear in
# an Excel sheet.
# 
# For example:
# 
# 
# ⁠   1 -> A
# ⁠   2 -> B
# ⁠   3 -> C
# ⁠   ...
# ⁠   26 -> Z
# ⁠   27 -> AA
# ⁠   28 -> AB 
# ⁠   ...
# 
# 
# Example 1:
# 
# 
# Input: 1
# Output: "A"
# 
# 
# Example 2:
# 
# 
# Input: 28
# Output: "AB"
# 
# 
# Example 3:
# 
# 
# Input: 701
# Output: "ZY"
# 
#
# Build the A-Z array and use its indices as the map to chars
# Pick each digit of the input n, but treat the n like a base26 
# number so we mod 26 then divid it by 26 to extract the ones digit
# every time. The tricky case is the "Z" = 26. Since
# "Z" % 26 = 0
# "Z" // 26 = 1
# its has a "carry" and the outter while loop will get entered again.
# So if we see the current number can be divied by 26 (ie. x % 26 == 0), 
# then we minus the carry for the next digit.
# The complexity is O(n) while n is the number of digits.
# The space complexity is O(n) too since need 1 slot for each char.

class Solution:
    def convertToTitle(self, n: int) -> str:
        
        s = []
        letter_map = []
        for i in range(0, 26):
            c = ord('A') + i
            letter_map.append(chr(c))
            
        x = n
        while x > 0:
            y = x % 26
            x = x // 26
            if y == 0:
                x = x - 1
                
            s.append(letter_map[y-1])
            
        
        return("".join(reversed(s)))
