// The idea is to make use of the recursion that will return from
// the end of the call chain.
// If we recursively trace the linked list to the end, and return
// from the end which the node will be a nullptr, we know the position
// from the end of the list. Example:
//  1 -> 2 -> 3 -> 4 -> null
//  4 <- 3 <- 2 <- 1 <---+   (return from recursion)
//
// If the position from end is equal to the target, then this is the node
// we like to delete.
//
// To delete a node in a linked list, the previous node's next should be
// changed to point to the current next node, ie, skip the current node, like:
//
// [ A ] -> [ B ] -> [ C ]
//   ^        ^
//   |        |
//  Prev     Current
//
//
// [ A ]-+    [ B ] -> [ C ]
//   ^   +               ^
//   |   +-----next------|
//  Prev
//
//
// A special case is, we want to delete the head, like:
//
//
// Head -> [ A ] -> [ B ]
//           ^
//           |
//           current
// prev -> null
//
//
// Here the prev is pointing to null, since head has no prev. Then we cannot
// change the prev (null)'s next to the current next.
// Since this only happens when we delete the head, we can mark this case
// specially let the main caller know the head pointer is going to change.
// We can mark the head next pointer point to itself, ie. a self loopback node.
// Then the main caller check the returned list and find the head is a single
// loopback node, it knows this head should be deleted and the new head pointer
// is actually the original 2nd node. For this case, we save the 2nd node first
// before calling the recursion function, otherwise after self-looping the head node
// we would lost the access to the 2nd node.
//
// Time: O(n) since scan the list once. Space: O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

int del_from_end(ListNode* current, ListNode* prev, int target)
{
    int from_end_pos = 0;

    if (current == nullptr) {
        return 1;
    } else {
        from_end_pos = del_from_end(current->next, current, target);
    }

    if (from_end_pos == target) {
        if (prev) {
            prev->next = current->next;
        } else {
            current->next = current; // tell the caller remove head
        }
    }

    return from_end_pos + 1;
}

class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {

        ListNode* head_remove = head->next;

        del_from_end(head, nullptr, n);
        if (head->next == head) {
            return head_remove;
        } else {
            return head;
        }
    }
};
