// To find a path from source to destination, we can use DFS to follow the path.
// For a node with multiple adjacent nodes, run multiple dfs with the next adjacent
// node to try if that path can reach the destination.
// Save the current node along the path. If finally hit a destination, save the path
// to the result and return to end the search for this path. If no more adjacent node can move on,
// just return as it is a deadend.
// Each time go down with DFS, we copy the current path. So for each trial of a node, it has its
// own copy of path. So we do not need to manually backtrack (add the current node, DFS, then pop it back out)
// the path. It is slower but code more clean.
// Time: O(2^n), since worse case is, the node 0 points to all n - 1 nodes, and the node 1 points to all next
// n - 2 nodes, and node 2 points to all next n - 3 node, for node i it points to all next n - (i + 1) nodes.
// For last node n - 1, it has only 1 path. For last last node n - 2, it has only 1 path too.
// For last last node n - 3, it has 2 paths (n-3 -> n-2 -> n-1, n-3 -> n-1). So for each node,
// you can pick next node i + 1, or destination node n - 1. This generate the super set of nodes, which is O(2^n).
// Space is O(2^n) for storing all paths.
//
// One easy mistake is the check only the term case when no more adjacent node, and check if it is a destination
// node at that moment. But a destination can appear before a leave node, so they should be both term cases and be checked.

void dfs(const vector<vector<int>>& g, int r, vector<int> path, vector<vector<int>>& result)
{
    path.push_back(r);

    if (r == (g.size() - 1)) {
        result.push_back(path);
        return;
    }

    vector<int> next_nodes = g[r];
    if (next_nodes.size() == 0) {
        return;
    }

    for (int n : next_nodes) {
        dfs(g, n, path, result);
    }

    return;
}

class Solution {
public:
    vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
        vector<vector<int>> result;

        dfs(graph, 0, vector<int>(), result);

        return result;
    }
};
