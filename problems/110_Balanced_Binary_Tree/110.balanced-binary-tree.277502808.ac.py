#
# @lc app=leetcode id=110 lang=python3
#
# [110] Balanced Binary Tree
#
# https://leetcode.com/problems/balanced-binary-tree/description/
#
# algorithms
# Easy (41.96%)
# Likes:    1547
# Dislikes: 139
# Total Accepted:    370.5K
# Total Submissions: 881.5K
# Testcase Example:  '[3,9,20,null,null,15,7]'
#
# Given a binary tree, determine if it is height-balanced.
# 
# For this problem, a height-balanced binary tree is defined as:
# 
# 
# a binary tree in which the left and right subtrees of every node differ in
# height by no more than 1.
# 
# 
# 
# 
# Example 1:
# 
# Given the following tree [3,9,20,null,null,15,7]:
# 
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
# Return true.
# 
# Example 2:
# 
# Given the following tree [1,2,2,3,3,null,null,4,4]:
# 
# 
# ⁠      1
# ⁠     / \
# ⁠    2   2
# ⁠   / \
# ⁠  3   3
# ⁠ / \
# ⁠4   4
# 
# 
# Return false.
# 
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# The idea is find the height of left and right subtrees,
# and compare their heights, recursively.
# This idea is the same as previous implementation, but return 
# -1 for unbalace subtree and propagate up, instead of 
# returning an extra "balance" flag for more elegant code.
# We visited each node once so O(n) and O(logn) for space.

class Solution:
    
    def tree_height(self, root):
        if root.left == None and root.right == None:
            return 1
        
        lh = rh = 0
        if root.left:
            lh = self.tree_height(root.left)
        
        if root.right:
            rh = self.tree_height(root.right)
            
        if lh == -1 or rh == -1 or abs(lh - rh) > 1:
            h = -1
        else:
            h = (max(lh, rh)) + 1
        
        return h
    
    def isBalanced(self, root: TreeNode) -> bool:
        if root == None:
            return True
        
        h = self.tree_height(root)
        if h == -1:
            return False
        else:
            return True
        
                
# @lc code=end
