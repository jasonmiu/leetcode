#
# @lc app=leetcode id=110 lang=python3
#
# [110] Balanced Binary Tree
#
# https://leetcode.com/problems/balanced-binary-tree/description/
#
# algorithms
# Easy (41.96%)
# Likes:    1547
# Dislikes: 139
# Total Accepted:    370.5K
# Total Submissions: 881.5K
# Testcase Example:  '[3,9,20,null,null,15,7]'
#
# Given a binary tree, determine if it is height-balanced.
# 
# For this problem, a height-balanced binary tree is defined as:
# 
# 
# a binary tree in which the left and right subtrees of every node differ in
# height by no more than 1.
# 
# 
# 
# 
# Example 1:
# 
# Given the following tree [3,9,20,null,null,15,7]:
# 
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
# Return true.
# 
# Example 2:
# 
# Given the following tree [1,2,2,3,3,null,null,4,4]:
# 
# 
# ⁠      1
# ⁠     / \
# ⁠    2   2
# ⁠   / \
# ⁠  3   3
# ⁠ / \
# ⁠4   4
# 
# 
# Return false.
# 
#

# @lc code=start
# The idea is find the height of left and right subtrees,
# and compare their heights, recursively.
# If this is the leave node, the height is 1 (self), and both subtrees are balance
# We return the height back to the upper nodes, if the different between 2 subtrees 
# is > 1, this node is not balance, need propagate back to the root.
# We test the balance of right and left subtrees too as each node must be balanced.
# Complexity is O(n), since we visit every node once.
# Space complexity is O(logn), as we visit the height once each time


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def tree_height(root):
    if root.left == None and root.right == None:
        return (True, 1)
    
    l_balance = True
    r_balance = True
    balance = True
    lh = 0
    rh = 0
    if root.left:
        (l_balance, lh) = tree_height(root.left)
    else:
        l_balance = True
        lh = 0
    
    if root.right:
        (r_balance, rh) = tree_height(root.right)
    else:
        r_balance = True
        rh = 0
        
    h = max(lh, rh) + 1
    if abs(lh - rh) > 1:
        balance = False
    else:
        balance = l_balance and r_balance
        
    return (balance, h)
        
    
class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        
        if root == None:
            return True
        
        (balance, h) = tree_height(root)
        return balance
        
# @lc code=end
