// If doing in two passes, first pass can pin-point the starting and
// ending nodes of the target sublist, ie, the pointer at left index
// and right index. Then reverse the list between them.
// To do in one pass, we need few info. For example:
//
//      L         R
// 1 -> 2 -> 3 -> 4 -> 5 becomes
//
// 1 -> 4 -> 3 -> 2 -> 5
//
// So the node pointed by L will has its next point to the original next of R,
// and the original previous pointer of L will has its next point to R.
//
//      sub_head
//      |         sub_tail
//      V         V
// 1 -> 2 -> 3 -> 4 -> 5
// ^                   ^
// |                   end_node
// start_node
//
// The node at left index, call it sub_head, the node at right index, call it sub_tail.
// The node previous of sub_head is start_node, and the after sub_tail is end_node.
// We want the final product, has the start_node's next pointing to sub_tail, and
// sub_head's next pointing to end_node. The sub list from sub_head to sub_tail
// will be reversed.
//
// We can recursively move forward to count the index, and pass the previous and current node to the function.
// If the current counter is at the left index, the current node is the sub_head, while the previous node
// is the start_node. Save them as static vars. The move forward until we see the right index. Save the
// current node as sub_tail, and its next node as end_node.
//
// When we find the right index, it is time to return with standard recursion list reversion. But we
// want it reverse until we return back to sub_head. The return value can help to keep track the returning.
// The number of step we want to return is current - left, for example, if current is 4, left is 2, we like
// it reverse two times.
//      sub_head
//      |         sub_tail
//      V         V
// 1 -> 2 <- 3 <- 4 -> 5
// ^         ^    ^     ^
// |         |    |    end_node
// start_node|    |
//           |    + return 2
//           + return 1
// Once the return is 0, means the recursion returned back to the sub_head, so the list reversion can be stopped.
// While the sublist is reversed, the start_node and end_node didn't get change. So have to fix them,
// to have the start_node next points to sub_tail, end_node next points to sub_head. The start_node has a special case,
// if the left index starts from 1, the start_node which is the previous node of the sub_head will be null,
// if this is the case the new head has to change to the sub_head.
//
// Time: O(n), space: O(1);


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:

    ListNode* start_node;
    ListNode* end_node;
    ListNode* sub_head;
    ListNode* sub_tail;

    int rev(int cnt, int left, int right, ListNode* prev, ListNode* cur)
    {
        if (cnt == left) {
            start_node = prev;
            sub_head = cur;
        }

        if (cnt == right) {
            end_node = cur->next;
            sub_tail = cur;

            cur->next = prev;
            return cnt - left;

        } else {

            int c = rev(cnt+1, left, right, cur, cur->next);
            if (c > 0) {
                cur->next = prev;
                return cnt - left;
            } else {
                return 0;
            }
        }
    }

    ListNode* reverseBetween(ListNode* head, int left, int right) {

        rev(1, left, right, nullptr, head);

        if (start_node != nullptr) {
            start_node->next = sub_tail;
        } else {
            head = sub_tail;
        }

        sub_head->next = end_node;

        return head;
    }
};
