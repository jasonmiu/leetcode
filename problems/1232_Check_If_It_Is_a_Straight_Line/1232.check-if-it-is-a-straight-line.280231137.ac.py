#
# @lc app=leetcode id=1232 lang=python3
#
# [1232] Check If It Is a Straight Line
#
# https://leetcode.com/problems/check-if-it-is-a-straight-line/description/
#
# algorithms
# Easy (47.27%)
# Total Accepted:    10.6K
# Total Submissions: 22.5K
# Testcase Example:  '[[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]'
#
# You are given an array coordinates, coordinates[i] = [x, y], where [x, y]
# represents the coordinate of a point. Check if these points make a straight
# line in the XY plane.
# 
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
# Output: true
# 
# 
# Example 2:
# 
# 
# 
# 
# Input: coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
# Output: false
# 
# 
# 
# Constraints:
# 
# 
# 2 <= coordinates.length <= 1000
# coordinates[i].length == 2
# -10^4 <= coordinates[i][0], coordinates[i][1] <= 10^4
# coordinates contains no duplicate point.
# 
# 
#
# The idea is to compute slopes of all pairs of coordinates.
# If it is a string line, all slopes are the same.
# The tricky part is if a line segment is a vertical line, ie.
# x1 = x2, then the slope is undefined. So we handle this with math.inf.
# The complexity is O(n).
# The space complexity is O(1)

import math

class Solution:
    def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
        
        n = len(coordinates)
        if n == 2:
            return True
        
        last_slope = None
        for i in range(1, n):
            p = coordinates[i-1]
            q = coordinates[i]
            try:
                s = abs((q[1] - p[1]) / (q[0] - p[0]))
            except ZeroDivisionError:
                s = math.inf
                
            if not last_slope:
                last_slope = s
            else:
                if s != last_slope:
                    return False
                
        return True
            
