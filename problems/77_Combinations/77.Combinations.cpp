// A DFS with backtrack combination generation.
// For the input [1 2 3 4], if k = 1,
// Then the output is,
// [[1], [2], [3], [4]]
// If k = 2, means for each element, want to select one more element which has index larger than the current
// one. For example, if the current index is 0 (1), then we want to have [1,2], [1,3], [1,4].
// So can think this is a DFS:
//                            1             2            3            4
//                       /  / |           / |            |
//                      2  3  4          3  4            4
//
// The path is the current generating combination.
// The if the path len is equal to k, means we got enough elements for this combination, save that.
// Otherwise, go down and search recursively.
// For every selection, we pick the current element to the current combination (var. cur). And move
// forward the start index since the next available choices are the indices after the current.
// If current start index is already over the last element, means the current path is not long enough
// to generate a combination, so stop search here.
// Time O(n!/(k! * (n-k)!)), Space O(n!/(k! * (n-k)!))

void gen_comb(const vector<int>& a, int start, int k, vector<int>& cur, vector<vector<int>>& result)
{
    if (cur.size() == k) {
        result.push_back(cur);
        return;
    }

    for (int i = start; i < a.size(); i++) {
        cur.push_back(a[i]);
        gen_comb(a, i+1, k, cur, result);
        cur.pop_back();
    }
    return;
}

class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
        vector<int> a;
        for (int i = 1; i <=n; i++) {
            a.push_back(i);
        }

        vector<vector<int>> result;
        vector<int> cur;
        gen_comb(a, 0, k, cur, result);

        return result;

    }
};
