// Bottom up generation algor.
// For every recursive case, return the current k combination, while the base case is k = 1.
// For example, input:
// [1 2 3 4], k = 1, return:
// [1] [2] [3] [4]
// If k = 2, expected result:
// [1,2], --+
// [1,3],   +--> current element is 1, it is the (input array - (head to current element)), with k - 1, then add back current element
// [1,4], --+
//
// [2,3], --+--> current element is 2, comb(input array - [1,2], k-1) => [3], [4], so add back the current element 2
// [2,4], --+
//
// [3,4]  --> current element is 3, comb(input array - [1,2,3], k-1) => [4], add back current element 3.
//
// So we can see, the current combination, is the combination of the subarray, and k - 1. Where the subarray is
// the input array, remove the elements from head to current index.




vector<vector<int>> gen_comb(const vector<int>& a, int k)
{
    vector<vector<int>> comb;

    for (int i = 0; i < a.size(); i++) {
        if (k == 1) {
            vector<int> v{a[i]};
            comb.push_back(v);
        } else {
            vector<int> sub_a(a.begin()+i+1, a.end());
            vector<vector<int>> sub = gen_comb(sub_a, k-1);
            for (auto & v : sub) {
                v.push_back(a[i]);
                comb.push_back(v);
            }
        }
    }

    return comb;
}

class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
        vector<int> a;
        a.resize(n);
        for (int i = 1; i <= n; i++) {
            a[i-1] = i;
        }
        return gen_comb(a, k);
    }
};
