# TO EXPLAIN
#
# @lc app=leetcode id=2 lang=python3
#
# [2] Add Two Numbers
#
# https://leetcode.com/problems/add-two-numbers/description/
#
# algorithms
# Medium (32.08%)
# Total Accepted:    1.1M
# Total Submissions: 3.4M
# Testcase Example:  '[2,4,3]\n[5,6,4]'
#
# You are given two non-empty linked lists representing two non-negative
# integers. The digits are stored in reverse order and each of their nodes
# contain a single digit. Add the two numbers and return it as a linked list.
# 
# You may assume the two numbers do not contain any leading zero, except the
# number 0 itself.
# 
# Example:
# 
# 
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
# Explanation: 342 + 465 = 807.
# 
# 
#
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None
#
#
# The idea is to convert both lists to equal size, so we can travel both
# at the same pace for each node. When we add each node, we create a
# new node for the result digit, and link with last result node. If the sum
# is => 10, we need to propagate the carry.
# The adding stops with we finishs travelling the linked list. If we still
# have a carry, we need to create 1 more result nodes as "1".
# Complexity is O(n), and space is O(n) for new result node list.

class Solution:
    def list_node_len(self, head):
        node = head
        node_len = 0
        if node == None:
            return node_len
        
        while(True):
            node_len = node_len + 1
            if node.next == None:
                break
            node = node.next
            
        return node_len
            
            
        
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        l1_len = self.list_node_len(l1)
        l2_len = self.list_node_len(l2)
        
        
        #print("l1_len {}, l2_len {}".format(l1_len, l2_len))
        
        if l1_len == 0 or l2_len == 0:
            return None
        
        if not l1_len == l2_len:
            #print("Node Len diff") # handle this special case later
            
            len_diff = 0
            shorter_list = None
            if l1_len < l2_len:
                shorter_list = l1
                len_diff = l2_len - l1_len
            else:
                shorter_list = l2
                len_diff = l1_len - l2_len
                
            n = shorter_list
            shorter_list_end = None
            while(True):
                if n == None:
                    break
                    
                shorter_list_end = n
                n = n.next
                
            for x in range(0, len_diff):
                shorter_list_end.next = ListNode(0)
                shorter_list_end = shorter_list_end.next
                    
                    

            
        result_list_head = None
        result_list_node = None
        result_list_last_node = None
        n1 = l1
        n2 = l2
            
        carry = 0
        while(True):

            if n1 == None:
                if carry > 0:
                    result_list_node = ListNode(1)
                    if not (result_list_last_node == None):
                        result_list_last_node.next = result_list_node
                
                break
                
            v = n1.val + n2.val + carry
            if v >= 10:
                v = v - 10
                carry = 1
            else:
                carry = 0
                    
            result_list_node = ListNode(v)
            if result_list_head == None:
                result_list_head = result_list_node
        
            if not (result_list_last_node == None):
                result_list_last_node.next = result_list_node
                    
            result_list_last_node = result_list_node
                
            n1 = n1.next
            n2 = n2.next
                
            #print(v)
            
        
        return result_list_head
