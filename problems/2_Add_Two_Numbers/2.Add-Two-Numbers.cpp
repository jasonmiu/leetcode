// The idea is start from the head of two lists, move forward together,
// add the current node values, if the sum has carry, propagate to the next node.
// The idea itself is not hard to think, issue is how to handle the different length
// of the two input list, also how to make the output node,
// and how to handle the last extra carry as a node.
// It is easier to think if we can traverse the lists recursively. First make sure
// the first input list is the longer one, so the empty checking will be easier.
// Start with the first node of both lists, for example:
// [1], [2]
// This is the easiest case. Add the values together, and make a new node.
// Then recursively set the next input as the current->next.
// For the output node, if we keep moving the output node pointer, we will lost the
// head pointer when we are done. So make a dummy head, and pass it to the function,
// so we can always append the output list.
// Since the first list is always longer, if the second list is now pointing to a
// empty node, means do not need to add it anymore, like:
//
//      V--- first list current node
// 9 -> 1 -> 5
// 2 -> null
//      ^--- second list current node
//
// Just need to add the current node of first list and the carry.
// But the traverse does not stop here, the traverse stop when both lists are pointing null,
// which means all digits of both list are processed. For those "extra" nodes of longer list,
// those values will eventually transfer to the output nodes.
// One special case is when the lists are ended, if there is still a carry,
// 1 more output node has to be added to the output list. But there is no need to traverse more.
//
// Time: O(n), Space: O(n).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

int list_len(ListNode* n)
{
    int cnt = 0;

    while (n != nullptr) {
        cnt ++;
        n = n->next;
    }

    return cnt;
}

void add(ListNode* a, ListNode* b, int carry, ListNode* output)
{
    int val = 0;
    int c = 0;

    if (a == nullptr && b == nullptr) {
        if (carry) {
            ListNode* cn = new ListNode(1);
            output->next = cn;
        }

        return;
    }

    if (a && b) {
        val = a->val + b->val + carry;
    } else if (a && b == nullptr) {
        val = a->val + carry;
    }

    if (val >= 10) {
        val = val - 10;
        c = 1;
    }

    ListNode* vn = new ListNode(val);
    output->next = vn;

    if (a && b) {
        add(a->next, b->next, c, vn);
    } else if (a && b == nullptr) {
        add(a->next, b, c, vn);
    }

    return;
}

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {

        int l1_len = list_len(l1);
        int l2_len = list_len(l2);

        ListNode* a = l1;
        ListNode* b = l2;

        if (l2_len > l1_len) {
            a = l2;
            b = l1;
        }

        ListNode head(0);
        add(a, b, 0, &head);

        return head.next;
    }
};
