// Since the question is asking *all* the combinations, we need a way to generate the combinations of the k numbers.
// And the possible values are only 1 - 9, also they can only appear once in the combinations. The first idea
// will be looping from 1 - 9, and have nested those loops for k times. This lead to a recusive generation, like a tree:
//
//                   1 2 3 4 ..... 9
//                   | | | ...
//                   | | 1 2 3 .... 9
//                   | 1 2... 9
//         1 2 3 4...9
//
// While the number of levels is k. And we do not need to loop every numbers from 1 - 9 every level, as:
// 1. Sum up the current path, and if the current testing number results a sum larger than n (the target sum),
//    the recusion can just return and do not need to go on. It is because we test number from smallest (1) to
//    largest (9). If the current sum is already larger than n, next larger number must also has a sum larger than n.
//    So can return from recusion.
// 2. Since a number can appear once in the combination, and the combination is the current generating tree path,
//    so if the current testing number appeared in the path already, we can skip. So the case [1 1 1] (k=3) will not
//    happen.
// 3. From point 2, we eliminated the repeating number from a single tree path. But repeating *combinations* can still
//    happen when we start from the next root. For example, k = 3, n = 7:
//      starts with root = 1: [1  2  4], [1  4  2]
//      starts with root = 2: [2  1  4], [2  4  1]
//      starts with root = 4: [4  1  2], [4  2  1]
//    It is because when we start from a root, the path (combination) content get reset, and we do not know if we have
//    visited that number or not before. But from the pattern we can see, the repeating combinations actually has the
//    smaller values come later. So if we keep looping monotonically increase, ie. start the next level loop from the
//    next value of the current value, the values in the low levels of the tree will not "go back". Hence the
//    combinations will not be repeated. And since it is not possible to loop the same values in the upper levels,
//    we do not need to check the existence of the current value in the current path in point 2 too.
//
// If the depth is not need k, and the current sum is not larger than n, we go down to next level and try to put the
// current number to the current combination. If the depth is k, and the sum is equal to n, save the current combination
// to the result. Otherwise, this combination does not give the target n, try next value in the current level, until all
// possible values on the current level get tested. Then remove the current value by track-back, and
// go back to last level to try another value.
//
// Time, O(9!), as the first round loop 9 times, then nested 8 times, then nested 7 times... which is like:
// 9*8*7*6*..*1. The max possible k is 9. Space is O(9!) for all possible combinations.

class Solution {

public:
    vector<vector<int>> ans;

    void search(unordered_set<int>& path, int cur_depth, int path_sum, int k, int n, int start)
    {
        for (int i = start; i <= 9; i++) {

            int s = path_sum + i;
            if (s > n) {
                return;
            }

            if (cur_depth == k) {
                if (s != n) {
                    continue;

                } else {

                    vector<int> v(path.begin(), path.end());
                    v.push_back(i);
                    ans.push_back(v);

                    return;
                }
            } else {
                path.insert(i);
                search(path, cur_depth + 1, s, k, n, i+1);
                path.erase(i);
            }
        }

        return;
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        unordered_set<int> path;

        search(path, 1, 0, k, n, 1);

        return ans;
    }
};
