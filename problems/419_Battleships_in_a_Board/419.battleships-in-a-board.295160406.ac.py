#
# @lc app=leetcode id=419 lang=python3
#
# [419] Battleships in a Board
#
# https://leetcode.com/problems/battleships-in-a-board/description/
#
# algorithms
# Medium (67.44%)
# Total Accepted:    76.8K
# Total Submissions: 113.5K
# Testcase Example:  '[["X",".",".","X"],[".",".",".","X"],[".",".",".","X"]]'
#
# Given an 2D board, count how many battleships are in it. The battleships are
# represented with 'X's, empty slots are represented with '.'s. You may assume
# the following rules:
# 
# 
# You receive a valid board, made of only battleships or empty slots.
# Battleships can only be placed horizontally or vertically. In other words,
# they can only be made of the shape 1xN (1 row, N columns) or Nx1 (N rows, 1
# column), where N can be of any size.
# At least one horizontal or vertical cell separates between two battleships -
# there are no adjacent battleships.
# 
# 
# Example:
# X..X
# ...X
# ...X
# 
# In the above board there are 2 battleships.
# 
# Invalid Example:
# ...X
# XXXX
# ...X
# 
# This is an invalid board that you will not receive - as battleships will
# always have a cell separating between them.
# 
# Follow up:Could you do it in one-pass, using only O(1) extra memory and
# without modifying the value of the board?
#
# We are trying to do a one pass without extra memory and modifying the board.
# The idea is to make use of the ship property that:
# 1. linear shape with on unit 1 width, ie:
# "XXXX" or "X"
#           "X"
#           "X"
# 2. Ships will not be connected.
# Hence, when we scan the board, if we see a "X", we know it is a part of a ship, and
# its a part of only 1 ship as ships cannot be connected. So if we hit a "X", we count 1 ship.
# But a ship can has many "X"s, we will over count. For each ship, we only want to count 1 "X"
# of it. Lets only count the "ship head". With property 1, we know a ship head:
# a. do not have "X" at it north, east, and west, if it is vertical, or
# b. do not have "X" at its west, north, and south, if it is horizontal.
# We also need to handle the ship at board boundary. Checking "not the ship head" seems easier, 
# with:
# i. do not have "X" at its north, and
# ii. do not have "X" at its west
# No need to handle the boundary as the ship body cannot on boundary otherwise the ship will 
# be over the board.
# So if not empty '.' and not body, we cound we see 1 ship.
# Complexity O(n), space O(1)
# 
# To check the "ship head", can use this condition:
#  if (i == 0 or board[i-1][j] == ".") and (j == 0 or board[i][j-1] == "."):
#                count += 1



class Solution:
    def countBattleships(self, board: List[List[str]]) -> int:
        
        n = len(board)
        m = len(board[0])
        
        ans = 0
        for i in range(0, n):
            for j in range(0, m):
                
                g = board[i][j]
                if g == '.':
                    continue                
                
                # From we see a ship, want to know which part
        
                # we see the 'body' or 'tail' of a ship        
                if (j > 0 and board[i][j-1] == 'X') or \
                   (i > 0 and board[i-1][j] == 'X'):
                    continue
                    
                # only count if this grid is the 'head' of a ship
                ans += 1
                
                
        return ans
                    
                
                
