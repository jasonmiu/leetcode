// logn algorithm can be related to binary search.
// In this question we do not know how many rotation got made.
// For 1 rotation, it means move the last element to the head, like:
// [ 1 2 3 4 ] -- 1 rotation --> [ 2 3 4 1 ]
//
// [0 1 2 4 5 6 7] -- 4 rotations --> [4 5 6 7 0 1 2]
//
// After n rotations, the first element become the original rank-n element.
// And the [n] element now is the original first element, which is the minimum.
// This is the output we want. So think how to find n.
// For a rotated array:
//
//        +- pivot point
//        V
// [4 5 6 7 0 1 2]
//  |     | |   |
//  \,,,,,/ \,,,/
//     |      | sorted 2nd half
//     |
//    sorted 1st half
//
// we can see if we find the pivot point, both halves are sorted.
// The pivot point has the length of n from the beginning. So
// if we can find pivot point, we can find n. To find pivot point,
// we want to find an index that both halves are sorted.
//
// We can cut the array at midpoint, check if both sides are sorted.
// If not, it must be:
// left side sorted, right side not. Or,
// left side not sorted, right side sorted.
//
// For the unsorted side, we recursively cut it in sub-halves,
// and check if those sub-havles are sorted. Keep doing this until
// we find a midpoint that both sides are sorted, that midpoint is the
// pivot point.
//
// A special case is if the array is already sorted, which means it rotated
// 0 times, or rotated multiple array.size() times. We need the pivot point
// as [bigger value (max)][smaller value (min)] where the bigger value is the
// pivot point. Already sorted array the pivot point is actually the last element,
// not the midpoint. To handle this, check the array if sorted first. This is a O(1) op.
//
// Time: O(logn) since we check half every time. Space: O(1).

bool is_sorted(const vector<int>& a, int s, int e)
{
    if (s < 0 || s >= a.size() || e < 0 || e >= a.size()) {
        return false;
    }

    if (a[s] <= a[e]) {
        return true;
    } else {
        return false;
    }
}

int find_pivot(const vector<int>& nums, int s, int e)
{
    int m = s + ((e - s) / 2);
    bool first_sorted = is_sorted(nums, s, m);
    bool second_sorted = is_sorted(nums, m+1, e);
    if (first_sorted && second_sorted) {
        // the midpoint m is always at the left side between s and e,
        // as the division is a floor. Example:
        // IDX: ... 3 4 5 6 ...
        // ARR:[... A B C D ...]
        // m = (3 + 6) / 2 = 4 which is "B"
        // So when the final pivot point come, it can be:
        // IDX: ... 3 4 5 6 ...
        // ARR:[... 7 8 0 1 ...]
        // We want the value on the left if not exact middle (odd size array),
        // which is the larger value we want as pivot.
        // An exceptional case is:
        // 0 1 2 3
        // 6 7 8 9
        // The pivot point is 9, but the middle point here tell us
        // both sides are sorted. This is the case the input is already
        // sorted. Handled that with sort checking.
        return m;
    }

    if (first_sorted && !second_sorted) {
        return find_pivot(nums, m+1, e);
    } else if (!first_sorted && second_sorted) {
        return find_pivot(nums, s, m);
    }

    return 0;
}

class Solution {
public:
    int findMin(vector<int>& nums) {

        if (is_sorted(nums, 0, nums.size()-1)) {
            return nums[0];
        }

        int i = find_pivot(nums, 0, nums.size()-1);
        int n = i + 1;
        return nums[n];
    }
};
