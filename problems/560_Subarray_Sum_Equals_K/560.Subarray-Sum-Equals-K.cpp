// Brute Force is having two loops to generate all subarrays, then
// for each subarrays add up all elements. This leads a O(n^3).
// For the sum up part, can use a sliding window with "remove last, add new"
// Still a O(n^2) (time limit exceeded)
//
// The most important observation is, for a array example:
//
// [1, 2, 2, 2, 4], k = 6
// the prefix sum (running sum) of a subarray, is the total sum
// we have covered.
//
// [1, 2, 2, 2, 4]
//  |     |
//  ```|```
//     prefix sum = 1 + 2 + 2 = 5
//
//
// in this subarray, does it contains any sub-subarray that can reach the K?
// reachable to the k means, given we are at the subarray prefix sum X,
// if adding K to X, we can hit a prefix sum later on, ie:
//
// [1, 2, 2, 2, 4]
//  |     |   \  \
//  ```|```   7   11
//     |
//     prefixSum 5 + K 6 = 11
//
// So at the index 2, with prefix sum now as 5, it can find a subarray that can make
// a sum as K. But we do not know the future, so think in reverse:
// [1, 2, 2, 2, 4]
//  \  \  \   \  \
//   1  3  5  7   11  <-- prefix sum
//                ^
//                |
//                11 - 6 is 5. Did we see 5 in prefix sum before?
//
// If we saw that prefix sum before, means the subarray from that prefix sum, to the current point,
// has a sum of target K. So we save prefix sum for all indices during scanning forward, also
// the prefix sum can be dupicated since the elements can be -ve or 0, so save the frequency of that prefix sum.
// for example:
// [1, 0, 0, 0, 1] , K = 1
//  \  \  \  \  \
//   1  1  1  1  2
// So when we are at index 3 (last 0), prefix sum 1 has frequency 3, and it is clear that there are 3 subarrays have
// the sum of 1: [1], [1,0], [1,0,0]. And also, count the subarray from beginning, ie. the current window, [1,0,0,0] so
// the totatl count at index 3 is 4. Thats why a hash map is used, where key for the prefix sum we saw, and the value for
// its frequency.
//
// Scan the input once, so Time is O(n), space need for prefix sum so O(n).


class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {

        int running_sum = 0;
        unordered_map<int, int> sum_freq;
        int cnt = 0;

        for (int x : nums) {
            running_sum += x;

            int sum = running_sum - k;
            if (sum_freq.find(sum) != sum_freq.end()) {
                cnt += sum_freq[sum];
            }

            if (running_sum == k) {
                cnt += 1;
            }

            sum_freq[running_sum] += 1;
        }

        return cnt;
    }
};
