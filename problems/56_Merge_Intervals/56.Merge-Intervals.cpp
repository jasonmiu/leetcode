// The idea is to keep comparing the previous interval, and the current interval,
// if the previous interval end >= current interval start, then it is an overlap.
// Since we scan from the starting, the intervals should be sorted by the their
// starting time.
// For two overlapping intervals, there are few case:
// case 1, previous interval end > current start, and <= current end
//  [ 1   3 ]
//    [ 2   4 ]
//
// case 2, previous interval end = current start, and <= current end
//  [ 1   3 ]
//      [ 3   4 ]
//
// case 3, previous interval end > current start, and > current end
//  [ 1        4 ]
//     [ 2  3 ]
//
// for case 1 and 2, the merged interval is [previous start, current end],
// for case 3, the merged interval is [previous start, previous end]
//
// If this two get merged, the merged interval is the new current, and the
// previous interval will be discarded since it get merged.
// Then we move forward, the current will be the next previous, hence
// for each comparison the previous is always the merged interval, if overlap
// happened in last round.
// If no overlapping, then just keep both intervals, and move to next one.
// Finally, remove all discarded interval that will give us only the merged
// intervals.
// Time is O(nlogn), due to the sort. Space is O(n), for saving the result.


void merge_intervals(vector<vector<int>>& intervals, int prev, int cur)
{
    if (cur >= intervals.size()) {
        return;
    }

    vector<int>& prev_iv = intervals[prev];
    vector<int>& cur_iv = intervals[cur];

    if (prev_iv[1] >= cur_iv[0]) {
        vector<int> new_iv = {prev_iv[0], cur_iv[1]};

        if (prev_iv[1] >= cur_iv[1]) {
            new_iv[1] = prev_iv[1];
        }

        intervals[prev] = {-1,-1};
        intervals[cur] = new_iv;

        merge_intervals(intervals, prev+1, cur+1);

    } else {

        merge_intervals(intervals, prev+1, cur+1);
    }

    return;
}

class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {

        if (intervals.size() == 1) {
            return intervals;
        }

        sort(intervals.begin(), intervals.end(), [](const vector<int>& a, const vector<int>& b) {
            if (a[0] < b[0]) {
                return true;
            } else {
                return false;
            }
        });

        vector<vector<int>> merged;
        merge_intervals(intervals, 0, 1);

        for (auto iv : intervals) {
            if (iv[0] != -1) {
                merged.push_back(iv);
            }
        }

        return merged;
    }
};
