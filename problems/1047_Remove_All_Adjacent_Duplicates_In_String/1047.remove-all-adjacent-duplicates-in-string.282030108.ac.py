#
# @lc app=leetcode id=1047 lang=python3
#
# [1047] Remove All Adjacent Duplicates In String
#
# https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/description/
#
# algorithms
# Easy (65.03%)
# Total Accepted:    39K
# Total Submissions: 59.6K
# Testcase Example:  '"abbaca"'
#
# Given a string S of lowercase letters, a duplicate removal consists of
# choosing two adjacent and equal letters, and removing them.
# 
# We repeatedly make duplicate removals on S until we no longer can.
# 
# Return the final string after all such duplicate removals have been made.  It
# is guaranteed the answer is unique.
# 
# 
# 
# Example 1:
# 
# 
# Input: "abbaca"
# Output: "ca"
# Explanation: 
# For example, in "abbaca" we could remove "bb" since the letters are adjacent
# and equal, and this is the only possible move.  The result of this move is
# that the string is "aaca", of which only "aa" is possible, so the final
# string is "ca".
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= S.length <= 20000
# S consists only of English lowercase letters.
# 
#
# The idea is to write input string S to output string 'out', one by one,
# let i be the index of S, and j be the index of out.
# Loop i thru S. Copy S[i] to out[j].
# If no dupicate, out == S
# If the newly written output out[j] is dupicated with previous one, out[j-1],
# reset j to the previous index, which effectively remove 2 dupicated chars.
# One tricky part is the out string should truncate to len j, to remove the
# possible tailing chars, since we may moved back the j.
# The complexity is O(n).
# The space complexity is O(n) for output string.
# The recusive algor with removing dups from start of string is correct but 
# O(n^2) which gives timeout.

# Another algor with same complexity, using 1 pointer. Scan the S with index i
# if the S[i] == S[i-1], remove S[i] and S[i-1], and **move i back 1, i = i - 1**,
# Since the resulted string len can be shorter than current i, like:
# mffm
#   ^ i = 2
# mm
#   ^ i = 2 ** The result string is shorter than i
# While the complexity is the same, but the list operation in the loop takes more time
# So the runtime is slower
# class Solution:
#     def removeDuplicates(self, S: str) -> str:
#        
#         S = list(S)
#         n = len(S)
#         if n == 1:
#             return S[0]
#        
#         i = 1
#         while i < n:
#             if i > 0 and S[i] == S[i-1]:
#                 S = S[0:i-1] + S[i+1:n]
#                 i = i -1
#             else:
#                 i = i + 1
#                
#             n = len(S)
#                
#         return "".join(S)


class Solution:
    def removeDuplicates(self, S: str) -> str:
        
        S = list(S)
        n = len(S)
        out = [""] * n
        i = 0
        j = 0
        
        for i in range(0, n):
            out[j] = S[i]
            if j > 0 and out[j] == out[j-1]:
                j = j - 1
            else:
                j = j + 1
                
        out = out[0:j]
        return "".join(out)
