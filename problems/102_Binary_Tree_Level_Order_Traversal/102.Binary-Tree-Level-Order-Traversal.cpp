// Basic BFS walk for each level.
// The main point is for each level, loop thru the fix size of queue,
// since we know the number of nodes of the current level, which is
// the size of queue at the beginning of each round.
// Time: O(n) for visiting each node. Space: O(n/2) for the queue.

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {

        if (! root) {
            return vector<vector<int>>();
        }

        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {

            vector<int> level;
            int qsize = q.size();

            for (int i = 0; i < qsize; i++) {
                TreeNode* n = q.front();
                q.pop();

                if (n->left) {
                    q.push(n->left);
                }
                if (n->right) {
                    q.push(n->right);
                }

                level.push_back(n->val);
            }

            ans.push_back(level);
        }

        return ans;
    }
};
