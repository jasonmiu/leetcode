#
# @lc app=leetcode id=102 lang=python3
#
# [102] Binary Tree Level Order Traversal
#
# https://leetcode.com/problems/binary-tree-level-order-traversal/description/
#
# algorithms
# Medium (50.69%)
# Total Accepted:    454.2K
# Total Submissions: 896.1K
# Testcase Example:  '[3,9,20,null,null,15,7]'
#
# Given a binary tree, return the level order traversal of its nodes' values.
# (ie, from left to right, level by level).
# 
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
# 
# 
# return its level order traversal as:
# 
# [
# ⁠ [3],
# ⁠ [9,20],
# ⁠ [15,7]
# ]
# 
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# The idea is simple, just do a BFS for the tree.
# For each level of the tree, store the nodes to a
# list. Read the queue length for each level first
# before drainning the queue for figuring out the
# end point of each level.

import queue
class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        if root == None:
            return []
        
        q = queue.Queue()
        q.put(root)
        ans = []
        while not q.empty():
            qs = q.qsize()
            level_nodes = []
            for i in range(0, qs):
                n = q.get()
                level_nodes.append(n.val)
                
                if n.left:
                    q.put(n.left)
                if n.right:
                    q.put(n.right)
                    
            ans.append(level_nodes)
            
        return ans
                    
        
                
