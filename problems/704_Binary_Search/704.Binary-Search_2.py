# Another solution of binary search using loop.
# keep using the start and end indices to shrinking the window,
# if the window size is 0 (start index > end index) means searched all,
# then return -1 as not found.

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        s = 0
        e = len(nums) - 1

        while s <= e:
            m = (s + e) // 2

            if nums[m] == target:
                return m
            elif nums[m] < target:
                s = m + 1
            elif nums[m] > target:
                e = m - 1

        return -1
