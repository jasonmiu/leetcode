# Do binary search recursively
# build a helper function bs() for starting and ending index.
# I found I am better with start end indexing (window), instead of
# using start and length (range).

def bs(a, t, s, e):
    if s > e:
        return -1

    m = (s + e) // 2
    if a[m] == t:
        return m
    elif a[m] < t:
        return bs(a, t, m+1, e)
    elif a[m] > t:
        return bs(a, t, s, m-1)

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        return bs(nums, target, 0, len(nums) - 1)
