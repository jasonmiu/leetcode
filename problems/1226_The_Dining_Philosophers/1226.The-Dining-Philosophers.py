# The problem of simple locking the fork is that, if everyone
# acquired their left fork, deadlock will happend.
# One way to resolve this is using asymmetric acquiring.
# For philosopher with even number, he pick up left fork first
# For philosopher with odd number, he pick up right fork first
# Hence the adjacency philosopher must acquire the same fork 
# at the same time, one must wait unilt he can pick up two forks.
# Anohter way is the philosopher can only eat when he can pick 
# up two forks at the same time. This need to put the 
# acquire/release in the CS.
# See operating system concepts book P177
#
# from threading import Lock
#
# fork_a = Lock()
# fork_b = Lock()
# fork_c = Lock()
# fork_d = Lock()
# fork_e = Lock()
# cs = Lock()
#
# # Phi id : (left, right)
# phi_fork = { 0 : (fork_a, fork_e),
#              1 : (fork_b, fork_a),
#              2 : (fork_c, fork_b),
#              3 : (fork_d, fork_c),
#              4 : (fork_e, fork_d)}
#
# class DiningPhilosophers:
#
#     def __init__(self):
#         self.has_fork = 0
#
#     # call the functions directly to execute, for example, eat()
#     def wantsToEat(self,
#                    philosopher: int,
#                    pickLeftFork: Callable[[], None],
#                    pickRightFork: Callable[[], None],
#                    eat: Callable[[], None],
#                    putLeftFork: Callable[[], None],
#                    putRightFork: Callable[[], None]) -> None:
#
#         l_fork = phi_fork[philosopher][0]
#         r_fork = phi_fork[philosopher][1]
#
#         ate = False
#         while not ate:
#             cs.acquire()
#             if not l_fork.locked() and not r_fork.locked():
#                 l_fork.acquire()
#                 r_fork.acquire()
#
#                 pickLeftFork()
#                 pickRightFork()
#                 eat()
#                 putLeftFork()
#                 putRightFork()
#                 l_fork.release()
#                 r_fork.release()
#                 ate = True
#
#             cs.release()
#
#
#

from threading import Lock

fork_a = Lock()
fork_b = Lock()
fork_c = Lock()
fork_d = Lock()
fork_e = Lock()
cs = Lock()

# Phi id : (left, right)
phi_fork = { 0 : (fork_a, fork_e),
             1 : (fork_b, fork_a),
             2 : (fork_c, fork_b),
             3 : (fork_d, fork_c),
             4 : (fork_e, fork_d)}

class DiningPhilosophers:

    def __init__(self):
        self.has_fork = 0
    
    # call the functions directly to execute, for example, eat()
    def wantsToEat(self,
                   philosopher: int,
                   pickLeftFork: 'Callable[[], None]',
                   pickRightFork: 'Callable[[], None]',
                   eat: 'Callable[[], None]',
                   putLeftFork: 'Callable[[], None]',
                   putRightFork: 'Callable[[], None]') -> None:
        
        l_fork = phi_fork[philosopher][0]
        r_fork = phi_fork[philosopher][1]

        if philosopher % 2 == 0:
            l_fork.acquire()
            r_fork.acquire()
            pickLeftFork()
            pickRightFork()
            eat()
            l_fork.release()
            r_fork.release()
            putLeftFork()
            putRightFork()
        else:
            r_fork.acquire()
            l_fork.acquire()
            pickRightFork()
            pickLeftFork()
            eat()
            r_fork.release()
            l_fork.release()
            putLeftFork()
            putRightFork()
            
        
