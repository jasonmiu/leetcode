# We have two things to search.
# 1. The rank of the queries[i] in P
# 2. Update the new rank queries[i] in P to 0, and increase 1 of the 
# ranks of those indices bigger than queries
# If we use hashtable for item 1, the search becomes O(1). 
# Update the indices takes O(m), hences the complexity is O(n*m), n is the
# size of queries.
# Space is O(m) for hashtable.

class Solution:
    def processQueries(self, queries: List[int], m: int) -> List[int]:
        
        p = {}
        for i in range(1, m+1):
            p[i] = i-1
            
        ans = []
        for q in queries:
            ans.append(p[q])
            
            for k in p:
                if p[k] < p[q]:
                    p[k] += 1
                    
            p[q] = 0
            
        return ans

