#
# @lc app=leetcode id=350 lang=python3
#
# [350] Intersection of Two Arrays II
#
# https://leetcode.com/problems/intersection-of-two-arrays-ii/description/
#
# algorithms
# Easy (49.44%)
# Likes:    891
# Dislikes: 311
# Total Accepted:    256.2K
# Total Submissions: 517.2K
# Testcase Example:  '[1,2,2,1]\n[2,2]'
#
# Given two arrays, write a function to compute their intersection.
# 
# Example 1:
# 
# 
# Input: nums1 = [1,2,2,1], nums2 = [2,2]
# Output: [2,2]
# 
# 
# 
# Example 2:
# 
# 
# Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
# Output: [4,9]
# 
# 
# Note:
# 
# 
# Each element in the result should appear as many times as it shows in both
# arrays.
# The result can be in any order.
# 
# 
# Follow up:
# 
# 
# What if the given array is already sorted? How would you optimize your
# algorithm?
# What if nums1's size is small compared to nums2's size? Which algorithm is
# better?
# What if elements of nums2 are stored on disk, and the memory is limited such
# that you cannot load all elements into the memory at once?
# 
# 
#

# @lc code=start
# The idea is convert input lists to set (hash table), and find their
# intersection. The ticket is the simple 'AND' operation will give only 
# intersected elements, eg:
# [1, 2, 2, 1] & [2, 2] = [2]
# But we want to handle the duplicated elementes. So we use the multi-set.
# The intersected elements have the repeated count, eg:
# [1, 2, 2, 1] & [2, 2] = [{2: 2}]
# We convert each element in the intersected elements by repeating itself to a 
# list. {2:2} -> [2, 2]. Then concat all converted lists together.
# The complexity is O(n) for scan thru the input lists
# The space complexity is O(n) for the sets


import collections

class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        
        c1 = collections.Counter(nums1)
        c2 = collections.Counter(nums2)
        
        ans = []
        inter = c1 & c2
        for c in inter:
            ans.extend([c] * inter[c])

        return ans
        
        
# @lc code=end
