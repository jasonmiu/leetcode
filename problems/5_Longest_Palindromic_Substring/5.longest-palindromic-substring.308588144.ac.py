#
# @lc app=leetcode id=5 lang=python3
#
# [5] Longest Palindromic Substring
#
# https://leetcode.com/problems/longest-palindromic-substring/description/
#
# algorithms
# Medium (28.16%)
# Total Accepted:    811.5K
# Total Submissions: 2.8M
# Testcase Example:  '"babad"'
#
# Given a string s, find the longest palindromic substring in s. You may assume
# that the maximum length of s is 1000.
# 
# Example 1:
# 
# 
# Input: "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.
# 
# 
# Example 2:
# 
# 
# Input: "cbbd"
# Output: "bb"
# 
# 
#
# The main point is the palindromic has a important property,
# to make a palindromic, the frist and last char needed to be
# the same, and the remaining substring is a palindromic too. For example:
# abba
# A[0] == A[3] == 'a'
# A[1:2] is palindromic
# abxba
# A[0] == A[4] == 'a'
# A[1:3] is palindromic
# So if A[s+1:e-1] is palindromic and A[s] == A[e], A is the palindromic. 
# Special cases are
# 1. if s (starting index) == e (ending index), it is a 
# single char, so it is palindromic.
# 2. if A[s] == A[e] and no remaining substring, ie. len(A) == 2, A is 
# a palindromic too.
# To have a brute force algorithm, we can do a sliding expending window, 
# start at 0, expending the window to the end, check if every substring
# in the windoe is palindromic. Then start over at 1. For example:
# "a b b a"
#  ^-^-^-^- <-- first scan ["a", "ab", "abb", "abba"]
#    ^-^-^- <--- 2nd scan  ["b", "bb", "bba"]
#      ^-^- <--- 3rd scan  ["b", "ba"]
#        ^- <--- 4th scan  ["a"]
# This is O(n^3) since we need to scan N^2 times, and each time we need to 
# check O(n) char to see if this substring is a palindromic.
# Can see we repeat many times to check the similar substrings.
# Since a string is palindromic or not depends on its substring, we can
# save the substring is a palindromic or not, to save the time to compute from 
# whole string agao. The substring we want to check is A[s+1:e-1]. We can make a 
# table, row is the starting index, col is the ending index. Each element means
# if the substring from row index to col index is a palindromic.
# For example: "aba"
#    0 1 2
#  0 T F T
#  1 F T F 
#  2 F F T
# Another implemenetation trick is we can see the current value always ref to the
# lower, left value of the table, since we check A[s+1][e-1] for the current value.
#                                    lower --------^     ^--------- left
# So we want to build the table from below, hence we start the scan backward.
# The complexity is O(n^2) and also the space for the table.


def palindrome(s, start, end, table):
    pals = []
    for i in range(end, start-1, -1):
        a = s[start]
        b = s[i]
        
        #print("start", start, "i", i, "a", a, "b", b, table)
        
        if i - start == 0:
            table[start][i] = True
        elif i - start == 1 and a == b:
            table[start][i] = True
        elif a == b and table[start+1][i-1]:
            table[start][i] = True
        else:
            table[start][i] = False
        
        if table[start][i]:
            pals.append(s[start:i+1])
            
    return pals
            
    
class Solution:
    def longestPalindrome(self, s: str) -> str:
        
        answers = []
        table = []
        
        for i in range(0, len(s)):
            table.append([False]*len(s))
        for i in range(0, len(s)):
            table[i][i] = True
            
        for i in range(len(s)-1, -1, -1):
            p = palindrome(s, i, len(s)-1, table)
            answers.extend(p)
            
        #print(answers)
        
        longest_pal = ""
        for a in answers:
            if len(a) > len(longest_pal):
                longest_pal = a
                
        return longest_pal
        
