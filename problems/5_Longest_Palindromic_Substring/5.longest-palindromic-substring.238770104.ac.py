#
# @lc app=leetcode id=5 lang=python3
#
# [5] Longest Palindromic Substring
#
# https://leetcode.com/problems/longest-palindromic-substring/description/
#
# algorithms
# Medium (28.16%)
# Total Accepted:    696.7K
# Total Submissions: 2.5M
# Testcase Example:  '"babad"'
#
# Given a string s, find the longest palindromic substring in s. You may assume
# that the maximum length of s is 1000.
# 
# Example 1:
# 
# 
# Input: "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.
# 
# 
# Example 2:
# 
# 
# Input: "cbbd"
# Output: "bb"
# 
# 
#
# The idea is concidering these cases, to have a palindromic, we have to:
# s = 0, dist = 0, e = 0, str + dist = "A"
# s = 0, dist = 1, e = 1, str + dist = "AA"
# s = 0, dist = 2, e = 2, str + dist = "ABA", str[s] == str[e] and str[s+1:e-1] is palindromic
# s = 0, dist = 3, e = 3, str + dist = "ABBA", str[s] == str[e] and str[s+1:e-1] is palindromic
# s = 0, dist = 4, e = 4, str + dist = "ABCBA", str[s] == str[e] and str[s+1:e-1] is palindromic
# We have two special cases as dist = 0 and dist = 1
# All other case need to check if str[s-1:e+1] is also a palindromic
# we build a memo table to remember if str[s-1:e+1] is a palindromic, to save the time to regenerate 
# the substr everything
 
class Solution:
    def longestPalindrome(self, s: str) -> str:
        
        n = len(s)
        
        if n <= 1:
            return s
        
        memo = []
        for i in range(0, n):
            memo.append([False] * n)
            
        for i in range(0, n):
            memo[i][i] = True
            
        max_len = 1
        max_idx = 0
        # the starting index, s in the idea aboce, started from the end of str
        # it because we need to check s+1 repeatly, ie. we need to compute the s+1
        # first. Looping s backward we make sure we have s+1 ready when we are at s
        # the "dist" is the distance from current s to the end of str, hence n - s
        for i in range(n-1, -1, -1):
            for dist in range(1, n - i):
                j = i + dist
                c = s[i]
                c2 = s[j]
                #print("i", i, "dist", dist, "c", c, "c2", c2)

                if dist == 1:
                    if c == c2:
                        memo[i][j] = True
                    else:
                        memo[i][j] = False
                else:
                    if c == c2 and memo[i+1][j-1]:
                        memo[i][j] = True
                    else:
                        memo[i][j] = False
                        
                if memo[i][j]:
                    #print("max_len", max_len)
                    if dist + 1 >= max_len:
                        max_len = dist + 1
                        max_idx = i
                        
        return s[max_idx:max_idx+max_len]
                    
