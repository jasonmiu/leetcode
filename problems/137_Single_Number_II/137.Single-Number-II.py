# The idea is to count how to have a counter to 
# be reset to zero if we counted K times (in this question
# K = 3).
# Check is example:
# [2, 3, 2, 2]
# '2' appears 3 times. We like to have a counter to count
# it 3 times and reset to zero. For the '3' it appear only
# once the counter should be the same.
# Think all values are in bits. For a number appears 3 times,
# all its bits appear 3 times as well. Make a table like:
#
# Num/Bit | 3 | 2 | 1 | 0
#   ----------------------
#      2  | 0 | 0 | 1 | 0
#      3  | 0 | 1 | 1 | 0
#      2  | 0 | 0 | 1 | 0
#      2  | 0 | 0 | 1 | 0
#   ----------------------
# Count   | 0 | 1 | 4 | 0
# The bit 1 appeared 4 times. We want to reset it for every K times (K=3), 
# so we can do: bitCount_i % k = bitCount_i
# Let all numbers are 32bits. We make a bits counter array with 32 elements,
# each element is a counter of bit_i, i = 0 -> 31.
# For each number, we check each bit. If that bit is 1, we increase the
# bit counter of that bit position. If that bit counter reach K,
# use mod to loop it back to 0.
# Finally the bit counter array has the bits only appeared once. We OR them 
# together.
# For -ve number, since the largest +ve can only be (2^31)-1, so if the result
# is larger than (2^31)-1, make it -ve by -((2^32) - result).
# Complexity: O(n)
# Space: O(1)
# 
# The plain counter dict gives a faster runtime also simular memory usage:
# Counter Dict: 60 ms, 15.4 MB
# Bit Counter: 156 ms, 15.6 MB

BITSIZE = 32
K = 3

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        bits = [0] * BITSIZE
        
        for i in range(0, len(nums)):
            x = nums[i]
            
            for j in range(0, BITSIZE):
                bit = (1 << j) & x
                if bit > 0:
                    bits[j] += 1
                    bits[j] = bits[j] % K
                            
        ans = 0
        for j in range(0, BITSIZE):
            if bits[j] > 0:
                ans = ans | (1 << j)
            
        if ans >= pow(2, 31):
            ans = -(pow(2, 32) - ans)
            
        return ans
