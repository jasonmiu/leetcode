/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

// The idea is to assume we have the merge sorted list already,
// and we are pointing at the tail of this merge sorted list,
// for the next node of this list, which node should we pick from
// l1 or l2? For sure to pick the smaller head node of two lists,
// then the list that can picked, will forward its head pointer
// to the next node. The merged sorted list tail now is pointing to
// the newly picked node. For example:
// l1 = [0, 1, 2, 4]
// l2 = [1, 2, 3, 4]
//
// First find the first smaller node, which is l1[0],
// merge sorted list tail (call it current) is pointing to it, l1 move to
// its next node:
//
// current
// |
// V
// 0 -> 1 -> 2 -> 4
//      ^- l1
//
// 1 -> 2 -> 3 -> 4
// ^- l2
//
// Then we recursively check the l1 and l2 head, see which node shoule be picked
// to join the tail of current
//
//      current
//      |
//      V
// 0 -> 1 -> 2 -> 4
//           ^- l1
//
// 1 -> 2 -> 3 -> 4
// ^- l2
//
//           current
//           |
//           |
// 0 -> 1    |    2 -> 4
//      |    |    ^- l1
//      |    V
//      +--> 1 -> 2 -> 3 -> 4
//                ^- l2
//
//
//
//                     current
//                     |
//                     V
// 0 -> 1        +-->  2 -> 4
//      |        |          ^- l1
//      |        |
//      +--> 1 --+     2 -> 3 -> 4
//                     ^- l2
//
//
//                              current
//                              |
//                              |
// 0 -> 1        +-->  2        |    4
//      |        |     |        |    ^- l1
//      |        |     |        V
//      +--> 1 --+     +----->  2 -> 3 -> 4
//                                   ^- l2
//
//
// So this until a list is empty. If a list is empty,
// the next of the current merge sorted list tail will point
// to another list.
//
// Time: O(n) since access all nodes once. Space: O(1).



void sort(ListNode* current, ListNode* l1, ListNode* l2)
{
    if (l1 == nullptr) {
        current->next = l2;
        return;
    }

    if (l2 == nullptr) {
        current->next = l1;
        return;
    }

    if (l1->val <= l2->val) {
        current->next = l1;
        sort(l1, l1->next, l2);
    } else {
        current->next = l2;
        sort(l2, l1, l2->next);
    }
}

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        if (l1 == nullptr) {
            return l2;
        }

        if (l2 == nullptr) {
            return l1;
        }

        ListNode* head = nullptr;
        if (l1->val <= l2->val) {
            head = l1;
            sort(head, l1->next, l2);
        } else {
            head = l2;
            sort(head, l1, l2->next);
        }

        return head;
    }
};
