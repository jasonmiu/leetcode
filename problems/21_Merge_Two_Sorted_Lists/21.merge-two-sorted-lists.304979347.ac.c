/*
 * @lc app=leetcode id=21 lang=c
 *
 * [21] Merge Two Sorted Lists
 *
 * https://leetcode.com/problems/merge-two-sorted-lists/description/
 *
 * algorithms
 * Easy (50.67%)
 * Total Accepted:    843.6K
 * Total Submissions: 1.6M
 * Testcase Example:  '[1,2,4]\n[1,3,4]'
 *
 * Merge two sorted linked lists and return it as a new list. The new list
 * should be made by splicing together the nodes of the first two lists.
 * 
 * Example:
 * 
 * Input: 1->2->4, 1->3->4
 * Output: 1->1->2->3->4->4
 * 
 * 
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2){
    struct ListNode *d = malloc(sizeof(struct ListNode));
    struct ListNode *dummy_head = d;
    
    struct ListNode *p1 = l1;
    struct ListNode *p2 = l2;
    struct ListNode *tmp = NULL;
    
    d->next = NULL;
    
    
    while (p1 != NULL && p2 != NULL) {
        if (p1->val <= p2->val) {
            tmp = p1->next;
            d->next = p1;
            p1->next = NULL;
            p1 = tmp;
            
        } else {
            tmp = p2->next;
            d->next = p2;
            p2->next = NULL;
            p2 = tmp;
        }
        
        d = d->next;
    }
    
    if (p1 != NULL) {
        d->next = p1;
    }
    
    if (p2 != NULL) {
        d->next = p2;
    }
    
    return (dummy_head->next);
    
}


