#
# @lc app=leetcode id=21 lang=python3
#
# [21] Merge Two Sorted Lists
#
# https://leetcode.com/problems/merge-two-sorted-lists/description/
#
# algorithms
# Easy (49.62%)
# Total Accepted:    822K
# Total Submissions: 1.6M
# Testcase Example:  '[1,2,4]\n[1,3,4]'
#
# Merge two sorted linked lists and return it as a new list. The new list
# should be made by splicing together the nodes of the first two lists.
# 
# Example:
# 
# Input: 1->2->4, 1->3->4
# Output: 1->1->2->3->4->4
# 
# 
#
# The idea is to merge the list1 to list2. We keep moving the 
# pointer of list1, n1, and compare each node in list2 for finding a
# insertion place.
# The complexity is O(n^2). A better idea is to use a dummy head for a
# new merge list, mantain a tail and keep adding the smaller node of L1 and L2.
# And aother good idea is resursivly join the lists. 
# We take the smaller value of two lists each time, it will join the merged list
# without current value. If a list is empty, means the its last value is the smallest
# at that monent, we return another list for that.

#class Solution {
#public:
#    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
#        if(l1 == NULL) return l2;
#        if(l2 == NULL) return l1;
#        
#        if(l1->val < l2->val) {
#            l1->next = mergeTwoLists(l1->next, l2);
#            return l1;
#        } else {
#            l2->next = mergeTwoLists(l2->next, l1);
#            return l2;
#        }
#    }
#};


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        
        h1 = l1
        h2 = l2
        
        n1 = h1
        n2 = h2
        while n1:
            found = False
            n2 = h2
            while n2:
                if n1.val >= n2.val and (n2.next == None or n1.val < n2.next.val):
                    next_n1 = n1.next
                    p = n2.next
                    n2.next = n1
                    n1.next = p
                    found = True
                    n1 = next_n1
                    break
                n2 = n2.next
                
            # n1 is the smallest. Put it in front and update the head
            if not found:
                p = n1.next
                n1.next = h2
                h2 = n1
                n1 = p
                
        
        return h2
                
            
