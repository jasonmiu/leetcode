// For an sorted input:
// [-4,-1,0,3,10]
// squaring all elements, become:
// [16,1,0,9,100]
// so looks like it got unsorted.
// re-sort it costs O(nlogn) time.
// but since the max element value is 10^4 (10000),
// have a counter list for bucket sorting the squared list
// looks possible.
// For a list have repeated elements like:
// [-7,2,2,3,11]
// the count_list indices are the input element values,
// while the count is the number of that element.
// first abs all input elements first:
// input:
// [-7,2,2,3,11]
// abs:
// [7,2,2,3,11]
// counter list:
//  0 1 2 3 4 5 6 7 8 9 10 11
// [0 0 2 1 0 0 0 1 0 0  0  1]
//
// for each value in counter list, if it is not zero,
// i.e. exists in input elements, we square the index,
// which is the original input element. Repeatly putting
// the same squared value to the answer list with the counter value
// ans list:
// [4, 4, 9, 49, 121]
//  |  |  |   |    |
//  2  2  3   7   11  <-- from counter list index
//
// O(n), since scan the input linearly. The space and time depends on the
// max input value. But it is a constance here.


#include <algorithm>

class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {
        vector<int> abs_list(nums.size());

        std::transform(nums.begin(), nums.end(), abs_list.begin(), [](auto i) -> int {
            if (i < 0) {
                return -i;
            } else {
                return i;
            }
        });

        int max_ele = *(std::max_element(abs_list.begin(), abs_list.end()));
        vector<int> count_list(max_ele+1);

        for(int x : abs_list) {
            count_list[x] += 1;
        }

        vector<int> ans;

        for (int i = 0; i < count_list.size(); i++) {
            if (count_list[i] != 0) {
                int sqt = i * i;
                for (int j = 0; j < count_list[i]; j++) {
                    ans.push_back(sqt);
                }
            }
        }

        return ans;
    }
};
