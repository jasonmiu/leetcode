// Another algor using two pointers.
// For input like:
// [-4,-1,0,3,10]
// after squared all elements, it becomes:
// [16,1,0,9,100]
// while it looks like unsorted, but it can be thought as
// sorted from the middle to both sides, like a valley.
// So if we have two pointers (s, e) start from both sides,
// the values they are pointing to will be decreasing.
// We move two pointers to the middle until processed all
// elements, and for each loop, compare the abs value,
// since larger abs value will give a larger square value too.
// For the larger one, place the squared value at the end of the
// result list.
// Time: O(n)
// Space: O(n)

#include <stdlib.h>
#include <math.h>

class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {
        int s = 0;
        int e = nums.size() - 1;

        vector<int> ans(nums.size());
        int ans_idx = nums.size() - 1;

        while (s <= e) {
            if (abs(nums[s]) >= abs(nums[e])) {
                ans[ans_idx] = nums[s] * nums[s];
                s = s + 1;
            } else {
                ans[ans_idx] = nums[e] * nums[e];
                e = e - 1;
            }

            ans_idx = ans_idx - 1;
        }

        return ans;
    }
};
