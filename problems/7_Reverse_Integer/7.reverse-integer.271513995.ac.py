#
# @lc app=leetcode id=7 lang=python3
#
# [7] Reverse Integer
#
# https://leetcode.com/problems/reverse-integer/description/
#
# algorithms
# Easy (25.52%)
# Total Accepted:    847.2K
# Total Submissions: 3.3M
# Testcase Example:  '123'
#
# Given a 32-bit signed integer, reverse digits of an integer.
# 
# Example 1:
# 
# 
# Input: 123
# Output: 321
# 
# 
# Example 2:
# 
# 
# Input: -123
# Output: -321
# 
# 
# Example 3:
# 
# 
# Input: 120
# Output: 21
# 
# 
# Note:
# Assume we are dealing with an environment which could only store integers
# within the 32-bit signed integer range: [−2^31,  2^31 − 1]. For the purpose
# of this problem, assume that your function returns 0 when the reversed
# integer overflows.
# 
#
# The main point is to get each digit from int x
# The while loop:
#  while n > 0:
#            r = n % 10
#            n = n // 10
#            digits.append(r)
# is an idiom should be remembered.
# Need to take care of the x is a negative, the starting condition will fail
# so need to check and save if x is a negative first.

class Solution:
    def reverse(self, x: int) -> int:
        
        n = int(x)
        
        is_neg = False
        if n < 0:
            is_neg = True
            n = -n
        
        digits = []
        while n > 0:
            r = n % 10
            n = n // 10
            digits.append(r)
            
        m = 0
        n = 0
        digits.reverse()
        #print("digits", digits)
        for i in digits:
            n = i * pow(10, m) + n
            m = m + 1
            
        if is_neg:
            n = -n
        #print(n)
        
        if n > (pow(2, 31) - 1) or n < (- pow(2, 31)):
            return 0
        else:
            return n
            
            
            
