#
# @lc app=leetcode id=230 lang=python3
#
# [230] Kth Smallest Element in a BST
#
# https://leetcode.com/problems/kth-smallest-element-in-a-bst/description/
#
# algorithms
# Medium (54.06%)
# Total Accepted:    308.6K
# Total Submissions: 550.3K
# Testcase Example:  '[3,1,4,null,2]\n1'
#
# Given a binary search tree, write a function kthSmallest to find the kth
# smallest element in it.
# 
# Note: 
# You may assume k is always valid, 1 ≤ k ≤ BST's total elements.
# 
# Example 1:
# 
# 
# Input: root = [3,1,4,null,2], k = 1
# ⁠  3
# ⁠ / \
# ⁠1   4
# ⁠ \
# 2
# Output: 1
# 
# Example 2:
# 
# 
# Input: root = [5,3,6,2,4,null,null,1], k = 3
# ⁠      5
# ⁠     / \
# ⁠    3   6
# ⁠   / \
# ⁠  2   4
# ⁠ /
# ⁠1
# Output: 3
# 
# 
# Follow up:
# What if the BST is modified (insert/delete operations) often and you need to
# find the kth smallest frequently? How would you optimize the kthSmallest
# routine?
# 
#
# Inorder traverse of a BST is the sorted list. Do
# a inorder traverse and then get the k-th element in 
# the sorted list.
# Complexity O(n), Space O(n).

# To have a k-th smallest searching in O(h) time, we can
# have a field in the node, node.left_count, which is the
# number of the nodes in left subtree of that node.
# When insert a node, increase the left_count of the nodes
# along the insertion search path.
# If k == left_count + 1, this node is k-th smallest.
# If k < left_count + 1, we keep search to the left tree
# if k > left_count + 1, the ans will be in right tree,
# since for each node we only count the nodes of left subtree,
# the node in right tree start the left_count with 0.
# So the k to the right tree will be k = k - left_count + 1
# https://www.geeksforgeeks.org/find-k-th-smallest-element-in-bst-order-statistics-in-bst/
# Searching a node with k means search from a subtree, which will be O(h)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def inorder(root, sorted_list):
    
    if root == None:
        return
    
    inorder(root.left, sorted_list)
    sorted_list.append(root.val)
    inorder(root.right, sorted_list)
    
    return


class Solution:
    def kthSmallest(self, root: TreeNode, k: int) -> int:
        
        sorted_list = []
        inorder(root, sorted_list)
        return sorted_list[k-1]
