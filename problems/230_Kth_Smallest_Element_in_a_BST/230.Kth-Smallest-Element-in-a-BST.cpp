// This make use of the fact that the inorder walk of BST
// is a sorted list. Walk the tree and return the sorted list.
// Get the k-1 element in the list.
// Time: O(n), Space O(n).

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void infix_walk(TreeNode* root, vector<int>& sorted)
{
    if (root == nullptr) {
        return;
    }

    if (root->left) {
        infix_walk(root->left, sorted);
    }

    sorted.push_back(root->val);

    if (root->right) {
        infix_walk(root->right, sorted);
    }

    return;
}

class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        vector<int> sorted;

        infix_walk(root, sorted);

        return sorted[k-1];
    }
};
