#
# @lc app=leetcode id=33 lang=python3
#
# [33] Search in Rotated Sorted Array
#
# https://leetcode.com/problems/search-in-rotated-sorted-array/description/
#
# algorithms
# Medium (33.31%)
# Total Accepted:    591K
# Total Submissions: 1.8M
# Testcase Example:  '[4,5,6,7,0,1,2]\n0'
#
# Suppose an array sorted in ascending order is rotated at some pivot unknown
# to you beforehand.
# 
# (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
# 
# You are given a target value to search. If found in the array return its
# index, otherwise return -1.
# 
# You may assume no duplicate exists in the array.
# 
# Your algorithm's runtime complexity must be in the order of O(log n).
# 
# Example 1:
# 
# 
# Input: nums = [4,5,6,7,0,1,2], target = 0
# Output: 4
# 
# 
# Example 2:
# 
# 
# Input: nums = [4,5,6,7,0,1,2], target = 3
# Output: -1
# 
#
# At any time, at least half of the array A are sorted.
# Make a middle point of the array, we can see 3 cases:
#
# Case 1
#
# [2, 4, 5, 6, 7, 0, 1]
#  s        m          e
#  First half s -> m is sorted
#
# Case 2
#
# [6, 7, 0, 1, 2, 4, 5]
#  s        m          e
# 2nd half m -> e is sorted
#
# Case 3
#
# [4, 5, 6, 7, 0, 1, 2]
#  s        m        e
# Mid point is the pivot point, 1st and 2nd halves are sorted
#
# For case 1, A[s] < A[m], if the target t is A[s] <= t <= A[m],
# means the t is in the sorted half, do a binary search there.
# If t is in the unsorted half, recursive search this sorted half
# with the same function as we knot the target t will not in the 
# first half.
# For case 2, A[m] < A[e-1], if the target t is A[m] >= t <= A[e-1],
# means the t is in the sorted 2nd half, we do a similar search as
# case 1.
# For case 3, we need to see which half the target belongs too.
# If t == A[m], then we got the ans.
# if A[s] <= t <= A[m-1], t is in the first half.
# if A[m+1] <= t <= A[e-1], t is in the 2nd half.
# if t does not belong to any half, it does not in the array A.
# A implementation detail is, when doing the recursion, since the 'e'
# is the last index + 1 (the array len), if the target is not found
# in all recursive halves, 's' will equal to 'e'. In that case, 
# we know target is not found.
# Complexity is O(logn) since we cut half everytime.
# Space is O(1) with no extra space.


def bs(a, t, lo, hi):
    i = bisect.bisect_left(a, t, lo, hi)
    if i != hi and a[i] == t:
        return i
    else:
        return -1
    

def bi_search(nums, s, e, t):
    
    if len(nums) == 0:
        return -1
    
    if s == (e-1):
        if nums[s] == t:
            return s
        else:
            return -1
        
    if s >= e:
        return -1

    m = (s + e) // 2
    
    if nums[s] < nums[m]:
        if t >= nums[s] and t <= nums[m]:
            return bs(nums, t, s, m+1)
        else:
            return bi_search(nums, m+1, e, t)
    elif nums[m] < nums[e-1]:
        if t >= nums[m] and t <= nums[e-1]:
            return bs(nums, t, m, e)
        else:
            return bi_search(nums, s, m, t)
    else:
        if t == nums[m]:
            return m
        else:
            if t >= nums[s] and t <= nums[m-1]:
                return bs(nums, t, s, m)
            elif m < len(nums)-1 and t >= nums[m+1] and t <= nums[e-1]:
                return bs(nums, t, m+1, e)
            else:
                return -1

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        return bi_search(nums, 0, len(nums), target)
