// While the first idea was to find the rotation pivot index, and
// try to restore the array, but indeed after few examples, we can
// see there are few cases depending on the middle point.
//
// case 1
//  0 1 2 3 4 5 6
// [3,4,5,6,7,0,1]
//        ^m
// The first half, 0 -> m is sorted. Since the orignal array was sorted, if a
// subarray is sorted too, the first value must <= the last value.
// in this case, a[0] (3) <= a[3] (6) so this half is sorted.
//
// case 2
//  0 1 2 3 4 5 6
// [4,5,6,0,1,2,3]
//        ^m
// The first half is not sorted, but the second half, a[4] (1) <= a[6] (3) is sorted.
//
// case 3
//  0 1 2 3 4 5 6
// [3,4,5,6,0,1,2]
//        ^m
//
// both halves are sorted.
//
// For every sorted half, we try to see if the target is fall in that range. If it is,
// it must exists in that half if the target actually exists in the input.
// If the half is sorted, and the target is in its range, do a binary search. If it get found,
// reutrn the index.
// Here since I tried to use the lower_bound(), which works like python bisect_left(). For the
// lower_bound(), if the target is found, it return the index of course. But if the target
// is *NOT* found, it can return:
// 1. The first place of the value that is greater than target.
// 2. The last iterator specified in the lower_bound().
// Since we test the target is in the range of the input, so the smallest value that larger than
// target must still in the range, for example:
// [1, 3, 5, 7], target = 6
// Then it will return index 3, which is value 7. If the target is 8, while the lower_bound()
// will return the last iterator ([].end() in this case) since nothing is larger than target 8,
// but since we checked the range already, we will only get the case 1 of lower_bound().
// So even the target is not in the input, the lower_bound() gives the next larger value. So
// when we get the returned index of the lower_bound(), we check if that index is really the target,
// if yes, then it is real found. If not, lower_bound() just returned the right handside of the
// supposed-to-be-target-position to us.
//
// If one half is sorted, but the target is not in that range, it must be in another half.
// If another half is sorted too (case 3), then do the same binary search with lower_bound().
// If another half is not sorted, recursively cut this half in to two, and repeat the process.
// Since the target must be in this half, we want to find a sorted-subhalf in it, and search
// the target in a sorted half that the range covers the target.
//
// Since we search half every time, time is O(logn). Space is O(1).


bool is_sorted(const vector<int>& a, int s, int e)
{
    if (s < 0 || s >= a.size() || e < 0 || e >= a.size()) {
        return false;
    }

    if (a[s] <= a[e]) {
        return true;
    } else {
        return false;
    }
}

int bs(const vector<int>& a, int s, int e, int t)
{
    if (s > e) {
        return -1;
    }

    int m = s + ((e - s) / 2);

    bool first_half_sorted = is_sorted(a, s, m);
    bool second_half_sorted = is_sorted(a, m+1, e);

    if (first_half_sorted && a[s] <= t && t <= a[m]) {
        auto it = lower_bound(a.begin(), a.begin()+m+1, t);

        int idx = it - a.begin();
        if (a[idx] != t) {
            return -1;
        } else {
            return idx;
        }
    }

    if (second_half_sorted && a[m+1] <= t && t <= a[e]) {
        auto it = lower_bound(a.begin() + m + 1,  a.end(), t);

        int idx = it - a.begin();
        if (a[idx] != t) {
            return -1;
        } else {
            return idx;
        }
    }

    if (!first_half_sorted) {
        return bs(a, s, m, t);
    } else if (!second_half_sorted) {
        return bs(a, m+1, e, t);
    }

    return -1;
}


class Solution {
public:
    int search(vector<int>& nums, int target) {
        return bs(nums, 0, nums.size() - 1, target);
    }
};
