// This make use of the BST tree inorder walk.
// If this tree is a BST tree, then the inorder walk
// result is a sorted list, from small to large. So
// Can get a inorder walk result and check if it is
// sorted.
// Time: O(n) for walking all nodes. Space: O(n) for saving all nodes.

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void inorder(TreeNode* r, vector<int>& sorted)
{
    if (!r) {
        return;
    }

    if (r->left) {
        inorder(r->left, sorted);
    }

    sorted.push_back(r->val);

    if (r->right) {
        inorder(r->right, sorted);
    }

    return;
}


class Solution {
public:
    bool isValidBST(TreeNode* root) {
        vector<int> sorted;

        inorder(root, sorted);

        if (sorted.size() == 0 || sorted.size() == 1) {
            return true;
        }

        for (int i = 1; i < sorted.size(); i++) {
            if (sorted[i] <= sorted[i-1]) {
                return false;
            }
        }

        return true;
    }
};
