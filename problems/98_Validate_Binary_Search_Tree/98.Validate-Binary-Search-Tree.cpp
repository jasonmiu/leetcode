// This is the Recusive algor same as in the python version.
// The idea is, for the left subtree, the current node should be
// the largest. So if we pass down the current node value as largest
// of its left subtree, the left subtree node should check if itself
// is smaller than the current largest. If so, recusively go down.
// Same as right subtree. The current node value should be the
// smallest of its right subtree. So we pass down the current node
// as the smallest for the right subtree to check. If both
// subtrees fullfill the checking, then this tree is ok and can return
// true up.
// One implmentation point of c++ is, the question include the value range
// from -2^31 to 2^31. So the INT_MAX/MIN are not enough since they only
// guarantee 2^16. Use the LLONG_MAX/MIN for 2^63 range.

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

bool is_bst(TreeNode* r, long long smallest, long long largest)
{
    if (!r) {
        return true;
    }

    if (r->val <= smallest || r->val >= largest) {
        return false;
    }

    bool l_ok = is_bst(r->left, smallest, r->val);
    bool r_ok = is_bst(r->right, r->val, largest);

    return (l_ok && r_ok);
}

class Solution {
public:
    bool isValidBST(TreeNode* root) {

        bool ans = is_bst(root, LLONG_MIN, LLONG_MAX);
        return ans;
    }
};
