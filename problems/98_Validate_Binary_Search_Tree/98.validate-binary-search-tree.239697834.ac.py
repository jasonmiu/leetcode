#
# @lc app=leetcode id=98 lang=python3
#
# [98] Validate Binary Search Tree
#
# https://leetcode.com/problems/validate-binary-search-tree/description/
#
# algorithms
# Medium (26.53%)
# Total Accepted:    499.3K
# Total Submissions: 1.9M
# Testcase Example:  '[2,1,3]'
#
# Given a binary tree, determine if it is a valid binary search tree (BST).
# 
# Assume a BST is defined as follows:
# 
# 
# The left subtree of a node contains only nodes with keys less than the node's
# key.
# The right subtree of a node contains only nodes with keys greater than the
# node's key.
# Both the left and right subtrees must also be binary search trees.
# 
# 
# 
# 
# Example 1:
# 
# 
# ⁠   2
# ⁠  / \
# ⁠ 1   3
# 
# Input: [2,1,3]
# Output: true
# 
# 
# Example 2:
# 
# 
# ⁠   5
# ⁠  / \
# ⁠ 1   4
# / \
# 3   6
# 
# Input: [5,1,4,null,null,3,6]
# Output: false
# Explanation: The root node's value is 5 but its right child's value is 4.
# 
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#
# The idea is for a BST, its left and right subtree are both BST.
# A subtree is BST, if
# 1. it is a leave node
# 2. all left nodes are smaller than root node, and all right nodes are larger
# than root node.
# So for each root node, we set a range, floor to ceiling, for a node, its value 
# must in between this range.
# For a left subtree, its ceiling will be the current root val.
# For a right subtree, its floor will be the current root val.
# We recusively search all subtree, if all nodes satisfiy the range condition,
# then we have a BST. Otherwise, false.
# The complexity is O(n) since we need to access all nodes.
# The space is the O(logn) for recusion stack.

import math

class Solution:
    def check_bst(self, root, floor, ceiling):
        if root == None:
            return True
        
        if root.val <= floor or root.val >= ceiling:
            return False
        
        l = self.check_bst(root.left, floor, root.val)
        r = self.check_bst(root.right, root.val, ceiling)
        return l and r
    
    def isValidBST(self, root: TreeNode) -> bool:
        return self.check_bst(root, -math.inf, math.inf)
