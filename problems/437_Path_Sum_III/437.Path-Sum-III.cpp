// The main problem is the sum is not only from the root or any leave, but any path in the
// middle can give a target sum. The first idea will be find the path sum from root to
// leave, and repeat this for every node as a root. This will give a O(n^2) complexity.
// This idea tell us there are a lot of path computations are repeated. For example:
//
//                          10
//                       /       \
//                      5        -3
//                    /  \         \
//                   3    2        11
//                 /  \    \
//                3   -2    1
//
// If start with 10, the path: 10, 5, 3, 3 give the sum 21. Then do the same thing from
// 5, which give a path 5, 3, 3, and the sum is 11. Can see this 5,3,3 is actually a
// repeated part.
// That led to the next idea which instead of passing down the nodes on the path,
// place the current sum of the prefix path. Since a sum path must be started from
// a node and go down only, it cannot "jump over" some middle nodes on the path. So it
// works like a prefix sum like:
//
//        prefix sum [ ]->             10
//                                 /       \
// prefix sum [10] ->             5        -3 <- prefix sum [10]
//                              /  \         \
// prefix sum [15,5] ->        3    2 <-.    11 <- prefix sum [7, -3]
//                           /  \    \   \
// prefix sum [18,8,3] ->   3   -2    1<. \
//                                       \  prefix sum [15, 5]
//                                        \
//                                         prefix sum [17, 7, 2]
//
//
// For each node, it add itself to the current prefix sum (cur_path_sum), also add itself at the end of the
// prefix sum array. So for example, at the leave node 3, it got the prefix sum array as:
// path:         [10, 5, 3]
// cur_path_sum: [18, 8, 3]
// The prefix sum array, cur_path_sum, each element means, if start from that node, the element value is the
// path sum from that node. So if adding the current node value equal to the target, we find a path that
// can sum to target.
// One issue is the cur_path_sum will be shared between 2 nodes under the same parent node, if using reference
// for cur_path_sum. For example, after visiting the leave node 3, the cur_path_sum reference become:
// [21, 11, 6] (the 4th element is removed by backtracking), and since the parent node 3 has a right child
// -2, the same cur_path_sum will be passed to the -2 node and cause problem. So the backtracking should be
// reverting those summed elements before returning to the parent like removing itself. Or make a copy for each
// children node do they do not interferring each other.
// We have to scan each node, and for each node, need to scan the prefix sum vector which has length of the tree
// height, logn, so the time is O(nlogn), and space is the number of levels of the tree and each level has logn of
// prefix sum vector, os it is O((logn)^2).
//
// So one thing can be optimized is the prefix sum vector scanning. It takes O(logn), but we can just save the
// prefix sum in a counter dict, use the prefix sum as the key and its freq as the value, then we can only
// compute the current sum, and check if the target - current sum found in the counter dict. This is the classic
// prefix sum finding number of subarrays summing to target problem, and can reduce the prefix sum finding to O(1).

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int ans;

    void dfs(TreeNode* root, vector<int> cur_path_sum, int target)
    {
        if (root) {
            for (int i = 0; i < cur_path_sum.size(); i++) {
                cur_path_sum[i] += root->val;
                if (cur_path_sum[i] == target) {
                    this->ans ++;
                }
            }
            cur_path_sum.push_back(root->val);
            if (root->val == target) {
                this->ans ++;
            }
            dfs(root->left, cur_path_sum, target);
            dfs(root->right, cur_path_sum, target);
            cur_path_sum.pop_back();
        }

        return;
    }

    int pathSum(TreeNode* root, int targetSum) {
        vector<int> cur_path_sum;
        dfs(root, cur_path_sum, targetSum);

        return this->ans;

    }
};
