// The question is asking, for any same task, there should be n units between them, example:
// Input: tasks = ["A","A","A","B","B","B"], n = 2
// Output: 8
// Explanation:
// A -> B -> idle -> A -> B -> idle -> A -> B
// ^    ^            ^    ^
// |    |            |    | 2 from previous B
// |                 | 2 from previous A
//
//
// The intuition is to have the minimum number of idle slot, we should pick the most
// frequent task first, then the next frequent task, to fill up the n required cool down time slot,
// so the next question is, how many tasks can be used to fill the slots?
// Example like:
// [ A A A C C C B ], n = 2
// Sort it with their frequence (count), it will be:
// A 3
// C 3
// B 1
//
// Pick A first. To full fill the n required cool down, all A have to be seperated like:
//
// A (slot) (slot) A (slot) (slot) A
//
// We want to use C to fill it those slot. How can C do? Few ways are:
// 1. A C (slot) A C (slot) A C
// 2. A C C A C (slot) A
// 3. A C (slot) A C C A
//
// 2 and 3 do not full fill the cool down requirment (C do not seperated enough), so only 1 is possible.
// So if we want to fill up the idle slot as much as possible (minimize the idle), we want to fill as many as
// other tasks between two same tasks.
//
// A (slot) (slot) A (slot) (slot) A
//   \          /     \          /
//    \  bank  /       \  bank  /
//
// Between two same task, call it "bank". A bank has n slots,
// there are total (bank * n) slots, while bank is max frequence - 1.
// For a task, we like to fill it to all banks if it has
// enough frequence. So for C, which has 3 frequence count:
// A C (slot) A C (slot) A C
// So C can fill up 2 idle slot.
// For B, which has 1 freuence count:
// A C B A C (slot) A C
// so it can fill up 1 idle slot.
// So we can see on the top of the total tasks, we need add one more idle slot in between,
// then the answer is total tasks + remaining slot.
// If all slot can be used up, means all no extra idle slot is needed, the answer is total tasks.
// Why no more extra idle is needed? In:
// [ A A A C C C B ]
//
// The 2nd most frequent task, can only has the same count as the most frequent task by definition,
// otherwise it will be the most frequent task.
// Since the bank is max frequent - 1, the biggest possible count of the 2nd most frequent task is "max frequent",
// hence it can only has the count of the number of banks + 1, ie:
// A [ bank ] A [ bank ] A
//      C          C       C <- can only has 1 more C at the end
//
// The last C will be last task, no more idle slot needed to be added for it. For the next possible task B, it must
// has the frequence lesser than or equal to C, so it must be able to fill in the bank, or adding at the end of the
// schduled tasks.
//
// Time: O(n) for scanning all tasks once for counting the frequence. Space O(n) for storing the counts.




class Solution {
public:
    int leastInterval(vector<char>& tasks, int n) {

        unordered_map<char, int> count_map;
        for (char c : tasks) {
            count_map[c] ++;
        }

        vector<pair<char, int>> counts;
        for (auto p : count_map) {
            counts.push_back(p);
        }

        sort(counts.begin(), counts.end(), [](const pair<char,int>& a, const pair<char,int>& b) {
            if (a.second > b.second) {
                return true;
            } else {
                return false;
            }
        });

        int bank_nr = counts[0].second - 1;
        int slot_nr = bank_nr * n;

        for (int i = 1; i < counts.size(); i++) {
            if (counts[i].second >= bank_nr) {
                slot_nr = slot_nr - bank_nr;
            } else {
                slot_nr = slot_nr - counts[i].second;
            }

            if (slot_nr <= 0) {
                break;
            }
        }

        if (slot_nr <= 0) {
            return tasks.size();
        } else {
            return tasks.size() + slot_nr;
        }

    }
};
