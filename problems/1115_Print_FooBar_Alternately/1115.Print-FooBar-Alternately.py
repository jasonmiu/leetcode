# The question is we want thread foo and bar run alternately,
# and run the foo first. So we use 2 locks, each thread unlock 
# for each other. The lock cannot be reentrant (RLock), as we want 
# to block the same thread before another thread releases its lock.
# We start with the lock b is acquired so the thread bar need is
# blocked at beginning.


import threading

class FooBar:
    def __init__(self, n):
        self.n = n
        self.lock_a = threading.Lock()
        self.lock_b = threading.Lock()
        self.lock_b.acquire()

    def foo(self, printFoo: 'Callable[[], None]') -> None:
        for i in range(self.n):
            self.lock_a.acquire()
            # printFoo() outputs "foo". Do not change or remove this line.
            printFoo()
            self.lock_b.release()
                       
        return


    def bar(self, printBar: 'Callable[[], None]') -> None:
        for i in range(self.n):
            self.lock_b.acquire()
            # printBar() outputs "bar". Do not change or remove this line.
            printBar()
            self.lock_a.release()
                
        return
