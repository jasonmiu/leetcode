// Using condition variable (CV) to single which thread should run.
// CV has 3 components:
// 1. A mutex for locking
// 2. A variable to be protected by the mutex, and having the info we want to single
// 3. CV itself, use to wait on the mutex lock and do the actual singling
//
// In the case, we want foo and bar get printed altenately, so for two threads,
//
// ++ start with foo_turn is true
// |
// |        wait on CV to check if foo_turn is true
// |         |
// |       /```\
// --- foo ----- foo ----- foo
//         V    ^    V    ^        <-- CV single direction
// -------- bar ----- bar ----- bar
// \______/
//    |
// wait on CV to check if foo_turn is false (so its bar turn)
//
//
// Both threads are contesting the varible 'foo_turn', which is protected by mutex m.
// Check and updating this varible is a CS, since a thread want to make sure when it
// accessing the foo_turn, it will not get changed in the middle, of the printing process.
// Once the foo_turn telling a thread it is now its printing turn, that thread holds the lock,
// and another thread has to wait until the foo_turn changed again to tell it is another turn.
// When the current running thread finished its work, it updates the foo_turn and single the CV
// let another thread knows it is done.


#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

class FooBar {
private:
    int n;

    mutex m;
    bool foo_turn;
    condition_variable foo_turn_cv;

public:
    FooBar(int n) {
        this->n = n;
        foo_turn = true;
    }

    void foo(function<void()> printFoo) {

        for (int i = 0; i < n; i++) {

            unique_lock<mutex> foo_lock(m);
            foo_turn_cv.wait(foo_lock, [this]() { return foo_turn; });

        	// printFoo() outputs "foo". Do not change or remove this line.
        	printFoo();

            foo_turn = false;
            foo_lock.unlock();
            foo_turn_cv.notify_one();
        }
    }

    void bar(function<void()> printBar) {

        for (int i = 0; i < n; i++) {

            unique_lock<mutex> foo_lock(m);
            foo_turn_cv.wait(foo_lock, [this]() { return !foo_turn; });

        	// printBar() outputs "bar". Do not change or remove this line.
        	printBar();

            foo_turn = true;
            foo_lock.unlock();
            foo_turn_cv.notify_one();
        }
    }
};
