#
# @lc app=leetcode id=872 lang=python3
#
# [872] Leaf-Similar Trees
#
# https://leetcode.com/problems/leaf-similar-trees/description/
#
# algorithms
# Easy (64.15%)
# Total Accepted:    66.2K
# Total Submissions: 102.5K
# Testcase Example:  '[3,5,1,6,2,9,8,null,null,7,4]\n[3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]'
#
# Consider all the leaves of a binary tree.  From left to right order, the
# values of those leaves form a leaf value sequence.
# 
# 
# 
# For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9,
# 8).
# 
# Two binary trees are considered leaf-similar if their leaf value sequence is
# the same.
# 
# Return true if and only if the two given trees with head nodes root1 and
# root2 are leaf-similar.
# 
# 
# 
# Note:
# 
# 
# Both of the given trees will have between 1 and 100 nodes.
# 
# 
#
# We can see the nodes in leaf sequence are from the left to right of the tree
# The nodes in leaf sequence may not come from the same levels of the tree,
# so not likely BFS.
# We can also see the deepest, left most node in the tree will be the first node in 
# the leaf sequence. For nodes next to it in the leaf sequence, that node must be on the 
# right of this first node or on upper level. Hence this is a DFS search to the left first.
# Complexity O(n) as visited all nodes. Space complexity is O(n) for storing leave nodes.
# The number of leave nodes of a full bin tree is (1+n)/2.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def dfs(root, seq):
    if root.left == None and root.right == None:
        seq.append(root.val)
        return
    
    if root.left:
        dfs(root.left, seq)
    
    if root.right:
        dfs(root.right, seq)
        
    return



class Solution:
    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
        seq1 = []
        seq2 = []
        
        dfs(root1, seq1)
        dfs(root2, seq2)
        
        return (seq1 == seq2)
            
