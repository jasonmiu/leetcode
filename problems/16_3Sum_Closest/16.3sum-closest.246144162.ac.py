#
# @lc app=leetcode id=16 lang=python3
#
# [16] 3Sum Closest
#
# https://leetcode.com/problems/3sum-closest/description/
#
# algorithms
# Medium (45.72%)
# Total Accepted:    392.9K
# Total Submissions: 859.4K
# Testcase Example:  '[-1,2,1,-4]\n1'
#
# Given an array nums of n integers and an integer target, find three integers
# in nums such that the sum is closest to target. Return the sum of the three
# integers. You may assume that each input would have exactly one solution.
# 
# Example:
# 
# 
# Given array nums = [-1, 2, 1, -4], and target = 1.
# 
# The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
# 
# 
#
# The idea is given a number nums[i], found another two numbers nums[j], nums[k] that can min the distance
# between target and nums[i] + nums[j] + nums[k],
# i.e. abs(target - nums[i] + nums[j] + nums[k]) is min
# sort the nums, so:
# nums[0], nums[1] ... nums[i], nums[j], nums[k] ... nums[n-2], nums[n-1]
# and nums[i] <= nums[j] <= nums[k]
# start with i, found the j and k that can min the distance between target
# if the sum of nums[i] + nums[j] + nums[k] > target, means we want to reduce the larger part of the formula,
# so the next k = k - 1 since it is sorted
# if the sum of nums[i] + nums[j] + nums[k] < target, means we want to increase the smaller part of the formula,
# so the next j = j + 1
# The inner loop works like:
# [-55, -24, -18, -11, -7, -3, 4, 5, 6, 9, 11, 23, 33]
#   ^i   ^j --->                             <---  ^k 
# The outter loop works like:
# [-55, -24, -18, -11, -7, -3, 4, 5, 6, 9, 11, 23, 33]
#  --->       ^i   ^j --->                   <---  ^k 
# We search n times for n times of i, so O(n^2)


import math
class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        
        nums = sorted(nums)
        n = len(nums)
        
        min_d = math.inf
        min_sum = math.inf
        dist_zero = False
        for i in range(0, n-1):
            j = i + 1
            k = n - 1
            while k > j:
                s = nums[i] + nums[j] + nums[k]
                d = abs(target - s)
                if d < min_d:
                    min_d = d
                    min_sum = s
                    
                if s > target: # if sum is larger than target, we reduce the larger limit
                    k = k - 1
                elif s < target: # if sum is smaller than target, we increase the smaller limit
                    j = j + 1
                else: # if the sum is equal to target, this is the optimal combination for this i, so go next i
                    dist_zero = True
                    break
                    
            # a code optimzation. If we found a zero distance of a sum and target it must be the min distance. Stop searching        
            if dist_zero:
                break
                
        return min_sum


# Here is the original similar idea but incorrect. This idea is based on 
# given *two* numbers nums[low], nums[up], find the nums[p] that gives the smallest
# distance between target and nums[low] + nums[up] + [p]
# It works like this:
# [-55,       -24,      -18,     -11,    -7,     -3,      4]
#   ^low --->   ^p --->                             <---  ^up
# [-55,       -24,      -18,     -11,    -7,     -3,      4]
#             ^low --->   ^p --->                   <---  ^up
# [-55,       -24,      -18,     -11,    -7,     -3,      4]
#             ^low --->   ^p --->          <---  ^up
# Since both ends can be moved and the search space of p is smaller than the above anwser
# With the same idea to check the sum to see which end (low or up) to move,
# This algor can miss the solution if the low or up boundary moved acrossed the solution
# elements and the p can never search the optimal solution

# import math
#
# class Solution:
#     def threeSumClosest(self, nums: List[int], target: int) -> int:
#
#         nums = sorted(nums)
#         t = target
#
#         min_d = math.inf
#         min_sum = math.inf
#         low = 0
#         up = len(nums) - 1
#
#         while (up - low) > 1:
#             p = low + 1
#             #print("up {}, low {}, p {}".format(up, low, p))
#             this_min_sum = math.inf
#             this_min_d = math.inf
#
#             while p < up:
#                 s = nums[up] + nums[low] + nums[p]
#                 d = abs(t-s)
#                 print("nums[up] {},  nums[low] {},  nums[p] {}, sum {}, d {}".format(nums[up], nums[low], nums[p], s, d))
#
#                 if d <= this_min_d:
#                     this_min_d = d
#                     this_min_sum = s
#
#                 if d <= min_d:
#                     min_d = d
#                     min_sum = s
# #                else:
# #                    print("inner break")
# #                    break
#
#                 #if s == t:
#                    # break
#
#                 p = p + 1
#
#             print("this_min_sum {}\n".format(this_min_sum))
#             if this_min_sum == t:
#                 break
#             elif this_min_sum < t:
#                 low = low + 1
#             elif this_min_sum > t:
#                 up = up - 1
#
#         #print("min_d", min_d)
#         #print("min_sum", min_sum)
#         return min_sum
#
#
