class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {

        sort(nums.begin(), nums.end());

        int min_diff = INT_MAX;
        int min_sum = 0;
        for (int i = 0; i < nums.size(); i++) {

            int j = i + 1;
            int k = nums.size() - 1;

            while (j < k) {
                int s = nums[i] + nums[j] + nums[k];
                if (s > target) {
                    k--;
                } else if (s < target) {
                    j++;
                } else if (s == target) {
                    return s;
                }

                if (abs(target - s) < min_diff) {
                    min_diff = abs(target - s);
                    min_sum = s;
                }
            }
        }

        return min_sum;


    }
};
