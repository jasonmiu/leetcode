#
# @lc app=leetcode id=1238 lang=python3
#
# [1238] Circular Permutation in Binary Representation
#
# https://leetcode.com/problems/circular-permutation-in-binary-representation/description/
#
# algorithms
# Medium (56.44%)
# Total Accepted:    6.1K
# Total Submissions: 9.7K
# Testcase Example:  '2\n3'
#
# Given 2 integers n and start. Your task is return any permutation p of
# (0,1,2.....,2^n -1) such that :
# 
# 
# p[0] = start
# p[i] and p[i+1] differ by only one bit in their binary representation.
# p[0] and p[2^n -1] must also differ by only one bit in their binary
# representation.
# 
# 
# 
# Example 1:
# 
# 
# Input: n = 2, start = 3
# Output: [3,2,0,1]
# Explanation: The binary representation of the permutation is (11,10,00,01). 
# All the adjacent element differ by one bit. Another valid permutation is
# [3,1,0,2]
# 
# 
# Example 2:
# 
# 
# Input: n = 3, start = 2
# Output: [2,6,7,5,4,0,1,3]
# Explanation: The binary representation of the permutation is
# (010,110,111,101,100,000,001,011).
# 
# 
# 
# Constraints:
# 
# 
# 1 <= n <= 16
# 0 <= start < 2 ^ n
# 
# 
#
# This question is actually asking gray code
# https://www.geeksforgeeks.org/generate-n-bit-gray-codes/
# For 1 bit gray code, C(1), the possible codes are
# [0, 1]
# for 2 bits gray code, C(2),  we copy the 1 bit gray codes, reverse their order,
# and concat both parts as first and second halives:
# [0, 1, 1, 0]
#  \     \__ 2nd half
#   \__ 1st half   
# then for first half, add "0" as prefix for each code. For 2nd half, add "1" as prefix for each code
# [00, 01, 11, 10]
# This the gray code each code differ with its adjacency codes by 1 bit, and circular.
# For C(3), we do the same:
# [ 00,  01,  11,  10,  10,  11,  01,  00]
# [000, 001, 011, 010, 110, 111, 101, 100]
# For C(n) we do the same operations on C(n-1)
# Hence this will generate all gray codes. Search the code for the starting number, read it to the end,
# then read from head to the starting number code, ie wrap around all the grap codes starting from the
# starting number.
# Complexity is O(2^n) for generating all gray codes
# Space is also O(2^n) for gray cods

def gray_code(codes, step, n):
    if step == 1:
        codes = ["0", "1"]
    
    if step == n:
        return codes
    
    r_codes = list(reversed(codes))
    com_codes = codes + r_codes
    codes_nr = len(com_codes)
    for (i, s) in enumerate(com_codes):
        if i < codes_nr//2:
            c = "0" + s
        else:
            c = "1" + s
            
        com_codes[i] = c
        
    return gray_code(com_codes, step+1, n)

def num_to_bin_str(x, n):
    s = ""
    for i in range(0, n):
        b = x & 0x01
        x = x >> 1
        if b == 1:
            s = "1" + s
        else:
            s = "0" + s
            
    return s

def bin_str_to_num(s):
    n = len(s)
    num = 0
    for i in range(n-1, -1, -1):
        b = int(s[i])
        p = n - 1 - i
        num = num + (b * pow(2, p))
    return num

class Solution:
    def circularPermutation(self, n: int, start: int) -> List[int]:
        
        all_codes = gray_code([], 1, n)
        
        start_s = num_to_bin_str(start, n)
        start_idx = all_codes.index(start_s)
        
        ans = all_codes[start_idx::] + all_codes[:start_idx]
        ans_num = []
        for a in ans:
            num = bin_str_to_num(a)
            ans_num.append(num)
            
        return ans_num
