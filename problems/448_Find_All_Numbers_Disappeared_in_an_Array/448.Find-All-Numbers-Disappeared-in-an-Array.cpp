void place(vector<int>& nums, int index, int e)
{
    if (nums[index] == e) {
        return;
    }

    if (nums[index] == 0) {
        nums[index] = e;
        return;
    }

    int d = nums[index];
    nums[index] = e;
    place(nums, d, d);
    return;
}

class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {

        nums.insert(nums.begin(), 0);

        for (int i = 1; i < nums.size(); i++) {
            if (nums[i] != i) {
                place(nums, i, 0);
            }
        }

        vector<int> ans;
        for (int i = 1; i < nums.size(); i++) {
            if (nums[i] == 0) {
                ans.push_back(i);
            }
        }

        return ans;

    }
};
