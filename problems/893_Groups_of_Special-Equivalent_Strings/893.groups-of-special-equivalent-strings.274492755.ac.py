#
# @lc app=leetcode id=893 lang=python3
#
# [893] Groups of Special-Equivalent Strings
#
# https://leetcode.com/problems/groups-of-special-equivalent-strings/description/
#
# algorithms
# Easy (63.85%)
# Total Accepted:    19.2K
# Total Submissions: 30K
# Testcase Example:  '["abcd","cdab","cbad","xyzz","zzxy","zzyx"]'
#
# You are given an array A of strings.
# 
# Two strings S and T are special-equivalent if after any number of moves, S ==
# T.
# 
# A move consists of choosing two indices i and j with i % 2 == j % 2, and
# swapping S[i] with S[j].
# 
# Now, a group of special-equivalent strings from A is a non-empty subset S of
# A such that any string not in S is not special-equivalent with any string in
# S.
# 
# Return the number of groups of special-equivalent strings from A.
# 
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: ["a","b","c","a","c","c"]
# Output: 3
# Explanation: 3 groups ["a","a"], ["b"], ["c","c","c"]
# 
# 
# 
# Example 2:
# 
# 
# Input: ["aa","bb","ab","ba"]
# Output: 4
# Explanation: 4 groups ["aa"], ["bb"], ["ab"], ["ba"]
# 
# 
# 
# Example 3:
# 
# 
# Input: ["abc","acb","bac","bca","cab","cba"]
# Output: 3
# Explanation: 3 groups ["abc","cba"], ["acb","bca"], ["bac","cab"]
# 
# 
# 
# Example 4:
# 
# 
# Input: ["abcd","cdab","adcb","cbad"]
# Output: 1
# Explanation: 1 group ["abcd","cdab","adcb","cbad"]
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= A.length <= 1000
# 1 <= A[i].length <= 20
# All A[i] have the same length.
# All A[i] consist of only lowercase letters.
# 
# 
# 
# 
# 
# 
#
# The question is actually asking, if the set of chars in odd index of S, 
# is equal to the set of char in the odd index of T, and 
# if the set of chars in even index of S, is equal to the set of char in the even index of T,
# the S and T is special-equivalent as we can swap those chars in S to match T.
# examples:
# a b c
# c b a
# True. S even set is "a c", T even set is "c a", S odd set is "b", T odd set is "b". 
# we only care the content of set, but we still want to sort the set so both even sets
# become "ac" string so we know they are equal by hashing. Using multiset can solve this
# but multiset is not hashable, make the remaining j+i strings need to be compared with previos
# i strings.
# Cannot use unorder set, as:
# a b c b x d
# a d c b c d
# using unorder set the odd sets become "b d". However they are actually not special-equivalent
# as "b b d" cannot be swapped to become "d b d".
# The trick is use tuple to group the sorted set string as an "object" which tuple is hashable.
# We can make use the hashed tuple to check if the current even and odd sets have a same
# group before.
# The complexity is O(nlogn) because of the sort.
# The space complexity is O(n) for the hash table to store each char.

class Solution:
    def numSpecialEquivGroups(self, A: List[str]) -> int:
        
        even_odd_dict = {}
        groups = 0

        for s in A:
            even = "".join(sorted(s[0::2]))
            odd = "".join(sorted(s[1::2]))

            if not (even, odd) in even_odd_dict:
                groups += 1
                even_odd_dict[(even, odd)] = 1

        return groups


