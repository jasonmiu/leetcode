#
# @lc app=leetcode id=1128 lang=python3
#
# [1128] Number of Equivalent Domino Pairs
#
# https://leetcode.com/problems/number-of-equivalent-domino-pairs/description/
#
# algorithms
# Easy (46.01%)
# Total Accepted:    12.9K
# Total Submissions: 28K
# Testcase Example:  '[[1,2],[2,1],[3,4],[5,6]]'
#
# Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] =
# [c, d] if and only if either (a==c and b==d), or (a==d and b==c) - that is,
# one domino can be rotated to be equal to another domino.
# 
# Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and
# dominoes[i] is equivalent to dominoes[j].
# 
# 
# Example 1:
# Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
# Output: 1
# 
# 
# Constraints:
# 
# 
# 1 <= dominoes.length <= 40000
# 1 <= dominoes[i][j] <= 9
# 
#
# The idea is to hash the dominoes[i], and its reverse. 
# Ie. hash [1, 2], [2, 1]
# Then scan the input list. If we saw the current element and its reverse before
# in the hash, we add pair count.
# A special case is, if we have duplicated element, like:
# [1, 2], [1, 2], [1, 2]
# The number of pair of the 2nd [1,2] is 1 ([1, 2]<->[1,2]).
# The number of pair of the 3rd [1,2] is 2, as it can pair with first and 2nd element.
# So, the number of pair of the current element is the number of seen element in hash table.
# Another tricky part is, for element has same memebers, like [1,1], we do not count its
# reverse as both will be the same and cause double count.
# Complexity is O(n)
# the Space Complexity is O(n) as hash table.

import collections

class Solution:
    def numEquivDominoPairs(self, dominoes: List[List[int]]) -> int:
        
        if len(dominoes) == 0:
            return 0
        
        dc = collections.Counter()
        
        dc[tuple(dominoes[0])] = 1
        rd = tuple(reversed(dominoes[0]))
        dc[rd] = 1
        
        ans = 0
        for d in dominoes[1:]:
            td = tuple(d)
            rd = tuple(reversed(d))
            
            if td in dc:
                ans += dc[td]
            elif rd in dc:
                ans += dc[rd]
                
            dc[td] += 1
            if not td == rd:
                dc[rd] += 1
            
        return ans
                
        
        
