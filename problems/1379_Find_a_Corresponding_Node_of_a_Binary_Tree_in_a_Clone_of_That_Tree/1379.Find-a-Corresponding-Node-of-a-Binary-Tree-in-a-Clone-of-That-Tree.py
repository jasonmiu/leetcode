# If all values of the nodes of the tree are unique, then
# searching the node in the cloned tree is simple. We can just
# get the target value, and do a DFS/BFS to find the same value
# in the cloned tree. But for the follow up question, if repeated
# values are allowed, then we cannot use this method. For example:
#
#                     7
#                 /      \
#               4         3 <-- target
#             /         /   \
#           3          6     9
#         /  \
#        6    9
#
# The target "3" is repeated and also has the same subtree under it,
# so we cannot compare the subtree in the cloned tree for a signal of
# the target node.
#
# Since the path to a target node is unique, we can find the path
# to the target node, which is the traverse direction, left or right.
# A special case is the root is the target node, then we have no
# direction left or right in the path. Use a special "S" marker as the
# starting point of the path which point to the root node.
# As we only has 1 path to the target, for each node if the travered 
# direction to its subtree return no path, we backtrack the tested
# direction and try a new direction.
# Then we can replay the path on cloned tree, with traversing the 
# saved direction for each node.
#
# Complexity is O(n) for visiting all nodes to find the path.
# Space is O(n) which for the saved path.


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def dfs(root, t, path):
    if root == t:
        return path
    
    if root == None:
        return None
    
    path.append("L")
    p = dfs(root.left, t, path)
    if p != None:
        return p
    
    path.pop(-1)
    path.append("R")
    p = dfs(root.right, t, path)
    if p != None:
        return p
    
    path.pop(-1)
    return None

def replay_path(root, path):
    n = None
    for d in path:
        if d == "S":
            n = root
        elif d == "L":
            n = n.left
        elif d == "R":
            n = n.right
            
    return n
        

class Solution:
    def getTargetCopy(self, original: TreeNode, cloned: TreeNode, target: TreeNode) -> TreeNode:
        
        path = dfs(original, target, ["S"])
        n = replay_path(cloned, path)
        
        return n
