#
# @lc app=leetcode id=1079 lang=python3
#
# [1079] Letter Tile Possibilities
#
# https://leetcode.com/problems/letter-tile-possibilities/description/
#
# algorithms
# Medium (74.39%)
# Total Accepted:    23.1K
# Total Submissions: 30.9K
# Testcase Example:  '"AAB"'
#
# You have a set of tiles, where each tile has one letter tiles[i] printed on
# it.  Return the number of possible non-empty sequences of letters you can
# make.
# 
# 
# 
# Example 1:
# 
# 
# Input: "AAB"
# Output: 8
# Explanation: The possible sequences are "A", "B", "AA", "AB", "BA", "AAB",
# "ABA", "BAA".
# 
# 
# 
# Example 2:
# 
# 
# Input: "AAABBC"
# Output: 188
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= tiles.length <= 7
# tiles consists of uppercase English letters.
# 
#
# A sequence can be formed from any number of chars in the input string (tiles)
# in any order.
# Firstly feel like a super set generation problem, but since we can form any order
# so it related to permutation too. We want to find the relationships between the 
# input and sequences it can form. Started with smallest input.
# input: "A" -> {"A"} (possible seq: 1)
# input: "AB" -> {"A", "B", "BA", "AB"} (possible seq: 4)
# To see the relationship between the strings "A" and "AB", we can see 
# "A" is substring of "AB", which is the "AB" - "A", so problem with input "A" is 
# a subproblem of input "AB". 
# For input "AB", if we removed "B" first, and resolve the substring "A", which return
# the subsequences set {"A"}, then we added back the "B" to each sequence in the returned
# subsequences set, then we can form {"A", "BA"}. Repeat this again by removing "A" and resolve
# substring "B", we can form {"A", "BA", "B", "AB"}.
# So this is a resursive problem of removing each char of the input string, and add back this char
# to the returned subsequence. Keep expending the result set and propagate it back up.
# A special case is when we have same chars in the string, we can from the same sequence. Like:
# input: "AA" -> {"A", "AA"}
# We can use the result set for eliminating the same sequences.

def gen(s):
    if len(s) == 1:
        return set(s)
    
    results = set()
    for i in range(0, len(s)):
        p = s[0:i] + s[i+1::]
        c = s[i]
        subseqs = gen(p)
        results = results | subseqs
        for sq in subseqs:
            results.add(c + sq)
            
    return results
        

class Solution:
    def numTilePossibilities(self, tiles: str) -> int:
        seqs = gen(tiles)
        
        return len(seqs)
