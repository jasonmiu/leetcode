#
# @lc app=leetcode id=13 lang=python3
#
# [13] Roman to Integer
#
# https://leetcode.com/problems/roman-to-integer/description/
#
# algorithms
# Easy (53.60%)
# Total Accepted:    515.2K
# Total Submissions: 961.3K
# Testcase Example:  '"III"'
#
# Roman numerals are represented by seven different symbols: I, V, X, L, C, D
# and M.
# 
# 
# Symbol       Value
# I             1
# V             5
# X             10
# L             50
# C             100
# D             500
# M             1000
# 
# For example, two is written as II in Roman numeral, just two one's added
# together. Twelve is written as, XII, which is simply X + II. The number
# twenty seven is written as XXVII, which is XX + V + II.
# 
# Roman numerals are usually written largest to smallest from left to right.
# However, the numeral for four is not IIII. Instead, the number four is
# written as IV. Because the one is before the five we subtract it making four.
# The same principle applies to the number nine, which is written as IX. There
# are six instances where subtraction is used:
# 
# 
# I can be placed before V (5) and X (10) to make 4 and 9. 
# X can be placed before L (50) and C (100) to make 40 and 90. 
# C can be placed before D (500) and M (1000) to make 400 and 900.
# 
# 
# Given a roman numeral, convert it to an integer. Input is guaranteed to be
# within the range from 1 to 3999.
# 
# Example 1:
# 
# 
# Input: "III"
# Output: 3
# 
# Example 2:
# 
# 
# Input: "IV"
# Output: 4
# 
# Example 3:
# 
# 
# Input: "IX"
# Output: 9
# 
# Example 4:
# 
# 
# Input: "LVIII"
# Output: 58
# Explanation: L = 50, V= 5, III = 3.
# 
# 
# Example 5:
# 
# 
# Input: "MCMXCIV"
# Output: 1994
# Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
# 
#
# The idea is to scan the string 1 char by 1 char basically,
# then convert the roman letter to value and accumulate
# to the num_sum. The num_sum will be the result.
# Since we have 6 special cases:
# IV, IX,
# XL, XC,
# CD, CM
# each special case started with I, X, or C and only 2 chars
# in length. So we read ahead of 1 char. If we found a case
# in above 6 cases, we consume 2 chars together and covert
# them to value and accumulate.

class Solution:
    def romanToInt(self, s: str) -> int:
        sl = list(s)
        n = len(sl)
        num_sum = 0
        i = 0
        
        while i < n:
            c = sl[i]
            if i + 1 < n:
                c1 = sl[i+1]
            else:
                c1 = ""
            
            if c == "M":
                num_sum += 1000

            elif c == "C":
                if c1 == "D":
                    num_sum += 400
                    i += 1
                elif c1 == "M":
                    num_sum += 900
                    i += 1
                else:
                    num_sum += 100
                    
            elif c == "X":
                if c1 == "L":
                    num_sum += 40
                    i += 1
                elif c1 == "C":
                    num_sum += 90
                    i += 1
                else:
                    num_sum += 10
                    
            elif c == "I":
                if c1 == "V":
                    num_sum += 4
                    i += 1
                elif c1 == "X":
                    num_sum += 9
                    i += 1
                else:
                    num_sum += 1
                    
            # current char is V, L or D,
            # if we have any special case
            # with them they have been consumed
            # by the previous char. So we can
            # safely add their value here
            elif c == "V":
                num_sum += 5
            elif c == "L":
                num_sum += 50
            elif c == "D":
                num_sum += 500
                    
            i += 1
            
        return num_sum

        
