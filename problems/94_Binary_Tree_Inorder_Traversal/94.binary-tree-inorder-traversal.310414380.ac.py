#
# @lc app=leetcode id=94 lang=python3
#
# [94] Binary Tree Inorder Traversal
#
# https://leetcode.com/problems/binary-tree-inorder-traversal/description/
#
# algorithms
# Medium (59.16%)
# Total Accepted:    614.9K
# Total Submissions: 1M
# Testcase Example:  '[1,null,2,3]'
#
# Given a binary tree, return the inorder traversal of its nodes' values.
# 
# Example:
# 
# 
# Input: [1,null,2,3]
# ⁠  1
# ⁠   \
# ⁠    2
# ⁠   /
# ⁠  3
# 
# Output: [1,3,2]
# 
# Follow up: Recursive solution is trivial, could you do it iteratively?
# 
#
# The inorder is firstly go down to the full left.
# Print the left most first, then current node,
# then goto the right node, then go down to the 
# full left AGAIN. So the main point is we need a pointer
# to move to the right node.
# Firstly go down to the left most path, and use stack to 
# save the nodes along the path, then the top element on the 
# stack will be the current left most node.
# Thats our current node. Pop it, and MOVE the pointer to the 
# current right node. Then stack the left most path again.
# If we have no more elements the loop ends.
# One trick is to set the pointer to the root, but stack it
# IN the top of loop so we can stack the right node as well.
# Complexity O(n)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        
        s = []
        ans = []
        n = root
        while n != None or len(s) > 0:
            while n:
                s.append(n)
                n = n.left
                
            n = s.pop(-1)
            ans.append(n.val)
            n = n.right
            
        return ans
