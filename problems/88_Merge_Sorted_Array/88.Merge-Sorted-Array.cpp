// As mentioned in the python version, this algor is using the empty tailing space of the array nums1.
// The idea is, if we want to have the space in O(1), then we want to do the sorting in place.
// First idea will be comparing two array from beginning, and put the smaller one at the end,
// finally we can reverse the array. However, in this case:
// [7 8 11 15 0 0 0]
// [1 2 13]
//
// If we compare from beginning and but the smaller to the end:
//
//         | the array1 current compare, and the result write position
//         V
// [7 8 11 15 13 2 1]
// [1 2 13]
//         ^
//         | array2 has been processed
//
// If the end of the array1 has the largest element, then after filled the tailing spaces
// the array1 has nothing to compare and will not know which element should be sorted next.
//
// But this idea leads to next one. Since the issue is the tailing space can be used before
// the end of the array1 can be processed, if we process the array1 from the last element,
// then the problem can be solved.
// We want to have a "end" pointer which pointing at the array end of the array1,
// this pointer is always having the current largest element in two arrays in comparing.
// A i pointer points to the array1 last element, and j pointer points to the array2 last element.
// Then for each round, put the larger element to the end pointer. Move the end pointer to the head,
// and the i or j pointer depending on which element is larger at this round. Do until any one input
// array get processed. The remaining elements of another array must be smaller than the current
// end pointer. Copy all remaining elements.
//
// Time O(m+n) as scan once. Space O(1).


class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {

        int end = m + n - 1;
        int i = m  - 1;
        int j = n - 1;

        while (i >= 0 && j >= 0) {

            int a = nums1[i];
            int b = nums2[j];

            if (a >= b) {
                nums1[end] = a;
                i --;
            } else {
                nums1[end] = b;
                j --;
            }

            end --;
        }

        if (i < 0) {
            while (j >= 0) {
                nums1[end] = nums2[j];
                end --;
                j --;
            }
        } else if (j < 0) {
            while (i >= 0) {
                nums1[end] = nums1[i];
                end --;
                i --;
            }
        }

        return;
    }
};
