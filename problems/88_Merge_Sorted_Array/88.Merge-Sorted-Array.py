# A simplest implementation will be, copy all nums2 at the end of the nums1,
# then run a sort of nums1.
#
# k = 0
# for i in range(m, m+n):
#     nums1[i] = nums2[k]
#     k = k + 1
# nums1.sort()
#
# which has time O(nlogn), and space O(1).
#
# Can use space to trade a faster big-O, as a bucket sort.
# Since the input lists are sorted already, always put the smaller
# element to the result list, and copy the result list back to the nums1.
# The implementation detail is, using 2 pointers pointing to the two input
# lists, which each one pointings to the current comparing values, then
# for each result slot we put the smaller value to it, then advance the
# pointer which had a smaller value in comparison. When a pointer is over
# the length, means nothing to compare for that list, just copy the values
# from another list. Issue is, we want to check both pointers are within
# range before doing the comprison.
# Time O(n), space O(n).
#
# Another better solution with space O(1), can be doing a rev lookup,
# using the empty space of num1:
# The way to think about the solution is that we will have to do a reverse sorting.
# We initialize k=m+n-1 as that will be the last location of nums1.
# We will keep checking for the greater element of the two arrays(i=m-1,j=n-1) and insert the values.
# nums1 = [1,2,3,0,0,0], m = 3
# nums2 = [2,5,6],       n = 3
#
# nums1 = [1,2,3,0,0,0]
#              |     |
#              i     k
#
# nums2 = [2,5,6]
#              |
#              j
# nums2[j]>nums1[i] thus nums1[k]=6
# k and j are decremented.
#
# nums1 = [1,2,3,0,0,6]
#              |     |
#              i     k
#
# nums2 = [2,5,6]
#            |
#            j
# nums2[j]>nums1[i] thus nums1[k]=5
# k and j are decremented.
#
# nums1 = [1,2,3,0,5,6]
#              |   |
#              i   k
#
# nums2 = [2,5,6]
#          |
#          j
# We keep following up this procedure and we get the desired reult.

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """

        result = [0] * (m+n)

        i = 0
        x = 0
        y = 0

        for i in range(0, m+n):
            if y >= n:
                result[i] = nums1[x]
                x = x + 1
                continue

            if x >= m:
                result[i] = nums2[y]
                y = y + 1
                continue

            if nums1[x] <= nums2[y]:
                result[i] = nums1[x]
                x = x + 1
            else:
                result[i] = nums2[y]
                y = y + 1

        for i in range(0, m+n):
            nums1[i] = result[i]
