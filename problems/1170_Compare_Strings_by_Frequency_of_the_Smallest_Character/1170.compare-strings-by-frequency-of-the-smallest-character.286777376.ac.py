#
# @lc app=leetcode id=1170 lang=python3
#
# [1170] Compare Strings by Frequency of the Smallest Character
#
# https://leetcode.com/problems/compare-strings-by-frequency-of-the-smallest-character/description/
#
# algorithms
# Easy (58.55%)
# Total Accepted:    21.8K
# Total Submissions: 37.6K
# Testcase Example:  '["cbd"]\n["zaaaz"]'
#
# Let's define a function f(s) over a non-empty string s, which calculates the
# frequency of the smallest character in s. For example, if s = "dcce" then
# f(s) = 2 because the smallest character is "c" and its frequency is 2.
# 
# Now, given string arrays queries and words, return an integer array answer,
# where each answer[i] is the number of words such that f(queries[i]) < f(W),
# where W is a word in words.
# 
# 
# Example 1:
# 
# 
# Input: queries = ["cbd"], words = ["zaaaz"]
# Output: [1]
# Explanation: On the first query we have f("cbd") = 1, f("zaaaz") = 3 so
# f("cbd") < f("zaaaz").
# 
# 
# Example 2:
# 
# 
# Input: queries = ["bbb","cc"], words = ["a","aa","aaa","aaaa"]
# Output: [1,2]
# Explanation: On the first query only f("bbb") < f("aaaa"). On the second
# query both f("aaa") and f("aaaa") are both > f("cc").
# 
# 
# 
# Constraints:
# 
# 
# 1 <= queries.length <= 2000
# 1 <= words.length <= 2000
# 1 <= queries[i].length, words[i].length <= 10
# queries[i][j], words[i][j] are English lowercase letters.
# 
# 
#
# Create the f(x) as problem description.
# The idea is for the words list, we sort it
# for faster search of freq(w) > freq(q[i]).
# use the bisect/bisect_right() to find the index of x
# so the elements on the right on x are all larger (>) than
# x. The number of the elements larger than x is the length
# of array - bisect index. 
# Complexity is O(max(m, n)logn), where n is the number of words, m
# is the number of queries
# Space complexity is O(m+n), for the freq lists.

import collections
import bisect

def f(s):
    smallest_c_ord = 256
    smallest_c = None
    freq = collections.Counter()
    for c in s:
        if ord(c) < smallest_c_ord:
            smallest_c_ord = ord(c)
            smallest_c = c
        freq[c] += 1
        
    return freq[smallest_c]


def search_larger(sorted_a, x):
    idx = bisect.bisect(sorted_a, x)
    return len(sorted_a) - idx
        

class Solution:
    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:
        
        qf = list(map(f, queries))
        wf = list(map(f, words))
        wf.sort()
        ans = [0] * len(queries)
        
        for i in range(0, len(queries)):
            ans[i] = search_larger(wf, qf[i])
            
        return ans
        
        
        
