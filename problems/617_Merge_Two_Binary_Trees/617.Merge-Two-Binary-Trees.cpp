// Instead of thinking create a *whole new* tree as the result,
// think update the input tree, which means I can directly copy
// a subtree to a node. Then it is easier.
// The idea is want to walk both trees, and at a time we are at
// the same node position of both trees. Question is, both trees
// have different structures and a node position can be null in
// another tree.
// One observation is, if a subtree of the current tree is null,
// while the subtree of another is not null, we indeed want to
// copy that non-null subtree to here. For example:
//
//           *1             1
//          /                 \
//         2                   3
//
// At the node 1, we want its right point to another tree's node 3,
// witch is the right subtree.
// If a subtree was present in the original tree, we go down to
// subtree, also together with another tree.
// If another subtree is a null node, means we have nothing to
// add to the output tree, we just set the another subtree node as
// null and go down to the output tree.
//
// This is a single walk algor. Another way to think is,
// For each node, if both side appear, we add them.
// If any side is null, we return the non-null side.
// A left subtree of a node should be a merged left subtree of both tree,
// and same as right subtree. Shown in solution_2();
//
// Time O(n) as access all nodes. Space O(logn) for the hight of tree, which is used in call stack.
//
//
// I got a wrong idea, what to convert a tree to a full tree and encode it as a array, like:
//
//      1
//    /   \
//   2     3
//  /  \
// 4   5
//
// [1 2 3 4 5]
//
//
//      1
//    /   \
//   2     3
//    \
//     5
//
// [1 2 3 null 5]
//
// Then add both array together:
// [2 4 6 4 10]
//
// But a problem is, even I can make a full tree, the tree size becomes (2^H) - 1.
// For a linear shape tree:
//
//  1
//   \
//    2
//     \
//      3
//
// The tree becomes very big, and while in this case the tree hight == n,
// The full tree building become 2^n, which is a bad idea.


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void merge(TreeNode* t1, TreeNode* t2)
{
    if (!t1) {
        return;
    }

    TreeNode* t2_left = nullptr;
    TreeNode* t2_right = nullptr;

    if (t2) {
        t1->val = t1->val + t2->val;
        t2_left = t2->left;
        t2_right = t2->right;
    }

    if (t1->left) {
        merge(t1->left, t2_left);
    } else if (t1->left == nullptr && t2 && t2->left) {
        t1->left = t2->left;
    }

    if (t1->right) {
        merge(t1->right, t2_right);
    } else if (t1->right == nullptr && t2 && t2->right) {
        t1->right = t2->right;
    }

    return;
}

TreeNode* merge2(TreeNode* root1, TreeNode* root2)
{
    if (!root1) {
        return root2;
    }

    if (!root2) {
        return root1;
    }

    if (root1 && root2) {
        root1->val = root1->val + root2->val;
    }

    root1->left = merge2(root1->left, root2->left);
    root1->right = merge2(root1->right, root2->right);

    return root1;
}

TreeNode* solution_1(TreeNode* root1, TreeNode* root2)
{
    if (!root1) {
        return root2;
    }

    if (!root2) {
        return root1;
    }

    merge(root1, root2);
    return root1;
}

TreeNode* solution_2(TreeNode* root1, TreeNode* root2)
{
    return merge2(root1, root2);
}

class Solution {
public:
    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {

        return solution_1(root1, root2);
        //return solution_2(root1, root2);
    }
};
