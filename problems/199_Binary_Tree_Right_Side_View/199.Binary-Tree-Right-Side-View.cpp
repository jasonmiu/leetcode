// The right most nodes, are the last node in the
// BFS queue. So run a BFS, and save all last node
// in each level.

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {

        vector<int> ans;

        if (!root) {
            return ans;
        }

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {

            int qsize = q.size();

            for (int i = 0; i < qsize; i++) {
                TreeNode* n = q.front();
                q.pop();

                if (n->left) {
                    q.push(n->left);
                }
                if (n->right) {
                    q.push(n->right);
                }

                if (i == qsize-1) {
                    ans.push_back(n->val);
                }
            }
        }

        return ans;
    }
};
