#
# @lc app=leetcode id=859 lang=python3
#
# [859] Buddy Strings
#
# https://leetcode.com/problems/buddy-strings/description/
#
# algorithms
# Easy (27.82%)
# Total Accepted:    32K
# Total Submissions: 115.1K
# Testcase Example:  '"ab"\n"ba"'
#
# Given two strings A and B of lowercase letters, return true if and only if we
# can swap two letters in A so that the result equals B.
# 
# 
# 
# Example 1:
# 
# 
# 
# Input: A = "ab", B = "ba"
# Output: true
# 
# 
# 
# Example 2:
# 
# 
# Input: A = "ab", B = "ab"
# Output: false
# 
# 
# 
# Example 3:
# 
# 
# Input: A = "aa", B = "aa"
# Output: true
# 
# 
# 
# Example 4:
# 
# 
# Input: A = "aaaaaaabc", B = "aaaaaaacb"
# Output: true
# 
# 
# 
# Example 5:
# 
# 
# Input: A = "", B = "aa"
# Output: false
# 
# 
# 
# 
# Note:
# 
# 
# 0 <= A.length <= 20000
# 0 <= B.length <= 20000
# A and B consist only of lowercase letters.
# 
# 
# 
# 
# 
# 
# 
#
# We have two special cases.
# Case 1: lengths of A and B are different. Thats no way to make them equal. So False
# Case 2: A and B are the same. If no char in A and B is repeated, then swap any pair of
# chars will make them different hence return False. If they have repeated char, however,
# swapping the repeated char does not change the string. So they are still the same, so 
# return True. eg:
# A = "abc", B = "abc", no repeating char and swapping any pair causes them became different.
# A = "aca", B = "aca", has a repeating char "a", swapping it and two strings are still the same.
# The case 3 is when the strings A and B are different, we enter the 3rd case. To fullfill the condition
# of swapping a pair of chars causing two strings become identical, we scan both strings,
# find the chars that are differnt in two strings. Since we can only swap once, the number of 
# different chars must be 2. Lets the first different char index is 'l', the 2nd different char
# index is 'r', to make the swapping success, A[l] == B[r] and A[r] == B[l], ie:
# A = "abc"
# B = "cba"
#      ^ ^
#    l | | r
# A[l] == B[r] == 'a' and A[r] == B[l] == 'c'
# If not fullfill this case, we return False
# We scan the lists once, and each operation in the loop is constance hence the complexity is O(n).
# We use set() to store each char in the string for checking the repeating char, so the space complexity is O(n).


class Solution:
    def buddyStrings(self, A: str, B: str) -> bool:
        
        # case 1
        if len(A) != len(B):
            return False
        
        a_set = set()
        b_set = set()
        repeat_a = False
        repeat_b = False
        diff_char_idx = []
        for i in range(0, len(A)):
            a = A[i]
            b = B[i]
            
            if a in a_set:
                repeat_a = True
                
            if b in b_set:
                repeat_b = True
                
            a_set.add(a)
            b_set.add(b)
            
            if a != b:
                diff_char_idx.append(i)

        # case 2. Need check this before case 3 since the diff_char_idx 
        # will not be 2 if A == B
        if A == B:
            if repeat_a and repeat_b:
                return True
                
        # case 3. Check the different chars
        if len(diff_char_idx) != 2:
            return False
        else:
            l = diff_char_idx[0]
            r = diff_char_idx[1]
            
            if A[l] == B[r] and A[r] == B[l]:
                return True
            else:
                return False
            
            
        return False
        
        
        
