// Another solution for using no extra space.
// For each node, create a next pointer relation between its left and right children.
// So if this node is not the root node, its next should point to the right.
// The point is how to "cross" the next pointer of of the children to the current next node.
//
//      1
//    /   \
//   2 ==> 3
//  /  \    \
// 4    5    6
//
// Let the current root node, n, is 2. The root node 1 has already bridge up 2 and 3.
// Then we like to bridge up 4 and 5:
//      1
//    /   \
//  *2 ==> 3
//  /  \    \
// 4==> 5    6
//
// To keep track the current next level node we like to bridge, another pointer should always
// point to the node we just bridged up, call it current.
//      1
//    /   \
//  *2 ==> 3
//  /  \    \
// 4==> 5    6
// ^    ^
// |    |
// |--> | current
//
// Then we can move the current root node n to its next, which is 3:
//      1
//    /   \
//   2 ==> 3*
//  /  \    \
// 4==> 5    6
//      ^
//      |current
//
// bridge the current to its first child:
//      1
//    /   \
//   2 ==> 3*
//  /  \    \
// 4==> 5 ==>6
//           ^
//           |current
//
// Once the root node n has null next, we move to the next level.
// But how to move to next level? A trick is to create a dummy node,
// call 'next_level_head', which had the next pointer point to the
// leftest node of each level. With a dummy head node,
// we can generalize the code that always work with a node->next
// without checking if this is a special case head node.



/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    Node* connect(Node* root) {
        Node* n = root;

        while (n) {
            Node next_level_head;
            Node* current = &next_level_head;

            while (n) {
                if (n->left) {
                    current->next = n->left;
                    current = n->left;
                }
                if (n->right) {
                    current->next = n->right;
                    current = n->right;
                }

                n = n->next;
            }

            n = next_level_head.next;
        }

        return root;
    }
};
