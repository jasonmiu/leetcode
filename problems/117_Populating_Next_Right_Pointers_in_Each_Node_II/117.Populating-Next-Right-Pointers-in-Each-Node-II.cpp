// The main idea is to how to walk a tree level.
// Using a BFS, for each queue is store all nodes,
// pop nodes for each level but store the previous node,
// the previous node->next will point to the current node.
// Time: O(n) as access once. Space O(n/2) for queue.

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    Node* connect(Node* root) {

        queue<Node*> q;
        if (!root) {
            return root;
        }

        q.push(root);

        while (!q.empty()) {
            Node* prev = nullptr;
            int qsize = q.size();

            for (int i = 0; i < qsize; i++) {
                Node* n = q.front();
                q.pop();

                if (prev != nullptr) {
                    prev->next = n;
                }

                if (i == qsize-1) {
                    n->next = nullptr;
                }

                prev = n;
                if (n->left) {
                    q.push(n->left);
                }
                if (n->right) {
                    q.push(n->right);
                }
            }
        }

        return root;
    }
};
