// The algor is the same as the python version. This question is basically
// asking us if there is a cycle in the graph. If these is a cycle, means
// there are courses being inter-dependent, hence cannot be finished.
// This implmentation uses graph matrix. Row is the course id we are looking at,
// columns are the prerequisites course id that the current row course depends on.
// We do a DFS to trace all prerequisites. If the current search path contains a
// id we have seen before, means cycle happens.
// Take this example, num courses = 5,
//
//   0 1 2 3 4
//--------------
// 0|F F F F F
// 1|F F F F T
// 2|F F F F T
// 3|F T T F F
// 4|F F F F F
//
// When we check course id 3, the DFS paths are:
// 3 -> 1 -> 4
// 3 -> 2 -> 4
//
// So starting from a starting point, it can has multiple paths,
// which can hit the same node on different paths. It is not a cycle,
// but we need to know we are not searching only 1 paths, but go back
// and try another path. This works like backtracking. So the set "seen"
// for tracking the seen id along the path has to remove the current node
// before returning back to the upper level. This is important otherwise
// the DFS trace will think a node being included in different paths as
// cycle, which is not correct.
//
// Another trick to make the algor faster is when we are looking at a course
// id, it is possible that it has been traced before as a part of dependency
// path. Like example above, when we looking at 3, it trace to id 1, which has
// been traced and know the subpath start from 1 has no cycle. So we can just
// return it as no cycle, it works like memo.
//
// For N course since we build a N*N graph matrix and visit all nodes once,
// time is O(n^2). Space is O(n^2) too for the graph.

class Solution {
public:

    vector<bool> known_ok;

    bool trace(const vector<vector<bool>>& g, int start, unordered_set<int>& seen)
    {
        if (known_ok[start]) {
            return true;
        }

        if (seen.find(start) != seen.end()) {
            return false;
        } else {
            seen.insert(start);
        }

        const vector<bool>& prereqs = g[start];

        for (int i = 0; i < g[start].size(); i++) {
            if (g[start][i] == true) {
                bool r = trace(g, i, seen);
                if (!r) {
                    return false;
                }
            }
        }

        seen.erase(start);
        return true;
    }


    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {

        vector<vector<bool>> g(numCourses);
        known_ok = vector<bool>(numCourses, false);

        for (int i = 0; i < numCourses; i++) {
            g[i] = vector<bool>(numCourses, false);
        }

        for (const vector<int>& req : prerequisites) {
            int a = req[0];
            int b = req[1];

            g[a][b] = true;
        }

        for (int i = 0; i < numCourses; i++) {

            unordered_set<int> seen;

            bool r = trace(g, i, seen);

            if (!r) {
                return false;
            } else {
                known_ok[i] = true;
            }
        }

        return true;
    }
};
