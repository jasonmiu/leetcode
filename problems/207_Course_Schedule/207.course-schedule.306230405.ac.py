#
# @lc app=leetcode id=207 lang=python3
#
# [207] Course Schedule
#
# https://leetcode.com/problems/course-schedule/description/
#
# algorithms
# Medium (39.74%)
# Total Accepted:    322.4K
# Total Submissions: 790.1K
# Testcase Example:  '2\n[[1,0]]'
#
# There are a total of n courses you have to take, labeled from 0 to n-1.
# 
# Some courses may have prerequisites, for example to take course 0 you have to
# first take course 1, which is expressed as a pair: [0,1]
# 
# Given the total number of courses and a list of prerequisite pairs, is it
# possible for you to finish all courses?
# 
# Example 1:
# 
# 
# Input: 2, [[1,0]] 
# Output: true
# Explanation: There are a total of 2 courses to take. 
# To take course 1 you should have finished course 0. So it is possible.
# 
# Example 2:
# 
# 
# Input: 2, [[1,0],[0,1]]
# Output: false
# Explanation: There are a total of 2 courses to take. 
# To take course 1 you should have finished course 0, and to take course 0 you
# should
# also have finished course 1. So it is impossible.
# 
# 
# Note:
# 
# 
# The input prerequisites is a graph represented by a list of edges, not
# adjacency matrices. Read more about how a graph is represented.
# You may assume that there are no duplicate edges in the input prerequisites.
# 
# 
#
# The question is actually asking if a graph contains a cycle.
# If this graph has a cycle, we cannot finish the courses.
# If this graph has no cycle, we can finish the courses.
# The main idea is to use DFS to detech the cycle, in this directed graph
# To use DFS, we convert the edges list to adjacency list first, since all
# courses has a unqiue number, the adjacency list is:
# 0 -> [1, 2]
# 1 -> [2]
# 2 -> []
# for a graph
# 0 --+--> 1
#     |    |
#     |    v
#     +--> 2
# We save the visited nodes. If a node has been visited, it is the subpath
# of the of some nodes we visited before. And we got the ans of that prev
# visited node, so we do not need to visit this node again.
# To find the cycle, we want to know if the current searching path, 
# coming back to the previous node in the current path. So we have a set of nodes
# for saving the node of the current path. We need to remove the current node in the path
# when we return to previous recursion call, since the path instance will be reused.
# Complexity is O(n + e), for n is the number of node (course), and e is the number of edges,
# since we will visit all nodes and edges once.
# Space complexity is O(n + e) as well since we need to save the adjacency list.


def dfs_cycle(alist, n, visited, path):
    if n in path:
        return True
    if n in visited:
        return visited[n]
    
    path.add(n)
    ans = False
    for v in alist[n]:
        ans = dfs_cycle(alist, v, visited, path)
        if ans:
            break
            
    visited[n] = ans
    path.remove(n)
    return ans
        
        
def to_alist(num, prereq):
    alist = []
    for i in range(0, num):
        alist.append([])
        
    for (u, v) in prereq:
        alist[u].append(v)
        
    return alist

class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        visited = dict()
        path = set()
        
        alist = to_alist(numCourses, prerequisites)
        has_cycle = False
        for i in range(0, numCourses):
            if i not in visited:
                has_cycle = dfs_cycle(alist, i, visited, path)
                if has_cycle:
                    return False
                
        return True
            
