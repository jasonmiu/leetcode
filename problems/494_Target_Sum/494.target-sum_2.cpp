// If we expend the tree, for example,
// [1,1,1], target = 3
//
//                                root
//                        /                   \
//                       +1                   -1
//                /       |                    |       \
//              +1       -1                   +1        -1
//         /    |      /  |                /   |         |    \
//       +1    -1     +1  -1              +1   -1        +1   -1
//
//                    \__.__/             \__.__/
//                       |                   |
//                       \                   /
//                      same level, same along path, same subtree
//
// Some subtrees are repeating. Which means at that node, if there is a previous record of
// same level and same current sum along the path, ie. a same subtree has be explored before, we can reuse its explored
// result, which can give a DP algor for speeding up.


struct pair_hash {
    size_t operator() (const pair<int,int>& p) const {
        return p.first + p.second * 37;
    }

};

typedef unordered_map<pair<int,int>, int, pair_hash> memo;


int gen_exp(const vector<int>& a, int start, int cur_sum, int target, memo& s)
{
    if (a.size() == start) {
        if (cur_sum == target) {
            return 1;
        } else {
            return 0;
        }
    }

    pair<int,int> p = make_pair(cur_sum, start);

    if (s.find(p) != s.end()) {
        return s[p];
    } else {

        int r1 = gen_exp(a, start+1, cur_sum + a[start], target, s);
        int r2 = gen_exp(a, start+1, cur_sum - a[start], target, s);

        s[p] = r1+r2;

        return r1 + r2;
    }

}

class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        memo s;
        int r = gen_exp(nums, 0, 0, target, s);

        return r;
    }
};
