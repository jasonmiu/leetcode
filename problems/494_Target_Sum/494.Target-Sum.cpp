// Forward generating the sum, for selecting each element with + / -.
// Since we need only the number of ways, we do not need to save the path, ie.
// the expression of +/-, to compute the expression result. As for every
// downward search, we can just pass on the add or substract the current
// value to the current sum.
// Since every select has two possible choices, the time is O(2^n).
//

void gen_exp(const vector<int>& a, int start, int cur_sum, int target, int& ways)
{
    if (a.size() == start) {
        if (cur_sum == target) {
            ways ++;
        }
        return;
    }

    gen_exp(a, start+1, cur_sum + a[start], target, ways);
    gen_exp(a, start+1, cur_sum - a[start], target, ways);

    return;
}

class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        int ways = 0;

        gen_exp(nums, 0, 0, target, ways);

        return ways;
    }
};
