#
# @lc app=leetcode id=763 lang=python3
#
# [763] Partition Labels
#
# https://leetcode.com/problems/partition-labels/description/
#
# algorithms
# Medium (72.62%)
# Total Accepted:    75.5K
# Total Submissions: 103.8K
# Testcase Example:  '"ababcbacadefegdehijhklij"'
#
# <p>
# A string <code>S</code> of lowercase letters is given.  We want to partition
# this string into as many parts as possible so that each letter appears in at
# most one part, and return a list of integers representing the size of these
# parts.
# </p><p>
# 
# <p><b>Example 1:</b><br />
# <pre>
# <b>Input:</b> S = "ababcbacadefegdehijhklij"
# <b>Output:</b> [9,7,8]
# <b>Explanation:</b>
# The partition is "ababcbaca", "defegde", "hijhklij".
# This is a partition so that each letter appears in at most one part.
# A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it
# splits S into less parts.
# </pre>
# </p>
# 
# <p><b>Note:</b><br><ol>
# <li><code>S</code> will have length in range <code>[1, 500]</code>.</li>
# <li><code>S</code> will consist of lowercase letters (<code>'a'</code> to
# <code>'z'</code>) only.</li>
# </ol></p>
#
# To make partitions, we cut substrings from beginning,
# then cut a substring after that, and so on.
# The idea is for a char that cannot appear in the next substrings (partitions),
# The last appearence of a repeating char will be the boundary of the 
# current partition. For example:
# "abca"
# For char 'a', the last appearence is index 3, and this will be the current boundary
# as it cannot appear in the indices > 3. For char 'b', if the last appearence is smaller
# than the current boundary (index 3), means it can be contained in the current partition,
# otherwise, we update the current boundary to its last appearence, like:
#  01234
# "abcab"
#  i = 0, char = a, boundary = 3
#  i = 1, char = b, boundary = 4
#  i = 2, char = c, boundary = 4
#  i = 3, char = a, boundary = 4
#  i = 4, char = b, boundary = 4
# When the current index is equal to current boundary, we found a partition. Then
# we can start to find next partition from the start index = boundary + 1.
# Since we need only the max index of a char, a function to scan the string to find
# the max index of a char, build_char_max_idx() is created and returns a dict
# of the max index of each char.
# We get all partition boundary indices. To find the length we substract each boundary index of
# its previous boundary index. The first one is set to "-1" since we start index from 0, boundary
# index 0 has also length 1.
# Complexity is O(n), we scanned the string twice, for building the char max index and
# finding each partition boundary.
# Space is O(n) for the char max index dict.

import collections

def build_char_max_idx(s):
    char_max_idx = collections.defaultdict(int)
    for i in range(0, len(s)):
        char_max_idx[s[i]] = i
            
    return char_max_idx

def find_part(s, start, char_max_idx):
    part_end = 0
    for i in range(start, len(s)):
        c = s[i]
        if char_max_idx[c] >= part_end:
            part_end = char_max_idx[c]
            
        if i == part_end:
            break       
            
    return part_end
    

class Solution:
    def partitionLabels(self, S: str) -> List[int]:
        char_max_idx = build_char_max_idx(S)
        part_idxs = [-1]
        ans = []
        start = 0
        part_end = -1
        while part_end < len(S)-1:
            part_end = find_part(S, start, char_max_idx)
            start = part_end + 1
            part_idxs.append(part_end)
            
        for i in range(1, len(part_idxs)):
            ans.append(part_idxs[i] - part_idxs[i-1])
        
        return ans

        
        
