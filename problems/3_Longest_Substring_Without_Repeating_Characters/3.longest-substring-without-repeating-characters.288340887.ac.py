#
# @lc app=leetcode id=3 lang=python3
#
# [3] Longest Substring Without Repeating Characters
#
# https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
#
# algorithms
# Medium (29.12%)
# Total Accepted:    1.2M
# Total Submissions: 4.2M
# Testcase Example:  '"abcabcbb"'
#
# Given a string, find the length of the longest substring without repeating
# characters.
# 
# 
# Example 1:
# 
# 
# Input: "abcabcbb"
# Output: 3 
# Explanation: The answer is "abc", with the length of 3. 
# 
# 
# 
# Example 2:
# 
# 
# Input: "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# 
# 
# 
# Example 3:
# 
# 
# Input: "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3. 
# ⁠            Note that the answer must be a substring, "pwke" is a
# subsequence and not a substring.
# 
# 
# 
# 
# 
#
# The idea is same as the previous submission,
# but this implmenation reset the dict make it much slower
# The previous submission use a "start" var to keep track the
# latest repeated char index which do not need to reset the dict
# and gives a faster runtime

def longsub(s):

    d = {}
    run = 0
    max_run = -99999
    i = 0
    while i < len(s):
        if s[i] not in d:
            d[s[i]] = i
            run += 1
            i += 1
        else:
            rep_idx = d[s[i]]
            d = {}
            i = rep_idx + 1
            if run > max_run:
                max_run = run

            run = 0


    if run > max_run:
        max_run = run

    return max_run



class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        return longsub(s)
        
