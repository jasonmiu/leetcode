// simplest way is to find all substrings which has no repeat chars,
// but gives O(n^2).
// The repeat char test can use hast table to check. Problem is how to
// find the longest length.
// As we do not need to find the actual substring, but just length,
// finding all substrings seems not needed.
// To find length, it can be use the current index, subtract the start index.
// The start index, is the last index we started to look at and so far no repeated
// char. This is the loop invariant. Once a repeated char is found, we move the
// start index to the index of last seen of this repeated char. For example:
// "pwwkew"
//  |||
//  ||seen at 2, here we have seen 'w' at 1 already, so the no repeat substring should start from 1 + 1 (next of the last seen)
//  |seen at 1
//  seen at 0
//
// For each advance of the pointer, we compute the length, by substracting the current index from last start.
// If the current length is the max, save it.
//
// Time: O(n), space O(1)

#include <unordered_map>
#include <algorithm>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> last_seen;

        int start = 0;
        int max_len = 0;
        for (int i = 0; i < s.size(); i++) {
            char c = s[i];

            if (last_seen.find(c) != last_seen.end()) {
                start = max(start, last_seen[c] + 1);
            }

            max_len = max(max_len, (i - start) + 1);
            last_seen[c] = i;
        }

        return max_len;
    }
};
