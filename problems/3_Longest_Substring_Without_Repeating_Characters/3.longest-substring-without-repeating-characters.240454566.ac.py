#
# @lc app=leetcode id=3 lang=python3
#
# [3] Longest Substring Without Repeating Characters
#
# https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
#
# algorithms
# Medium (29.05%)
# Total Accepted:    1.2M
# Total Submissions: 4M
# Testcase Example:  '"abcabcbb"'
#
# Given a string, find the length of the longest substring without repeating
# characters.
# 
# 
# Example 1:
# 
# 
# Input: "abcabcbb"
# Output: 3 
# Explanation: The answer is "abc", with the length of 3. 
# 
# 
# 
# Example 2:
# 
# 
# Input: "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# 
# 
# 
# Example 3:
# 
# 
# Input: "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3. 
# ⁠            Note that the answer must be a substring, "pwke" is a
# subsequence and not a substring.
# 
# 
# 
# 
# 
#
# The idea is we scan the string from left. If we didn't see the current letter before,
# we increase the max len. If we saw that before, we reset the starting point of the
# current sequence (var start) to the last seen index + 1, and count the length between
# the new starting point and the current position. If it is larger than max length,
# update the max length. We save the current letter index in a dict so we can find the
# index for next starting point later.
# Example:
# [a, b, c, a, b, c, b, b]
# 'a', not seen, start = 0, i = 0, maxlen = 1
# 'b', not seen, start = 0, i = 1, maxlen = 2
# 'c', not seen, start = 0, i = 2, maxlen = 3
# 'a', seen, reset the substring with start = 1, i = 3, maxlen = 3
# 'b', seen, reset the substring with start = 2, i = 4, maxlen = 3

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        sl = list(s)
        n = len(sl)
        if n == 0:
            return 0
        if n == 1:
            return 1
        

        maxlen = 0
        start = 0
        subs = dict()
        
        for i in range(0, n):
            c = sl[i]
            
            if c in subs:
                start = max(start, subs[c] + 1) # make sure the new starting point will not go backward
            
            maxlen = max(maxlen, i - start + 1)
            subs[c] = i
            
        return maxlen
