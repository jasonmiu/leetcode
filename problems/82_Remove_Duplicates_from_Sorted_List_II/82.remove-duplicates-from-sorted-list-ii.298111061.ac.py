#
# @lc app=leetcode id=82 lang=python3
#
# [82] Remove Duplicates from Sorted List II
#
# https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/description/
#
# algorithms
# Medium (34.45%)
# Total Accepted:    219.3K
# Total Submissions: 624.9K
# Testcase Example:  '[1,2,3,3,4,4,5]'
#
# Given a sorted linked list, delete all nodes that have duplicate numbers,
# leaving only distinct numbers from the original list.
# 
# Example 1:
# 
# 
# Input: 1->2->3->3->4->4->5
# Output: 1->2->5
# 
# 
# Example 2:
# 
# 
# Input: 1->1->1->2->3
# Output: 2->3
# 
# 
#
# Since the list is sorted, we know the duplicated numbers will be clustered together.
# If we scan to the end of the list, we want to find which node is the starting point
# of the duplicated sequence, like:
#
# 1 2 3 3 4
# | | | | |
#     | |
# At 4 we want to know when the hill of 3 start. We want to remove the newly 
# inputted 3s and get back the 2. It is last in first out sounds like a place for stack.
# We use a counter dict to count how many times we saw a number. If the counter of a 
# number is >= 2, we know it is duplicated. If we count a duplicated number when it
# starts to repeat, we know how far we like to rewind our stack. 
# We rewind back the stack to the pior of the duplicated number, get that node
# and link it to the current node which effectively skipped the duplciated nodes.
# Few special cases we need to handle:
# 1. when we rewind back the stack becomes empty, means the duplicated number happens
# at the beginning of the list, like [1, 1, 1, 2], we assign the head to the current node.
# 2. After we visited the whole list, the element of the stack top has counter >= 2. Means 
# the deplicated number happens at the end of the list, like [1, 2, 2, 2]. We rewind the
# stack and assign the 'next' field of the last node to None.
# 3. After we visited the whole list, the element of the stack top has counter >= 2, and 
# after we rewinded the stack, stack becomes empty. It is the combination of case 1, 2.
# Means all nodes in the list are deplicated, like [1, 1, 2, 2]. We set head as None
# and return means it is a empty list.
# Complexity is O(n) for scanning the list. The space complexity is O(n) for the 
# counter dict + stack.
#
# Have thought the similar 2 pointers idea as well:
#public class Solution {
#public ListNode deleteDuplicates(ListNode head) {
#	//use two pointers, slow - track the node before the dup nodes,
#	// fast - to find the last node of dups.
#    ListNode dummy = new ListNode(0), fast = head, slow = dummy;
#    slow.next = fast;
#    while(fast != null) {
#    	while (fast.next != null && fast.val == fast.next.val) {
#     		fast = fast.next;    //while loop to find the last node of the dups.
#    	}
#    	if (slow.next != fast) { //duplicates detected.
#    		slow.next = fast.next; //remove the dups.
#    		fast = slow.next;     //reposition the fast pointer.
#    	} else { //no dup, move down both pointer.
#    		slow = slow.next;
#    		fast = fast.next;
#    	}
#    	
#    }
#    return dummy.next;
#} }


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

import collections

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        
        if head == None:
            return None
        
        cnt = collections.Counter()
        s = [head]
        cnt[head.val] += 1
        n = head.next
        
        while n:
            if n.val != s[-1].val and cnt[s[-1].val] >= 2:
                top = s[-1].val
                while cnt[top] > 0:
                    e = s.pop(-1)
                    cnt[e.val] -= 1
                    
                if len(s) > 0:
                    p = s[-1]   
                    p.next = n
                else:
                    head = n
                
            s.append(n)
            cnt[n.val] += 1
            n = n.next
            
        if cnt[s[-1].val] >= 2:
            top = s[-1].val
            while cnt[top] > 0:
                e = s.pop(-1)
                cnt[e.val] -= 1
                
            if len(s) > 0:
                p = s[-1]
                p.next = None
            else:
                head = None
            
        return head
