// Since the list is sorted, so the duplicated values must appear conscutively.
// With a slow fast pointer algor, the idea is to have the fast pointer move forward,
// to search the next dupication. Since we need to remove the found duplications,
// a previous node that points to the source node is needed, which is the role of the
// slow pointer. The slow pointer should always behind the fast pointer.
// For example:
//
// NEW_HEAD -> 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5
//      ^      ^
//      s      f
//
// The NEW_HEAD is actually a dummy node to preserve the head.
// Move the slow and fast pointer *together* one node by one node.
// Two cases:
// 1. Fast pointer see no dupication. Move forward together.
// NEW_HEAD -> 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5
//             ^    ^
//             s    f
//
// 2. Fast pointer found duplication at the current position:
// NEW_HEAD -> 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5
//                  ^    ^
//                  s    f <- fast pointer see duplication
//
//    The move the fast pointer to the end of the duplicated sequence:
// NEW_HEAD -> 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5
//                  ^         ^
//                  s         f
//
//    Now the slow pointer next is going to point to the fast pointer next,
//    which has the effect of jump over all duplicated nodes:
//
//                  +--------------+
//                  |              V
// NEW_HEAD -> 1 -> 2    3 -> 3 -> 4 -> 4 -> 5
//                  ^              ^
//                  s              f <-- move the fast pointer to the new slow->next
//
// The fast pointer move to the new node pointed by slow next, as to skip all
// discovered duplicated nodes.
//
// The NEW_HEAD dummy node trick can handle the duplicated head nodes nicely:
//
//
// NEW_HEAD -> 1 -> 1 -> 3 -> 3 -> 4 -> 4 -> 5
//      ^      ^
//      s      f <-- fast pointer found duplication, move to the end of the sequence
//
// NEW_HEAD -> 1 -> 1 -> 3 -> 3 -> 4 -> 4 -> 5
//      ^           ^
//      s           f
//      |
//      slow pointer here has a next pointer that can point to the fast point next,
//      what has the effect of the removed head.
//
// Since removing head from a linked list is always a special case, with a dummy head,
// we make the linked list head will never be removed always, so have a general case
// of slow pointer always pointing node behind of fast pointer.
// Should think about this trick every time when head updating can be happened.
//
// Time O(n) for accessing all nodes once. Space O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {

        ListNode new_head;

        ListNode* slow = &new_head;
        ListNode* fast = head;

        slow->next = fast;
        while (fast != nullptr) {

            while (fast->next != nullptr && fast->val == fast->next->val) {
                fast = fast->next;
            }

            if (slow->next != fast) {
                slow->next = fast->next;
                fast = slow->next;
            } else {
                slow = slow->next;
                fast = fast->next;
            }
        }

        return new_head.next;
    }
};
