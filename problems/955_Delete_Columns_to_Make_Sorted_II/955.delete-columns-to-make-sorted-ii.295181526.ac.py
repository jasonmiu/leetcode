#
# @lc app=leetcode id=955 lang=python3
#
# [955] Delete Columns to Make Sorted II
#
# https://leetcode.com/problems/delete-columns-to-make-sorted-ii/description/
#
# algorithms
# Medium (32.69%)
# Total Accepted:    7.8K
# Total Submissions: 23.9K
# Testcase Example:  '["ca","bb","ac"]'
#
# We are given an array A of N lowercase letter strings, all of the same
# length.
# 
# Now, we may choose any set of deletion indices, and for each string, we
# delete all the characters in those indices.
# 
# For example, if we have an array A = ["abcdef","uvwxyz"] and deletion indices
# {0, 2, 3}, then the final array after deletions is ["bef","vyz"].
# 
# Suppose we chose a set of deletion indices D such that after deletions, the
# final array has its elements in lexicographic order (A[0] <= A[1] <= A[2] ...
# <= A[A.length - 1]).
# 
# Return the minimum possible value of D.length.
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: ["ca","bb","ac"]
# Output: 1
# Explanation: 
# After deleting the first column, A = ["a", "b", "c"].
# Now A is in lexicographic order (ie. A[0] <= A[1] <= A[2]).
# We require at least 1 deletion since initially A was not in lexicographic
# order, so the answer is 1.
# 
# 
# 
# Example 2:
# 
# 
# Input: ["xc","yb","za"]
# Output: 0
# Explanation: 
# A is already in lexicographic order, so we don't need to delete anything.
# Note that the rows of A are not necessarily in lexicographic order:
# ie. it is NOT necessarily true that (A[0][0] <= A[0][1] <= ...)
# 
# 
# 
# Example 3:
# 
# 
# Input: ["zyx","wvu","tsr"]
# Output: 3
# Explanation: 
# We have to delete every column.
# 
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= A.length <= 100
# 1 <= A[i].length <= 100
# 
# 
# 
# 
# 
# 
# 
#
# The idea is to understand the definition of lex ordered
#    If two words have the same first letter, we compare the second. 
#      If the second letters are the same, we compare the third, etc. 
#      Finally, one word comes before the other if the first differing letter 
#      comes before the corresponding letter.
#    If two words are identical up to the length of the shorter word, the shorter word comes first.
# For each pair of words in array A, A[i] and A[j], if A[j] => A[i], its lex ordered.
# We want to compare the the whole word including the prefix, in case we deleted a char, we 
# want the next compare the prefix as well. And we want to keep the prefix all lex ordered,
# so we just need to keep compare the word by moving the column.
# For example:
# [ x g a ]
# [ x f b ]
# [ y f a ]
# current col is 0, x <= x <= y, this col is ordered,  we move col to col + 1
# current col is 1, xg > xf, from this col the word is not ordered, we want to delete this col, so
# become:
# [ x a ]
# [ x b ]
# [ y a ]
# a same problem with a new input. And the current col is still 1, since we removed the last col
# and the prefix columns are lex ordered.
# current col is 1, xa < xb < xy, this col is ordered, we move col to col + 1
# cuurent col is 2, which is >= the len of the current word. We have done the algor,
# and pass back the deleted col count.
# complexity O(n * m^2), while the m is the len of word, m^2 is for the each column comparision
# and we need to do this n times, so * n. Space complexity is O(n*m) for the table.

def check_col(A, col, d):
    if col >= len(A[0]):
        return d

    has_del = False
    for i in range(1, len(A)):
        if A[i][0:col+1] < A[i-1][0:col+1]:
            d = d + 1
            has_del = True
            break
            
    ans = 0
    if has_del:
        for i in range(0, len(A)):
            del A[i][col]
        ans = check_col(A, col, d)
            
    else:
        ans = check_col(A, col+1, d)
    
    return ans
    
class Solution: 
    
    def minDeletionSize(self, A: List[str]) -> int:
        B = [list(a) for a in A]
        
        ans = check_col(B, 0, 0)
       
        return ans
