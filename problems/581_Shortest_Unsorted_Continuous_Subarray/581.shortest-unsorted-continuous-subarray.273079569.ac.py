#
# @lc app=leetcode id=581 lang=python3
#
# [581] Shortest Unsorted Continuous Subarray
#
# https://leetcode.com/problems/shortest-unsorted-continuous-subarray/description/
#
# algorithms
# Easy (30.54%)
# Total Accepted:    85.7K
# Total Submissions: 280.5K
# Testcase Example:  '[2,6,4,8,10,9,15]'
#
# Given an integer array, you need to find one continuous subarray that if you
# only sort this subarray in ascending order, then the whole array will be
# sorted in ascending order, too.  
# 
# You need to find the shortest such subarray and output its length.
# 
# Example 1:
# 
# Input: [2, 6, 4, 8, 10, 9, 15]
# Output: 5
# Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the
# whole array sorted in ascending order.
# 
# 
# 
# Note:
# 
# Then length of the input array is in range [1, 10,000].
# The input array may contain duplicates, so ascending order here means . 
# 
# 
#
# Sort the whole list, and compare it to the original unsorted one.
# The elements need to be sorted, the position after sorting must be
# different from the original list. Find the index from left and the
# index from right, they contains the shortest sub-list that has been
# sorted.
# The complexity is O(nlogn), because of the sort.
# The space complexity is O(n) for the sorted list.

class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        
        ss = sorted(nums)
        cnt = 0
        
        s_idx = 0
        has_s_idx = False
        e_idx = 0
        has_e_idx = False
        i = 0
        j = len(nums) - 1
        
        while i < len(nums):
            # Oncer we found the start and end indeice, 
            # we need not to find further
            if ss[i] != nums[i] and not has_s_idx:
                s_idx = i
                has_s_idx = True
                
            if ss[j] != nums[j] and not has_e_idx:
                e_idx = j
                has_e_idx = True
                
            i += 1
            j -= 1
            
        #print("s_idx", s_idx)
        #print("e_idx", e_idx)
                
        # found no change in sorted positions. Original list is sorted
        if (not has_s_idx) and (not has_e_idx):
            return 0
                
        return e_idx - s_idx + 1
