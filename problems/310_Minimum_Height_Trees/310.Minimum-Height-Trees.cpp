// The first idea is to find a tree height of setting a node as root,
// then find all n tree heights by setting n nodes as root.
// Use the DFS to search the tree height, and this will cost O(n).
// And repeat this n times, the total time complexity is O(n^2).
// And then this get TLE.
//
// Have the O(n) is hard. There are few important ideas for the
// properties of tree:
//
// 1. For any two nodes, their distance is their edges along their path,
//    since it is a tree, any two nodes are connected ONLY one edge. So
//    the case of multiple paths to a node in the general graph case like
//    this will not happend:
//    1-----------------------3
//    |                       |
//    |                       |
//    +-----------2-----------+
//
//    in this case 3 has two paths to it, it is NOT a tree.
//
// 2. If a tree has only 1 node, its height must be 0.
// 3. If a tree has only 2 node, its height must be 1, since two nodes has only 1 edge
//    to connect together.
// 4. If a tree has 3 nodes, two tree cases can form, with edges:
//    [1, 2], [1, 3]
//
//                       1                           2
//                      / \                          |
//                     /   \                         |
//                    /     \                        |
//                   /       \                       1
//                  /         \                      |
//                 2           3                     |
//                                                   |
//                                                   3
//
// The left case has height 1. The right case has height 2.
// We like to have a node which has more edges as root. The idea is,
// if a node has more edges (higher degree), it has more nodes for 1 distances.
// For the next level, if do the same thing, it will also has more nodes for the this
// level, which is better than selecting lower degree nodes.
//
//
//                2    5                     1
//                |   /                 / |  |  |  \
//            3---1---+    <==         2  3  4  5   6
//                |   \
//                4    6
//
//
// Rearrange the tree using 1 as root to a "star" shape, with the root at the center.
// We can see the root with higher degree is actually the center of the graph.
// So the problem is indeed to find the best center nodes, which can only be one node,
// or 2 nodes. For two nodes case since both sides can be balance like this:
//
//               3      4
//               |      |
//               1 ---- 2
//               |      |
//               5      6
//
// Become two rooted trees:
//
//               1                          2
//             / | \                      / | \
//            3  5  2                    4  6  1
//                 / \                        / \
//                4   6                      3   5
//
// using 1 or 2 as root both give same height tree (2).
//
// So the aglorthm is similar to the BFS topological sort, for every around,
// remove the leave nodes that has only 1 edge. Remove their edges as well,
// so their adjacent nodes degree will be decreased too. Once we found a leaf node
// put it for the next around processing.
// For each around actually its like walking down 1 level with BFS. Remove
// the nodes until we have only 2 or 1 nodes.
//
// For example:
//
//               3      4
//               |      |
//               1 ---- 2    7
//               |      |   /
//               5      6--+--8
//
// First around, leaf nodes are: 3, 4, 5, 7, 8, remove them, also their edges:
//
//
//
//               1 ---- 2
//                      |
//                      6
//
// Second round, leaf nodes are 1, 6, remove them, also their edges:
//
//               2
//
//
// Which has min height 2, and the original tree with root 2:
//
//                   2
//                /  |  \
//               1   4   6
//             / |       | \
//            3  5       7  8
//
// Since only access all nodes once, the time complexity is O(n). Space is O(n).



class Solution {
public:
    vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges) {

        if (n <= 2) {
            vector<int> ans;
            for (int i = 0; i < n; i++) {
                ans.push_back(i);
            }
            return ans;
        }

        vector<unordered_set<int>> g(n);
        for (const vector<int>& e : edges) {
            int a = e[0];
            int b = e[1];

            g[a].insert(b);
            g[b].insert(a);
        }

        int nr_nodes = n;
        queue<int> leaves;
        for (int i = 0; i < nr_nodes; i++) {
            if (g[i].size() <= 1) {
                leaves.push(i);
            }
        }

        while (nr_nodes > 2) {
            nr_nodes = nr_nodes - leaves.size();
            int leaves_size = leaves.size();
            for (int i = 0; i < leaves_size; i++) {

                int leaf_node = leaves.front();
                leaves.pop();

                int leaf_connect = *(g[leaf_node].begin()); // only 1 node connecting to a leaf node
                g[leaf_node].erase(leaf_connect);
                g[leaf_connect].erase(leaf_node);

                if (g[leaf_connect].size() == 1) {
                    leaves.push(leaf_connect);
                }
            }
        }

        vector<int> ans;
        while (!leaves.empty()) {
            ans.push_back(leaves.front());
            leaves.pop();
        }


        return ans;
    }
};
