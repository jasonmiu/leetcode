#
# @lc app=leetcode id=4 lang=python3
#
# [4] Median of Two Sorted Arrays
#
# https://leetcode.com/problems/median-of-two-sorted-arrays/description/
#
# algorithms
# Hard (27.64%)
# Total Accepted:    532.4K
# Total Submissions: 1.9M
# Testcase Example:  '[1,3]\n[2]'
#
# There are two sorted arrays nums1 and nums2 of size m and n respectively.
# 
# Find the median of the two sorted arrays. The overall run time complexity
# should be O(log (m+n)).
# 
# You may assume nums1 and nums2 cannot be both empty.
# 
# Example 1:
# 
# 
# nums1 = [1, 3]
# nums2 = [2]
# 
# The median is 2.0
# 
# 
# Example 2:
# 
# 
# nums1 = [1, 2]
# nums2 = [3, 4]
# 
# The median is (2 + 3)/2 = 2.5
# 
# 
#
# https://www.youtube.com/watch?v=LPFhl65R7ww
# The idea is, by defintion of the median, for the sorted result set, cut it in 2 halves, left half and right half
# are the same size.
# We want to find a partition, for nums1 and nums2, which the resulted 2 halves are the same size
# x = [1, 2]
# y = [3, 4]
# starting partition from the smaller list
# part_x = 1
# part_y = 1
#   Left   |   Right
# x [1]    |   [2]
# y [3]    |   [4]
# the max of left x, the min of right y, the max of left y, and the min of right x,
# is the boundary of the resulted joined list, so the median will be in this 4 numbers
# we want to make sure the all numbers on left hand side, are smaller than right hand side
# so if max of left x <= min of right y, and max of left y <= min of right x,
# we found the boundary
# if not, and if the max of left x is > min of right y, means we are now too "right" from the 
# real median, we should shift the partition left, so the high bound of the search space is -1
# if max of left y is > min of right x, means we are too "left" from the real median so we shift
# the partition to right, hence the low bound of the search space is +1


import math
class Solution:
    
    def find_median(self, nums1, nums2, evencase):
        if len(nums1) <= len(nums2):
            nums_x = nums1
            nums_y = nums2
        else:
            nums_x = nums2
            nums_y = nums1
            
        low = 0
        high = len(nums_x)
        
        while (low <= high):
            part_x = (low + high) // 2
            part_y = ((len(nums_x) + len(nums_y) + 1) // 2) - part_x
            
            if part_x == 0:
                max_left_x = -math.inf
            else:
                max_left_x = nums_x[part_x-1]
            
            if part_x == len(nums_x):
                min_right_x = math.inf
            else:
                min_right_x = nums_x[part_x]
                
            if part_y == 0:
                max_left_y = -math.inf
            else:
                max_left_y = nums_y[part_y-1]
                
            if part_y == len(nums_y):
                min_right_y = math.inf
            else:
                min_right_y = nums_y[part_y]
                
            if (max_left_x <= min_right_y) and (max_left_y <= min_right_x):
                
                if evencase:
                    return (max(max_left_x, max_left_y) + min(min_right_x, min_right_y)) / 2
                else:
                    return max(max_left_x, max_left_y)
                
            elif max_left_x > min_right_y:
                high = part_x - 1
            else:
                low = part_x + 1
            
    
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        
        evencase = False
        if (len(nums1) + len(nums2)) % 2 == 0:
            evencase = True
        
        return self.find_median(nums1, nums2, evencase)
