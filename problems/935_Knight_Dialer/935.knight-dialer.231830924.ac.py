# TO EXPLAIN
#
# @lc app=leetcode id=935 lang=python3
#
# [935] Knight Dialer
#
# https://leetcode.com/problems/knight-dialer/description/
#
# algorithms
# Medium (42.51%)
# Total Accepted:    20K
# Total Submissions: 47K
# Testcase Example:  '1'
#
# A chess knight can move as indicated in the chess diagram below:
# 
# .           
# 
# 
# 
# This time, we place our chess knight on any numbered key of a phone pad
# (indicated above), and the knight makes N-1 hops.  Each hop must be from one
# key to another numbered key.
# 
# Each time it lands on a key (including the initial placement of the knight),
# it presses the number of that key, pressing N digits total.
# 
# How many distinct numbers can you dial in this manner?
# 
# Since the answer may be large, output the answer modulo 10^9 + 7.
# 
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: 1
# Output: 10
# 
# 
# 
# Example 2:
# 
# 
# Input: 2
# Output: 20
# 
# 
# 
# Example 3:
# 
# 
# Input: 3
# Output: 46
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= N <= 5000
# 
# 
# 
# 
# 
#
# The idea is to use DFS to simulate the dial. Since each digit can lead to
# certain other digits with knight path. So we firstly build a adjacency matrix
# for next possible digits.
# Since we want to have N-1 hops, means we want to see if we can complete a DFS
# path with N nodes. If we can, then we have 1 possible phone number.
# If we can't, like get stuck to digit 5 which can go nowhere, then we return 0.
# When building the tree, we can see for i_th step of the dialing, we may return
# to the same digit. We can use DP to memorize those result, with:
# DP[(digit, level)]. If we returned to the same situration, we can reuse the
# memory.
# Complexity O(n), since we cached each level in n, when we see same digit again
# we have O(1) operation to get back the result. The space complexity is O(n)
# for DP cache.

class Solution:
    
    cache = {}
    
    knight_steps = {}
    knight_steps[0] = [4, 6]
    knight_steps[1] = [6, 8]
    knight_steps[2] = [7, 9]
    knight_steps[3] = [4, 8]
    knight_steps[4] = [3, 9, 0]
    knight_steps[5] = []
    knight_steps[6] = [1, 7, 0]
    knight_steps[7] = [2, 6]
    knight_steps[8] = [1, 3]
    knight_steps[9] = [2, 4]
    
    def dfs(self, root, level):
        # leave node case 1
        if level == 0:
            return 1
        
        # leave node case 2
        if len(self.knight_steps[root]) == 0:
            return 0
        
        if (root, level) in self.cache:
            return self.cache[(root, level)]
        
        leave_nodes = 0
        for n in self.knight_steps[root]:
            w = self.dfs(n, level-1)
            leave_nodes += w
        
        self.cache[(root, level)] = leave_nodes
        
        return leave_nodes
            

    def knightDialer(self, N: int) -> int:
        
#        self.cache = {}
        
        m = 10**9 + 7

        w = 0
        for i in range(0, 10):
            w = self.dfs(i, N-1) + w

        return w % m
