#
# @lc app=leetcode id=841 lang=python3
#
# [841] Keys and Rooms
#
# https://leetcode.com/problems/keys-and-rooms/description/
#
# algorithms
# Medium (61.60%)
# Total Accepted:    58.5K
# Total Submissions: 93.2K
# Testcase Example:  '[[1],[2],[3],[]]'
#
# There are N rooms and you start in room 0.  Each room has a distinct number
# in 0, 1, 2, ..., N-1, and each room may have some keys to access the next
# room. 
# 
# Formally, each room i has a list of keys rooms[i], and each key rooms[i][j]
# is an integer in [0, 1, ..., N-1] where N = rooms.length.  A key rooms[i][j]
# = v opens the room with number v.
# 
# Initially, all the rooms start locked (except for room 0). 
# 
# You can walk back and forth between rooms freely.
# 
# Return true if and only if you can enter every room.
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: [[1],[2],[3],[]]
# Output: true
# Explanation:  
# We start in room 0, and pick up key 1.
# We then go to room 1, and pick up key 2.
# We then go to room 2, and pick up key 3.
# We then go to room 3.  Since we were able to go to every room, we return
# true.
# 
# 
# Example 2:
# 
# 
# Input: [[1,3],[3,0,1],[2],[0]]
# Output: false
# Explanation: We can't enter the room with number 2.
# 
# 
# Note:
# 
# 
# 1 <= rooms.length <= 1000
# 0 <= rooms[i].length <= 1000
# The number of keys in all rooms combined is at most 3000.
# 
# 
#
# The keys in a room means edges to the next rooms, for eg:
# room[0] = [1 2 3]
# means:
# 0 --+--> 1
#     +--> 2
#     +--> 3
# Since we can *freely* move between the rooms, means if we have
# a key we must able to access that room. We can use a DFS to travel
# all rooms and save the visited rooms. If we can visited all rooms,
# then return True.
# Thought about using union-find as thought this can be a problem
# to detect if all nodes are connected. The problem is the graph is
# directed, so a case like:
# 0 ----> 1
# ^       ^
# |       |
# 2 ----> 3
# The union-find will say True since they are connected but indeed
# room 2 and 3 are not accessible. So U-F algor is better for
# undirected case.
# Complexity O(N+E), E is the number of keys.
# Space is O(N) for the visited hash.

def dfs(rooms, rid, visited):
    if rid in visited:
        return
    
    visited.add(rid)
    keys = rooms[rid]
    for k in keys:
        dfs(rooms, k, visited)
        
    return
    

class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:
        visited = set()
        
        dfs(rooms, 0, visited)
        
        if len(visited) != len(rooms):
            return False
        else:
            return True
