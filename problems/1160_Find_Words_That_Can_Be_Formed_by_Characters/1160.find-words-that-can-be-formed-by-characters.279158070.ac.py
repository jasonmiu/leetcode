#
# @lc app=leetcode id=1160 lang=python3
#
# [1160] Find Words That Can Be Formed by Characters
#
# https://leetcode.com/problems/find-words-that-can-be-formed-by-characters/description/
#
# algorithms
# Easy (66.55%)
# Total Accepted:    22.3K
# Total Submissions: 33.5K
# Testcase Example:  '["cat","bt","hat","tree"]\n"atach"'
#
# You are given an array of strings words and a string chars.
# 
# A string is good if it can be formed by characters from chars (each character
# can only be used once).
# 
# Return the sum of lengths of all good strings in words.
# 
# 
# 
# Example 1:
# 
# 
# Input: words = ["cat","bt","hat","tree"], chars = "atach"
# Output: 6
# Explanation: 
# The strings that can be formed are "cat" and "hat" so the answer is 3 + 3 =
# 6.
# 
# 
# Example 2:
# 
# 
# Input: words = ["hello","world","leetcode"], chars = "welldonehoneyr"
# Output: 10
# Explanation: 
# The strings that can be formed are "hello" and "world" so the answer is 5 + 5
# = 10.
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= words.length <= 1000
# 1 <= words[i].length, chars.length <= 100
# All strings contain lowercase English letters only.
# 
#
# The idea is to create a multi-set (Counter set), let each letter in 
# chars as the keys, the numbers of letters as the values. For each word
# in the input list, count each letter in the word to see if it is in the 
# set, and the set still has enough letter for the word. If not, this word
# is 'not good' and we do next.
# We copy the set for every word checking, as we will decrement the counts
# for 'use up' the letter for a word. Deep copy is the not a fastest way but
# clear and easy.
# Complexity is O(n), n is the number of words
# Space complexity is O(n) for saving the returned words. Can optimize to O(1)
# if we count the word len in the loop.

import collections

class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        
        chars_dict = collections.Counter(chars)

        good_words = []
        for w in words:
            is_good = True
            d = collections.Counter(chars_dict)
            for c in w:
                if c in d and d[c] > 0:
                    d[c] -= 1
                else:
                    is_good = False
                    break
                    
            if is_good:
                good_words.append(w)
           
        return sum(map(len, good_words))
