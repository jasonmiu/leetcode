// This is a graph adjacency matrix, while all edges are symmetric.
// The basic idea is similar to flood fill (200 number of island),
// for each city (node), trace down all its connected nodes,
// color the path to 0, which means counted.
// Since 1 city can be its own province, we count province
// every city still labeled as one (not visited).
// We can use DFS to trace down the paths.
// For each node, send it to DFS as the root, mark the
// matrix[root][root], i.e. the self reflecting edge,
// to 0 so this node will not be visited again.
// Also need to mark the symmetric edge to 0,
// so we will not cyclically come back.
// The loop *all* nodes in the row of the adjacency matrix of root
// not just the colume from root.
// It is because in the example:
//   0 1 2 3
//   -------
// 0|1 0 0 1
// 1|0 1 1 0
// 2|0 1 1 1
// 3|1 0 1 1
//
// The graph is: 0 --> 3 --> 2 --> 1
// So when the root is 0, it go to 3. At the node 3 (row 3), it should
// scan from column 0 to check if it is connecting to any node, instead
// just from (3, 3).
//
// Time: O(n), as accessing all matrix elements once. Space O(1).


void dfs(vector<vector<int>>& isConnected, int root)
{
    int n = isConnected[0].size();

    isConnected[root][root] = 0;
    for (int j = 0; j < n; j++) {
        isConnected[j][root] = 0;
        if (isConnected[root][j] == 1) {
            dfs(isConnected, j);
        }
    }

    return;
}

class Solution {
public:
    int findCircleNum(vector<vector<int>>& isConnected) {
        int ans = 0;

        for (int i = 0; i < isConnected.size(); i++) {
            if (isConnected[i][i] == 1) {
                dfs(isConnected, i);
                ans ++;
            }
        }

        return ans;
    }
};
