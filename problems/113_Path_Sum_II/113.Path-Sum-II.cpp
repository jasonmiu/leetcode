// Idea is simple, which is do a DFS, go downward of the tree and save the path.
// Sum along the path.
// If we hit the leaf node, check if we have the target sum.
// One implementation point is, we want to check at the leaf node,
// *not* the null nodes after leaf node. It is because a leaf node has 2 null nodes,
// when checking at a null node, it is actually checking the sum up to its parent
// (that leaf node). And because a leaf node has 2 null nodes, the same sum will be
// computed twice and treated as two paths coming down to those null nodes.
// Time: O(n) for visiting all nodes once. Space: O(n) for saving paths.
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:

    vector<vector<int>> ans;

    void dfs(TreeNode* r, int t, int sum, vector<int>& path)
    {
        if (r == nullptr) {
            return;
        }

        path.push_back(r->val);

        if (r->left == nullptr && r->right == nullptr) {
            if (r->val + sum == t) {
                ans.push_back(path);
                path.pop_back();
                return;
            }
        }

        if (r->left) {
            dfs(r->left, t, sum + r->val, path);
        }
        if (r->right) {
            dfs(r->right, t, sum + r->val, path);
        }

        path.pop_back();

        return;
    }

    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {

        vector<int> path;
        dfs(root, targetSum, 0, path);

        return ans;

    }
};
