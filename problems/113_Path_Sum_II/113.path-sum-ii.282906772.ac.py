#
# @lc app=leetcode id=113 lang=python3
#
# [113] Path Sum II
#
# https://leetcode.com/problems/path-sum-ii/description/
#
# algorithms
# Medium (42.79%)
# Total Accepted:    273.8K
# Total Submissions: 633.6K
# Testcase Example:  '[5,4,8,11,null,13,4,7,2,null,null,5,1]\n22'
#
# Given a binary tree and a sum, find all root-to-leaf paths where each path's
# sum equals the given sum.
# 
# Note: A leaf is a node with no children.
# 
# Example:
# 
# Given the below binary tree and sum = 22,
# 
# 
# ⁠     5
# ⁠    / \
# ⁠   4   8
# ⁠  /   / \
# ⁠ 11  13  4
# ⁠/  \    / \
# 7    2  5   1
# 
# 
# Return:
# 
# 
# [
# ⁠  [5,4,11,2],
# ⁠  [5,8,4,5]
# ]
# 
# 
#
# The idea is to do a depth first search, record the nodes along the path.
# When we hit a leave node, sum the values of the nodes on the path to check if
# we match the sum target. If so, we return a list of nodes on this path. We want
# to use another list to contain this nodes list (so the return value is a list of list),
# so the parent nodes can merge two lists from both left and right subtrees to get a 
# list of node lists.
# One tricky part is when we pass down the nodes, we need to append the current node to the
# end for maintaining the nodes order along the path from top to down.
# The complexity is O(n), which we need to access all nodes once.
# The space complexity is O(n) too for storing the result values which can be n.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def paths(root, target, path):
    
    if root == None:
        return []
    
    if root.left == None and root.right == None:
        s = root.val
        for n in path:
            s = s + n.val
            
        if s == target:
            p = [n.val for n in path]
            return [p + [root.val]]
        else:
            return []
        
    l_path = []
    if root.left:
        l_path = paths(root.left, target, path + [root])
    r_path = []
    if root.right:
        r_path = paths(root.right, target, path + [root])
        
    return l_path + r_path
    

class Solution:
    def pathSum(self, root: TreeNode, sum: int) -> List[List[int]]:
        
        ans = paths(root, sum, [])
        return ans
            
        
