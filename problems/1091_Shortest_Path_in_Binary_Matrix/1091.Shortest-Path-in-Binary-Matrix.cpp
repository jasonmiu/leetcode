// for unit distance graph, BFS can be used to find the shortest path.
// Since BFS is like a ripple, start from the source point,
// for each reached point it is the current shortest.
// One issue is how to keep track of the visited cells. The way is when
// we find a connected cell and ready to push it to the BFS queue,
// mark it to the current step count + 1. And only push the cell has value 0,
// i.e. not pushed to the queue before (not visited). One puzzle is, can the
// same visited cell can be revisited for a shorter path?
// For example (# as INT_MAX):
// 0 0 0
// # 0 0
// # # 0
//
// next step:
// 0 1 0
// # 1 0
// # # 0
//
// next step:
// 0 1 2
// # 1 2 <-- here, the (1,2) was already updated by (0,1),
// # # 0     so when (1,1) want to visit it, it cannot
//
//
// 0 1 2
// # 1 2
// # # 2 <-- the (2,2) get pushed by (1,1). So later in the
//           next step, the (1,2) like to visit (2,2), it cannot not,
//           the shorter path always visited first.
//
// Time: O(n) as visited all nodes once. Space: O(1).


class Solution {
public:
    int shortestPathBinaryMatrix(vector<vector<int>>& grid) {
        int n = grid.size();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    grid[i][j] = INT_MAX;
                }
            }
        }

        if (grid[0][0] == INT_MAX) {
            return -1;
        }

        if (n == 1 && grid[0][0] == 0) {
            return 1;
        }

        queue<pair<int,int>> q;
        q.push({0,0});

        while (!q.empty()) {
            int qsize = q.size();

            for (int i = 0; i < qsize; i++) {
                pair<int, int> pos = q.front();
                q.pop();

                vector<pair<int,int>> dirs = {
                    {pos.first - 1, pos.second},
                    {pos.first, pos.second + 1},
                    {pos.first + 1, pos.second},
                    {pos.first, pos.second - 1},
                    {pos.first - 1, pos.second + 1},
                    {pos.first - 1, pos.second - 1},
                    {pos.first + 1, pos.second + 1},
                    {pos.first + 1, pos.second - 1}
                };

                for (auto d : dirs) {
                    if (d.first >= 0 && d.first < n &&
                        d.second >= 0 && d.second < n) {
                        if (grid[d.first][d.second] == 0) {
                            grid[d.first][d.second] = grid[pos.first][pos.second] + 1;
                            q.push(d);
                        }
                    }
                }
            }

        }

        if (grid[n-1][n-1] == INT_MAX || grid[n-1][n-1] == 0) {
            return -1;
        } else {
            return grid[n-1][n-1] + 1;
        }
    }
};
