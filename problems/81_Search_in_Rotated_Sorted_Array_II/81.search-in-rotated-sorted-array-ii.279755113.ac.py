#
# @lc app=leetcode id=81 lang=python3
#
# [81] Search in Rotated Sorted Array II
#
# https://leetcode.com/problems/search-in-rotated-sorted-array-ii/description/
#
# algorithms
# Medium (32.81%)
# Likes:    866
# Dislikes: 370
# Total Accepted:    198K
# Total Submissions: 603.2K
# Testcase Example:  '[2,5,6,0,0,1,2]\n0'
#
# Suppose an array sorted in ascending order is rotated at some pivot unknown
# to you beforehand.
# 
# (i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).
# 
# You are given a target value to search. If found in the array return true,
# otherwise return false.
# 
# Example 1:
# 
# 
# Input: nums = [2,5,6,0,0,1,2], target = 0
# Output: true
# 
# 
# Example 2:
# 
# 
# Input: nums = [2,5,6,0,0,1,2], target = 3
# Output: false
# 
# Follow up:
# 
# 
# This is a follow up problem to Search in Rotated Sorted Array, where nums may
# contain duplicates.
# Would this affect the run-time complexity? How and why?
# 
# 
#

# @lc code=start
# The main idea is we want to search the target in a sorted portion.
# If we divide the input list in two 2 halves, 1 half must be sorted.
# for eg:
# [6 7 8 8 2 2 2 2]
#  l     m       r
# if nums[l] <= nums[m], first half is sorted, else, like:
# [2 4 5 2 2 2 2 ]
#  l     m     r
# the 2nd half is sorted
# Then check the target is in which half, in first case:
# [6 7 8 8 2 2 2 2]
#  l t   m       r
# nums[m] > nums[t] >= nums[l], search target in first half, else search 2nd half. In another case:
# [2 4 5 1 2 2 2 ]
#  l     m   t r
# nums[m] < nums[t] <= nums[r], search target in 2nd half, else search 1st half.
# 
# The tricky part is, when we have dupicated elements, nums[m] can be equal to nums[l] or nums[r], like:
# [3 3 4 3 1 1 2 3]
#  l     m       r
# Then we cannot sure if the 1st half is sorted, so the code 'if nums[l] <= nums[m]', 
# it can be a case that nums[l] == num[m].
# So we do "while nums[l] == nums[m] and l < m:" to move the 'l' to right until it is not equal to nums[m], 
# to eliminate this equal case.
# We do not need to move 'r' to left in the same case, as we handled the left side case, the right side
# case will go in to the 2nd half sorted path. The new 'l' will check with the new 'm'.
# The worse case, we need to search all elements, like:
# [ 3 3 3 3 3], target = 4
# We have to search from nums[2] to nums[4] as the 'l' needed to be move all the way to right.
# So it is O(n). But avg cases should be O(logn)
# The space complexity is O(1) as we do not need extra space.


class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        
        l = 0
        r = len(nums) - 1
        
        while l <= r:
            m = (l + r) // 2
            
            if nums[m] == target:
                return True
            
            while nums[l] == nums[m] and l < m:
                l += 1
                
            if nums[l] <= nums[m]:
                # 1st half sorted
                if target >= nums[l] and target < nums[m]:
                    r = m - 1
                else:
                    l = m + 1
                    
            else:
                # 2nd half sorted
                if target <= nums[r] and target > nums[m]:
                    l = m + 1
                else:
                    r = m - 1
                    
        return False


# @lc code=end
