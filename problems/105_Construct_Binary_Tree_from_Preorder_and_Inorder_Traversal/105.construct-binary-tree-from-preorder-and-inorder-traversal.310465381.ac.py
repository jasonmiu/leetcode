#
# @lc app=leetcode id=105 lang=python3
#
# [105] Construct Binary Tree from Preorder and Inorder Traversal
#
# https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/
#
# algorithms
# Medium (43.86%)
# Total Accepted:    306.8K
# Total Submissions: 668.6K
# Testcase Example:  '[3,9,20,15,7]\n[9,3,15,20,7]'
#
# Given preorder and inorder traversal of a tree, construct the binary tree.
# 
# Note:
# You may assume that duplicates do not exist in the tree.
# 
# For example, given
# 
# 
# preorder = [3,9,20,15,7]
# inorder = [9,3,15,20,7]
# 
# Return the following binary tree:
# 
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
#
# For a root node, we need to know which one is its left and right node.
# Its left node is the root node of its left-tree, and its right node is
# its right-tree. So we want to know which part is its left tree which
# part is its right tree. For example:
#    3
#   / \
#  9  20
#    /  \
#   15   7
# 
# preorder = [3,9,20,15,7]
# inorder = [9,3,15,20,7]
# The first node of preorder is the root node.
# preorder = [3,9,20,15,7]
#             ^ ^ ^     ^
#             | | +-----+ Right Tree
#             | +--- Left Tree
#             +--- Root
#
# inorder = [9,3,15,20,7]
#            ^ ^ ^     ^
#            | | +-----+--- Right Tree
#            | +---- Root
#            +---- Left Tree
#
# The main idea is, for inorder list, the left elements of the root are the left tree.
# The right elements of the root are the right tree.
# So get the inorder index of the root, which is also the number of left tree elements.
# The cut the preorder with the number of the left tree elements, also use the
# remaining as the right tree elements, we can recursively put the left and right 
# preorder and inorder lists, and we always return the current root.
# For a node, we can get the root of the left and right subtree of it.
# Complexity, search the index take O(n) and we cut the list N times, so O(n^2)
# Space is O(n^2) since we pass the list with N size N times.



# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    
    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        
        if len(preorder) == 0:
            return None
        
        v = preorder[0]
        
        inorder_idx = inorder.index(v)
        
        left_inorder = inorder[0:inorder_idx]
        right_inorder = inorder[inorder_idx+1::]
        
        left_num_of_nodes = inorder_idx
        left_preorder = preorder[1:left_num_of_nodes+1]
        right_preorder = preorder[1+left_num_of_nodes::]
        
        root = TreeNode(v)
        root.left = self.buildTree(left_preorder, left_inorder)
        root.right = self.buildTree(right_preorder, right_inorder)
        
        return root
