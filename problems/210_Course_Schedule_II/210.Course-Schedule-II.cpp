// The idea is using the topological sort, to sort out the order of the courses.
// The main point of the topological sort is, since for a node we need to access all its adjacency nodes, like:
//
// 0 -> 1 -> 2
// +---------^
//
// For node, it has two paths, 0->1->2, and 0->2. Node 2 get visited twice, but it is not a cycle.
// Compare with:
//
// 0 -> 1 -> 2
// ^---------+
//
// For node 0, it has a cycle like 0 -> 1 -> 2 -> 0 -> ... Node 0 also get visited twice. How to seperate these two cases?
//
// The main point is not to save the path for each search path. For detecting cycle in DFS, use a set to save the current
// search path is the standard method. The idea is to mark the current search path temporary visited (in_path var),
// when a node has done *all* its adjacency nodes search, mark it as permanent visited (visited var), and remove its
// temporary visited mark. If we re-visited a permanent visited node, its ok as we know we done explore it. It is not a
// cycle. But if re-visited a temporary visited node, means we see it again in the current search path, which is a cycle.
// Like DFS detecting cycle, we backtrack the current node for removing the temporary visited node, as a kind of
// restarting another search path. But with topological sort, we mark it permanent visited as well.
// Once we searched all adjacency node paths, we know we have get all its dependency, so we just need to save the current
// node to the final sorted list, as we only need to access a node once.
// Time: O(n + e), e is the edge number. Space is O(n+e) for the graph.

bool dfs(const vector<vector<int>>& g, int course, unordered_set<int>& visited, unordered_set<int>& in_path, vector<int>& topo)
{
    const vector<int>& prereqs = g[course];

    if (visited.find(course) != visited.end()) {
        return true;
    }

    if (in_path.find(course) != in_path.end()) {
        return false;
    }

    if (prereqs.empty()) {
        topo.push_back(course);
        visited.insert(course);
        return true;
    } else {

        in_path.insert(course);

        for (int pr : prereqs) {
            bool r = dfs(g, pr, visited, in_path, topo);
            if (!r) {
                return false;
            }
        }

        in_path.erase(course);

        topo.push_back(course);
        visited.insert(course);

        return true;
    }

    return true;
}

class Solution {
public:
    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {

        vector<vector<int>> g(numCourses);

        for (const auto& pr : prerequisites) {
            int a = pr[0];
            int b = pr[1];

            g[a].push_back(b);
        }

        vector<int> ans;
        unordered_set<int> visited;
        for (int i = 0; i < numCourses; i++) {
            unordered_set<int> in_path;
            bool r = dfs(g, i, visited, in_path, ans);
            if (!r) {
                return vector<int>();
            }
        }

        return ans;
    }
};
