#
# @lc app=leetcode id=210 lang=python3
#
# [210] Course Schedule II
#
# https://leetcode.com/problems/course-schedule-ii/description/
#
# algorithms
# Medium (36.82%)
# Total Accepted:    211.7K
# Total Submissions: 556.7K
# Testcase Example:  '2\n[[1,0]]'
#
# There are a total of n courses you have to take, labeled from 0 to n-1.
# 
# Some courses may have prerequisites, for example to take course 0 you have to
# first take course 1, which is expressed as a pair: [0,1]
# 
# Given the total number of courses and a list of prerequisite pairs, return
# the ordering of courses you should take to finish all courses.
# 
# There may be multiple correct orders, you just need to return one of them. If
# it is impossible to finish all courses, return an empty array.
# 
# Example 1:
# 
# 
# Input: 2, [[1,0]] 
# Output: [0,1]
# Explanation: There are a total of 2 courses to take. To take course 1 you
# should have finished   
# course 0. So the correct course order is [0,1] .
# 
# Example 2:
# 
# 
# Input: 4, [[1,0],[2,0],[3,1],[3,2]]
# Output: [0,1,2,3] or [0,2,1,3]
# Explanation: There are a total of 4 courses to take. To take course 3 you
# should have finished both     
# ⁠            courses 1 and 2. Both courses 1 and 2 should be taken after you
# finished course 0. 
# So one correct course order is [0,1,2,3]. Another correct ordering is
# [0,2,1,3] .
# 
# Note:
# 
# 
# The input prerequisites is a graph represented by a list of edges, not
# adjacency matrices. Read more about how a graph is represented.
# You may assume that there are no duplicate edges in the input prerequisites.
# 
# 
#
# The ida is clearly to write a topological sort algor.
# We also need to check if we find a cycle during the topological sort.
# If we have a cycle, the topo sort is not possible.
# Using DFS to find topological sort, is appending the current node to 
# the result stack, then *ALL* paths from the current node have been 
# explored.
# If we have visited the current node, means we saw it in a path of 
# other nodes. Having a path from this visited node means this is a 
# subpath of another visited node, so we just return the boolean of
# sortable or not and need not to put it to the result stack.
# Complexity is O(n + e), n is the number of courses, and e is the number
# of edges.
# Space complexity is also O(n+e), for the adj list.

def to_alist(num, prereq):
    alist = []
    for i in range(0, num):
        alist.append([])
        
    for (u, v) in prereq:
        alist[u].append(v)
        
    return alist

def dfs_topo(alist, n, visited, path, stack):
    if n in path:
        return False
    
    if n in visited:
        return visited[n]
    
    path.add(n)
    can_sort = True
    for v in alist[n]:
        can_sort = dfs_topo(alist, v, visited, path, stack)
        if not can_sort:
            break
            
    if can_sort:
        stack.append(n)
        
    path.remove(n)
    visited[n] = can_sort
    return can_sort
    
    

class Solution:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        alist = to_alist(numCourses, prerequisites)
        can_sort = True
        
        topo = []
        visited = dict()
        path = set()
        for i in range(0, numCourses):
            can_sort = dfs_topo(alist, i, visited, path, topo)
            if not can_sort:
                break
                
        if not can_sort:
            return []
        else:
            return topo
