// The first idea will be to sort the input array, and scan the positive
// numbers for their continuity. This is O(nlogn), the hard part is the question
// require O(n) and space O(1).
//
// Next idea is to think, for counting 1, 2, 3 ... to the max possible value (2^31-1 in this question),
// is there a fast way to check if this value exists in the input array? We can build a large array,
// the index is the all possible value, so it will be 0 - (2^31 - 1), which is a 2^31 size array.
// if this is too big, we can have size w array, do (2^31 / w) rounds, and scan the input each time
// to fill this hash array. Like:
//
////  class Solution {
////  public:
////      int firstMissingPositive(vector<int>& nums) {
////
////          const long long w = 8192;
////          long long max_poss = pow(2, 32);
////
////          for (int n = 0; n < (max_poss / w) - 1; n++) {
////
////              bool arr[w] = {0};
////
////              for (int x : nums) {
////                  int idx = x % w;
////                  if (x > 0) {
////                      arr[idx] = true;
////                  }
////              }
////
////              for (int i = 0; i < w; i++) {
////                  if (arr[i] == false) {
////                      if (n == 0 && i == 0) {
////                          continue;
////                      } else {
////                          return n * w + i;
////                      }
////                  }
////              }
////          }
////
////          return -1;
////      }
////  };
//
// While the n, which is the 2^31 / w is a constant, and we scan the input array n times. If the input array has
// size m, it is O(nm) but since n is constant, this is still O(m) in term of complexity. However, if the n is big,
// this is not good and indeed we got a Time Limit Exceeded.
//
// Keep thinking what else info we can use during a input scan. The question said *all positive* numbers, which means
// we do not care 0 and netgative. And since we want to find the missing number, any repeated number in the array
// is not useful. This looks like we can have a bucket sort, by reusing the same input array.
// Example:
//  0 1 2 3 4
//-------------
//[ 2 3 5 4 1 ] <- input
//[ 1 2 3 4 5 ] <- bucket sorted
//
// To do a bucket sort, we start from the first value. If it is "misplaced", ie. its index is not its value - 1, we want
// to move it to the right place, for example, if the current value is 2, we want to move it to the index 1.
//
//  0 1 2 3 4
//-------------
//[ 2 3 5 4 1 ] <- input
//  ^
//  | Move to index 1
//
//  0 1 2 3 4
//-------------
//[ 0 2 5 4 1 ] <- input
//    ^
//    | Put the starting value as 0 to prevent loopback,
//    | now 2 is placed correctly, the orginial value 3 at index 1 is now looking for
//    | its correct place, which is index 2
//
//  0 1 2 3 4
//-------------
//[ 0 2 3 4 1 ] <- input
//      ^
//      | 3 is now placed correctly. The original value 5 is going to index 4
//
//  0 1 2 3 4
//-------------
//[ 0 2 3 4 5 ] <- input
//          ^
//          | original value 1 is going to index 0
//
//  0 1 2 3 4
//-------------
//[ 1 2 3 4 5 ] <- input
//  ^
//  | Once the original value is 0, means it is originally useless (don't care value <= 0),
//  | or it is the current bucket sort starting point. So we can stop here, and go
//  | for the next index.
//
//  0 1 2 3 4
//-------------
//[ 1 2 3 4 5 ] <- input
//    ^
//    | start bucket sort for the next value. This time this value is placed correctly already,
//    | just move forward, until all values are processed.
//
//
// What if the input contains 0 and netgative values?
//
//   0 1 2 3 4
//-------------
//[ -1 3 5 0 1 ] <- input
//   ^
//   | netgative, do not bucket sort, just set it to zero means this is a empty bucket can be used later.
//
//   0 1 2 3 4
//-------------
//[  0 3 5 0 1 ] <- input
//     ^
//     | some bucket sort algor
//
//   0 1 2 3 4
//-------------
//[  0 0 3 0 5 ] <- input
//           ^
//           | original value 1 now going to index 0
//
//   0 1 2 3 4
//-------------
//[  1 0 3 0 5 ] <- input
//   ^
//   | original value is 0, so this bucket sort round stop here.
//
//   0 1 2 3 4
//-------------
//[  1 0 3 0 5 ] <- input
//     ^
//     | next value, but it is 0, we do not care, and move forward, until all
//     | values get bucket sorted.
//
// What if some values is outside of the size of the array?
//
//   0 1   2 3   4
//----------------
//[  1 200 2 300 5 ] <- input
//   ^
//   | value 1 is at its correct place, move to next
//
//   0 1   2 3   4
//----------------
//[  1 200 2 300 5 ] <- input
//     ^
//     | value 200 is out of the array size. If a value is out of the array size, it will not be the smallest
//     | number. As:
//     | 1. If the array does not contain 1, the smallest positive number will be 1. And if a value is larger than array
//     |    size, it is larger than 1, since the array size now is at least 1.
//     | 2. If the array contains all continous number from 1, means the array is full filled from index 0. If this value
//     |    is larger than the array, means the smallest number must appear in the last full filled number in the array
//     |    and this number. And this number is clearly larger than that last full filled number, so this value
//     |    can't be the smallest positive number too.
//     | Hence, base on the 2 cases above, this number is not useful and just take it as the netgative number, set to 0.
//
//   0 1   2 3   4
//----------------
//[  1 0   2 300 5 ] <- input
//         ^
//         | now value 2 can move to index 1
//
//   0 1   2 3   4
//----------------
//[  1 2   0 300 5 ] <- input
//           ^
//           | process all values
//
// So after bucket sort inplace, the sorted input array will be filled from the smallest positive value it contains.
// The first 0, which is the first non-continue positive value, its index is the smallest missing positive we want.
// If all elements are filled, means this is a full continue array. The smallest missing positive value is the size of
// of the input array + 1.
//
// The bucket sort touch all elements once.  Then re-scan for checking the 0, so the time is O(n). Space is O(1).


class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {

        for (int i = 0; i < nums.size(); i++) {

            int cur = nums[i];
            // Bucket sort for this value as it is not placed correctly
            if (cur > 0 && cur != i + 1) {

                nums[i] = 0; // the starting point set to 0
                while (true) {

                    // The target index. If out of range, skip this bucket
                    int t_idx = cur - 1;
                    if (t_idx < 0 || t_idx >= nums.size()) {
                        break;
                    }

                    // The value at the target index.
                    // If the value at the target index is placed correctly
                    // already, skip this bucket as do not need to place the same value
                    // again, also prevent the forever loop when repeated values happens like:
                    // [1, 1]
                    int t = nums[t_idx];
                    if (t == t_idx + 1) {
                        break;
                    }

                    // Place the current value to the correct place.
                    // If the original value of this correct place is 0, we end this bucket.
                    nums[t_idx] = cur;
                    if (t <= 0) {
                        break;
                    } else {
                        cur = t; // otherwise, look for next correct index
                    }
                }

            } else if (cur <= 0) {
                // This value is netgative, just set it to 0
                nums[i] = 0;
            }
        }

        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == 0) {
                return i + 1;
            }
        }

        return nums.size() + 1;
    }
};
