#
# @lc app=leetcode id=263 lang=python3
#
# [263] Ugly Number
#
# https://leetcode.com/problems/ugly-number/description/
#
# algorithms
# Easy (41.14%)
# Total Accepted:    181.1K
# Total Submissions: 440.2K
# Testcase Example:  '6'
#
# Write a program to check whether a given number is an ugly number.
# 
# Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.
# 
# Example 1:
# 
# 
# Input: 6
# Output: true
# Explanation: 6 = 2 × 3
# 
# Example 2:
# 
# 
# Input: 8
# Output: true
# Explanation: 8 = 2 × 2 × 2
# 
# 
# Example 3:
# 
# 
# Input: 14
# Output: false 
# Explanation: 14 is not ugly since it includes another prime factor 7.
# 
# 
# Note:
# 
# 
# 1 is typically treated as an ugly number.
# Input is within the 32-bit signed integer range: [−2^31,  2^31 − 1].
# 
#
# The ideas are:
# 1. ugly number must be +ve
# 2. 1 is ugly number
# 3. prime factor (d) only has 2, 3, 5
#   Hence, num = d * m, while:
#   a. if m is ugly, num is ugly
#   b. m use be an interger, ie. num % d == 0, for a ugly num,
#      otherwise, for eg, n = 14, d = 2, m = 7, 7 has no factor of
#      2, 3, or 5, m is not ugly, and num is not ugly
# We can see num is try to be divided by 2, 3, 5, recusivly:
#             num
#          /   |   \
#         2    3    5
#       / | \  /|\  / | \
#     2  3  5
# if a path has a non interger quotient, that path return need not to be 
# continue. If there is a path end up with 1, means that path has all
# dividable 2, 3, or 5, that it is a ugly num.
# Complexity is O(logn), space is also O(logn) for the stack

class Solution:
    def isUgly(self, num: int) -> bool:
        
        if num <= 0:
            return False
        
        if num == 1:
            return 1
        
        ans = False
        for d in (2, 3, 5):
            if num % d != 0:
                continue
                
            m = int(num / d)
            ans = self.isUgly(m)
            if ans:
                break
                
        return ans
