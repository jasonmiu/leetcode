#
# @lc app=leetcode id=543 lang=python3
#
# [543] Diameter of Binary Tree
#
# https://leetcode.com/problems/diameter-of-binary-tree/description/
#
# algorithms
# Easy (47.64%)
# Total Accepted:    170.7K
# Total Submissions: 358.3K
# Testcase Example:  '[1,2,3,4,5]'
#
# 
# Given a binary tree, you need to compute the length of the diameter of the
# tree. The diameter of a binary tree is the length of the longest path between
# any two nodes in a tree. This path may or may not pass through the root.
# 
# 
# 
# Example:
# Given a binary tree 
# 
# ⁠         1
# ⁠        / \
# ⁠       2   3
# ⁠      / \     
# ⁠     4   5    
# 
# 
# 
# Return 3, which is the length of the path [4,2,1,3] or [5,2,1,3].
# 
# 
# Note:
# The length of path between two nodes is represented by the number of edges
# between them.
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    
    diameter = 0
    
    
    # for each node, the diameter is the sum of the longest path of the right side 
    # and the longest path of the left side
    # we search down the tree, if the current path is longest (diameter), then we update the
    # globle var as the answer
    # for each node, return the longest side back to the parent
    def find_dia(self, root):
        # we went thru the leave, the leave node should have 0 path length
        # should return -1 here then the leave node will +1 to get the correct 0 path length
        if root == None:
            return -1
        
        r = self.find_dia(root.right) + 1
        l = self.find_dia(root.left) + 1
        
        #print("root.val", root.val, "l", l, "r", r)
        
        if (r + l) >= self.diameter:
            self.diameter = r + l
            
        return max(r, l)
        
    
    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        self.diameter = 0
        
        self.find_dia(root)
        
        return self.diameter
