// The brute force way is looping the output,
// then loop the input and product all elements,
// except the element index that same as the current
// output element index. But this gives O(n^2).
// The question said no division, but indeed with division,
// firstly product up all elements, then divided by the
// output index, this idea will not work. It is because
// the input can contains 0, like [-1,1,0,-3,-3].
// Then the all product is 0, when dividing the index 2,
// element 0, the idea will not work, since the expected
// ans of index 2 is '9'.
// The math way is not working, so think in the way of,
// how the multiplcation process knows not to include
// the current element.
// For example:
// [ 1 2 3 4 5 ]
// We can build a array, which each element means,
// the product *before* the current element.
// [ 1 1 2 6 24 ]
//   ^ ^ ^ ^
//   | | | |
//   | | | + the product before index 3, is 1*2*3 = 6
//   | | + the product before index 2, is 1*2 = 2
//   | + the product before index 1, is 1
//   + the product before index 0, is always 1
//
// So for this array, each element is the product before
// the input element, except itself.
// We want to have the same things for the product *after*
// the input element. So do a 2nd scan, and from *backward*.
//
// [ 1    2  3 4 5 ] (input)
//--------------------
// [ 120 60 20 5 1 ]
//        ^  ^ ^ ^
//        |  | | |
//        |  | | + the product after the last element is always 1
//        |  | + the product after index 3 is index 4, which is 5
//        |  + the product after index 2 is 4*5 = 20
//        + the product after index 1 is 3 * 4 * 5 = 60
//
//
// With the first pass array, it is the product *before* a element
// With the 2nd pass array, it is the product *after* a element
// Since both products in the arrays do not contain the element itself,
// multiply the elements one by one from first and 2nd arrays give the result.
//
// To make the space O(1), we do not need the 2nd pass array, since the
// product generated in the 2nd pass can be rolling in the answer.
// The first pass array just store in the anwser array,
// Then have a single integer, p, work as our current backward product
// in the 2nd pass array.
//
//   0  1  2  3  4   (Index)
// {             1 } (p value)
// First the p is 1, just like the right most of the 2nd pass array.
// The current index is 4, and the final result is :
// first pass [4] * 2nd pass [4] , which is now:
// ans[4] * p
// Then update the p. Like the 2nd pass array,
// the 2nd pass [i+1] = 2nd pass [i] * input [i], which is now:
// p = p * input[i]
// Then move the index to left.
//   0  1  2  3  4   (Index)
// {          5  1 } (p value)
// Since each element only needed once, we can keep rolling it and save the space,
// but virtually giving the 2nd pass elements with p values.
//
// Time O(n), space (1).



vector<int> two_scans(const vector<int>& a)
{
    vector<int> ans(a.size());
    vector<int> first_pass(a.size());
    vector<int> sec_pass(a.size());

    first_pass[0] = 1;
    for (int i = 1; i < a.size(); i++) {
        first_pass[i] = first_pass[i-1] * a[i-1];
    }

    sec_pass[a.size()-1] = 1;
    for (int i = a.size() - 2; i >= 0; i--) {
        sec_pass[i] = sec_pass[i+1] * a[i+1];
    }

    for (int i = 0; i < a.size(); i++) {
        ans[i] = first_pass[i] * sec_pass[i];
    }

    return ans;
}

vector<int> two_scans_no_space(const vector<int>& a)
{
    vector<int> ans(a.size());

    ans[0] = 1;
    for (int i = 1; i < a.size(); i++) {
        ans[i] = ans[i-1] * a[i-1];
    }

    int p = 1;
    for (int i = a.size() - 1; i >= 0; i--) {
        ans[i] = p * ans[i];
        p = a[i] * p;
    }

    return ans;
}

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
#if 0
        vector<int> ans = two_scans(nums);
#else
        vector<int> ans = two_scans_no_space(nums);
#endif

        return ans;
    }
};
