#
# @lc app=leetcode id=680 lang=python3
#
# [680] Valid Palindrome II
#
# https://leetcode.com/problems/valid-palindrome-ii/description/
#
# algorithms
# Easy (35.32%)
# Total Accepted:    106K
# Total Submissions: 299.9K
# Testcase Example:  '"aba"'
#
# 
# Given a non-empty string s, you may delete at most one character.  Judge
# whether you can make it a palindrome.
# 
# 
# Example 1:
# 
# Input: "aba"
# Output: True
# 
# 
# 
# Example 2:
# 
# Input: "abca"
# Output: True
# Explanation: You could delete the character 'c'.
# 
# 
# 
# Note:
# 
# The string will only contain lowercase characters a-z.
# The maximum length of the string is 50000.
# 
# 
#
# If the input is an palindrome, then remove 1 char always able to make a Palindrome
# odd len case: aba -> remove b -> aa
# even len case, middle two chars must be the same: abba -> remove b -> aba
# So if input is an Palindrome we can return True

# To check if this is a Palindrome, we use 2 pointers to search inward.
# If two chars are different, we can remove 1 char, create a new edited
# string, put it back to the checking function recursivly. If one of two 
# of these new strings can become Palindrome, then the original string 
# can be Palindrome with 1 edit. If both edited strings cannot be Palindrome,
# then return False. This is a backtracking.

# The complexity is O(n), since we search from both ends, and if editing happens,
# we search the substrings 2 times.
# The space complexity is O(n) for edited substring.

def check_pal(s, edited):    
    if len(s) == 2:
        return True
    if len(s) == 3:
        if s[0] == s[-1]:
            return True
        else:
            return False
        
    i = 0
    j = len(s) - 1
    while i < j:
        if s[i] != s[j]:
            if edited:
                return False
            else:
                s2 = s[0:i] + s[i+1::] # remove s[i]
                r1 = check_pal(s2, True)
                s2 = s[0:j] + s[j+1::] # remove s[j]
                r2 = check_pal(s2, True)
                if r1 == False and r2 == False:
                    return False
                else:
                    return True
        i += 1
        j -= 1
                

    return True
                

class Solution:
    def validPalindrome(self, s: str) -> bool:
        return check_pal(s, False)
