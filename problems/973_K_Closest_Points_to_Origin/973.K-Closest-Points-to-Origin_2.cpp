// Using quick select algor. Quick select uses a property
// of partition algor, which the partition algor should
// return the correct sorted index of the pivot. Ie, if
// the pivot should be in the index 3 in a sorted list,
// the partition should return 3 for the pivot. Example:
//  0 1 2 3 4
// [2 3 1 4 0], pivot = 1
// If it is totally sorted:
//  0 1 2 3 4
// [0 1 2 3 4]
//    ^ sorted index of pivot
//
// Of parition, all left of the pivot should be smaller or
// equal to pivot. All values on right should be larger than
// pivot:
//
//  0 1 2 3 4
// [0 1 2 4 3]
//    ^- partition return pivot index
//
// Since the pivot index is correct, we know the rank of this
// pivot value, which is at the (index + 1)th rank in the overall
// input.
// And if we know the rank of a value, we know there are rank - 1
// number of values are smaller or equal to it. So when we want to
// find the kth value, we check if it is larger than partition
// rank. If k is larger, means the actual kth value is on the
// current right hand side. Othwise, it is on the left hand side.
// If k is equal to our current partition rank, we find the kth value.
// In this question, we want to get the values smaller than kth value,
// including the kth value. We know all values on the left hand side of the partitioned index
// from the starting of the partitioned array are smaller than this kth value,
// so copy all of them as our result.
// On average, each parition should cut the search space half. Partition cost O(n), so
// it will be n + n/2 + n/4 + n/8 ... = 2n. Hence time is O(n). Space is O(1) as inplace.
//
// For c++, the STL partition function is *NOT* same as the pivot partition. The STL partition
// does not provide the returned index as the kth element but just partition the input in two
// parts of true and false of the testing function. In c++, quick select should use
// STL nth_element() (https://www.cplusplus.com/reference/algorithm/nth_element).

double dist(const vector<int>& p)
{
    return sqrt(pow(p[0], 2) + pow(p[1], 2));
}

struct point_dist {
    vector<int> point;
    double dist;
};

int partition(vector<point_dist>& pds, int s, int e)
{
    const point_dist& pivot = pds[e];
    double pivot_dist = pivot.dist;

    int i = s;
    int left_of_pivot = s;

    while (i <= e - 1) {
        if (pds[i].dist <= pivot_dist) {
            swap(pds[i], pds[left_of_pivot]);
            left_of_pivot++;
        }
        i++;
    }

    swap(pds[i], pds[left_of_pivot]);

    return left_of_pivot;
}


void quick_select_to_k(vector<point_dist>& pds, int k,
                       int s_idx, int e_idx,
                       vector<vector<int>>& result)
{
    int p_idx = partition(pds, s_idx, e_idx);

    int k_idx = k - 1;
    if (p_idx == k_idx) {
        for (int i = 0; i <= p_idx; i++) {
            result.push_back(pds[i].point);
        }
        return;
    } else if (p_idx < k_idx) {
        quick_select_to_k(pds, k, p_idx, e_idx, result);
    } else if (p_idx > k_idx) {
        quick_select_to_k(pds, k, s_idx, p_idx-1, result);
    }

    return;
}


class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int k) {

        double max_dist = 0.0;
        vector<point_dist> point_dists;
        for (auto p : points) {
            double d = dist(p);
            point_dist pd = {p, d};
            point_dists.push_back(pd);
            max_dist = max(d, max_dist);
        }

        vector<vector<int>> ans;
        quick_select_to_k(point_dists, k,
                          0, points.size()-1,
                          ans);

        return ans;
    }
};
