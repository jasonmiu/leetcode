// The first idea will be sort the input points with their Euclidean distances.
// This will lead to O(nlogn). But since we only need first k points, can use
// heap to store the points, and return only the first k heap root. The C++
// heap is a max-heap, so the comparison function has to invert. The
// make_heap takes O(3n) time, and each pop takes O(logn), so in total it is
// O(n + klogn). Space is in-space, so it is O(1).
// For C++, the pop_heap() function also need comparison function not only
// make_heap().
//
// Extra, for question like "return kth element" or "return 1 to kth elements",
// can think quick select algorithm, which is a binary search based algor,
// can find the kth element in O(logn).

double dist(const vector<int>& p)
{
    return sqrt(pow(p[0], 2) + pow(p[1], 2));
}

class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int k) {

        vector<vector<int>> ans;

        auto comp = [](const vector<int>& a, const vector<int>& b) {
            if (dist(a) > dist(b)) {
                return true;
            } else {
                return false;
            }
        };

        make_heap(points.begin(), points.end(), comp);

        for (int i = 0; i < k; i++) {
            vector<int> p = points.front();
            pop_heap(points.begin(), points.end(), comp);
            points.pop_back();
            ans.push_back(p);
        }

        return ans;
    }
};
