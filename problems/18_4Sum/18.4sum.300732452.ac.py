#
# @lc app=leetcode id=18 lang=python3
#
# [18] 4Sum
#
# https://leetcode.com/problems/4sum/description/
#
# algorithms
# Medium (31.77%)
# Total Accepted:    292.3K
# Total Submissions: 902.5K
# Testcase Example:  '[1,0,-1,0,-2,2]\n0'
#
# Given an array nums of n integers and an integer target, are there elements
# a, b, c, and d in nums such that a + b + c + d = target? Find all unique
# quadruplets in the array which gives the sum of target.
# 
# Note:
# 
# The solution set must not contain duplicate quadruplets.
# 
# Example:
# 
# 
# Given array nums = [1, 0, -1, 0, -2, 2], and target = 0.
# 
# A solution set is:
# [
# ⁠ [-1,  0, 0, 1],
# ⁠ [-2, -1, 1, 2],
# ⁠ [-2,  0, 0, 2]
# ]
# 
# 
#
# This question is simular to the problem 15 3sum.
# The idea is similar, first to fix two points, a and b, and search c and d
# which can sum up to be the target.
# To find the c and d effectively, we sort the input nums first.
# So if the sum is smaller than target, we want to be bigger, so we move c to right
# If the sum is bigger than target, we want it to be smaller so we move d to left
# Then we keep moving a and b to right for trying different combinations.
# If we brute force to search all combinations, there will be nC4 combinations to test.
# The complexity is O(n^3), since the inner c and d closing cost O(n), we need run it
# O(n^2) times for outter a and b combinations. So total O(n^3).
# The space is O(n) for the answer set.
# Solved in 38mins.
# Tag: Array, sort, two pointers


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        
        ans = set()
        
        nums.sort()
        n = len(nums)
        for a in range(0, n - 4 + 1):
            for b in range(a + 1, n - 3 + 1):
                c = b + 1
                d = n - 1
                while c < d:
                    s = nums[a] + nums[b] + nums[c] + nums[d]
                    if s < target:
                        c += 1
                    elif s > target:
                        d -= 1
                    elif s == target:
                        ans.add((nums[a], nums[b], nums[c], nums[d]))
                        c += 1
                        d -= 1
                        
        #print(ans)
        
        al = []
        for a in ans:
            al.append(list(a))
            
        return al
