#
# @lc app=leetcode id=70 lang=python3
#
# [70] Climbing Stairs
#
# https://leetcode.com/problems/climbing-stairs/description/
#
# algorithms
# Easy (45.47%)
# Total Accepted:    529.6K
# Total Submissions: 1.2M
# Testcase Example:  '2'
#
# You are climbing a stair case. It takes n steps to reach to the top.
# 
# Each time you can either climb 1 or 2 steps. In how many distinct ways can
# you climb to the top?
# 
# Note: Given n will be a positive integer.
# 
# Example 1:
# 
# 
# Input: 2
# Output: 2
# Explanation: There are two ways to climb to the top.
# 1. 1 step + 1 step
# 2. 2 steps
# 
# 
# Example 2:
# 
# 
# Input: 3
# Output: 3
# Explanation: There are three ways to climb to the top.
# 1. 1 step + 1 step + 1 step
# 2. 1 step + 2 steps
# 3. 2 steps + 1 step
# 
# 
#
# A naive recusive algor can be:
# def jump(n, cur_stair):
#     cnt = 0
#     if cur_stair == n:
#         return 1
#     else:
#         for i in range(1, 3):
#             if cur_stair + i <= n:
#                 cnt += jump(n, cur_stair + i)
#
#     return cnt
#
# It is computing all possible steps we can jump on each stair.
# If plot the tree, we see a lot of same cases, like if we are now
# standing on the stair 1, we can jump 2 to step 3, and compute the
# possible combination. Later we will be on start 2, we can jump 1 to 
# step 3 again. If doing recusion here we are repeating the computation
# as we did on stair 1. So we can use DP to memorize the results before.
# The current possible combination is the sum of the step 1 and step 2
# before, we we can taking 1 step from i-1 step, or 2 steps from i-2 step to
# reach the current step.


class Solution:
    def climbStairs(self, n: int) -> int:
        ways = []
        ways.append(0) # ways[0] = 0
        ways.append(1) # ways[1] = 1
        ways.append(2) # ways[2] = 2
        
        if n < 3:
            return ways[n]
        
        for i in range(3, n+1):
            ways.append(ways[i-1] + ways[i-2])
            
        return ways[n]
