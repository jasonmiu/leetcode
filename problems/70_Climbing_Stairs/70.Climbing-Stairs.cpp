class Solution {
public:
    int climbStairs(int n) {
        if (n == 1) {
            return 1;
        }

        if (n == 2) {
            return 2;
        }

        int prev_1_way = 2;
        int prev_2_way = 1;

        int ans = 0;
        for (int i = 3; i <= n; i++) {
            ans = prev_1_way + prev_2_way;
            prev_2_way = prev_1_way;
            prev_1_way = ans;
        }

        return ans;

    }
};
