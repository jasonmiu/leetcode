// The idea is to find the index of the insertion place. While the list is sorted,
// made me think of using binary search to find the index. For example:
//   0     1     2     3      4
// [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
// The new interval with binary search lower_bound() will give the index
// 2, as it is [6,7] is the first element that has larger start number (6).
// Since the newInterval.start will always smaller or equal to the searched
// index, we check if the previous one has to be merged.
//         V- returned index
// [3 . 5][6 7]
//   [4 .  . . 8]
// hence we need to merge with the previous interval, and set the merge_start index to this
// previous index.
//
// But it can has a special case, which the new interval is totally covered by previous one, like:
//
//         V- returned index
// [3 . 5][6 7]
//   [4 5]
//
// In this case the ending point of the merged interval needed to be included in this previous
// interval. Hence we set the merge end index also to this previous interval.
//
// If the newInterval.end is spanning over the previous interval, like
//
//         V- returned index, ins_idx
// [3 . 5][6 7][8 . 10]
//   [4 .  . .  8]
//
// then we check from the current returned index, to see if the newInterval.end
// is larger or equal to the current checking interval end. If so, we need to merge
// this interval, and keep go ahead, until we find a non-overlapping interval or list end.
//
// Once we find the merge start and end indices, we create a merge interval from it.
// If no merge start of end indices found, the merge interval coming directly from
// newInterval start or end.
//
// If we has merge start, we copy all elements from list start till merge start. Otherwise,
// we copy all elements from start till the returned insert index.
// We then copy the merged interval.
// Similarly, if we has merge end, we copy all elements from merge end to list end. Otherwise,
// copy all elements from returned insert index to the end.
// Time: O(n) for searching the list start to end. Space O(n) for the answer list.
//
// While the idea works, but the main coding difficulty is because the lower_bound() is returing
// an index that is next to the possible merge start. And the merge start interval can also
// be merge end. This special case makes the merge end searching harder. Also, lower_bound()
// will return the index even the target newInterval has no overlapping with the input elements.
// Hence need a case to seperate if we merged an interval or not.
// Using lower_bound() just because I thought binary search can be faster. But indeed in the middle
// I already dicovered I have to copy the input to the output which leads a O(n) anyway.
// A better and lesser special cases way to search from beginning, copy still we need to merge start point,
// then search all mergable intervals, then from the next non-overlapping interval copy them all
// till end. Indeed this is the last 3-parts copying in this implementation. The binary search index
// is not needed. Lesson: when a algor can't be faster using such implementation, just think in a simpler
// way to avoid unthinkable special cases.



class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {

        auto ins_it = lower_bound(intervals.begin(), intervals.end(), newInterval,
                               [](const vector<int>& a, const vector<int>& b) {
                                   if (a[0] < b[0]) {
                                       return true;
                                   } else {
                                       return false;
                                   }
                               });

        int n = intervals.size();

        int ins_idx = ins_it - intervals.begin();

        int merge_start = -1;
        int merge_end = -1;

        if (ins_idx > 0) {
            auto pre_intv = intervals[ins_idx-1];

            if (pre_intv[1] >= newInterval[0]) {
                merge_start = ins_idx-1;
            }

            if (pre_intv[1] >= newInterval[1]) {
                merge_end = ins_idx-1;
            }
        }

        if (ins_idx <= n && merge_end == -1) {
            int next_idx = ins_idx;

            while (next_idx <= n-1) {
                auto next_intv = intervals[next_idx];

                if (next_intv[0] > newInterval[1]) {
                    break;
                }

                if (newInterval[1] >= next_intv[0]) {
                    merge_end = next_idx;
                }

                next_idx++;
            }
        }

        vector<int> merge_intv;
        int s = newInterval[0];
        int e = newInterval[1];
        if (merge_start != -1) {
            s = min(intervals[merge_start][0], s);
        }
        if (merge_end != -1) {
            e = max(intervals[merge_end][1], e);
        }
        merge_intv = {s,e};

        vector<vector<int>> ans;
        if (merge_start != -1) {
            for (int i = 0; i < merge_start; i++) {
                ans.push_back(intervals[i]);
            }
        } else {
            for (int i = 0; i < ins_idx; i++) {
                ans.push_back(intervals[i]);
            }
        }

        ans.push_back(merge_intv);

        if (merge_end != -1) {
            for (int i = merge_end + 1; i < n; i++) {
                ans.push_back(intervals[i]);
            }
        } else {
            for (int i = ins_idx; i < n; i++) {
                ans.push_back(intervals[i]);
            }
        }

        return ans;

    }
};
