// Since the question is asking for O(logn), it is a hint for binary search.
// However, since this is not a sorted array, consider the inputs:
// 0 1 2 3 4 5 6 7 8
//-------------------
// 1 2 3 4 5 6 7 9 8   <-- peak at 7 (value 9)
//
// 1 9 3 4 5 6 7 8 9   <-- peak at 1, and 8 (value 9)
//         ^
//         |
//         mid point
//
// For binary search, the mid points of both input are both index 4.
// So which side should go for the next binary search?
//
// Since the question ask ANY peak, if a value X next to current mid element
// is smaller than the current mid element (ie. a[m-1] < a[m]),
// this value X cannot be a peak, since a peak must be larger than its both sides.
// So we can only move to the side that has a larger element.
//
// in the case of:
// 1 9 3 4 5 6 7 8 9
// we will go to the right side, while looks like there is no peak, but indeed
// the index 8 (value 9) is a peak since its at the edge and larger than the element
// next to it.
// Is it possible the edge not be a peak?
// this case cannot happen: [1 9 3 4 5 6 7 8 8], since question said no conscutive same element.
// this case: [1 9 3 4 5 6 7 8 7], the edge is not peak but it is small than previous, so index 7 is peak.
// So if an edge is previous, it is a peak. It cannot be the same as previous. If smaller than previous
// previous is a peak.
// It is similar for the case on the left too, for example:
//
// [10, 9, 8, 7, 5, 6, 7, 8, 9]
// While moving to left has no peak until the edge, index 0.
//
// Time O(logn) since search half every time. Space O(1).

int bs(const vector<int>& a, int s, int e)
{
    int m = s + ((e-s) / 2);

    int left = INT_MIN;
    int right = INT_MIN;
    if (m >= 1) {
        left = a[m-1];
    }

    if (m <= a.size()-2) {
        right = a[m+1];
    }

    if (left < a[m] && a[m] > right) {
        return m;
    } else if (s >= e) {
        return -1;
    }

    if (left > a[m]) {
        return bs(a, s, m-1);
    } else if (right > a[m]) {
        return bs(a, m+1, e);
    }

    return -1;
}

class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        if (nums.size() == 1) {
            return 0;
        }

        return bs(nums, 0, nums.size()-1);
    }
};
