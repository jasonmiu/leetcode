#
# @lc app=leetcode id=162 lang=python3
#
# [162] Find Peak Element
#
# https://leetcode.com/problems/find-peak-element/description/
#
# algorithms
# Medium (42.06%)
# Total Accepted:    314.5K
# Total Submissions: 737.6K
# Testcase Example:  '[1,2,3,1]'
#
# A peak element is an element that is greater than its neighbors.
# 
# Given an input array nums, where nums[i] ≠ nums[i+1], find a peak element and
# return its index.
# 
# The array may contain multiple peaks, in that case return the index to any
# one of the peaks is fine.
# 
# You may imagine that nums[-1] = nums[n] = -∞.
# 
# Example 1:
# 
# 
# Input: nums = [1,2,3,1]
# Output: 2
# Explanation: 3 is a peak element and your function should return the index
# number 2.
# 
# Example 2:
# 
# 
# Input: nums = [1,2,1,3,5,6,4]
# Output: 1 or 5 
# Explanation: Your function can return either index number 1 where the peak
# element is 2, 
# or index number 5 where the peak element is 6.
# 
# 
# Note:
# 
# Your solution should be in logarithmic complexity.
# 
#
# The idea is use the property of for each element A[i],
# the elements next to it will not equal to A[i]. So,
# [0, 0, 1] or [1, 2, 2, 1] are invaild.
# To do O(logn) complexity, first idea is binary search.
# Since for an element it must not equal to the next element,
# the peak must be the largest one between the the current element
# and its next element. For example, 
# [1, 2, 3, 1]
#     i  j
# Compare the A[i] and A[j] the peak must be on the right side.
# So we can search recursively to the larger side, until we have 
# only 1 element, which must be the peak.
# If the size of input array is odd, we have the middle element
# can be directly compare with the elements of its two sides.
# If the middle current element is the largest, it is a peak, and return.
# Else we search the larger value side.
# One implemenation trick is when use the start and end index for 
# the range of the current input, the middle point needs to be 
# added to the start as the middle point is the relative middle of
# the start and end indices.
# Complexity O(logn), as we cut half every time.
# Space O(1), no extra space is needed.

def findp(nums, s, e):
    if s == e:
        return s
    
    n = e - s + 1
    if n % 2 == 0:
        i = ((e - s) // 2) + s
        j = i + 1
        if nums[j] > nums[i]:
            return findp(nums, j, e)
        else:
            return findp(nums, s, i)
    else:
        i = ((e - s) // 2) + s
        h = i - 1
        j = i + 1
        if nums[i] > nums[h] and nums[i] > nums[j]:
            return i
        elif nums[h] > nums[i]:
            return findp(nums, s, h)
        elif nums[j] > nums[i]:
            return findp(nums, j, e)
        

class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        ans = findp(nums, 0, len(nums)-1)
        return ans
