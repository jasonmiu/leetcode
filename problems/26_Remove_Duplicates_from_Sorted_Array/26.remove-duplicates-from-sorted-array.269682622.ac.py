#
# @lc app=leetcode id=26 lang=python3
#
# [26] Remove Duplicates from Sorted Array
#
# https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/
#
# algorithms
# Easy (42.29%)
# Total Accepted:    722.3K
# Total Submissions: 1.7M
# Testcase Example:  '[1,1,2]'
#
# Given a sorted array nums, remove the duplicates in-place such that each
# element appear only once and return the new length.
# 
# Do not allocate extra space for another array, you must do this by modifying
# the input array in-place with O(1) extra memory.
# 
# Example 1:
# 
# 
# Given nums = [1,1,2],
# 
# Your function should return length = 2, with the first two elements of nums
# being 1 and 2 respectively.
# 
# It doesn't matter what you leave beyond the returned length.
# 
# Example 2:
# 
# 
# Given nums = [0,0,1,1,1,2,2,3,3,4],
# 
# Your function should return length = 5, with the first five elements of nums
# being modified to 0, 1, 2, 3, and 4 respectively.
# 
# It doesn't matter what values are set beyond the returned length.
# 
# 
# Clarification:
# 
# Confused why the returned value is an integer but your answer is an array?
# 
# Note that the input array is passed in by reference, which means modification
# to the input array will be known to the caller as well.
# 
# Internally you can think of this:
# 
# 
# // nums is passed in by reference. (i.e., without making a copy)
# int len = removeDuplicates(nums);
# 
# // any modification to nums in your function would be known by the caller.
# // using the length returned by your function, it prints the first len
# elements.
# for (int i = 0; i < len; i++) {
# print(nums[i]);
# }
#
# The input array is sorted, so the items on the right must be 
# larger than the left. The first item, nums[0], must be the 
# smallest. All items after it, has two cases:
# 1. same value as nums[0]
# 2. bigger than nums[0]
# let the nums[0] as the current max value (curmax), start
# scanning the items on its right, with index 'j'.
# The next index for replacment is 'i'.
# If case 1, keep the i, and let j move forward.
# If case 2, replace the nums[i] with nums[j], 
# then move i and j forward. The curmax become nums[j]. Repeat this
# until the end of the array.
# We only scan the array once, so O(n).


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        
        # special case of the zero size nums
        if len(nums) == 0:
            return 0
        
        # the nums array has at least one item. So the result length must
        # at least be 1
        curmax = nums[0]
        i = 1
        
        for j in range(1, len(nums)):
            if nums[j] > curmax:
                curmax = nums[j]
                nums[i] = nums[j]
                i = i + 1
                
        return i
