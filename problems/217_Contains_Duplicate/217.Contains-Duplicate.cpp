class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> has_nums;

        for (int a : nums) {
            if (has_nums.find(a) != has_nums.end()) {
                return true;
            } else {
                has_nums.insert(a);
            }
        }

        return false;

    }
};
