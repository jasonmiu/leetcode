// The question, "start from any index of the 'fruits' array, then move forward continuously
// we can collect at most 2 types of fruits, return the max amount of fruits.", means:
// "Find out the longest length of subarrays with at most 2 different numbers?"
// First idea is to start with index 0, then move forward using a set to count the element values
// we encounter, if we saw more than 2 different numbers, check the current sequence length,
// then start from next index 1, 2 ... n-1. This leads a O(n^2) and time limit excess.
//
// Another idea is to use sliding window, the length of subarray is usually related to sliding window.
// Try to keep the current sliding window contains only 2 different numbers at most.
// This is the loop invariant we want to keep for checking whole input. If we hit a 3rd type
// of number in the current window, check the window size, and shrink the window from left until it contains
// only 2 different numbers again. We can check the types of numbers using a count dictionary.
// Example:
//
//  0 1 2 3 4
// [0 0 1 2 2]
//  ^
//  |- s (start (left) of the current window)
//  |- e (end (right) of the current window)
//
// From beginning, the window has size 1 and starts from the first element.
// Check the current element at the end of the window, add it to the count dictionary:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:1]
//  ^
//  |- s (start (left) of the current window)
//  |- e (end (right) of the current window)
//
// since we only saw 1 type of number, enlarge the window from right:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:1]
//  ^ ^
//  | e
//  s
//
// Check the current element, "0", and add to the counts:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:2]
//  ^ ^
//  | e
//  s
//
// Check the current element, "1", and add to the counts:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:2, 1:1]
//  ^   ^
//  |   e
//  s
//
// window has two different numbers, can still enlarge the window
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:2, 1:1]
//  ^     ^
//  |     e
//  s
//
// Now add the current element "2" to the counts:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:2, 1:1, 2:1]
//  ^     ^
//  |     e
//  s
//
// Now the window has 3 different numbers, which violated loop invariant. So we want to shrink
// the window from its starting point, which like sliding the window to right. We want to shrink
// the window until it contains only two different numbers, ie. the count dictionary size = 2.
// Thats why we have the count dictionary instead of simple hash set, as we want to check
// when to stop the shrink. Also, save the current window size to see if it is the largest so far.
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:2, 1:1, 2:1]
//  ^     ^
//  |     e
//  s
//
// Now move the 's' pointer to right, and decrease the count of the element it was pointing:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:1, 1:1, 2:1]
//    ^   ^
//    |   e
//    s
//
// Count dictionary still has 3 different numbers. Keep shrinking:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [0:0, 1:1, 2:1]
//      ^ ^
//      | e
//      s
//
// Now the count of the number "0" is zero now, means the window is small enough to keeping out
// those "0" on the left. Can remove the "0" element in the count dictionary:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [1:1, 2:1]
//      ^ ^
//      | e
//      s
//
// Try next element by enlarge the window:
//
//  0 1 2 3 4
// [0 0 1 2 2]          count: [1:1, 2:2]
//      ^   ^
//      |   e
//      s
//
// Finally the window hit the end of the input:
//  0 1 2 3 4
// [0 0 1 2 2]          count: [1:1, 2:2]
//      ^     ^
//      |     e
//      s
//
// Since the window testing condition was only happening when we see the 3rd type of number, here
// we also want to check the last window size when the window end point hit the end of the input.
// Finally we return the max window size so far. Since the window always contain 2 different numbers
// at most, and every time a new element appear or at the end of the input we check the window size,
// we get the max window in the input. Time: O(n) since scan the input with window forward once,
// Space: O(k) k for the number of different numbers we want to save, in this case k is 2 hence
// space is constance.


class Solution {
public:
    int totalFruit(vector<int>& fruits) {

        unordered_map<int,int> counts;

        int s = 0;
        int e = 0;
        int max_len = INT_MIN;
        while (e < fruits.size()) {
            int ele = fruits[e];

            counts[ele] ++;

            if (counts.size() > 2) {

                max_len = max(max_len, (e-s));

                while (true) {
                    int left_hand_ele = fruits[s];
                    counts[left_hand_ele] --;
                    s++;
                    if (counts[left_hand_ele] == 0) {
                        counts.erase(left_hand_ele);
                    }

                    if (counts.size() <= 2) {
                        break;
                    }
                }
            }

            e++;
        }

        max_len = max(max_len, (e-s));

        return max_len;
    }
};
