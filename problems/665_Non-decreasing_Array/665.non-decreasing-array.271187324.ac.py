#
# @lc app=leetcode id=665 lang=python3
#
# [665] Non-decreasing Array
#
# https://leetcode.com/problems/non-decreasing-array/description/
#
# algorithms
# Easy (19.41%)
# Total Accepted:    66.7K
# Total Submissions: 343.8K
# Testcase Example:  '[4,2,3]'
#
# 
# Given an array with n integers, your task is to check if it could become
# non-decreasing by modifying at most 1 element.
# 
# 
# 
# We define an array is non-decreasing if array[i]  holds for every i (1 
# 
# Example 1:
# 
# Input: [4,2,3]
# Output: True
# Explanation: You could modify the first 4 to 1 to get a non-decreasing
# array.
# 
# 
# 
# Example 2:
# 
# Input: [4,2,1]
# Output: False
# Explanation: You can't get a non-decreasing array by modify at most one
# element.
# 
# 
# 
# Note:
# The n belongs to [1, 10,000].
# 
#
# This is a 2 phases algor. The first phase, find if the list is already Non-decreasing, 
# ie. nums[i] >= nums[i-1]. If this is the case, we return True.
# If we found a nums[i] is smaller than nums[i-1], we have 2 ways to modify the list
# 1. new_nums[i] = nums[i-1]. eg: [2, 3, 3, 2, 4], i = 3, new_nums = [2, 3, 3, 3, 4]
# 2. new_nums[i-1] = nums[i]. eg: [2, 3, 3, 2, 4], i = 3, new_nums = [2, 3, 2, 2, 4]
# Then put two lists to scan if they are non-decreasing. If any one case is non-decreasing,
# return True, else, False.
# Since we can only modify once, we can make sure we will have only 1 split to create two
# lists. This is a O(3n) = O(n) algor.
# We can think recursively, at i, all nums[j] is <= nums[i], while j < i. When we found a 
# decreasing value, we can create two cases with two lists and put in the same recursive
# function


class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:
        
        modi_lists = []
        for i in range(1, len(nums)):
            if nums[i] >= nums[i-1]:
                continue
            else:
                modi_lists.append(list(nums))
                modi_lists.append(list(nums))
                
                nums1 = modi_lists[0]
                nums2 = modi_lists[1]
                
                nums1[i-1] = nums[i]
                nums2[i] = nums[i-1]
                break
                
        if len(modi_lists) == 0:
            return True
        else:
            r1 = True
            r2 = True
            
            #print("nums1", nums1)
            #print("nums2", nums2)
            
            for i in range(1, len(nums)):
                if nums1[i] < nums1[i-1]:
                    r1 = False
                    
                if nums2[i] < nums2[i-1]:
                    r2 = False
                    
            return (r1 or r2)
                    
            
                
