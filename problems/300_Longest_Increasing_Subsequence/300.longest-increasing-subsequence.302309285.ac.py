#
# @lc app=leetcode id=300 lang=python3
#
# [300] Longest Increasing Subsequence
#
# https://leetcode.com/problems/longest-increasing-subsequence/description/
#
# algorithms
# Medium (41.60%)
# Total Accepted:    309.4K
# Total Submissions: 738.9K
# Testcase Example:  '[10,9,2,5,3,7,101,18]'
#
# Given an unsorted array of integers, find the length of longest increasing
# subsequence.
# 
# Example:
# 
# 
# Input: [10,9,2,5,3,7,101,18]
# Output: 4 
# Explanation: The longest increasing subsequence is [2,3,7,101], therefore the
# length is 4. 
# 
# Note: 
# 
# 
# There may be more than one LIS combination, it is only necessary for you to
# return the length.
# Your algorithm should run in O(n^2) complexity.
# 
# 
# Follow up: Could you improve it to O(n log n) time complexity?
# 
#
# The two main ideas both led to the DP solutions:
# 1. For each element, if it is bigger than last selected value, we can select this or not, 
#    and get the length for that increasing seq. Recusively to get the longest one.
# 2. If we know the longest seq of array A[i], we should know the longest seq of array A[i+1],
#    if the value of A[i+1] is larger than A[i], we want to know which previous i it should be 
#    joined to create the new longest seq. The best i it should join will be the one has the
#    current longest seq. If we use a DP array to store the longest seq up to i, we 
#    search back the DP array to find the previous longest seq len which the A[i+1] is still larger
#    than that A[i]
# But I had difficulty to implement both ideas. For the recursive search idea, I didn't know how to 
# select/unselect an element in the array to pass on. I might not even figure out the correct out
# the correct burte force algor, which I missed the case [1, 5, 2, 3], where the 5 should be skipped
# to get a longer seq. Larger value between two smaller values (hill) confused me.
# For the DP while I know the DP array should be the longest seq up to current i, but I could not
# find a way of connecting the current value i previous longest seq to get the new longest seq len.
# 
# For recursion, the point is to pass on the next value to make progress, also the value we want to compare.
# For DP, need to think how to mantain the ans of the subproblem. The elements in DP array should all be
# the ans of the subproblem we solved so far.
#
# Recusive with memo has time complexity O(n^2), and space O(n^2)
# DP has time complexity O(n^2) and space O(n)
#
# Another confusion is both ans are still O(n^2), I get dragged to think the O(nlogn) algor.
# For O(nlogn) ref: https://algorithmsandme.com/longest-increasing-subsequence-in-onlogn/


def lis(nums, prev_num, i, memo):
    if i >= len(nums):
        return 0
    
    if (prev_num, i) in memo:
        return memo[(prev_num, i)]
    
    if nums[i] > prev_num:
        use_this_val = lis(nums, nums[i], i+1, memo) + 1
        not_use_this_val = lis(nums, prev_num, i+1, memo)
    else:
        use_this_val = 0
        not_use_this_val = lis(nums, prev_num, i+1, memo)
    
    r = max(use_this_val, not_use_this_val)
    
    memo[(prev_num, i)] = r
    
    return r

class Solution:
    def lengthOfLIS_memo(self, nums: List[int]) -> int:
        ans = lis(nums, -9999999999999, 0, {})
        return ans
    
    def lengthOfLIS_dp(self, nums: List[int]) -> int:
        
        n = len(nums)
        if n == 0:
            return 0
        
        dp = [0] * n
        dp[0] = 1
        ans = 1
        
        for i in range(1, n):
            cur_max_len = 0
            for j in range(0, i):
                if nums[i] > nums[j]:
                    cur_max_len = max(cur_max_len, dp[j])
            dp[i] = cur_max_len + 1
            
            ans = max(ans, dp[i])
        
        return ans
    
    def lengthOfLIS(self, nums: List[int]) -> int:
        #ans = self.lengthOfLIS_memo(nums)
        ans = self.lengthOfLIS_dp(nums)
    
        return ans
    
        
