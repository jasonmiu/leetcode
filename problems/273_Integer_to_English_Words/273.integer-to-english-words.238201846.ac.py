# TO EXPLAIN
#
# @lc app=leetcode id=273 lang=python3
#
# [273] Integer to English Words
#
# https://leetcode.com/problems/integer-to-english-words/description/
#
# algorithms
# Hard (25.18%)
# Total Accepted:    125.1K
# Total Submissions: 496.8K
# Testcase Example:  '123'
#
# Convert a non-negative integer to its english words representation. Given
# input is guaranteed to be less than 2^31 - 1.
# 
# Example 1:
# 
# 
# Input: 123
# Output: "One Hundred Twenty Three"
# 
# 
# Example 2:
# 
# 
# Input: 12345
# Output: "Twelve Thousand Three Hundred Forty Five"
# 
# Example 3:
# 
# 
# Input: 1234567
# Output: "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty
# Seven"
# 
# 
# Example 4:
# 
# 
# Input: 1234567891
# Output: "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven
# Thousand Eight Hundred Ninety One"
# 
# 
#
class Solution:
    
    BILL = 1000000000
    MILL = 1000000
    THOU = 1000
    
    tens = {2: "Twenty",
            3: "Thirty",
            4: "Forty",
            5: "Fifty",
            6: "Sixty",
            7: "Seventy",
            8: "Eighty",
            9: "Ninety"}
    ones = {
        1: "One",
        2: "Two",
        3: "Three",
        4: "Four",
        5: "Five",
        6: "Six",
        7: "Seven",
        8: "Eight",
        9: "Nine",
        10: "Ten",
        11: "Eleven",
        12: "Twelve",
        13: "Thirteen",
        14: "Fourteen",
        15: "Fifteen",
        16: "Sixteen",
        17: "Seventeen",
        18: "Eighteen",
        19: "Nineteen"
    }
    
    
    def numberToWords(self, num: int) -> str:
        
        if num == 0:
            return "Zero"
        
        s = ""
        
        if num // self.BILL != 0:
            s = s + self.numberToWords(num // self.BILL) + " Billion "
            num = num % self.BILL
            
        if num // self.MILL != 0:
            s = s + self.numberToWords(num // self.MILL) + " Million "
            num = num % self.MILL
        
        if num // self.THOU != 0:
            s = s + self.numberToWords(num // self.THOU) + " Thousand "
            num = num % self.THOU
            
        if num // 100 != 0:
            s = s + self.numberToWords(num // 100) + " Hundred "
            num = num % 100
            
        # from 20
        if num >= 20 and (num // 10 != 0):
            s = s + self.tens[num // 10] + " "
            num = num % 10
        
        # from 1 - 19
        if num != 0:
            s = s + self.ones[num]
            
        return s.strip()
