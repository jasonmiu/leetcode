#
# @lc app=leetcode id=198 lang=python3
#
# [198] House Robber
#
# https://leetcode.com/problems/house-robber/description/
#
# algorithms
# Easy (41.41%)
# Total Accepted:    386.6K
# Total Submissions: 933.7K
# Testcase Example:  '[1,2,3,1]'
#
# You are a professional robber planning to rob houses along a street. Each
# house has a certain amount of money stashed, the only constraint stopping you
# from robbing each of them is that adjacent houses have security system
# connected and it will automatically contact the police if two adjacent houses
# were broken into on the same night.
# 
# Given a list of non-negative integers representing the amount of money of
# each house, determine the maximum amount of money you can rob tonight without
# alerting the police.
# 
# Example 1:
# 
# 
# Input: [1,2,3,1]
# Output: 4
# Explanation: Rob house 1 (money = 1) and then rob house 3 (money =
# 3).
# Total amount you can rob = 1 + 3 = 4.
# 
# Example 2:
# 
# 
# Input: [2,7,9,3,1]
# Output: 12
# Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5
# (money = 1).
# Total amount you can rob = 2 + 9 + 1 = 12.
# 
# 
#
#
# The max return of nums[i] is the 
# nums[i] + max_return[i-2]
# Build a list, all_sums, for storing the max return of
# each index i

class Solution:
    def rob(self, nums: List[int]) -> int:
        self.all_sums = [] 
        self.all_sums_max = -1
        
        if len(nums) == 0:
            return 0
        
        for i in range(0, len(nums)):
            
            # case 1:
            # nums = [1]
            # all_sums = [1]
            if i == 0:
                v = nums[i]
                if v >= self.all_sums_max:
                    self.all_sums_max = v
                    
                self.all_sums.append(v)
            
            # case 2:
            # nums = [1, 2]
            # all_sums = [2] (max(1, 2))
            elif i == 1:
                v = max(nums[i], nums[i-1])
                self.all_sums.append(v)
            
            # case 3:
            # nums = [1, 2, 3]
            # all_sums = [1, 2, 4] (nums[i] + max(all_sums[0:i-1]))
            else:
                # only store the max of i-2, e.g.
                # nums =     [1, 2, 3]
                # all_sums = [1, 2, 4]
                # i        = 2
                # i - 2    = 0
                # all_sums_max = all_sums[0] = 1
                if self.all_sums[i-2] >= self.all_sums_max:
                    self.all_sums_max = self.all_sums[i-2]
                    
                v = nums[i] + self.all_sums_max
                self.all_sums.append(v)        

        
        print(self.all_sums)
        
        return(max(self.all_sums))
