// When the rows and columns are sorted, first idea may be the binary search.
// But the problem is, in this question, it is asking to get the k-th element,
// which is the position, ie. the index if it is a flatten list, of some element.
// So what is that "element" we want to search? Usually the binary search is to have
// a target element, and get the index of it, like the std::lower_bound(). What if
// not searcting the target element, but the target index?
//
// The main point is to have the target index k, there is at least k numbers of elements
// are lesser or equal than k. For example, [1 2 3 4 5 5 5 6], and k is 5, there are
// 8 elements lesser or equal than k. So given a any element x, we want to see
// how many elements is lesser or equal than k. If this count is lesser than k, means
// this element x must at an index smaller than k, and we want to find another element
// that is larger than x.
//
// To pick the x, the intuitive way is to get the mid of the range of the input. The mix and max
// of this sorted matrix is the upper left corner and lower right corner. If the number of
// lesser or equal of mid is smaller than k, move the mid to right. Otherwise, move it to left.
//
// If the current range has more than 1 element, keep cutting it in half and find the range of the target.
// If the count of the lesser or equal of mid is equal to k, while it can mean the current mid is the
// anwser, be the smaller element can also be too -- since the current mid may not be a member element of
// the matrix. For example:
//
// rank     1  2  3   4   5   6   7   8   9
//------------------------------------------
// k = 8 : (1, 5, 9, 10, 11, 12, 13, 13, 15)
// [[ 1, 5, 9]
//  [10,11,13]
//  [12,13,15]]
//
// [1, 15], mid = 8, count of lesser or equal of 8: (1, 5) = 2. Move to right
// [9, 15], mid = 12, count of lesser of equal of 12: (1, 5, 9, 10, 11) = 5, move to right
// [13, 15], mid = 14, count of lesser of equal of 14: (1, 5, 9, 10, 11, 12, 13, 13) = 8, move to left
// [13, 14], mid = 13, count of lesser of equal of 13: (1, 5, 9, 10, 11, 12, 13, 13) = 8, move to left
// [13, 13], the range min max is the same which means has only 1 target element left, so answer is 13.
//
// Since if the count of mid is lesser than k, we move the min to mid + 1, so the final min must has count bigger or equal to k.
// And if the count of mid is equal or bigger than k, we move the max to mid, so the final max must be smaller or equal to k.
// So when the final min and max are equal, it has "count bigger or equal to k AND count lesser or equal to k", and
// since it cannot be bigger and lesser than k at the same time, so it must has the count equal to k.
//
// Another point is how to find the count fast, instead of count it from beginning.
// In a sorted array, the count of the elements that are smaller or equal to a target is the index of the target + 1. Like:
// [1 2 3 4 5]
// The count of elements that are smaller of equal of 4 will be index(4) + 1 = 4.
// so if start from the first row, and the last column, and compare that element to the target. If the target is smaller,
// then it can only be on the left of this row.
// If target is bigger, just add the count on this row. The target can be bigger than the the element of the next row
// in the same column too, so try move down one row and repeat. This is similar to the saddleback search.
// https://www.geeksforgeeks.org/saddleback-search-algorithm-in-a-2d-array/
//
// Since each time in the count function we eliminate 1 row or 1 column, it takes n time to scan all rows or columns.
// And takes O(logn) to find the final mid, so the total time is O(nlogn). Space is O(1).

int count_lesser_eql(int x, const vector<vector<int>>& a)
{
    int n = a.size();

    int i = 0;
    int j = n - 1;
    int cnt = 0;
    while (i < n && j >= 0) {
        if (a[i][j] <= x) {
            cnt += j + 1;
            i ++;
        } else {
            j = j - 1;
        }
    }

    return cnt;
}


class Solution {
public:
    int kthSmallest(vector<vector<int>>& matrix, int k) {

        int n = matrix.size();

        int matrix_min = matrix[0][0];
        int matrix_max = matrix[n-1][n-1];
        int mid = 0;
        while (matrix_min < matrix_max) {
            mid = matrix_min + ((matrix_max - matrix_min) / 2);

            int cnt = count_lesser_eql(mid, matrix);

            if (cnt < k) {
                matrix_min  = mid + 1;
            } else {
                matrix_max = mid;
            }
        }

        return matrix_min;
    }
};
