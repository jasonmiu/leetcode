# The tree is left-right flipped, ie, the left nodes become right, right nodes become
# left. So the idea is for each node, we swap its left right subtree.
# Complexity: O(n) to access each node.
# Space: O(1) as no new storage needed.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def inv(root):
    if root == None:
        return root
    
    left = root.left
    right = root.right
    root.left = right
    root.right = left
    
    inv(root.left)
    inv(root.right)
    
    return root

class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        
        n = inv(root)
        return n

