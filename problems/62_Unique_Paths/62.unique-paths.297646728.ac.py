#
# @lc app=leetcode id=62 lang=python3
#
# [62] Unique Paths
#
# https://leetcode.com/problems/unique-paths/description/
#
# algorithms
# Medium (49.82%)
# Total Accepted:    382.3K
# Total Submissions: 751.1K
# Testcase Example:  '3\n2'
#
# A robot is located at the top-left corner of a m x n grid (marked 'Start' in
# the diagram below).
# 
# The robot can only move either down or right at any point in time. The robot
# is trying to reach the bottom-right corner of the grid (marked 'Finish' in
# the diagram below).
# 
# How many possible unique paths are there?
# 
# 
# Above is a 7 x 3 grid. How many possible unique paths are there?
# 
# Note: m and n will be at most 100.
# 
# Example 1:
# 
# 
# Input: m = 3, n = 2
# Output: 3
# Explanation:
# From the top-left corner, there are a total of 3 ways to reach the
# bottom-right corner:
# 1. Right -> Right -> Down
# 2. Right -> Down -> Right
# 3. Down -> Right -> Right
# 
# 
# Example 2:
# 
# 
# Input: m = 7, n = 3
# Output: 28
# 
#
# The main point of the question is the robot can only move right and down. Means
# it cannot loop back. Another point is the goal is always bottom right. 
# Instead of using DFS to search all paths, we can see a bottom right corner, 
# can be arrived from two directions, its top and left.
# o o o
# o o V
# o > x
# To go x, can only go from its top and left.
# So we can think the bottom right corner has two ways to be reached.
# If we have 'a' ways to arrive its top (V) and 'b' ways to arrives its
# left (>), then the ways arrive to this corner will be a + b.
# So it looks like a DP problem. And we want to translate all subproblems
# as a smaller map, going to its bottom right corner, like:
# o o o 
# o o o    => o o  => x 
# o o x       o x
# We treat a cell is the ways arrive to that cell, hence
# the way to arrive to cell c[i][j] = c[i-1][j] + c[i][j-1] which is
# the ways from its top and left.
# Two special cases is a horizontal and vertical grids, like:
# o o x  or o
#           o
#           x
# Then they has only 1 way to arrive to the end of the grids. Hence
# we initialize the DP cells to 1 for its first row and column.
# The complexity is O(mn), since we walk all cells once. And the space is
# O(mn) for the DP map.

class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        
        dp = [[0] * m for x in range(0, n)]
        for i in range(0, n):
            dp[i][0] = 1
            
        for j in range(0, m):
            dp[0][j] = 1
            
        for i in range(1, n):
            for j in range(1, m):
                dp[i][j] = dp[i-1][j] + dp[i][j-1]
                
        return dp[n-1][m-1]
