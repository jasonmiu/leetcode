# The idea is splitting the array in two halves, left and right.
# With the max value as the current root, pass the left and right halves
# to the same function recursively. It is the same as finding the max value
# of each half of the input array.
# Finding the max takes O(n), if the max is always the left (or right) most,
# we need to call the functions n times. Hence O(n^2).
# FOr the space, if the max is also always the left or right most, means we
# are moving the index from left to right (or reverse) one by one. Hence the
# space will be O(n).

# Intesting O(n) monotonic stack algor
# https://leetcode.com/problems/maximum-binary-tree/discuss/258364/Python-O(n)-solution-with-explanation.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


def max_tree(nums):
    n = len(nums)
    if n == 0:
        return None
    
    m = max(nums)
    
    root = TreeNode(m, None, None)
    
    m_idx = nums.index(m)
    l = nums[0:m_idx]
    r = nums[m_idx+1::]
    
    l_tree = max_tree(l)
    r_tree = max_tree(r)
    
    root.left = l_tree
    root.right = r_tree
    
    return root
    
    

class Solution:
    def constructMaximumBinaryTree(self, nums: List[int]) -> TreeNode:
        ans = max_tree(nums)
        return ans
