#
# @lc app=leetcode id=908 lang=python3
#
# [908] Smallest Range I
#
# https://leetcode.com/problems/smallest-range-i/description/
#
# algorithms
# Easy (64.97%)
# Total Accepted:    31.4K
# Total Submissions: 48.3K
# Testcase Example:  '[1]\n0'
#
# Given an array A of integers, for each integer A[i] we may choose any x with
# -K <= x <= K, and add x to A[i].
# 
# After this process, we have some array B.
# 
# Return the smallest possible difference between the maximum value of B and
# the minimum value of B.
# 
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: A = [1], K = 0
# Output: 0
# Explanation: B = [1]
# 
# 
# 
# Example 2:
# 
# 
# Input: A = [0,10], K = 2
# Output: 6
# Explanation: B = [2,8]
# 
# 
# 
# Example 3:
# 
# 
# Input: A = [1,3,6], K = 3
# Output: 0
# Explanation: B = [3,3,3] or B = [4,4,4]
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= A.length <= 10000
# 0 <= A[i] <= 10000
# 0 <= K <= 10000
# 
# 
# 
# 
#
# The question is actually asking, for all A[i] we can expand it
# to a range, from -K <= A[i] <= K. For all ranges, what is the
# smallest difference of the smallest range max and largest range min.
# Start with the simplest case:
#
#            2  -- ceiling
# K = 1, A = 1
#            0  -- floor
# If ceiling >= floor, we can pick a number in this range which is A[i], so 
# the smallest difference will be 0.
#
# K = 2, A = 1, 10
#
#                12 
#                10   A[1]
#                8  -- highest_floor
#             
#            2     -- lowest_ceiling
#            0     -- A[0]
#           -2
# if the highest_floor > lowest_ceiling, we cannot pick a number which satisfy 
# b = A[0] = A[1] where b is the number can be selected from 2 ranges.
# The smallest possible difference will be highest_floor - lowerest_ceiling.
# The complexity is O(n) for scanning the input list.
# The space complexity is O(1) as we need no extra space.

import math

class Solution:
    def smallestRangeI(self, A: List[int], K: int) -> int:
        
        lowest_ceiling = math.inf
        highest_floor = - math.inf
        
        for a in A:
            floor = a + (-K)
            ceiling = a + K
            if floor >= highest_floor:
                highest_floor = floor
            if ceiling <= lowest_ceiling:
                lowest_ceiling = ceiling
                
        if lowest_ceiling >= highest_floor:
            return 0
        else:
            return highest_floor - lowest_ceiling
        
        
