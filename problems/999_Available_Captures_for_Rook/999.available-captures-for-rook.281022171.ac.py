#
# @lc app=leetcode id=999 lang=python3
#
# [999] Available Captures for Rook
#
# https://leetcode.com/problems/available-captures-for-rook/description/
#
# algorithms
# Easy (65.71%)
# Total Accepted:    20.7K
# Total Submissions: 31.4K
# Testcase Example:  '[[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","R",".",".",".","p"],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]'
#
# On an 8 x 8 chessboard, there is one white rook.  There also may be empty
# squares, white bishops, and black pawns.  These are given as characters 'R',
# '.', 'B', and 'p' respectively. Uppercase characters represent white pieces,
# and lowercase characters represent black pieces.
# 
# The rook moves as in the rules of Chess: it chooses one of four cardinal
# directions (north, east, west, and south), then moves in that direction until
# it chooses to stop, reaches the edge of the board, or captures an opposite
# colored pawn by moving to the same square it occupies.  Also, rooks cannot
# move into the same square as other friendly bishops.
# 
# Return the number of pawns the rook can capture in one move.
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input:
# [[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","R",".",".",".","p"],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 3
# Explanation: 
# In this example the rook is able to capture all the pawns.
# 
# 
# Example 2:
# 
# 
# 
# 
# Input:
# [[".",".",".",".",".",".",".","."],[".","p","p","p","p","p",".","."],[".","p","p","B","p","p",".","."],[".","p","B","R","B","p",".","."],[".","p","p","B","p","p",".","."],[".","p","p","p","p","p",".","."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 0
# Explanation: 
# Bishops are blocking the rook to capture any pawn.
# 
# 
# Example 3:
# 
# 
# 
# 
# Input:
# [[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","p",".",".",".","."],["p","p",".","R",".","p","B","."],[".",".",".",".",".",".",".","."],[".",".",".","B",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 3
# Explanation: 
# The rook can capture the pawns at positions b5, d6 and f5.
# 
# 
# 
# 
# Note:
# 
# 
# board.length == board[i].length == 8
# board[i][j] is either 'R', '.', 'B', or 'p'
# There is exactly one cell with board[i][j] == 'R'
# 
# 
#
# The idea is to search all positions to find out the position of
# "R" (rook). Then from this position, search 4 directions.
# If in a direction, we see a "B", then we stop.
# If we see a "p", we stop as can only capture 1 pawn, increase the capture counter.
# The point is for upward and to-left direction, we need to count the
# positions in a reverse way.
# The complexity is O(n), n is the number of posistions.
# The time complexity is O(1), as no need to get new space.
#
# A more elegent way instead of using 4 loops. Using incremental offsets in 4 directions
#    def numRookCaptures(self, board):
#        for i in range(8):
#            for j in range(8):
#                if board[i][j] == 'R':
#                    x0, y0 = i, j
#        res = 0
#        for i, j in [[1, 0], [0, 1], [-1, 0], [0, -1]]:
#            x, y = x0 + i, y0 + j
#            while 0 <= x < 8 and 0 <= y < 8:
#                if board[x][y] == 'p': res += 1
#                if board[x][y] != '.': break
#                x, y = x + i, y + j
#        return res

class Solution:
    
    def check_capture(self, board, row, col):
        if board[row][col] == "B":
            return 0
        if board[row][col] == "p":
            return 1
        
        return -1
    
    def numRookCaptures(self, board: List[List[str]]) -> int:
                
        rook_pos = None
        
        row_len = len(board)
        col_len = len(board[0])
        
        for r in range(0, row_len):
            for c in range(0, col_len):
                if board[r][c] == "R":
                    rook_pos = [r, c]
                    break
                    
        capture = 0

        r = rook_pos[0]
        for c in range(rook_pos[1]-1, -1, -1):
            ret = self.check_capture(board, r, c)
            if ret >= 0:
                capture += ret
                break
            
        for c in range(rook_pos[1]+1, col_len):
            ret = self.check_capture(board, r, c)
            if ret >= 0:
                capture += ret
                break

        c = rook_pos[1]
        for r in range(rook_pos[0]-1, -1, -1):
            ret = self.check_capture(board, r, c)
            if ret >= 0:
                capture += ret
                break
        
        for r in range(rook_pos[0]+1, row_len):
            ret = self.check_capture(board, r, c)
            if ret >= 0:
                capture += ret
                break
                
        return capture


