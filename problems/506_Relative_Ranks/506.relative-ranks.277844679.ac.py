#
# @lc app=leetcode id=506 lang=python3
#
# [506] Relative Ranks
#
# https://leetcode.com/problems/relative-ranks/description/
#
# algorithms
# Easy (49.10%)
# Likes:    206
# Dislikes: 430
# Total Accepted:    47.5K
# Total Submissions: 96.6K
# Testcase Example:  '[5,4,3,2,1]'
#
# 
# Given scores of N athletes, find their relative ranks and the people with the
# top three highest scores, who will be awarded medals: "Gold Medal", "Silver
# Medal" and "Bronze Medal".
# 
# Example 1:
# 
# Input: [5, 4, 3, 2, 1]
# Output: ["Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"]
# Explanation: The first three athletes got the top three highest scores, so
# they got "Gold Medal", "Silver Medal" and "Bronze Medal". For the left two
# athletes, you just need to output their relative ranks according to their
# scores.
# 
# 
# 
# Note:
# 
# N is a positive integer and won't exceed 10,000.
# All the scores of athletes are guaranteed to be unique.
# 
# 
# 
#

# @lc code=start
# The question is not clear about if the scores are continuous, ie. [5, 4, 3, 2, 1] or
# not. Eventually it is not, case like "[10,3,8,9,4]" can appear.
# Since the highest score (rank 1) may not be the len(nums), and the list is not continuous so
# we cannot use the different of the current number and the highest score to compute the rank.
# Hence, we need to sort the input list to find the ranks. Sorting it will change element positions
# but the result should be returned in the same order of the input. So we use a hash table to 
# save the original positions, key is the score number and the value is the original position index.
# The scores are unique so they are good for the hash key.
# Then we scan the sorted input list which known as the ranks, and re-construct the posistions in the
# result list with the hash table.
# Complexity is O(nlogn) for the sort.
# Space complexity is O(n) for the hash table and result list.

class Solution:
    def findRelativeRanks(self, nums: List[int]) -> List[str]:
        nd = {}
        for i, n in enumerate(nums):
            nd[n] = i
            
        nums.sort()
        nums.reverse()
        ans = [0] * len(nums)
        
        for i, n in enumerate(nums):
            if i == 0:
                ans[nd[n]] = "Gold Medal"
            elif i == 1:
                ans[nd[n]] = "Silver Medal"
            elif i == 2:
                ans[nd[n]] = "Bronze Medal"
            else:
                ans[nd[n]] = str(i + 1)
                
        return ans
                
# @lc code=end
