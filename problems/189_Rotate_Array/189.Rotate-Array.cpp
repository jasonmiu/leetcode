// For input:
// nums = [1,2,3,4,5,6,7], k = 3
// output is:
// [5,6,7,1,2,3,4]
// If treat the last k elements of the input as the "tail array",
// then the tail array will be moved to the head of the output.
//  0 1 2 3  4 5 6    N = 7
// [1,2,3,4,(5,6,7)]
//           |   |
//          N-k  N-1
// If want to use only O(1) space, easiest way is to bouble up
// each element in tail array to the array head + current tail array index, like:
// [1,2,3,4,(5,6,7)]
//        \__|
//           swap
//
// [1,2,3,5,4,6,7]
//      \__|
//         swap
//
// [1,2,5,3,4,6,7]
//    \__|
//       swap
//
// [1,5,2,3,4,6,7]
//  \__|
//     swap
//
// [5,1,2,3,4,6,7]
//          \__|
//             swap
//
// [5,6,1,2,3,4,7]
//          \__|
//             swap
//
// But this will cost O(n^2).
// If we check the input:
// [1,2,3,4,5,6,7]
// reverse it we got:
// [7,6,5,4,3,2,1]
//  \  /  \    /
//    |     |
//    |     We want this part at the end of output
//    The tail array is now at the head
//
// Both partitions are in their places of output, but each element is reversed.
// So just reverse two partitions individually, to get the result:
// [7,6,5,4,3,2,1]
//  \  /  \    /
//    |     |
//    |     Reverse
//   Reverse
//
// [5,6,7,1,2,3,4]
// Since 1 Reverse operation take O(n) and inplace, the overall time is O(n), while space is O(1).
// Another point is if the rotaion number k is larger than n, it means repeat from the start again.
// So real rotaion number is k % n. If real rotation number is 0, means no rotaion then just return.


#include <algorithm>

class Solution {
public:
    void rotate(vector<int>& nums, int k) {

        k = k % nums.size();

        if (k == 0) {
            return;
        }

        std::reverse(nums.begin(), nums.end());
        std::reverse(nums.begin(), nums.begin() + k);
        std::reverse(nums.begin() + k, nums.end());
    }
};
