#
# @lc app=leetcode id=189 lang=python3
#
# [189] Rotate Array
#
# https://leetcode.com/problems/rotate-array/description/
#
# algorithms
# Easy (31.90%)
# Total Accepted:    412.7K
# Total Submissions: 1.3M
# Testcase Example:  '[1,2,3,4,5,6,7]\n3'
#
# Given an array, rotate the array to the right by k steps, where k is
# non-negative.
# 
# Example 1:
# 
# 
# Input: [1,2,3,4,5,6,7] and k = 3
# Output: [5,6,7,1,2,3,4]
# Explanation:
# rotate 1 steps to the right: [7,1,2,3,4,5,6]
# rotate 2 steps to the right: [6,7,1,2,3,4,5]
# rotate 3 steps to the right: [5,6,7,1,2,3,4]
# 
# 
# Example 2:
# 
# 
# Input: [-1,-100,3,99] and k = 2
# Output: [3,99,-1,-100]
# Explanation: 
# rotate 1 steps to the right: [99,-1,-100,3]
# rotate 2 steps to the right: [3,99,-1,-100]
# 
# 
# Note:
# 
# 
# Try to come up as many solutions as you can, there are at least 3 different
# ways to solve this problem.
# Could you do it in-place with O(1) extra space?
# 
#
# To have O(1) space, we can only update 1 element at a time.
# First we can see if the k >= n, the net effect is like rotated
# the array k % n times. 
# For example: [1 2 3 4 5 6], k = 2
# The last two elements will be moved to the head, "5"
# will be at the head of new array. We can think to reverse that part
# [1 2 3 4 6 5]
# looking from right hand side, the 1 - 4 is reversed, so reverse them as well
# [4 3 2 1 6 5]
# Then it is correct from right. So reverse them all.
# Each reverse takes O(n) by using two-pointers.
# Complexity O(n)

def rev(arr, s, e):
    i = s
    j = e
    while i < j:
        t = arr[i]
        arr[i] = arr[j]
        arr[j] = t
        i += 1
        j -= 1
        
    return
    
    
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        n = len(nums)
        d = k % n
        if d == 0:
            return
        
        rev(nums, n-d, n-1)
        rev(nums, 0, n-d-1)
        rev(nums, 0, n-1)
        
        return


"""
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:

        
        n = len(nums)
        
        d = k % n
        if d == 0:
            return
        
        for i in range(0, d):
            a = nums.pop(-1)
            nums.insert(0, a)
            
        return
"""        
