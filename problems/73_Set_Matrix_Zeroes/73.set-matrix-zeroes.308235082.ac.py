#
# @lc app=leetcode id=73 lang=python3
#
# [73] Set Matrix Zeroes
#
# https://leetcode.com/problems/set-matrix-zeroes/description/
#
# algorithms
# Medium (41.19%)
# Total Accepted:    274.4K
# Total Submissions: 653.6K
# Testcase Example:  '[[1,1,1],[1,0,1],[1,1,1]]'
#
# Given a m x n matrix, if an element is 0, set its entire row and column to 0.
# Do it in-place.
# 
# Example 1:
# 
# 
# Input: 
# [
# [1,1,1],
# [1,0,1],
# [1,1,1]
# ]
# Output: 
# [
# [1,0,1],
# [0,0,0],
# [1,0,1]
# ]
# 
# 
# Example 2:
# 
# 
# Input: 
# [
# [0,1,2,0],
# [3,4,5,2],
# [1,3,1,5]
# ]
# Output: 
# [
# [0,0,0,0],
# [0,4,5,0],
# [0,3,1,0]
# ]
# 
# 
# Follow up:
# 
# 
# A straight forward solution using O(mn) space is probably a bad idea.
# A simple improvement uses O(m + n) space, but still not the best
# solution.
# Could you devise a constant space solution?
# 
# 
#
# The difficulty is updating a row and column of a Zero element,
# will also affect the elements afterward. Assuming we scan the matrix
# from top to down left to right, for a matrix like:
# 1 1 0
# 1 1 0
# 1 1 1
#
# For the top right Zero, if we update the row and column to Os for it,
# the matrix becomes:
# 0 0 0
# 1 1 0
# 1 1 0
# The bottom right element became Zero too, in the last scanning the final 
# matrix becomes:
# 0 0 0
# 0 0 0
# 0 0 0
# While the correct one should be:
# 0 0 0
# 0 0 0
# 1 1 0
# So if the original value is not zero, even it get changed to zero due to the
# previous zero elements, the its row and column should not be changed to zeros.
# So we need a way to remember the original values. 
# The easiest way is to copy the whole matrix and refer the copy as the original 
# values. Space complexity is O(mn)
# Or we can check if the element is zero, we save its row and column for the 2nd
# phase. In the 2nd phase, we update the saved rows and columns to zeros.
# Space complexity is O(m+n), in case all rows and columns needed to be changed, 
# we have : rows = [0, 1 ... m-1], cols = [0, 1, ... n-1].
# To have space complexity O(1), we need to use the matrix itself.
# The main problem is both values before and after the update are both Zeros. 
# We like to break this "updated values become future trigger values". We marks all
# Zeros with 'x' (special value other than all values in the input), they are the
# original "trigger values". In the 2nd phase, we update the rows and columns 
# for the 'x' element, but *do not* update any other 'x' element in the affected
# areas, so other trigger values will not get overwrote. Then we change the current
# scanned trigger value to Zero.
# The Time complexity is O((m+n)mn). since the worse case is 
# all are zeros. For each element, we need to visit m rows and n cols, hence (m+n),
# and we have m*n elements, so O((m+n)mn)
# The space complexity is O(1).
#
# Another faster solution is using the first element of each row to save if this 
# row has a trigger value. And first element of each col to save if this col has a 
# trigger, and process only A[1:m][1:n], ie, exclude the first row and first col.
# To save if we did have trigger values in first row or first col,
# we have two boolean vars. If any one is true, we update the first row or first col 
# to zeros as well. This combined the 'x' idea and the idea saving rows and cols 
# (the O(m+n) space idea).

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        
        m = len(matrix)
        if m == 0:
            return
        n = len(matrix[0])
        if n == 0:
            return
        
        for i in range(0, m):
            for j in range(0, n):
                if matrix[i][j] == 0:
                    matrix[i][j] = 'x'
                    
        for i in range(0, m):
            for j in range(0, n):
                if matrix[i][j] == 'x':
                    for c in range(0, n):
                        if matrix[i][c] != 'x':
                            matrix[i][c] = 0
                            
                    for r in range(0, m):
                        if matrix[r][j] != 'x':
                            matrix[r][j] = 0
                            
                    matrix[i][j] = 0
                    
        return
        
