// With C++, since the vector can only store int type,
// update a cell to a special value that is not int like the anwser in python is not possible.
// Use special int value like INT_MAX does not work since any int can appear in the matrix.
// To make a space O(1), need to find a way to store the original zero positions.
// Since the rows and cols of a zero will get zero-ed finally, some space in those get-zeroed-soon
// cells may be useful.
// The thinking process is to think what space may only used once and will not be needed anymore.
// For a zero cell, the row will get zeroed anyway. If a row has multiple zero cells, it will get
// zeroed once. And if we scan rows downward, each row only need to be processed once and no need to
// look back.
// So if a row has a zero, just make the whole row zero. It is ok for row, since we process row
// as whole and only update the current row, the rows afterward will not be touched, those values
// will be remain the same until we process that row. However, we cannot update the col for a zero
// cell like this, since if we do so, the whole col will get updated then the cell in the afterward
// rows will be changed to 0 which affect the result.
// For the cols to be changed, we cannot update together with the row. So need a place to save them.
// The first row, can be used as saving the cols to be updated. A zero cell will zero its col anyway,
// so mark the first row with the col of a zero cell is ok. Since the first row has to store the
// zero cols, if there is a origin zero in the row, the whole row will get zeroed which means
// all cols will also be zeroed, which is not we want. So check if the first row has zero first,
// save it as a bool value. Then process row from the 2nd row, and mark the zero cols in the first row.
// Finally if the first row has zero originally, mark the whole row to zero.
// Time: O(mn), Space: O(1).

bool row_has_zero(vector<vector<int>>& a, int r)
{
    bool found = false;
    for (int c = 0; c < a[0].size(); c++) {
        if (a[r][c] == 0) {
            a[0][c] = 0;
            found = true;
        }
    }

    return found;
}

void set_row_zero(vector<vector<int>>& a, int r)
{
    for (int c = 0; c < a[0].size(); c++) {
        a[r][c] = 0;
    }

    return;
}

void set_col_zero(vector<vector<int>>& a, int c)
{
    for (int r = 0; r < a.size(); r++) {
        a[r][c] = 0;
    }

    return;
}

class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {

        bool first_row_has_zero = row_has_zero(matrix, 0);

        for (int i = 1; i < matrix.size(); i++) {
            if (row_has_zero(matrix, i)) {
                set_row_zero(matrix, i);
            }
        }

        for (int j = 0; j < matrix[0].size(); j++) {
            if (matrix[0][j] == 0) {
                set_col_zero(matrix, j);
            }
        }

        if (first_row_has_zero) {
            set_row_zero(matrix, 0);
        }

        return;

    }
};
