// It is related to the problem 39, combination sum, but has some important differents:
// 1. The input elements can be duplicated
// 2. The input elements will only be used once in a combination
//
// To check all possible combinations, the basic technique is search all possible choices like
// searching down a tree, with all choices as the nodes. For this problem, since a element can
// only be used once, going down each level we move forward to the next choice.
// Since we are try to find a sum to match the target, to reduce the search space, which means
// to reduce the possible choices, if we can sort the input first, and sum from left to right,
// once the sum is over the target, we no longer need to try the remaining elements since elements
// on the right hand side are bigge and can only give a even bigger sum.
// For input example:
// [2,5,2,1,2], target = 5
// Sort it:
// [1 2 2 2 5], target = 5
//
// Then start from first index, element 1, we try to pick next possible element to generate a combination:
//
//                                              root
//                              /       /        |       \         \
//              ,,,,,,,,,,,,,,,1       2        ,2        2         5
//             /    /    /    /       /|\      / |
//      ,,,,,,2    2    2    5       2 2 5    2  5
//     /  /  /     | \              /| |\     |
//    2  2  5      2  5            2 5 2 5    5
//    ^  ^         ^
//    |  |         |
//    |  [1 2 2]   [1 2 2]
//    [1 2 2]
//
// The problem is, since the elements can be duplicated, same choice can be appear again in the selection path.
// In the example above, first selected [1] (lv1), then the index 1 [1,2] (lv2), then the index 2, [1,2,2] (lv3).
// Once this is a match, the search goes back to lv2, and select the next possible choice, which is index 2, and
// the element is also 2. So a repeated combination appeared. Same thing happens when the search goes back to lv1,
// current select at lv1 is [1], and it try to select the next choice, which is index 2, and it is "2" again like
// its previous element. Hence the same combination will appear.
//
// To prevent this, we want to process the same element *once* at the *same level*. That is, when we see repeated
// choice, we skip it. Since input is sorted, repeated elements mean they are clustered together. We like the
// to process those clustered elements once at this level, we can skip the element if it is not the first one
// and found it is the same as the previous. I use a set to stored seen elements, if this is a first-seen element,
// we process and put it to the seen set. If we saw the same element again, just skip it. This use bit more space (O(n))
// but make thing easier and cleaner as no need to take care the index processing.
//
// Time is O(2^n) since the worse case is to generate all unique combinations where no duplication can be found.
// Space is O(n) for the current combination storage, also the seen set. But since we have to store all
// combinations so the space will be O(2^n).



void find_comb(const vector<int>& a, int s, int target, int sum, vector<int> cur, vector<vector<int>>& result)
{
    if (sum == target) {
        result.push_back(cur);
        return;
    }

    if (s >= a.size()) {
        return;
    }

    unordered_set<int> seen;

    for (int i = s; i < a.size(); i++) {
        if (seen.find(a[i]) == seen.end()) {
            seen.insert(a[i]);

            if (a[i] + sum <= target) {
                cur.push_back(a[i]);
                find_comb(a, i+1, target, a[i]+sum, cur, result);
                cur.pop_back();
            } else {
                return;
            }
        }
    }

    return;
}

class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {

        sort(candidates.begin(), candidates.end());

        vector<int> cur;
        vector<vector<int>> result;

        find_comb(candidates, 0, target, 0, cur, result);

        return result;
    }
};
