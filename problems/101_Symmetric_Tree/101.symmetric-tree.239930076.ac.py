#
# @lc app=leetcode id=101 lang=python3
#
# [101] Symmetric Tree
#
# https://leetcode.com/problems/symmetric-tree/description/
#
# algorithms
# Easy (44.77%)
# Total Accepted:    484.9K
# Total Submissions: 1.1M
# Testcase Example:  '[1,2,2,3,4,4,3]'
#
# Given a binary tree, check whether it is a mirror of itself (ie, symmetric
# around its center).
# 
# For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
# 
# 
# ⁠   1
# ⁠  / \
# ⁠ 2   2
# ⁠/ \ / \
# 3  4 4  3
# 
# 
# 
# 
# But the following [1,2,2,null,3,null,3] is not:
# 
# 
# ⁠   1
# ⁠  / \
# ⁠ 2   2
# ⁠  \   \
# ⁠  3    3
# 
# 
# 
# 
# Note:
# Bonus points if you could solve it both recursively and iteratively.
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# The idea is to traverse the tree level by level with BFS.
# For each level, convert the node values in to a list
# and check if the list is a mirror, like:
# [1, 2, 3, 3, 2, 1], by cutting it in to 2 halves and 
# compare the revered second half to the first half.
# If all levels are mirrors, then the tree is symmetric,
# otherwise, not symmetric.
# One trick is we have to fill in the Nones as the values
# used in the mirror list checking for those empty nodes.

import queue
class Solution:
    def is_list_mirror(self, elements):
        n = len(elements)
        if n % 2 == 0:
            mid = n // 2
            first_half = elements[0:mid]
            sec_half = list(reversed(elements[mid::]))
        else:
            mid = n // 2
            first_half = elements[0:mid]
            sec_half = list(reversed(elements[mid+1::]))
            
        #print("first", first_half)
        #print("sec", sec_half)
        if first_half == sec_half:
            return True
        else:
            return False
            
    def isSymmetric(self, root: TreeNode) -> bool:

        q = queue.Queue()       
        q.put(root)
        
        while not q.empty():
            qsize = q.qsize()
            
            level_nodes = []
            level_vals = []
            for i in range(0, qsize):
                n = q.get()
                level_nodes.append(n)
                if n:
                    level_vals.append(n.val)
                else:
                    level_vals.append(None)
                
            if self.is_list_mirror(level_vals):
                for n in level_nodes:
                    if n:
                        q.put(n.left)
                        q.put(n.right)
            else:
                return False
            
        return True

            
        
