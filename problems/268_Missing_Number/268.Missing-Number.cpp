void place(vector<int>& nums, int idx, int e, int token)
{
    if (nums[idx] == e) {
        return;
    }

    if (nums[idx] == token) {
        nums[idx] = e;
        return;
    }

    int d = nums[idx];
    nums[idx] = e;
    place(nums, d, d, token);
    return;
}

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int n = nums.size();
        int token = n+1;

        nums.push_back(token);
        nums.push_back(token);

        for (int i = 0; i <= n; i++) {
            if (nums[i] != i) {
                place(nums, i, token, token);
            }
        }

        for (int i = 0; i <= n; i++) {
            if (nums[i] == token) {
                return i;
            }
        }

        return 0;
    }
};
