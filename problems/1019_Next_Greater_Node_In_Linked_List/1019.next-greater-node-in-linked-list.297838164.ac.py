#
# @lc app=leetcode id=1019 lang=python3
#
# [1019] Next Greater Node In Linked List
#
# https://leetcode.com/problems/next-greater-node-in-linked-list/description/
#
# algorithms
# Medium (56.82%)
# Total Accepted:    28K
# Total Submissions: 49.4K
# Testcase Example:  '[2,1,5]'
#
# We are given a linked list with head as the first node.  Let's number the
# nodes in the list: node_1, node_2, node_3, ... etc.
# 
# Each node may have a next larger value: for node_i, next_larger(node_i) is
# the node_j.val such that j > i, node_j.val > node_i.val, and j is the
# smallest possible choice.  If such a j does not exist, the next larger value
# is 0.
# 
# Return an array of integers answer, where answer[i] =
# next_larger(node_{i+1}).
# 
# Note that in the example inputs (not outputs) below, arrays such as [2,1,5]
# represent the serialization of a linked list with a head node value of 2,
# second node value of 1, and third node value of 5.
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: [2,1,5]
# Output: [5,5,0]
# 
# 
# 
# Example 2:
# 
# 
# Input: [2,7,4,3,5]
# Output: [7,0,5,5,0]
# 
# 
# 
# Example 3:
# 
# 
# Input: [1,7,5,1,9,2,5,1]
# Output: [7,9,9,9,0,5,0,0]
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= node.val <= 10^9 for each node in the linked list.
# The given list has length in the range [0, 10000].
# 
# 
# 
# 
#
# The idea is using monotonic descreasing stack.
# Monotonic stack is a stack that elements are monotonic descreasing or inscreasing,
# for example a desceasing stack:
# 
# 1  <- stack top
# 3
# 5
# 6  <- bottom
#
# Mono stack is good for finding next larger/lesser number in a list.
# We scan a list, if the current value is larger than the current stack top, 
# which means the list is no longer descreasing, like:
# [ 7 5 9 ]
#   ^      S = [7]
#     ^    S = [7 5]
#       ^  S = [9] <-- the current value 9 is larger than current stack top 5, 
#                      pop 5, and the current stack is 7, 9 is still larger than stack top 7,
#                      pop 7, repeat this until the stack top is larger than current value,
#                      or the stack is empty. 
# To do this we can record the "valley" of the list, and find the next larger value on the 
# right hand side.
# The tricky part is how to use the info of this stack. The elements in the stack, 
# are the "waiting" elements to see if any value on the right hand side is larger. 
# We can save their index, and when they are being poped out, means we find a value on their right
# which is larger than them, we can update the larger value at their index.
# Compliexity is O(n) for scanning the list once. Space is O(n) for the result array.


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def nextLargerNodes(self, head: ListNode) -> List[int]:
        s = []
        i = 0
        n = head
        s.append((i, n.val))
        ans = [0]

        n = n.next
        while n:
            i += 1
            ans.append(0)
            #print("i", i, "s", s, "n", n.val)
            while len(s) > 0 and n.val > s[-1][1]:
                e = s.pop()
                ans[e[0]] = n.val
                
            s.append((i, n.val))
            n = n.next
            
        return ans
                
            
            
            
        
