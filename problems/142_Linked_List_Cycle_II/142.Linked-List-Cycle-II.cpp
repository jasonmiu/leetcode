// This is the typical Floyd's tortoise and hare algor, for detecting the link list cycle.
// This algor can be used to find the starting point of a link list cycle too. The starting point
// of the cycle is, the first node has visited twice, ie. in-degree is 2.
// We can scan and use visited hash table to find it, but space is O(n). Tortoise and hare (fast slow pointer)
// do not need extra space.
// The idea is, fast pointer always move 2 nodes forward, the slow pointer always move only 1 node forward.
// If there is a cycle, the fast pointer will loopback at some point, and the slow pointer will catch up,
// then they will meet at some node. If they meet, means a cycle is detected. If any one is nullptr, means
// it moved to the end and no cycle is detected.
// To return the cycle starting point, should reset the fast pointer back to the head. And let the slow pointer
// start move forward 1 node from the meeting point, while the new reset fast pointer move 1 node at a time too.
// They will meet again, and the new meeting point is the cycle starting point. The proof can be found here:
// https://medium.com/@edgar-loves-python/the-tortoise-the-hare-and-the-cyclical-linked-list-1b51acab5b
// One important special case is, if the first round of scanning both pointers meet at *the head* of the list,
// means the list is actually a big circle, the second scan is not needed. For example:
//
// s
// f
// 1 -> 2 -> 3 -> 4
// ^              |
// |______________|
//
//
//      s
//           f
// 1 -> 2 -> 3 -> 4
// ^              |
// |______________|
//
//
//           s
// f
// 1 -> 2 -> 3 -> 4
// ^              |
// |______________|
//
//
//                s
//           f
// 1 -> 2 -> 3 -> 4
// ^              |
// |______________|
//
//
// s
// f
// 1 -> 2 -> 3 -> 4
// ^              |
// |______________|
//
// First scan ended here, and both pointers meet at the head. In this case, the cycle starting point is the head.
//
// Time: O(n), linearly scan twice. Space: O(1).


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {

        ListNode* fast = head;
        ListNode* slow = head;

        while (fast != nullptr && slow != nullptr) {

            if (fast->next != nullptr) {
                fast = fast->next->next;
            } else {
                fast = nullptr;
                break;
            }

            slow = slow->next;

            if (fast == slow) {
                break;
            }

        }

        if (fast == nullptr || slow == nullptr) {
            return nullptr;
        }

        if (slow == head) {
            return head;
        }

        fast = head;
        while (fast != slow) {
            slow = slow->next;
            fast = fast->next;
        }

        return slow;

    }
};
