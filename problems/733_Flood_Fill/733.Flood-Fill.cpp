// A basic flood fill 4 ways recursive algor.
// For each position, fill the new color, and then recursively fill the north, east, south west
// directions. A special case is when original color equals to new color, the algor
// need to figure out if the current position get visited or not.
// For example:
//
///   0 1 2
//   |-----
// 0 |8 8 8
// 1 |8 9 9
//
// star (1, 1), new color = 9
//
///   0 1 2
//   |-----
// 0 |8 8 8
// 1 |8 9 9
//      ^- start from this, it updates (0,1), then (1,2).
//         Since (1,2) has value 9, it is also the original original color,
//         Then it can then loop back to (1,1), then again (1,2), and loop forever
//
// To break this case, we can check if the current position color is alread the new color,
// then we do not go ahead. Or can use set, keep track the visited positions.
// Set is more general, but costs more memory also have to write a custom hash function in c++
// for the position (pair<int,int>).
//
// Time O(n) since visit all elements. Space O(1).



#include <unordered_set>

using namespace std;

void ff(vector<vector<int>>& image, int sr, int sc, int org_color, int new_color)
{
    if ((sr < 0) || (sr >= image.size()) || (sc < 0) || (sc >= image[0].size())) {
        return;
    }

    if (image[sr][sc] != org_color || image[sr][sc] == new_color) {
        return;
    }

    image[sr][sc] = new_color;
    ff(image, sr - 1, sc, org_color, new_color);
    ff(image, sr, sc + 1, org_color, new_color);
    ff(image, sr + 1, sc, org_color, new_color);
    ff(image, sr, sc - 1, org_color, new_color);

    return;
}

class Solution {
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor) {

        ff(image, sr, sc, image[sr][sc], newColor);

        return image;
    }
};
