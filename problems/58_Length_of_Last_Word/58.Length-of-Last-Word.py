# Make use of the python advantage.
# The str.split(), without input argument, handles consecutive spaces as
# single separator. ie., for string like:
# "   First    Second three3   Four   "
# str.split() returns:
# ["First", "Second", "three3", "Four"]
# This makes the solution very easy. Just split into all substrings,
# all whitespaces get handled automatically.
# Then return the len of the last word.
#
# From the doc: https://docs.python.org/3/library/stdtypes.html#str.split
#  str.split(sep=None, maxsplit=- 1)
#
#     If sep is given, consecutive delimiters are not grouped together
#     and are deemed to delimit empty strings (for example,
#     '1,,2'.split(',') returns ['1', '', '2']). The sep argument may
#     consist of multiple characters (for example,
#     '1<>2<>3'.split('<>') returns ['1', '2', '3']). Splitting an
#     empty string with a specified separator returns [''].
#
# So if the separator has been specified, consecutive separators behave differently
# from whitespaces.
# Another algor that run faster and space with O(1), is to scan char from the end of
# the input string, skip all tailing spaces, when hit a non-space char, means we are
# at the last word, count to the next space from right. That will be the last word
# len.
# Time: O(n), Space: O(n)


class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        words = s.split()
        return len(words[-1])
