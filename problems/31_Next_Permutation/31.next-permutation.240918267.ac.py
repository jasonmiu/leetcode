#
# @lc app=leetcode id=31 lang=python3
#
# [31] Next Permutation
#
# https://leetcode.com/problems/next-permutation/description/
#
# algorithms
# Medium (31.26%)
# Total Accepted:    284.8K
# Total Submissions: 911.1K
# Testcase Example:  '[1,2,3]'
#
# Implement next permutation, which rearranges numbers into the
# lexicographically next greater permutation of numbers.
# 
# If such arrangement is not possible, it must rearrange it as the lowest
# possible order (ie, sorted in ascending order).
# 
# The replacement must be in-place and use only constant extra memory.
# 
# Here are some examples. Inputs are in the left-hand column and its
# corresponding outputs are in the right-hand column.
# 
# 1,2,3 → 1,3,2
# 3,2,1 → 1,2,3
# 1,1,5 → 1,5,1
# 
#
# The idea start with 'how to generate permutation'.
# For example we have [1, 2, 3], to generate permutation,
# we fill in blanks with possible values, and backtrack 
# the swapped value. 
# 1 (_) _
# The first choice will be 2 by lex order.
# 1 2 (_)
# The only choice now is 3, so the first permutation is 1, 2, 3
# To find the next permutation, we want to know which position
# can still has choice, ie. which position we are going to 
# do backtrack swap. 
# For 1, 2, 3, '3' is the last choice so we do not backtrack
# then 1, 2, _, 2 can be backtrack and swapped with 3
# So if 1, 3, _, then the sequence will be 1, 3, 2, which
# is the next permutation.
# For sequenace 3, 4, 2, 1, the next permutation is 
# 4, 1, 2, 3
# We can see from the right, if there is a subsequence
# that is in a strictly decreasing order 
# (for all num[i], num[i-1] >= num[i]),
# that subsequence used up all choice and the number right 
# before it is the swap backtrack point.
# So we want to find the first number from right, 
# which is the first one that its left handside is smaller than
# it. We call this number "first_num".
# The swap point is the number from right, the first, min
# number that is bigger than "first_num", we can this "sec_num".
# The subsequence from first_num index + 1 must be strictly 
# decreasing order since we are looking for the last permutation
# of that subsequence. We swap the first_num and sec_num as we
# doing the permutation. Then reverse the subsequence from 
# first_num index + 1 to get the next permutation. It is because
# the first permutation is always in a strictly increasing order.
# The complexity is O(n)

class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        
        n = len(nums)
        first_num = -1
        first_num_idx = 0
        sec_num = 9999999999
        sec_num_idx = 0
        for i in range(n-2, -1, -1):
            if nums[i] < nums[i+1]:
                first_num = nums[i]
                first_num_idx = i
                break
                
        if first_num == -1:
            nums.reverse()
            return
        
                
        for i in range(n-1, first_num_idx-1, -1):
            if nums[i] > first_num and nums[i] < sec_num:
                sec_num = nums[i]
                sec_num_idx = i
                
                
        #print("first_num", first_num, "first_idx", first_num_idx, "sec_num", sec_num, "sec_idx", sec_num_idx)
                
        nums[first_num_idx] = sec_num
        nums[sec_num_idx] = first_num
        
        nums[first_num_idx+1::] = reversed(nums[first_num_idx+1::])
        
        return
        
        
                
