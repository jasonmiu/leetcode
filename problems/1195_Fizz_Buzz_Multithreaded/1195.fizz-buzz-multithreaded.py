# The idea is to have a work queue, containes all numbers from 1 to n.
# Then have the threads pick the task from the queue head, so the output
# will be in series. This is the comsumer model.
# The queue is the CS, so we need to have lock to guard it. The trick is
# after release the lock, we put the thread to sleep for a very short time,
# to let the thread scheduler to pick another thread, otherwise the first thread
# can occupy forever and other threads have no chance to jump in.
# Another way is the producer model. Each thread generate the output, 
# from 1 to n. The number() thread need to run first, and be blocked
# then we see a number can be divisible by 3, 5, or (3 and 5), the thread
# release the lock of that thread.

import threading
import time

us = 1 / 1000000.0
m = 0.01

class FizzBuzz:
    def __init__(self, n: int):
        self.n = n
        self.q = list(range(1, n+1))
        self.lock = threading.Lock()

    # printFizz() outputs "fizz"
    def fizz(self, printFizz: 'Callable[[], None]') -> None:
        while True:
            
            self.lock.acquire()
            if len(self.q) == 0:
                self.lock.release()
                break
                
            else:
                p = self.q[0]
                if (p % 3 == 0) and (p % 5 != 0):
                    p = self.q.pop(0)
                    printFizz()
                
            self.lock.release()
            time.sleep(m * us)
            
        return
            

    # printBuzz() outputs "buzz"
    def buzz(self, printBuzz: 'Callable[[], None]') -> None:
        while True:
            
            self.lock.acquire()
            if len(self.q) == 0:
                self.lock.release()
                break
                
            else:
                p = self.q[0]
                if (p % 3 != 0) and (p % 5 == 0):
                    p = self.q.pop(0)
                    printBuzz()
                
            self.lock.release()
            time.sleep(m * us)
            
        return        

    # printFizzBuzz() outputs "fizzbuzz"
    def fizzbuzz(self, printFizzBuzz: 'Callable[[], None]') -> None:
        while True:
            
            self.lock.acquire()
            if len(self.q) == 0:
                self.lock.release()
                break
                
            else:
                p = self.q[0]
                if (p % 3 == 0) and (p % 5 == 0):
                    p = self.q.pop(0)
                    printFizzBuzz()
                
            self.lock.release()
            time.sleep(m * us)
            
        return
        
    # printNumber(x) outputs "x", where x is an integer.
    def number(self, printNumber: 'Callable[[int], None]') -> None:
        while True:
            
            self.lock.acquire()
            if len(self.q) == 0:
                self.lock.release()
                break
                
            else:
                p = self.q[0]
                if (p % 3 != 0) and (p % 5 != 0):
                    p = self.q.pop(0)
                    printNumber(p)
                
            self.lock.release()
            time.sleep(m * us)

        return
