#
# @lc app=leetcode id=744 lang=python3
#
# [744] Find Smallest Letter Greater Than Target
#
# https://leetcode.com/problems/find-smallest-letter-greater-than-target/description/
#
# algorithms
# Easy (44.60%)
# Likes:    272
# Dislikes: 402
# Total Accepted:    52.3K
# Total Submissions: 117.2K
# Testcase Example:  '["c","f","j"]\n"a"'
#
# 
# Given a list of sorted characters letters containing only lowercase letters,
# and given a target letter target, find the smallest element in the list that
# is larger than the given target.
# 
# Letters also wrap around.  For example, if the target is target = 'z' and
# letters = ['a', 'b'], the answer is 'a'.
# 
# 
# Examples:
# 
# Input:
# letters = ["c", "f", "j"]
# target = "a"
# Output: "c"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "c"
# Output: "f"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "d"
# Output: "f"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "g"
# Output: "j"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "j"
# Output: "c"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "k"
# Output: "c"
# 
# 
# 
# Note:
# 
# letters has a length in range [2, 10000].
# letters consists of lowercase letters, and contains at least 2 unique
# letters.
# target is a lowercase letter.
# 
# 
#

# @lc code=start
# When the question says "wrap around", indeed two cases:
# target = 'z', then all char from 'a' is larger than it
# target = anychar, but no char in the list is larger than it,
# the min in the list is the smallest letter greater than it.
# We first map a-z to 1-26. The target value is it mapped value mod 26,
# so target value of a-z is 1, 2 ... 25, 0
# Then scan the letter list and find the smallest letter greater than target value
# If we cannot find one, means the case 2. We return the min value in the list
# The complexity is O(n)
# The space complexity is O(1)
# I missed the list is sorted. If so we can seach via bin-search

class Solution:
    def nextGreatestLetter(self, letters: List[str], target: str) -> str:

        char_map = {}
        for i in range(0, 26):
            c = chr(ord('a') + i)
            char_map[c] = i + 1

        #print("char_map" , char_map)
        
        min_val = 65536
        min_idx = -1
        min_in_list = 65536
        min_in_list_idx = -1
        target_val = char_map[target] % 26
        
        for i, c in enumerate(letters):
            c_val = char_map[c]
            if c_val > target_val and c_val < min_val:
                min_val = c_val
                min_idx = i
            
            if c_val <= min_in_list:
                min_in_list = c_val
                min_in_list_idx = i
                
        if min_idx == -1:
            min_idx = min_in_list_idx
            
        
        return letters[min_idx]
            
                
        
        
            
        

# @lc code=end
