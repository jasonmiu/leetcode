#
# @lc app=leetcode id=979 lang=python3
#
# [979] Distribute Coins in Binary Tree
#
# https://leetcode.com/problems/distribute-coins-in-binary-tree/description/
#
# algorithms
# Medium (67.92%)
# Total Accepted:    31.1K
# Total Submissions: 45.6K
# Testcase Example:  '[3,0,0]'
#
# Given the root of a binary tree with N nodes, each node in the tree has
# node.val coins, and there are N coins total.
# 
# In one move, we may choose two adjacent nodes and move one coin from one node
# to another.  (The move may be from parent to child, or from child to
# parent.)
# 
# Return the number of moves required to make every node have exactly one
# coin.
# 
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: [3,0,0]
# Output: 2
# Explanation: From the root of the tree, we move one coin to its left child,
# and one coin to its right child.
# 
# 
# 
# Example 2:
# 
# 
# 
# 
# Input: [0,3,0]
# Output: 3
# Explanation: From the left child of the root, we move two coins to the root
# [taking two moves].  Then, we move one coin from the root of the tree to the
# right child.
# 
# 
# 
# Example 3:
# 
# 
# 
# 
# Input: [1,0,2]
# Output: 2
# 
# 
# 
# Example 4:
# 
# 
# 
# 
# Input: [1,0,0,null,3]
# Output: 4
# 
# 
# 
# 
# Note:
# 
# 
# 1<= N <= 100
# 0 <= node.val <= N
# 
# 
# 
# 
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# The main idea is the concept "Balance" of a tree.
# Balance means the number of coins need to fill all
# nodes in this subtree with 1 coin to each node. And it
# can be negative. For example:
#
#                   0  <- bal = -2
#                 /  \
#    bal = 0 ->  1    0  <- bal = -1
#
#                   2  <- bal = 2
#                 /  \
#    bal = 2 ->  3    0  <- bal = -1
#
#
# For a leaf node, if its value is 1, then it need not to change
# the balance hence 0. If its value is 0, it need get a coin from 
# others so it balance is -1.
# If its value is > 1, it can give some coins out, so its balace will be 
# positive. So for a single node, its self balance is (node.val - 1).
# For a subtree, the tree balance is (self balance) + (left balance) + (right balance).
# If we know the tree balance of a node, that means to fill this subtree,
# a number of balance coins need to be passed thru this node. 
# Think this node as a transport hub, and a coin is a cargo. For each time,
# only 1 cargo can pass this hub, and we need N cargos (tree balance).
# So to let those N cargos pass thru this hub, we need N steps.
# So the coins need to be moved to this node will be abs(tree balance). 
# The total move of this subtree will be 
# abs(tree balance) + (left subtree move) + (right subtree move).
# We do a post-visit of DFS, ie. process all child nodes first, and procss the
# current node. We return both balance and move up.
# Complexity O(n) for visiting all node.
# Space is O(1) for no extra space.


def dfs(root):
    if root == None:
        return (0, 0)
    
    (l_bal, l_move) = dfs(root.left)
    (r_bal, r_move) = dfs(root.right)
    
    bal = (root.val - 1) + l_bal + r_bal
    move = abs(bal) + l_move + r_move
    
    return (bal, move)
    
    

class Solution:
    def distributeCoins(self, root: TreeNode) -> int:
        (bal, move) = dfs(root)
        
        return move
