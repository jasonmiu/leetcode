#
# @lc app=leetcode id=521 lang=python3
#
# [521] Longest Uncommon Subsequence I 
#
# https://leetcode.com/problems/longest-uncommon-subsequence-i/description/
#
# algorithms
# Easy (56.90%)
# Total Accepted:    52.7K
# Total Submissions: 92.5K
# Testcase Example:  '"aba"\n"cdc"'
#
# 
# Given a group of two strings, you need to find the longest uncommon
# subsequence of this group of two strings.
# The longest uncommon subsequence is defined as the longest subsequence of one
# of these strings and this subsequence should not be any subsequence of the
# other strings.
# 
# 
# 
# A subsequence is a sequence that can be derived from one sequence by deleting
# some characters without changing the order of the remaining elements.
# Trivially, any string is a subsequence of itself and an empty string is a
# subsequence of any string.
# 
# 
# 
# The input will be two strings, and the output needs to be the length of the
# longest uncommon subsequence. If the longest uncommon subsequence doesn't
# exist, return -1.
# 
# 
# Example 1:
# 
# Input: "aba", "cdc"
# Output: 3
# Explanation: The longest uncommon subsequence is "aba" (or "cdc"), because
# "aba" is a subsequence of "aba", but not a subsequence of any other strings
# in the group of two strings. 
# 
# 
# 
# Note:
# 
# Both strings' lengths will not exceed 100.
# Only letters from a ~ z will appear in input strings. 
# 
# 
#
# For two strings, if they are equal, then no uncommon subsequence,
# as they are common subsequence to each other
# If their lengths are not equal, and contents are not equal, 
# the longer one must be the longest uncommon subsequence.
# If their length are equal, but contents are not equal, they
# are uncommon subsequences to each other, then the longest 
# uncommon subsequence is just the length of any string.
# Complexity O(1), given that the length checking operation is O(1)
# Space Complexity is O(1), no need extra space

class Solution:
    def findLUSlength(self, a: str, b: str) -> int:
        
        if len(a) != len(b):        
            l = max(len(a), len(b))
        else:
            if a == b:
                l = -1
            else:
                l = len(a)
                
        return l
