// The idea is basically use DFS to search down the selection tree.
// Hard part is how to generate the partitions.
// To have a input like:
// "aab"
//
//                               root
//                       /         |           \
//                      a          aa          aab
//                    / |          |
//                   a  ab         b
//                 /
//               b
//
// Each node at a level is a substring partition. A path is the array of the partitions.
// Once we hit the leaf of the search tree, we generated combination of partitions, and
// save to the result.
// Make a partition can think as, for a substring starts with current index, extend its length
// 1 by 1 until the end of the string, get a this sub string, put it to the current path,
// start the next level from the end of the current substring, like:
// [a a b]
//  ^- substring starts from 0, ends with 0, "a", next starting point is 1.
//  ^+^- substring starts from 0, ends with 1, "aa", next starting point is 2.
//  ^+++^- substring starts from 0, ends with 2, "aab"
//    ^- substring starts from 1, ends with 1, "a"
//
// We want to backtrack the current substring when we return to last level.
// Since it is looking for palindrome, if a partition is not palindrome, no more search is needed.
// Time: O(2^n), since every selection it can be selected into two sides. Space is O(2^n) for storing
// all results.


bool is_pal(const string& s)
{
    int i = 0;
    int j = s.size() - 1;

    while (i <= j) {
        if (s[i] != s[j]) {
            return false;
        }
        i++;
        j--;
    }

    return true;
}

void gen(const string& s, int start, vector<string>& cur, vector<vector<string>>& result)
{
    if (start >= s.size()) {
        result.push_back(cur);
        return;
    }

    for (int i = start; i < s.size(); i++) {
        string ss(s.begin()+start, s.begin()+i+1);
        if (is_pal(ss)) {
            cur.push_back(ss);

            gen(s, i + 1, cur, result);

            cur.pop_back();
        }
    }

}

class Solution {
public:
    vector<vector<string>> partition(string s) {
        vector<vector<string>> ans;
        vector<string> cur;
        gen(s, 0, cur, ans);
        return ans;
    }
};
