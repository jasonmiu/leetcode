// The idea is if we have a node, and know its previous node,
// then we can easily reverse then, by setting the next pointer of the
// node to the previous node. Problem is, linked list is single direction,
// how to get the previous node if we are at a middle of a list?
// We can recursively pass down the current node, and its previous node,
// when we move forward, the next previous node is the current node, the next
// current node is the current node next pointer.
// Do this until we hit the null end. When the recursion folds back up,
// we have the every pair of a node and its previous node. So just reverse them.
// But we want to know the last node of the list, which will be the new head.
// We can propagate the last node back to the recursion chain, since the return
// of a recursed function need no more processing, we can use it to progagate
// back the last node.
//
// Time O(n) as access all nodes once. Space O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

ListNode* rev(ListNode* head, ListNode* prev)
{
    if (head == nullptr) {
        return nullptr;
    }

    ListNode* next = rev(head->next, head);
    head->next = prev;
    if (next == nullptr) {
        return head;
    } else {
        return next;
    }
}

class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* last = rev(head, nullptr);

        return last;
    }
};
