// This is the iteration version of the algor.
// The idea is similar to the recursion version,
// we keep the current pointer and the previous node pointer,
// for every loop, point the next pointer of the current node to its
// previous node.
// The implementation detail is, once the next pointer of a node get
// updated, the next node access will be lost, so have to save it
// first in a temp var (ListNode* next in the loop body).
// Then forward the current pointer to the next node.
// If the next node is null, means we are at the tail node,
// save it for the new reversed list head.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {

        ListNode* prev = nullptr;
        ListNode* current = head;

        ListNode* tail = nullptr;

        while (current != nullptr) {
            ListNode* next = current->next;
            if (next == nullptr) {
                tail = current;
            }

            current->next = prev;
            prev = current;
            current = next;
        }

        return tail;
    }
};
