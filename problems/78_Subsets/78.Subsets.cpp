// It is the same algor that, for each element of the input, we can select, or not select
// to the current subset selection. So it is a two recursions for two paths, select or not select.
// Use a var cur_set to store the current subset selection, which can think as a dfs path of a
// selection tree. Then we hit the end of the input, save the current subset selection.
// One trick to try this time was, instead of copying the cur_set every time when going down
// to the selection tree, pass down the reference of the cur_set instead of its copy.
// Add the current element to the current set, after finished the "select" path, pop it back,
// which restores the cur_set, and do the "not select" path.
//
// Time O(2^n) and space to store all results is O(2^n).

void power_set(const vector<int>& nums, int i, vector<int>& cur_set, vector<vector<int>>& result)
{
    if (i >= nums.size()) {
        result.push_back(cur_set);
        return;
    }

    cur_set.push_back(nums[i]);
    power_set(nums, i + 1, cur_set, result);

    cur_set.pop_back();
    power_set(nums, i + 1, cur_set, result);

    return;
}

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {

        vector<vector<int>> result;
        vector<int> cur_set;

        power_set(nums, 0, cur_set, result);

        return result;

    }
};
