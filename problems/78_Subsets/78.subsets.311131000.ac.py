#
# @lc app=leetcode id=78 lang=python3
#
# [78] Subsets
#
# https://leetcode.com/problems/subsets/description/
#
# algorithms
# Medium (55.95%)
# Total Accepted:    491.3K
# Total Submissions: 845.2K
# Testcase Example:  '[1,2,3]'
#
# Given a set of distinct integers, nums, return all possible subsets (the
# power set).
# 
# Note: The solution set must not contain duplicate subsets.
# 
# Example:
# 
# 
# Input: nums = [1,2,3]
# Output:
# [
# ⁠ [3],
# [1],
# [2],
# [1,2,3],
# [1,3],
# [2,3],
# [1,2],
# []
# ]
# 
#
# First idea with bottom up, for single element set,
# [1], the power sets are [[1], []]
# For two elements set, [1], [2]
# It is the power sets of the last element, [[2], []]
# for each set, we add the current element to it, except empty set,
# and appending to the power sets:
# like:
# [[2, 1], [2]]
# Then append the power set of the current element as a single set:
# [[2, 1], [2], [1]]
# The net effect is, for each set in the power sets, 
# we can put (add to the set), or not put (append the single set to power set)
# the current element in it.
# Finally at the top of the recursion, we append empty set [] to the power set,
# which is "add empty set to each set in power set (do nothing)", and
# "do not add empty set to any set in power set (just append the empty set to power set)"
#
# The top down is similar, but use two recursion for two paths, 
# 1. add current element
# 2. not add current element
# If used up the elements in the input set, means we did all choices. Save the current 
# element selection.
#
# Complexity is O(2^n) which is the set of power set. Space is also O(2^n).

def powerset(s, cur_set, idx, sets):
    if idx >= len(s):
        if len(cur_set) >= 0:
            sets.append(cur_set)
        return


    c = cur_set.copy()

    # for each element, it can be added to current subset, or not
    powerset(s, c + [s[idx]], idx + 1, sets)
    powerset(s, c, idx+1, sets)
    
    return

def gen_sets(nums, i):
    n = len(nums)
    if n == 0:
        return list()
    
    if i == n-1:
        s = [[nums[i]]]
        return s
    
    ps = gen_sets(nums, i+1)
    e = []
    for s in ps:
        ss = s + [nums[i]]
        e.append(ss)
        
    e.append([nums[i]])
    ps.extend(e)
    return ps
        

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        ans = self.subsets_bottom_up(nums)
        #ans = self.subsets_top_down(nums)
        return ans
        
    def subsets_top_down(self, nums: List[int]) -> List[List[int]]:
        ans = []
        powerset(nums, [], 0, ans)
        return ans

        
    def subsets_bottom_up(self, nums: List[int]) -> List[List[int]]:
        
        ps = gen_sets(nums, 0)
        ps.append([])
        
        return ps
        
