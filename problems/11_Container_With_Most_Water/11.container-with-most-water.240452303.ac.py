#
# @lc app=leetcode id=11 lang=python3
#
# [11] Container With Most Water
#
# https://leetcode.com/problems/container-with-most-water/description/
#
# algorithms
# Medium (47.36%)
# Total Accepted:    475.1K
# Total Submissions: 1M
# Testcase Example:  '[1,8,6,2,5,4,8,3,7]'
#
# Given n non-negative integers a1, a2, ..., an , where each represents a point
# at coordinate (i, ai). n vertical lines are drawn such that the two endpoints
# of line i is at (i, ai) and (i, 0). Find two lines, which together with
# x-axis forms a container, such that the container contains the most water.
# 
# Note: You may not slant the container and n is at least 2.
# 
# 
# 
# 
# 
# The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In
# this case, the max area of water (blue section) the container can contain is
# 49. 
# 
# 
# 
# Example:
# 
# 
# Input: [1,8,6,2,5,4,8,3,7]
# Output: 49
#
# First idea was to using 2 loops, to find all widths and height 
# combinations, which is a O(n^2) algor.
# To find a O(n) algor, can think in this way. Lets the container
# starts with the leftmost and rightmost bar, which has the max
# width. Lets say the leftmost bar is shorter than the rightmost bar,
# since this is their max width and this is the max water this leftmost
# bar can hold. We can then throw it away no need to consider it anymore.
# For the middle bars that can hold more water, they must have higher
# height. We move the leftmost bar to right to check if it can hold more
# water. We keep remove the shorter bar of each side out and move both
# bars inward, until they cross each other and we checked all bars with
# their max water can hold.


class Solution:
    def maxArea(self, height: List[int]) -> int:
        
        n = len(height)
        i = 0
        j = n - 1
        max_water = 0
        while (i < j):
            w = j - i
            h = min(height[i], height[j])
            a = w * h
            if a >= max_water:
                max_water = a
                
            if height[i] <= height[j]:
                i += 1
            else:
                j -= 1
                
        return max_water
