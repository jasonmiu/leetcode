// The code is easy, the hardest thing is why it works in linear way.
//
// While picking any vertical bar as the left bar, any right hand side of it
// can be the right bar, and can be an anwser. To think in this way we have to
// find all left right combinations with is O(n^2).
//
// It is possible some combinations cannot be the max area (the anwser)?
// Assuming the left most and right most are the tallest bars in the axis,
// max area we can get must be setting the left and right bar as these two
// vertical bars.
//
// If any vertical bar between the the left and right most bars is higher than
// these two, it is possible that setting a bar there can yield the anwser.
// Any vertical bar is lower than the left and right most is not possible be
// then anwser since its width must be smaller.
//
// So for a current left bar that has the current known max area, we only
// need to think if we move the right bar inward, will the max area gain?
// Only case we want to move the right bar inward is the right bar is lower
// than this left bar. If right bar is higher its no way to gain area since
// the height does not gain (locked by left bar) and width becomes smaller.
// So if right bar is lower, move in. Same as the left bar, if left bar is lower
// than right bar, move in.
//
// For each move in, try if the max area increase. Keep trying move both sides
// inward by testing the low vertical bar removeal, until they meet which has
// no area any more. We can get the max area.
//
// Time O(n), space O(1).

class Solution {
public:
    int maxArea(vector<int>& height) {

        int l = 0;
        int r = height.size() - 1;

        int max_area = 0;

        while (l < r) {
            int area = min(height[l], height[r]) * (r - l);
            if (area >= max_area) {
                max_area = area;
            }

            if (height[l] <= height[r]) {
                l = l + 1;
            } else if (height[r] < height[l]) {
                r = r - 1;
            }
        }

        return max_area;
    }
};
