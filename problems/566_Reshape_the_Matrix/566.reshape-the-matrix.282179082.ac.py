#
# @lc app=leetcode id=566 lang=python3
#
# [566] Reshape the Matrix
#
# https://leetcode.com/problems/reshape-the-matrix/description/
#
# algorithms
# Easy (59.48%)
# Total Accepted:    87.8K
# Total Submissions: 147.5K
# Testcase Example:  '[[1,2],[3,4]]\n1\n4'
#
# In MATLAB, there is a very useful function called 'reshape', which can
# reshape a matrix into a new one with different size but keep its original
# data.
# 
# 
# 
# You're given a matrix represented by a two-dimensional array, and two
# positive integers r and c representing the row number and column number of
# the wanted reshaped matrix, respectively.
# 
# ⁠The reshaped matrix need to be filled with all the elements of the original
# matrix in the same row-traversing order as they were.
# 
# 
# 
# If the 'reshape' operation with given parameters is possible and legal,
# output the new reshaped matrix; Otherwise, output the original matrix.
# 
# 
# Example 1:
# 
# Input: 
# nums = 
# [[1,2],
# ⁠[3,4]]
# r = 1, c = 4
# Output: 
# [[1,2,3,4]]
# Explanation:The row-traversing of nums is [1,2,3,4]. The new reshaped matrix
# is a 1 * 4 matrix, fill it row by row by using the previous list.
# 
# 
# 
# Example 2:
# 
# Input: 
# nums = 
# [[1,2],
# ⁠[3,4]]
# r = 2, c = 4
# Output: 
# [[1,2],
# ⁠[3,4]]
# Explanation:There is no way to reshape a 2 * 2 matrix to a 2 * 4 matrix. So
# output the original matrix.
# 
# 
# 
# Note:
# 
# The height and width of the given matrix is in range [1, 100].
# The given r and c are all positive.
# 
# 
#
# The idea is scan the matrix from rows to columns, 
# and assume a row in new matrix has columns, c.
# We copy each element to the columns of a new cols,
# if the current cols is full, create a new list, and reset the counter,
# append this cols as a row of new matrix. 
# The tricky part when we get out of the loops, the last row will not have 
# chance to append to the new matrix as the matrix append happends at the beginning 
# of a iteration. So append any remaining row to the new matrix.
# Complexity is O(n), n is the number of elements.
# Space is O(n) for the new matrix.

# Nice 2D indices conversion solution:
#class Solution {
#public:
#    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
#        int m = nums.size();
#        int n = nums[0].size();
#        int sz = m * n;
#        if(r * c != sz) return nums;
#        vector<vector<int>> ans(r, vector<int>(c, 0));
#        for(int i = 0; i < sz; i++) {
#            ans[i / c][i % c] = nums[i / n][i % n]; 
#        }
#        return ans;
#    }
#};

class Solution:
    def matrixReshape(self, nums: List[List[int]], r: int, c: int) -> List[List[int]]:
        
        cur_r = len(nums)
        cur_c = len(nums[0])
        if (cur_r * cur_c) != (r * c):
            return nums
        
        c_cnt = 0
        cols = []
        rows = []
        for i in range(0, cur_r):
            for j in range(0, cur_c):
                if c_cnt >= c:
                    rows.append(cols)
                    cols = []
                    c_cnt = 0
                
                cols.append(nums[i][j])
                c_cnt += 1
                
        if len(cols) > 0:
            rows.append(cols)
            
        return rows
