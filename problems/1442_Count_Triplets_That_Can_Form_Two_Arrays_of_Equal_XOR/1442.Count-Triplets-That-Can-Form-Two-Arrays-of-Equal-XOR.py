# For :
# a = arr[i] ^ arr[i+1] ^ ... ^ arr[j-1]
# b = arr[j] ^ arr[j+1] ^ ... ^ arr[k]
# if a == b, a ^ b = 0, since self XOR => 0.
# which means:
# arr[i] ^ arr[i+1] ^ ... ^ arr[j-1] ^ arr[j] ^ arr[j+1] ^ ... ^ arr[k]
# \_______________ a ____________/     \_____________ b _____________/
#                  \_____________ XOR ______________/
#                                  \__ => Zero
# If we find a subsequence, from i to k, and the XOR this subsequence is 0,
# then all j between i and k are possible cut.
# So it is the index distance between i to k.
# Sum this index distance for all subsequences.
# Complexity O(n^2).
# Space O(1)

class Solution:
    def countTriplets(self, arr: List[int]) -> int:
        
        ans = 0
        n = len(arr)
        for i in range(0, n):
            a = arr[i]
            # we do not need to find j!
            for k in range(i+1, n):
                a = a ^ arr[k]
                
                if a == 0:
                    ans = ans + (k - i)
                
        return ans

