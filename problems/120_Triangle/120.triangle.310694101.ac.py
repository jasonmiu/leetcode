#
# @lc app=leetcode id=120 lang=python3
#
# [120] Triangle
#
# https://leetcode.com/problems/triangle/description/
#
# algorithms
# Medium (41.21%)
# Total Accepted:    224K
# Total Submissions: 528K
# Testcase Example:  '[[2],[3,4],[6,5,7],[4,1,8,3]]'
#
# Given a triangle, find the minimum path sum from top to bottom. Each step you
# may move to adjacent numbers on the row below.
# 
# For example, given the following triangle
# 
# 
# [
# ⁠    [2],
# ⁠   [3,4],
# ⁠  [6,5,7],
# ⁠ [4,1,8,3]
# ]
# 
# 
# The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).
# 
# Note:
# 
# Bonus point if you are able to do this using only O(n) extra space, where n
# is the total number of rows in the triangle.
# 
#
# The top-down DFS is relatively simple. For each element, we search
# down to find the min sum. And some elements will be visited multiple times, 
# like:
#      [2],
#     [3,4],
#   [6,5,7],
#  [4,1,8,3]
#
#         2
#      /     \
#    3        4
#  /   \     /  \
# 6    *5  *5    7
#
# The element 5 will be visited twice hence this is a chance to use DP to save the
# sum path from 5, the key is the element position.
#
# We can see the current level min path is depending on the next 1 level ONLY.
# If we can process the whole level at one time, then we do not need to save
# more than 1 level.
# Think bottom up, start from the last row:
#   [4   1   8   3]
#
# We can think each element of this array is "Starting from this position, this is
# the min path value we get from the bottom"
# So 1 upper level will be

# min(4,1)      min(1,8)         min(8,3)
#     +            +                +
#    6            5                7
# For the level 2 ([6, 5, 7]), the min path value we can get is its value plus
# the min of its next level adjacent elements. Hence
# this_level[i] = min(next_level[i], next_level[i+1]) + triangle[level][i]
# But we dun need the "next_level" once we have the "this_level". So can reuse
# the same array:
# this_level[i] = min(this_level[i], this_level[i+1]) + triangle[level][i]
# Complexity is O(n^2) for n is number of row. We visit all n rows, and each row
# can has n elements.
# Space is O(n), for having only 1 level info which max is n.

class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        
        level_min = list(triangle[-1]) # last row of the triangle
        
        for i in range(len(triangle)-2, -1, -1):
            for j in range(0, len(triangle[i])):
                level_min[j] = min(level_min[j], level_min[j+1]) + triangle[i][j]
        
        return level_min[0]
            

"""
def dfs(triangle, level, i, memo):
    m = len(triangle)
    if level == m-1:
        return triangle[level][i]
    
    if (level, i) in memo:
        return memo[(level, i)]
    
    lmin = dfs(triangle, level+1, i, memo)
    rmin = dfs(triangle, level+1, i+1, memo)
    
    memo[(level, i)] = triangle[level][i] + min(lmin, rmin)
    
    return memo[(level, i)]

class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        
        ans = dfs(triangle, 0, 0, {})
        
        return ans
"""
