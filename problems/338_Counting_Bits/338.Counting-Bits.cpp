#include <cmath>

int cb(int a)
{
    int bits =  ceil(log2(a+1));
    int ones = 0;
    for (int i = 0; i < bits; i++) {
        int b = a & 0x01;
        if (b == 1) {
            ones ++;
        }
        a = a >> 1;
    }

    return ones;
}

class Solution {
public:
    vector<int> countBits(int n) {
        vector<int> ans(n+1, 0);

        for (int i = 0; i <= n; i++) {
            int b = cb(i);
            ans[i] = b;
        }

        return ans;
    }
};
