#
# @lc app=leetcode id=338 lang=python3
#
# [338] Counting Bits
#
# https://leetcode.com/problems/counting-bits/description/
#
# algorithms
# Medium (65.93%)
# Total Accepted:    217.5K
# Total Submissions: 326.1K
# Testcase Example:  '2'
#
# Given a non negative integer number num. For every numbers i in the range 0 ≤
# i ≤ num calculate the number of 1's in their binary representation and return
# them as an array.
# 
# Example 1:
# 
# 
# Input: 2
# Output: [0,1,1]
# 
# Example 2:
# 
# 
# Input: 5
# Output: [0,1,1,2,1,2]
# 
# 
# Follow up:
# 
# 
# It is very easy to come up with a solution with run time
# O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a
# single pass?
# Space complexity should be O(n).
# Can you do it like a boss? Do it without using any builtin function like
# __builtin_popcount in c++ or in any other language.
# 
#
# The main observation come from doing some examples like:
# n = 9
# Binaries: 
# [0, 01, 10, 11, 100, 101, 110, 111, 1000, 1001]
#  0   1   2   3    4    5    6    7     8     9
# [0, 1,  1,  2    1    2     2    3     1     2]
# Bit 1 counts: ^^^
#
# Since we want to know all numbers from 0 to n,
# number i , 0 <= i <= n must be able to be summed up
# by a and b, while a and b are <= i. 
# And from the example above we know the power of 2
# must has 1 bit of 1. Then the i > power of 2 which just smaller
# than i is: i =  last_power_of_2 + (last_power_of_2 - i)
# We can also see the 1 bit count of i is also 
# equal to 1-bit count of last_power_of_2 + 1-bit count of j,
# where j is the diff between last_power_of_2 and i, as:
# 5 = 4 + 1 == 101 = 100 + 001
# Hence we know the DP relation will be
# bins[i] = bins[i-last_power_of_2] + bins[last_power_of_2]
# where bins[i] is the 1-bit count of interger i.
#
# We need to test if i is power of 2, so we need to do this op in
# O(1). A bit operation can do this.
# The complexity is O(n) for scanning all numbers, and space is O(n)
#

def pow_of_2(x):
    if ((x - 1) & x) == 0:
        return True
    else:
        return False

class Solution:

    def countBits(self, num: int) -> List[int]:
        
        if num == 0:
            return [0]
        elif num == 1:
            return [0, 1]
        elif num == 2:
            return [0, 1, 1]

        bins = [0] * (num+1)
        bins[0] = 0
        bins[1] = 1
        bins[2] = 1
        
        last_pow_2 = 2
        for i in range(3, num+1):
            if pow_of_2(i):
                last_pow_2 = i
                bins[i] = 1
            else:
                bins[i] = bins[(i-last_pow_2)] + bins[last_pow_2]
                
        return bins
