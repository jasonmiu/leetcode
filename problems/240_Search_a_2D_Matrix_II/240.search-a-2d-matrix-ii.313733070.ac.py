#
# @lc app=leetcode id=240 lang=python3
#
# [240] Search a 2D Matrix II
#
# https://leetcode.com/problems/search-a-2d-matrix-ii/description/
#
# algorithms
# Medium (41.74%)
# Total Accepted:    272.2K
# Total Submissions: 640.6K
# Testcase Example:  '[[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]]\n5'
#
# Write an efficient algorithm that searches for a value in an m x n matrix.
# This matrix has the following properties:
# 
# 
# Integers in each row are sorted in ascending from left to right.
# Integers in each column are sorted in ascending from top to bottom.
# 
# 
# Example:
# 
# Consider the following matrix:
# 
# 
# [
# ⁠ [1,   4,  7, 11, 15],
# ⁠ [2,   5,  8, 12, 19],
# ⁠ [3,   6,  9, 16, 22],
# ⁠ [10, 13, 14, 17, 24],
# ⁠ [18, 21, 23, 26, 30]
# ]
# 
# 
# Given target = 5, return true.
# 
# Given target = 20, return false.
# 
#
# Since for a row it is sorted, and for a column it is sorted too, 
# for example a matrix:
# [
#  [1,   4,  7, 11, 15],
#  [2,   5,  8, 12, 19],
#  [3,   6,  9, 16, 22],
#  [10, 13, 14, 17, 24],
#  [18, 21, 23, 26, 30]
# ]
# The mid-point is [m//2, n//2] = 9. 
# If the target is == to mid-point, we got the ans.
# If the target is > than mid-point, it can be in the rows below, or cols on the right
# If the target is < than mid-point, it can be in the rows upper, or cols on the left
# For both cases, we cut 2 sub-matrices and pass back recusively. For exmple, target is 5:
# 
#   [1,   4,  7, 11, 15],
#  [2,   5,  8, 12, 19],  -> [1, 4, 7, 11, 15]  -> []
#                         +                     +
#                         +                     + [[1, 4]] -> []
#                         +> [1, 4]                        + 
#                            [2, 5] -> mid-point 5         +> [[1]] -> []
#                                      == target                    +> [[]]
#
# So it is like a DFS for each sub matrix. If a path can give True, we return True
# Complexity O(log(mn)).
# Space O(mn), we can use start,end index for the original matrix instead of copy submatrix
# for saving space.

def search(matrix, target):
    if len(matrix) == 0:
        return False
    if len(matrix[0]) == 0:
        return False
    
    m = len(matrix)
    n = len(matrix[0])
    
    mid_r = m // 2
    mid_c = n // 2
    
    if matrix[mid_r][mid_c] == target:
        return True
    
    if target > matrix[mid_r][mid_c]:
        m2 = matrix[mid_r+1::]
        r1 = search(m2, target)
        
        m2 = []
        for i in range(0, m):
            m2.append([])
        for i in range(0, m):
            for j in range(mid_c+1, n):
                m2[i].append(matrix[i][j])
        
        r2 = search(m2, target)
        
        if r1 == False and r2 == False:
            return False
        else:
            return True
        
    elif target < matrix[mid_r][mid_c]:
        m2 = matrix[0:mid_r]
        r1 = search(m2, target)
        
        m2 = []
        for i in range(0, m):
            m2.append([])
        for i in range(0, m):
            for j in range(0, mid_c):
                m2[i].append(matrix[i][j])
                
        r2 = search(m2, target)
        
        if r1 == False and r2 == False:
            return False
        else:
            return True
        

class Solution:
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        
        ans = search(matrix, target)
        return ans
        
