// Do a binary search lower bound on the first column. Since it is
// sorted, also the row, the target can only be exist in the upper
// part of the lower bound row.  Start from there, and search the
// target. If the current value is the target, return true.  If the
// target is larger than current value, search down row, and right
// column.  If the target is smaller than current value, search up
// row, and left column.  Since every move elimutat a row or column,
// the time is O(logm + m + n) where m is the rows number, n is the
// columns number. Space is O(1).

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {

        int m = matrix.size();
        int n = matrix[0].size();

        auto row_it = lower_bound(matrix.begin(), matrix.end(), vector<int>{target},
                                  [](const vector<int>& a, const vector<int>& b) {
                                      if (a[0] < b[0]) {
                                          return true;
                                      } else {
                                          return false;
                                      }
                                  });
        int row = row_it - matrix.begin();
        if (row >= m) {
            row = m - 1;
        }

        queue<pair<int,int>> q;
        int r = row;
        int c = 0;
        q.push({r, c});

        while (!q.empty()) {
            pair<int,int> pos = q.front();
            q.pop();

            r = pos.first;
            c = pos.second;

            int val = matrix[r][c];
            matrix[r][c] = INT_MAX;

            if (val == target) {
                return true;
            } else if (target > val) {
                if (r < m - 1 && matrix[r+1][c] != INT_MAX) {
                    q.push({r+1, c});
                }

                if (c < n - 1 && matrix[r][c+1] != INT_MAX) {
                    q.push({r, c+1});
                }
            } else if (target < val) {
                if (r > 0 && matrix[r-1][c] != INT_MAX) {
                        q.push({r-1, c});
                }

                if (c > 0 && matrix[r][c-1] != INT_MAX) {
                    q.push({r, c-1});
                }
            }
        }

        return false;
    }
};
