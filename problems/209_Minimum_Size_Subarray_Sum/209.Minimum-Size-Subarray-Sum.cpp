// This is similar to 713 subarray producy less than K.
// For example:
// [2,3,1,2,4,3] , target = 7
// If use a slide window, with two pointers, i (left) and j (right),
// We can expend the window to right side (increase j), until the sum in the
// window just >= target:
//
// [2 3 1 2 4 3]
//  i     j
//  |,,,,,|
//     |
//  sum = 8
//
// We can try to shrink the window from left (increase i) to see if decreasing
// the window side (subarray length), can still mantain the sum >= target invariant:
//
// [2 3 1 2 4 3]
//    i   j
//    |,,,|
//      |
//  sum = 6
//
// It cannot in this case, so try again expend from right until the sum is >= target:
// [2 3 1 2 4 3]
//    i     j
//    |,,,,,|
//       |
//   sum = 10
//
// Try shrink again:
// [2 3 1 2 4 3]
//      i   j
//      |,,,|
//        |
//    sum = 7
//
// We can still matain the invariant, and the min subarray length get updated.
// Try shrink again:
// [2 3 1 2 4 3]
//        i j
//        |,|
//         |
//    sum = 6
//
// So now expend the window again. Repeat this until the right boundary hit the
// end of the input array.
//
// This works because we always maintain the invariant that the sum in the window
// is >= target. So we like to slide the window with this invariant and find the
// shortest length one.
// Time O(n) since only scan once. Space (1).
//
// One implmentation detail is the index i , j updating position is important.
// I like the j stops *at the last index* inclusively, so only move it forward only
// if the invariant is not keep. Once we hit the target, do not move it forward.
// For i we like to substract current sum by [i], which means remove the current left
// element, then move forward i. So move forward i and substraction are indeed
// one logical operation means remove the current left element, so update i
// before testing the invariant.


class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {

        int i = 0, j = 0;
        int sum = 0;
        int min_len = INT_MAX;

        while (j < nums.size()) {
            while (j < nums.size()) {
                sum += nums[j];
                if (j == nums.size() - 1 && i == 0 && sum < target) {
                    return 0;
                }

                if (sum >= target) {
                    min_len = min(min_len, j - i + 1);
                    break;
                }
                j++;
            }

            while (i < j) {
                sum -= nums[i];
                i++;
                if (sum < target) {
                    break;
                } else {
                    min_len = min(min_len, j - i + 1);
                }
            }

            j++;
        }

        return min_len;
    }
};
