#
# @lc app=leetcode id=801 lang=python3
#
# [801] Minimum Swaps To Make Sequences Increasing
#
# https://leetcode.com/problems/minimum-swaps-to-make-sequences-increasing/description/
#
# algorithms
# Medium (36.14%)
# Total Accepted:    17.9K
# Total Submissions: 49.4K
# Testcase Example:  '[1,3,5,4]\n[1,2,3,7]'
#
# We have two integer sequences A and B of the same non-zero length.
# 
# We are allowed to swap elements A[i] and B[i].  Note that both elements are
# in the same index position in their respective sequences.
# 
# At the end of some number of swaps, A and B are both strictly increasing.  (A
# sequence is strictly increasing if and only if A[0] < A[1] < A[2] < ... <
# A[A.length - 1].)
# 
# Given A and B, return the minimum number of swaps to make both sequences
# strictly increasing.  It is guaranteed that the given input always makes it
# possible.
# 
# 
# Example:
# Input: A = [1,3,5,4], B = [1,2,3,7]
# Output: 1
# Explanation: 
# Swap A[3] and B[3].  Then the sequences are:
# A = [1, 3, 5, 7] and B = [1, 2, 3, 4]
# which are both strictly increasing.
# 
# 
# Note:
# 
# 
# A, B are arrays with the same length, and that length will be in the range
# [1, 1000].
# A[i], B[i] are integer values in the range [0, 2000].
# 
# 
#
class Solution:
    def minSwap(self, A: List[int], B: List[int]) -> int:
        MAX = 9999
        n = len(A)
        
        swap_cnt = [MAX] * n
        noswap_cnt = [MAX] * n
        swap_cnt[0] = 1
        noswap_cnt[0] = 0
        
        # Case 1:
        # A and B are in order already, ie:
        # A[i] > A[i-1] AND B[i] > B[i-1]
        # like:
        # A = [ 1, 2 ]
        # B = [ 3, 4 ]
        # We can have two conditions to make this case
        # We swapped A[i-1], B[i-1] before, we must swap
        # the current A[i] and B[i] to maintain the order.
        # orginal:
        # A = [3, 4]
        # B = [1, 2]
        #      ^- swap (i-1)
        # A = [1, 4]
        # B = [3, 2]
        #         ^- swap (i) 
        # so need to swap at i as well to get it in order
        # A = [1, 2]
        # B = [3, 4]
        # similar if no swap at (i-1)
        # A = [ 1, 2 ]
        # B = [ 3, 4 ]
        #       ^- no swap (i-1)
        # A = [ 1, 2 ]
        # B = [ 3, 4 ]
        #          ^- no swap (i)
        # we check two conditions, 
        # number swap at (i-1) + 1 <-- we swap at i
        # number of swap if no swap at (i-1)
        
        # Case 2:
        # A and B are not in order, need swap A[i] and B[i] to
        # get them in order, ie:
        # A = [ 1, 4 ]
        # B = [ 3, 3 ]
        #          ^- swap at (i)
        # two conditions:
        # we swapped at i-1, we do not swap at i
        # we did not swap at i-1, we swap at i
        
        for i in range(1, n):
            if A[i] > A[i-1] and B[i] > B[i-1]:                
                swap_cnt[i] = min(swap_cnt[i-1] + 1, swap_cnt[i])
                noswap_cnt[i] = min(noswap_cnt[i-1], noswap_cnt[i])
              
            if B[i] > A[i-1] and A[i] > B[i-1]:
                swap_cnt[i] = min(noswap_cnt[i-1] + 1, swap_cnt[i])
                noswap_cnt[i] = min(swap_cnt[i-1], noswap_cnt[i])
                

                
        return min(swap_cnt[n-1], noswap_cnt[n-1])
