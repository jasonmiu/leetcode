#
# @lc app=leetcode id=599 lang=python3
#
# [599] Minimum Index Sum of Two Lists
#
# https://leetcode.com/problems/minimum-index-sum-of-two-lists/description/
#
# algorithms
# Easy (48.95%)
# Total Accepted:    69.1K
# Total Submissions: 141.1K
# Testcase Example:  '["Shogun","Tapioca Express","Burger King","KFC"]\n["Piatti","The Grill at Torrey Pines","Hungry Hunter Steakhouse","Shogun"]'
#
# 
# Suppose Andy and Doris want to choose a restaurant for dinner, and they both
# have a list of favorite restaurants represented by strings. 
# 
# 
# You need to help them find out their common interest with the least list
# index sum. If there is a choice tie between answers, output all of them with
# no order requirement. You could assume there always exists an answer.
# 
# 
# 
# Example 1:
# 
# Input:
# ["Shogun", "Tapioca Express", "Burger King", "KFC"]
# ["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"]
# Output: ["Shogun"]
# Explanation: The only restaurant they both like is "Shogun".
# 
# 
# 
# Example 2:
# 
# Input:
# ["Shogun", "Tapioca Express", "Burger King", "KFC"]
# ["KFC", "Shogun", "Burger King"]
# Output: ["Shogun"]
# Explanation: The restaurant they both like and have the least index sum is
# "Shogun" with index sum 1 (0+1).
# 
# 
# 
# 
# Note:
# 
# The length of both lists will be in the range of [1, 1000].
# The length of strings in both lists will be in the range of [1, 30].
# The index is starting from 0 to the list length minus 1.
# No duplicates in both lists.
# 
# 
#
# The idea is to find the common set of list1 and list2.
# Convert them to sets and use AND operator to get their intersection.
# For each element in the intersection, find its index sum of list1 and 2,
# add it to a dict use the index sum as the key.
# Find the min of those keys, which is the min index sum, return the attached
# intersection elements.
# Complexity is O(n). The set AND operator should use hash table inside, and compare the
# hashed values of two sets.
# Space complexity is O(n) for having the sets for input lists

class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:
        """
        :type list1: List[str]
        :type list2: List[str]
        :rtype: List[str]
        """
        
        a_set = set(list1)
        b_set = set(list2)        
        union = a_set & b_set
        
        idx_dict = {}
        for e in union:
            i = list1.index(e) + list2.index(e)
            if i in idx_dict:
                idx_dict[i].append(e)
            else:
                idx_dict[i] = [e]
                
        i = min(idx_dict.keys())
        
        return idx_dict[i]
