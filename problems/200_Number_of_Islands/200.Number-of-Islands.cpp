// This is a standard flood fill with BFS. For each element in the matrix,
// check if it is '1'. If it is '1', it must a starting point of an island.
// Use this starting point to flood fill this island, so all connecting
// elements will be filled as "0", and will not be count again in the next
// element checking loop. This is like destroy an island one by one.
// For the flood fill, use a queue to push the correct starting point,
// then pop it out as the root, then push in the adjacent elements.
// One case to prevent the repeating push of a same cell. For example:
//     0 1 2
//    -------
// 0 | 1 1 1
// 1 | 1 1 1
// 2 | 0 0 0
//
// Start from (0, 0), BFS level 0
//
//     0 1 2
//    -------
// 0 | x 1 1
// 1 | 1 1 1
// 2 | 0 0 0
//
// it then push the (0, 1) to queue:
//
//     0 1 2
//    -------
// 0 | x q 1
// 1 | 1 1 1
// 2 | 0 0 0
//
// Then queue the (1, 0)
//
//     0 1 2
//    -------
// 0 | x q 1
// 1 | q 1 1
// 2 | 0 0 0
//
// For the next BFS level 1, (0, 1) and (1, 0) get popped *one by one*:
//     0 1 2
//    -------
// 0 | 0 x 1
// 1 | q 1 1
// 2 | 0 0 0
//
// Now it queues (0, 2) and (1, 1)
//     0 1 2
//    -------
// 0 | 0 x Q
// 1 | q Q 1
// 2 | 0 0 0
//
// now the (1,0) get popped in ths same level 1:
//     0 1 2
//    -------
// 0 | 0 0 Q
// 1 | x Q 1
// 2 | 0 0 0
//
// The same (1,1) get queued, which cause it being revisted in the future. Thats
// no needed.
// So when push a element to the queue, mark it as '0' so it will not be pushed again
// even on the same level:
//     0 1 2
//    -------
// 0 | 0 x 0
// 1 | q 0 1
// 2 | 0 0 0
//
// So when (1,0) get popped, it needs not to queue any cell since those have been queued for next level.
//
// Time: Visit all cells once, O(n). Space for the queue, which can be full fill of the matrix,
// so O(n).



void flood_fill(vector<vector<char>>& g, int i, int j)
{
    int m = g.size();
    int n = g[0].size();

    queue<pair<int, int>> q;
    q.push({i, j});
    g[i][j] = '0';

    while (!q.empty()) {

        int qsize = q.size();

        for (int i = 0; i < qsize; i++) {
            pair<int, int> pos = q.front();
            q.pop();

            vector<pair<int,int>> dirs = {
                {pos.first - 1, pos.second},
                {pos.first, pos.second + 1},
                {pos.first + 1, pos.second},
                {pos.first, pos.second - 1}
            };

            for (auto d : dirs) {
                if (d.first >= 0 && d.first < m &&
                    d.second >= 0 && d.second < n &&
                    g[d.first][d.second] == '1') {
                    g[d.first][d.second] = '0'; // do not queue the same cell if it has multiple adj. cells in this round
                    q.push(d);
                }
            }
        }
    }

    return;
}


class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {

        int ans = 0;

        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                char c = grid[i][j];
                if (c == '1') {
                    flood_fill(grid, i, j);
                    ans ++;
                }
            }
        }

        return ans;

    }
};
