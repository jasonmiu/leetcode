# TO EXPLAIN
#
# @lc app=leetcode id=200 lang=python3
#
# [200] Number of Islands
#
# https://leetcode.com/problems/number-of-islands/description/
#
# algorithms
# Medium (43.40%)
# Total Accepted:    458.1K
# Total Submissions: 1.1M
# Testcase Example:  '[["1","1","1","1","0"],["1","1","0","1","0"],["1","1","0","0","0"],["0","0","0","0","0"]]'
#
# Given a 2d grid map of '1's (land) and '0's (water), count the number of
# islands. An island is surrounded by water and is formed by connecting
# adjacent lands horizontally or vertically. You may assume all four edges of
# the grid are all surrounded by water.
# 
# Example 1:
# 
# 
# Input:
# 11110
# 11010
# 11000
# 00000
# 
# Output: 1
# 
# 
# Example 2:
# 
# 
# Input:
# 11000
# 11000
# 00100
# 00011
# 
# Output: 3
# 
#
class Solution:
    
    def dfs(self, grid, i, j, row_len, col_len, color):
        #print("dfs", i, j)
                
        if i < 0 or i >= row_len:
            return
        if j < 0 or j >= col_len:
            return
        
        if grid[i][j] == color:
            return
        
        if grid[i][j] == "0":
            return
        
        grid[i][j] = color
        
        self.dfs(grid, i-1, j, row_len, col_len, color)
        self.dfs(grid, i, j-1, row_len, col_len, color)
        self.dfs(grid, i+1, j, row_len, col_len, color)
        self.dfs(grid, i, j+1, row_len, col_len, color)
        
        return
    
    def numIslands(self, grid: List[List[str]]) -> int:
        
        color = "2"
        for i in range(0, len(grid)):
            for j in range(0, len(grid[i])):
                
                v = grid[i][j]
                if v == "1":
                    self.dfs(grid, i, j, len(grid), len(grid[i]), color)
                    color = str(int(color) + 1)

#        for i in range(0, len(grid)):
#            for j in range(0, len(grid[i])):
#                print(grid[i][j], end="")
#            print("")

        return (int(color) - 2)
                
