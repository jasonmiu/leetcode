// This is a BFS access of a tree.
// For each tree level, put all nodes in a queue, from left to right.
// Since queue is FIFO, then pop each node out, they will come out as
// left from right. For each node in the queue (current level), we
// want the its left node point the next pointer to it. So we save the
// left node as the "prev" node, except the left most node, point the
// prev node next to the current node.
// After poping out all nodes in the current queue, the last prev node
// is the right most node, so its next is null.
// During poping out the nodes from the queue, it also push its left right
// children to the queue, for next level processing. Problem is, if we
// process the queue while pushing the next level to the same queue,
// we do know if we are moving to the next level or not. We need to know
// the level since we need to null-term the right most node of each level.
// Have two ways to overcome this. First is to save the queue size at beginning
// of a level, then only pop the number of the saved size. Then we know
// number of nodes of 1 level. Another way is to have a special mark,
// like nullptr, push at the end of the queue for each level. So when
// we drain the queue and see this mark, we know it is the end of the queue.
// Time: O(n) as accessing all nodes, Space O(n/2) for the width of a level.

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

#include <queue>

using namespace std;

class Solution {
public:
    Node* connect(Node* root) {

        if (!root) {
            return root;
        }

        queue<Node*> q;

        q.push(root);

        while (!q.empty()) {

            Node* prev = nullptr;
            int qsize = q.size();

            for (int i = 0; i < qsize; i++) {
                Node* n = q.front();
                q.pop();
                if (prev) {
                    prev->next = n;
                }
                prev = n;

                if (n->left)
                    q.push(n->left);

                if (n->right)
                    q.push(n->right);
            }

            prev->next = nullptr;
        }

        return root;
    }
};
