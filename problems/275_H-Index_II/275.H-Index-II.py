# the "has H papers that has at least H citations" means,
# in a sorted array with citation number, if the array size is N,
# at index H, the number of papers with citation > H is (N - H).
# for example:
#  0 1 2 3
# [1 2 5 20]
# If H is 3, means there are at least 3 elements (papers) has value >= 3.
# Values >= 3 are [5, 20], which is smaller than current guessing H.
# If H is 2, means there are least 2 elements has value >= 2
# Values >= 2 are [2, 5, 20].
# the values is no more than 2 is [1, 2], N = 4, and H = 2 match the requirment.
# So for given a H, it is the index of the sorted array, and the number of 
# elements has larger values than A[H] is (N - H). 
# We want to guess a H, which is a index of array, the number of element has larger values
# than A[H] is > (N - H).
# We can do a binary search guess from middle, M
# If the (N - M) is > than A[M], means the number of "at least has M citations" can be more,
# we move the lower bound to M+1, so search to right.
# If (N - M) is <= than A[M], means the number of "at least has M citation" should be less,
# we move the upper bound to M-1, so search to left.
# O(logn) and space is O(1)

class Solution:
    def hIndex(self, citations: List[int]) -> int:
        n = len(citations)
        
        low = 0
        high = n - 1
        
        while low <= high:
            m = (low + high) // 2
            if (n - m) > citations[m]:
                low = m + 1
            else:
                high = m - 1
                
        return n - low
