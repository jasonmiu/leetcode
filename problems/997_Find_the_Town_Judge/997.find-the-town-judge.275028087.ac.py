#
# @lc app=leetcode id=997 lang=python3
#
# [997] Find the Town Judge
#
# https://leetcode.com/problems/find-the-town-judge/description/
#
# algorithms
# Easy (49.36%)
# Likes:    266
# Dislikes: 40
# Total Accepted:    32.6K
# Total Submissions: 66.2K
# Testcase Example:  '2\n[[1,2]]'
#
# In a town, there are N people labelled from 1 to N.  There is a rumor that
# one of these people is secretly the town judge.
# 
# If the town judge exists, then:
# 
# 
# The town judge trusts nobody.
# Everybody (except for the town judge) trusts the town judge.
# There is exactly one person that satisfies properties 1 and 2.
# 
# 
# You are given trust, an array of pairs trust[i] = [a, b] representing that
# the person labelled a trusts the person labelled b.
# 
# If the town judge exists and can be identified, return the label of the town
# judge.  Otherwise, return -1.
# 
# 
# 
# Example 1:
# 
# 
# Input: N = 2, trust = [[1,2]]
# Output: 2
# 
# 
# 
# Example 2:
# 
# 
# Input: N = 3, trust = [[1,3],[2,3]]
# Output: 3
# 
# 
# 
# Example 3:
# 
# 
# Input: N = 3, trust = [[1,3],[2,3],[3,1]]
# Output: -1
# 
# 
# 
# Example 4:
# 
# 
# Input: N = 3, trust = [[1,2],[2,3]]
# Output: -1
# 
# 
# 
# Example 5:
# 
# 
# Input: N = 4, trust = [[1,3],[1,4],[2,3],[2,4],[4,3]]
# Output: 3
# 
# 
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= N <= 1000
# trust.length <= 10000
# trust[i] are all different
# trust[i][0] != trust[i][1]
# 1 <= trust[i][0], trust[i][1] <= N
# 
# 
#

# @lc code=start
# Define the trust as graph, like, a trusts b, then
# a -> b
# A judge has only fan in, but no fan out.
# And since every one in the town trust him, except himself,
# So a judge must has N-1 fan in, and 0 fan out.
# Make a list, each element is a town person, with the index as
# their name. Each town person has to element, [Fan in, Fan out].
# Hence the data structure is a 2D array:
# [N+1][2]
# Use N+1 instead of N as we start the person number from 1.
# Scan the trust, and fill in each one's fan in and fan out count.
# Finally check the 2D array to see who can fullfill the judge
# condition.
# The complexity is O(n)
# The space complexity is O(n) too for the array to store the
# trust map.

class Solution:
    def findJudge(self, N: int, trust: List[List[int]]) -> int:

        # Cannot use * operator to expend the list. 
        # As each element is a object ref which is only 1 copy across the list
#        trust_list = [[0, 0]] * (N+1)

        # [FAN IN, FAN OUT]
        trust_list = []
        
        if N == 0:
            return -1
        if N == 1:
            return 1
    
        for i in range(0, N+1):
            trust_list.append([0,0])

        for p in trust:
            a = p[0]
            b = p[1]

            trust_list[a][1] += 1
            trust_list[b][0] += 1

        print(trust_list)
        for i in range(1, len(trust_list)):
            t = trust_list[i]
            if t[0] == (N-1) and t[1] == 0:
                return i

        return -1


        

# @lc code=end
