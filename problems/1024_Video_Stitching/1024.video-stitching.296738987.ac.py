#
# @lc app=leetcode id=1024 lang=python3
#
# [1024] Video Stitching
#
# https://leetcode.com/problems/video-stitching/description/
#
# algorithms
# Medium (47.51%)
# Total Accepted:    16.1K
# Total Submissions: 33.4K
# Testcase Example:  '[[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]]\n10'
#
# You are given a series of video clips from a sporting event that lasted T
# seconds.  These video clips can be overlapping with each other and have
# varied lengths.
# 
# Each video clip clips[i] is an interval: it starts at time clips[i][0] and
# ends at time clips[i][1].  We can cut these clips into segments freely: for
# example, a clip [0, 7] can be cut into segments [0, 1] + [1, 3] + [3, 7].
# 
# Return the minimum number of clips needed so that we can cut the clips into
# segments that cover the entire sporting event ([0, T]).  If the task is
# impossible, return -1.
# 
# 
# 
# Example 1:
# 
# 
# Input: clips = [[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]], T = 10
# Output: 3
# Explanation: 
# We take the clips [0,2], [8,10], [1,9]; a total of 3 clips.
# Then, we can reconstruct the sporting event as follows:
# We cut [1,9] into segments [1,2] + [2,8] + [8,9].
# Now we have segments [0,2] + [2,8] + [8,10] which cover the sporting event
# [0, 10].
# 
# 
# Example 2:
# 
# 
# Input: clips = [[0,1],[1,2]], T = 5
# Output: -1
# Explanation: 
# We can't cover [0,5] with only [0,1] and [0,2].
# 
# 
# Example 3:
# 
# 
# Input: clips =
# [[0,1],[6,8],[0,2],[5,6],[0,4],[0,3],[6,7],[1,3],[4,7],[1,4],[2,5],[2,6],[3,4],[4,5],[5,7],[6,9]],
# T = 9
# Output: 3
# Explanation: 
# We can take clips [0,4], [4,7], and [6,9].
# 
# 
# Example 4:
# 
# 
# Input: clips = [[0,4],[2,8]], T = 5
# Output: 2
# Explanation: 
# Notice you can have extra video after the event ends.
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= clips.length <= 100
# 0 <= clips[i][0], clips[i][1] <= 100
# 0 <= T <= 100
# 
# 
#
# The idea is to use a greedy algorithm, to fill up 0->T space.
# For example, 
# T = 10, clips = [[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]]
# First clip wil be [0,2]
# Then the space to be filled in becomes 2->10
# The next starting point of next clip needed to be smaller than the current space start,
# which is the last clip end, for overlapping these two clips.
# The possible clip now is : [1,9], [1,5]
# We want to select the next clip that has larger ending. So we take [1,9] here,
# and the space to be filled in becomes 9->10
# The next possible cip is [8,10]
# Then the space become 10->10. If the current ending point is >= T, we have the whole
# space covered, can stop.
# We need to search the longest lasting chip in the possible clips for each round,
# two corner cases are:
# 1. We used up clips, but the ending point does not reach T, like
#  [[0, 5], [4, 8]], T = 10
# 2. We cannot find a possible clip that fullfill the condidtion: 
#    next clip starting point <= current ending point, like
# [[0, 5], [7, 10]], T=10
# If both case we stop the search since we have no way to cover the whole space,
# we return -1 for them.
# The complexity is O(n^2), n is the number of clips, since we need to scan thru each
# clip, and for possible clips we need to scan thru again to find the longest lasting.
# The space complexity is O(1)
# 
# Can sort first, and scan thru the clips, which can do in O(nlogn)
# def videoStitching(self, clips, T):
#        end, end2, res = -1, 0, 0
#        for i, j in sorted(clips):
#            if end2 >= T or i > end2:
#                break
#            elif end < i <= end2:
#                res, end = res + 1, end2
#            end2 = max(end2, j)
#        return res if end2 >= T else -1

class Solution:
    def videoStitching(self, clips: List[List[int]], T: int) -> int:
        
        ans = 0
        e = 0
        while e < T:
            
            max_clip = None
            max_chip_end = -999
            
            for c in clips:
                if c[0] <= e:
                    if c[1] > max_chip_end:
                        max_clip = c
                        max_chip_end = c[1]
            
            if max_clip == None:
                break
                
            e = max_clip[1]
            clips.remove(max_clip)
            ans += 1
            
            if len(clips) <= 0:
                break
                
        if e < T:
            return -1
        else:            
            return ans
                
