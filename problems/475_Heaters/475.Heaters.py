# The idea is for each house, need to find the nearest heater.
# The bisect_left() tells which heater is on your right,
# since bisect_left() gives the index of the *inserting position* of the
# left of the target.
# If bisect_left() returns 0, means the house is on the left of the 
# first heater.
# If bisect_left() return len(heaters), means the house is on the right
# of the last heater, as no possible left inserting position.
# Both farends are the min radius need to be covered.
# For all other indices, the house is in the middle of two heaters.
# Check which heater is closer. If the closerest heater will larger than
# the current known min radius, update the min radius.
# Complexity: O(nlogn + mlogn), n is the size of heaters, m is the size of houses
# Space: O(1)

import bisect


class Solution:
    def findRadius(self, houses: List[int], heaters: List[int]) -> int:
        
        heaters.sort()
        min_r = 0
        for p in houses:
            
            # idx means if I place this house p on the left of a house,
            # which index of this house?
            idx = bisect.bisect_left(heaters, p)
            
            # this house is on the left most
            if idx == 0:
                r = heaters[idx] - p
                
            # this house is on the right most
            elif idx == len(heaters):
                r = p - heaters[idx-1]
                
            else:
                left_heater_r = p - heaters[idx-1]
                right_heater_r = heaters[idx] - p
                r = min(left_heater_r, right_heater_r)
                
            if r > min_r:
                min_r = r
                
        return min_r
  

