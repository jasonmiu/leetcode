#
# @lc app=leetcode id=111 lang=python3
#
# [111] Minimum Depth of Binary Tree
#
# https://leetcode.com/problems/minimum-depth-of-binary-tree/description/
#
# algorithms
# Easy (36.09%)
# Total Accepted:    348.1K
# Total Submissions: 959.4K
# Testcase Example:  '[3,9,20,null,null,15,7]'
#
# Given a binary tree, find its minimum depth.
# 
# The minimum depth is the number of nodes along the shortest path from the
# root node down to the nearest leaf node.
# 
# Note: A leaf is a node with no children.
# 
# Example:
# 
# Given binary tree [3,9,20,null,null,15,7],
# 
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
# return its minimum depth = 2.
# 
#
# The idea is to use depth first search, to go thru each path of the 
# the tree from root to leaves. If this is leave node, return the
# depth of the current path. For the middle nodes, find the shorter
# path from left or right subtrees, and return upper level.
# Complexity is O(n) for accessing all nodes.
# Space complexity is O(logn) for the recusive stack.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def dfs(root, depth):
    
    if root == None:
        return 0
    
    if root.left == None and root.right == None:
        return depth + 1
    
    left_depth = 999999999
    right_depth = 999999999
    if root.left:
        left_depth = dfs(root.left, depth+1)
    if root.right:
        right_depth = dfs(root.right, depth+1)
        
    return min(left_depth, right_depth)

class Solution:
    def minDepth(self, root: TreeNode) -> int:
        
        ans = dfs(root, 0)
        
        return ans
