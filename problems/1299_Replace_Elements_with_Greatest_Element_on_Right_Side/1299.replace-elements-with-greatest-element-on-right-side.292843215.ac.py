#
# @lc app=leetcode id=1299 lang=python3
#
# [1299] Replace Elements with Greatest Element on Right Side
#
# https://leetcode.com/problems/replace-elements-with-greatest-element-on-right-side/description/
#
# algorithms
# Easy (75.95%)
# Total Accepted:    7.3K
# Total Submissions: 9.6K
# Testcase Example:  '[17,18,5,4,6,1]'
#
# Given an array arr, replace every element in that array with the greatest
# element among the elements to its right, and replace the last element with
# -1.
# 
# After doing so, return the array.
# 
# 
# Example 1:
# Input: arr = [17,18,5,4,6,1]
# Output: [18,6,6,6,1,-1]
# 
# 
# Constraints:
# 
# 
# 1 <= arr.length <= 10^4
# 1 <= arr[i] <= 10^5
# 
#
# The idea is we want to know the largest num of the right hand side of i.
# Given the j > i and arr[j] > arr[i], we mark the output out[i] to arr[j]
# Scanning from right gives us the current largest from right at i.
# The complexity is O(n), for linear scanning.
# The space complexity is O(n), for output array.

class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        
        n = len(arr)
        out = [0] * n
        r_max = arr[-1]
        out[-1] = -1
        
        for i in range(n - 2, -1, -1):
            out[i] = r_max
            if arr[i] > r_max:
                r_max = arr[i]
                
        return out
