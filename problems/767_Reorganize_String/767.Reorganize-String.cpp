// Start from a simple example, "aab", how to arrage it to a string
// that any two adjacent chars are not the same?
// Combinations are:
// 1. aab
// 2. aba
// 3. baa
// Only the combination 2 is the valid result. If we start to construct the
// result string from the starting char of the input ("a" in this example),
// How to build the result string?
// Since the first char is "a", we cannot pick "a" again. The next possible
// selection is "b". So pick "b", and repeat this process, the next possible
// selection is "a", then finish the result.
// However, what if start with "b"? It then pick the "a", then since no other
// possible selection, it can only pick "a" but the result will not be a valid
// output. So Starting from "b" is not correct. Then the question will be,
// which char should be the starting point, and given that there are multiple
// non-repeating chars, what is the next one should be picked?
//
// Now try another example, "abbccc". If start with "a", then it can be 'b' or 'c'
// for the next. If pick 'b', when next one can be 'a' or 'c'. Let it be 'c'. So the
// output is now: "abc". Since no more 'a' left, it pick 'b', then 'c', the output is
// now: "abcbc". For the last char, only 1 'c' letter remains so the result will be:
// "abcbcc" which is an invalid output. However, a valid combination is possible:
// "cbcbca". So the picking order is indeed related to the frequency of a char in the
// input string. And we should start from the most frequent char.
// In the example "abbccc", "c" is the most frequent one, so start with "c". After
// picking "c", the "b" and "c" have the same frequency, but since we cannot repeat the
// adjacent char, we cannot pick "c", then the next most frequent char is "b", so next
// is "b". Then the output now is:
// "cb" , count dict: {c: 2, b: 1, a: 1}
// We cannot pick "b" now, and the next most frequent char is "c", so take it and output is now:
// "cbc" , count dict: {c: 1, b: 1, a: 1}
//
// So we need we should pick the most frequent char for the next one, except we cannot pick the
// last one even it still has the highest frequency. To model this, use a max heap, which
// always have the most frequent element at front. After selected that most frequent element,
// pop it, *do not push it back at once*, but save it for next round, since we cannot select it
// from the heap consecutively. Now the heap, is having the next most frequent element at the front.
// Select and pop that, then we can put back the previously saved char to the heap for next selection.
// The idea is to treat the heap as our bag of selection. For example:
// input : "abbccc"
// output: "", last: "", heap: {c:3, b:2, a:1},
// output: "c", last: "c:2", heap: {b:2, a:1}
// output: "cb", last: "b:1", heap: {c:2, a:1}
// output: "cbc", last: "c:1", heap: {b:1, a:1}
// output: "cbcb", last: "b:0", heap: {c:1, a:1}
// output: "cbcbc", last: "c:0", heap: {a:1} <- If last element has count 0 already, do not need to add back to heap
// output: "cbcbca", last: "a:0", heap: { } <- stop when heap is empty
//
// Another case is when there is no valid combination:
// input : "aaab"
// output: "", last: "", heap: {a:3, b:1}
// output: "a", last: "a:2", heap: {b:1}
// output: "ab", last: "b:0", heap: {a:2}
// output: "aba", last: "a:1", heap: { } <- All possible valid selections will be used up, if no valid combination can
//                                          be found. The output string cannot be completed which has different size
//                                          from the input string.
//
// heapify the input takes O(nlogn), so time is: O(nlogn) and space is O(n) for the heap.



struct freq_char {
    int freq;
    char c;
};

class Solution {
public:
    string reorganizeString(string s) {

        unordered_map<char, int> counts;
        for (char c : s) {
            counts[c] ++;
        }

        vector<freq_char> freq_chars;
        for (auto p : counts) {
            freq_char fc;
            fc.c = p.first;
            fc.freq = p.second;
            freq_chars.push_back(fc);
        }

        auto heap_func = [](const freq_char& fc1, const freq_char& fc2) {
            if (fc1.freq < fc2.freq) {
                return true;
            } else {
                return false;
            }
        };
        make_heap(freq_chars.begin(), freq_chars.end(), heap_func);

        vector<char> ans;
        freq_char last_fc;

        pop_heap(freq_chars.begin(), freq_chars.end(), heap_func);
        freq_chars.back().freq --;
        last_fc = freq_chars.back();
        freq_chars.pop_back();
        ans.push_back(last_fc.c);

        while (freq_chars.size() > 0) {

            pop_heap(freq_chars.begin(), freq_chars.end(), heap_func);
            freq_chars.back().freq --;
            freq_char cur_fc = freq_chars.back();
            freq_chars.pop_back();
            ans.push_back(cur_fc.c);

            if (last_fc.freq > 0) {
                freq_chars.push_back(last_fc);
                push_heap(freq_chars.begin(), freq_chars.end(), heap_func);
            }

            last_fc = cur_fc;
        }

        if (ans.size() < s.size()) {
            return "";
        }

        return string(ans.begin(), ans.end());
    }
};
