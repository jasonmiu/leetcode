// For two nodes in a tree, they can have multiple common ancestors.
// If they have a common acenstors, the nodes above their first common
// ancestor from below (the lowest common ancestor in question) will be the
// same as they will share the same path. So the idea is to find the paths
// to each target node, and check their common path, and see where their
// paths start to seperate, which is asking for the longest common prefix
// of two paths of nodes.
// First, use DFS to find the path to a node. Convert the path to an array.
// Get two paths of two target nodes, and match their elements one by one.
// Once an element started to be different, means the previous element is
// the last common ancestor, which is the answer.
// Time: O(n) for access all nodes, space: O(n) for storing the paths and common
// ancestors.

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void get_path(TreeNode* n, TreeNode* t, vector<TreeNode*>& path, vector<TreeNode*>& result)
{
    if (n == nullptr) {
        return;
    }

    if (n == t) {
        path.push_back(n);
        result = path;
        path.pop_back();
        return;
    }

    path.push_back(n);
    get_path(n->left, t, path, result);
    get_path(n->right, t, path, result);
    path.pop_back();

    return;
}

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {

        vector<TreeNode*> path;
        vector<TreeNode*> p_path;
        vector<TreeNode*> q_path;

        get_path(root, p, path, p_path);
        get_path(root, q, path, q_path);

        int i = 0;
        int j = 0;
        vector<TreeNode*> comm;

        while (i < p_path.size() && j < q_path.size()) {
            TreeNode* pn = p_path[i];
            TreeNode* qn = q_path[j];
            if (pn == qn) {
                comm.push_back(pn);
            } else {
                break;
            }

            i++;
            j++;
        }

        return comm.back();
    }
};
