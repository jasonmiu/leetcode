;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.

(let ((f (lambda (x) (* x x))))
  (funcall f 2))

(find-file "*.pem" t)

(let* ((prefix "/Users/jmiu/projects/leetcode/problems")
       (expfile "TO_EXP")
       (open-buf (lambda (f) (find-file (format "%s/%s/*.py" prefix f) t))))
                              
  (mapc open-buf
        (with-temp-buffer
          (insert-file-contents (format "%s/%s" prefix expfile))
          (split-string (buffer-string) "\n" t))))


  


(let* ((prefix "/Users/jmiu/projects/leetcode/problems")
       (expfile "TO_EXP")
       (p (lambda (l) (if l
                          (progn (message "%s" (car l))
                                 (funcall p (cdr l)))))))
                        
  (funcall p
           (with-temp-buffer
             (insert-file-contents (format "%s/%s" prefix expfile))
             (split-string (buffer-string) "\n" t))))

(message "%s" "HOHO")

