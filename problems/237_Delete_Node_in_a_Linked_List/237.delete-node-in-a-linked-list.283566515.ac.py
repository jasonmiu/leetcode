#
# @lc app=leetcode id=237 lang=python3
#
# [237] Delete Node in a Linked List
#
# https://leetcode.com/problems/delete-node-in-a-linked-list/description/
#
# algorithms
# Easy (56.54%)
# Total Accepted:    335.4K
# Total Submissions: 590.4K
# Testcase Example:  '[4,5,1,9]\n5'
#
# Write a function to delete a node (except the tail) in a singly linked list,
# given only access to that node.
# 
# Given linked list -- head = [4,5,1,9], which looks like following:
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: head = [4,5,1,9], node = 5
# Output: [4,1,9]
# Explanation: You are given the second node with value 5, the linked list
# should become 4 -> 1 -> 9 after calling your function.
# 
# 
# Example 2:
# 
# 
# Input: head = [4,5,1,9], node = 1
# Output: [4,5,9]
# Explanation: You are given the third node with value 1, the linked list
# should become 4 -> 5 -> 9 after calling your function.
# 
# 
# 
# 
# Note:
# 
# 
# The linked list will have at least two elements.
# All of the nodes' values will be unique.
# The given node will not be the tail and it will always be a valid node of the
# linked list.
# Do not return anything from your function.
# 
# 
#
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

import gc

# Since this is a singly linked list and we have no reference back to the
# previous nodes. We cannot use the common node deletion by pointing the
# next pointer of the previous node to the current next node. So the 
# solution is not to delete, but copy the next node value to replace the current
# one, and point the next node to the next-next node, which skips the current
# current next node. Thats why we cannot "delete" the tail node as it has no
# next node for it to skip.
#
# But using the garbage collector and reflection of python we can find all
# objects and rebuild the reference between the nodes. With the "previous_of"
# dictionary we can find the previous node hence we can update its next pointer
# and delete the current node as usual.
# 
# This should work for deleting the head node too. However since we cannot
# change the object reference of the "head" variable outside this function,
# we copy the value of the next node to the current one and delete the next 
# node instead.

class Solution:
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """

        previous_of = {}
        
        for n in gc.get_objects():
            if type(n) == ListNode and "val" in dir(n):
                next_n = n.next
                previous_of[next_n] = n
                
        if node in previous_of:
            p = previous_of[node]
            p.next = node.next
        else:
            d = node.next
            node.val = node.next.val
            node.next = node.next.next
            del d
            
        del n
        
                



            
                    
                

        
        
                
        
                    
