#
# @lc app=leetcode id=12 lang=python3
#
# [12] Integer to Roman
#
# https://leetcode.com/problems/integer-to-roman/description/
#
# algorithms
# Medium (52.49%)
# Total Accepted:    276.3K
# Total Submissions: 526.4K
# Testcase Example:  '3'
#
# Roman numerals are represented by seven different symbols: I, V, X, L, C, D
# and M.
# 
# 
# Symbol       Value
# I             1
# V             5
# X             10
# L             50
# C             100
# D             500
# M             1000
# 
# For example, two is written as II in Roman numeral, just two one's added
# together. Twelve is written as, XII, which is simply X + II. The number
# twenty seven is written as XXVII, which is XX + V + II.
# 
# Roman numerals are usually written largest to smallest from left to right.
# However, the numeral for four is not IIII. Instead, the number four is
# written as IV. Because the one is before the five we subtract it making four.
# The same principle applies to the number nine, which is written as IX. There
# are six instances where subtraction is used:
# 
# 
# I can be placed before V (5) and X (10) to make 4 and 9. 
# X can be placed before L (50) and C (100) to make 40 and 90. 
# C can be placed before D (500) and M (1000) to make 400 and 900.
# 
# 
# Given an integer, convert it to a roman numeral. Input is guaranteed to be
# within the range from 1 to 3999.
# 
# Example 1:
# 
# 
# Input: 3
# Output: "III"
# 
# Example 2:
# 
# 
# Input: 4
# Output: "IV"
# 
# Example 3:
# 
# 
# Input: 9
# Output: "IX"
# 
# Example 4:
# 
# 
# Input: 58
# Output: "LVIII"
# Explanation: L = 50, V = 5, III = 3.
# 
# 
# Example 5:
# 
# 
# Input: 1994
# Output: "MCMXCIV"
# Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
# 
#
# The idea is to recursively divide the number in to
# different digit. Since the max possible input 3999,
# the largest digit is 1000. The current digit is 'n',
# the remainder is 'r'. For eg, input 3999, in the first 
# recursion, n = 3, r = 999
# We check the var n to see what roman letters should we 
# use. We divide it in to 3 main cases, smaller than 5 or 
# bigger than 5 or equal to 5.
# If n is < 5, we repeat the current roman letter for this 
# digit, in the code it is the var "u".
# Need to handle the special case 4, which we need to use
# the roman letter for 5, which is var "f".
# if n > 5, we repeat the current roman letter n - 5, with
# the roman letter for 5 in front of it.
# We need to handle the special cases 6, and 9. For 9, we
# need the roman letter for next digit, which is var "d"
# if n == 5, we use the roman letter for 5.
# Then we input the remainder to the same function and pass
# the partially translated roman string along.
# If no more remainder, the recusion stop and pass the 
# roman string back

class Solution:
    def recur(self, num, roman):
        if num == 0:
            return roman
        
        n = 0
        r = 0
        u = ""
        if num >= 1000:
            n = num // 1000
            r = num - (1000 * n)
            u = "M" # current roman digit
            f = ""  # no 5000 is possible
            
        elif num >= 100:
            n = num // 100
            r = num - (100 * n)
            u = "C" # current roman digit
            f = "D" # current roman digit for 5
            d = "M" # next roman digit for handling 9
            
        elif num >= 10:
            n = num // 10
            r = num - (10 * n)
            u = "X"
            f = "L"
            d = "C"
            
        elif num >= 1:
            n = num
            r = 0
            u = "I"
            f = "V"
            d = "X"
            
        s = ""
        if n == 4:
            s = u + f
        elif n >= 1 and n <= 3:
            s = u * n
        elif n == 5:
            s = f
        elif n >= 6 and n <= 8:
            s = f + (u * (n-5))
        elif n == 9:
            s = u + d
        
        return self.recur(r, roman + s)
        
 
    
    def intToRoman(self, num: int) -> str:
        return self.recur(num, "")
        
