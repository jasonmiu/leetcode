# Direct solution is using a counter dict. The key is the element in 
# the input nums. The ans is the key with counter value has 1.
# But to use no extra memory, we need to use the info of
# All numbers appear "TWICE" except one. Feel like some math way
# to elimiate those repeated numbers.
# For two same numbers, A xor A = 0,
# and 0 xor A = A
# so A xor A xor B xor B xor C = 0 xor 0 xor C = C
# We can xor all numbers in the input array and get
# the single number.
# Complexity O(n)
# Space O(1)

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        ans = 0
        for i in range(0, len(nums)):
            ans = ans ^ nums[i]
            
        return ans

