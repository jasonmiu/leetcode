// For constance extra space, we can use the bool array that has the max range of the element.
// Flip the array element when the index appear in the input list. So the element appear twice
// will become F -> T -> F, while the element appear once will become F -> T,
// so finally find the element index that is T. Use two arrays for positive and negative elements.
// The best solution will be using XOR, where:
// for (int i : nums) {
//   ans = ans ^ i;
// }
// Since the XOR can eliminate same value, like :
// 10 = b0101
// 15 = b0111
// 10 = b0101
//
//     10 = b0101
// XOR 15 = b0111
//          b0010
// XOR 10 = b0101
//          b0111
//
// XOR can eliminate the same values even they are not next to each other.
// Time: O(n) for scanning. Space: O(1) with fixed array size.


class Solution {
public:
    int singleNumber(vector<int>& nums) {

        array<bool, 30000> pos = {0};
        array<bool, 30000> neg = {0};

        for (int i = 0; i < nums.size(); i++) {
            int v = nums[i];

            if (v >= 0) {
                pos[v] = !pos[v];
            } else {
                v = abs(v);
                neg[v] = !neg[v];
            }
        }

        for (int i = 0; i < pos.size(); i++) {
            if (pos[i]) {
                return i;
            }
        }

        for (int i = 0; i < neg.size(); i++) {
            if (neg[i]) {
                return -i;
            }
        }

        return 0;
    }
};
