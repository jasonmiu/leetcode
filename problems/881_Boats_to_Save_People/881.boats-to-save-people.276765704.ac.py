#
# @lc app=leetcode id=881 lang=python3
#
# [881] Boats to Save People
#
# https://leetcode.com/problems/boats-to-save-people/description/
#
# algorithms
# Medium (44.82%)
# Likes:    346
# Dislikes: 25
# Total Accepted:    20.3K
# Total Submissions: 45.3K
# Testcase Example:  '[1,2]\n3'
#
# The i-th person has weight people[i], and each boat can carry a maximum
# weight of limit.
# 
# Each boat carries at most 2 people at the same time, provided the sum of the
# weight of those people is at most limit.
# 
# Return the minimum number of boats to carry every given person.  (It is
# guaranteed each person can be carried by a boat.)
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: people = [1,2], limit = 3
# Output: 1
# Explanation: 1 boat (1, 2)
# 
# 
# 
# Example 2:
# 
# 
# Input: people = [3,2,2,1], limit = 3
# Output: 3
# Explanation: 3 boats (1, 2), (2) and (3)
# 
# 
# 
# Example 3:
# 
# 
# Input: people = [3,5,3,4], limit = 5
# Output: 4
# Explanation: 4 boats (3), (3), (4), (5)
# 
# Note:
# 
# 
# 1 <= people.length <= 50000
# 1 <= people[i] <= limit <= 30000
# 
# 
# 
# 
# 
#

# @lc code=start
# This is greedly algor. If a boat can place heavy and light persons, then
# put them together. Otherwise, one whole boat for heavy person.
# Sort the list so left hand side is light persons, right side side is heavy persons.
# Move 2 pointers from both sides, until all paired up.
# p[0], p[1], ... , p[n-1], p[n]
# Take the p[n] the heaviest person in consideration. Two cases:
# 1. The lightest person p[0] cannot onboard with him. So all p[i], i > 0 will not be able to be on the same boat
# 2. The ligher person up to p[k] can onboard with him. Then the next heavy person p[n-1] can also onboard with 
# people up to p[k] as well, since p[n-1] <= p[n], p[n] + p[k] <= limit then p[n-1] + p[k] <= limit also
# Hence if the heavy person can find the lightest person to pair with, then pair with the lightest person p[i].
# Pairing with p[j] where k >= j > i will be the same as p[n-1] can pair with them as well. So we can use greedly
# algor.
# complexity is O(nlogn)
# space complexity is O(1)

class Solution:
    def numRescueBoats(self, people: List[int], limit: int) -> int:
        people.sort()

        lo = 0
        hi = len(people) - 1
        boats = 0
        while lo <= hi:
            boats += 1
            if people[lo] + people[hi] <= limit:
                lo += 1

            hi -= 1

        return boats
        
        
# @lc code=end
