#
# @lc app=leetcode id=881 lang=python3
#
# [881] Boats to Save People
#

# @lc code=start

# O(n^2) algor time exceed

class Solution:
    def boat(self, people, limit, boats):
        n = len(people)
        if n == 0:
            return boats

        p = people[0]
        if n == 1 or p == limit:
            del people[0]
            return self.boat(people, limit, boats + 1)

        min_r = 65536
        min_idx = 0
        
        for i in range(1, n):
            q = people[i]
            r = limit - (p + q)
            if r >= 0 and r < min_r:
                min_r = r
                min_idx = i

        if min_idx == 0:
            del people[0]
            return self.boat(people, limit, boats + 1)
        else:
            del people[min_idx]
            del people[0]
            return self.boat(people, limit, boats + 1)
            
            
    def numRescueBoats(self, people: List[int], limit: int) -> int:
        people.sort()
        return self.boat(people, limit, 0)
        
# @lc code=end
