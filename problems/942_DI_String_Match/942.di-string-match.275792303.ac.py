#
# @lc app=leetcode id=942 lang=python3
#
# [942] DI String Match
#
# https://leetcode.com/problems/di-string-match/description/
#
# algorithms
# Easy (69.96%)
# Likes:    566
# Dislikes: 207
# Total Accepted:    45.6K
# Total Submissions: 65.1K
# Testcase Example:  '"IDID"'
#
# Given a string S that only contains "I" (increase) or "D" (decrease), let N =
# S.length.
# 
# Return any permutation A of [0, 1, ..., N] such that for all i = 0, ...,
# N-1:
# 
# 
# If S[i] == "I", then A[i] < A[i+1]
# If S[i] == "D", then A[i] > A[i+1]
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: "IDID"
# Output: [0,4,1,3,2]
# 
# 
# 
# Example 2:
# 
# 
# Input: "III"
# Output: [0,1,2,3]
# 
# 
# 
# Example 3:
# 
# 
# Input: "DDI"
# Output: [3,2,0,1]
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= S.length <= 10000
# S only contains characters "I" or "D".
# 
#

# @lc code=start
# While N is the len(S), and A is a set of interger
# [0, 1, ... N], so we know the largest value will be N,
# and the smallest value is 0.
# Start with 2 values, a and b, for checking S[i]. 
# a is monotonic increase and b is monotonic decrease.
# For anytime, a < b to fullfil th I and D. This is the invariant.
# b start with N (max value), and a start with 0 (min value).
# If all S is all I, then A will be filled with increasing a. 
# No b will be added hence always a < b. Same reason for S is all D.
# If S is [ID], the max N will be used by b hence max of a can only be
# N-1. So the invariant holds. 
# Need added 1 more element to the end of A. As need to fullfill the last
# A[i+1] element for S[i].
# The complexity is O(n), and space complexity is O(n).

class Solution:
    def diStringMatch(self, S: str) -> List[int]:
        N = len(S)
        a = 0
        b = N

        last = None
        
        A = []
        for s in S:
            last = s
            if s == "I":
                A.append(a)
                a += 1
            elif s == "D":
                A.append(b)
                b -= 1

        if last == "I":
            A.append(a)
        elif last == "D":
            A.append(b)
            
        return A

# @lc code=end
