#
# @lc app=leetcode id=1 lang=python3
#
# [1] Two Sum
#
# https://leetcode.com/problems/two-sum/description/
#
# algorithms
# Easy (44.69%)
# Total Accepted:    2.3M
# Total Submissions: 5M
# Testcase Example:  '[2,7,11,15]\n9'
#
# Given an array of integers, return indices of the two numbers such that they
# add up to a specific target.
# 
# You may assume that each input would have exactly one solution, and you may
# not use the same element twice.
# 
# Example:
# 
# 
# Given nums = [2, 7, 11, 15], target = 9,
# 
# Because nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1].
# 
#
# The first idea was doing a O(n^2) algorithm with 
# two loops to find all combinations.
# Here is a faster O(n) algoritm. The idea is to make
# use of the math rule, for A + B = C, A = C - B
# If we know A is existing, and we see B now, we know
# they can sum to C. So we can build a dictionary
# when we read each elements. When read a element (B), 
# we check if we saw its counter part (C-B=A) before.
# If we saw that before we can return their indices.

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        
        n = len(nums)
        counterparts = {}
        for i in range(0, n):
            
            c = target - nums[i]
            if c in counterparts:
                return [counterparts[c], i]
                
            counterparts[nums[i]] = i
            
        return []
