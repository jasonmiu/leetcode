#
# @lc app=leetcode id=1038 lang=python3
#
# [1038] Binary Search Tree to Greater Sum Tree
#
# https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/description/
#
# algorithms
# Medium (78.01%)
# Total Accepted:    38.8K
# Total Submissions: 48.7K
# Testcase Example:  '[4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]'
#
# Given the root of a binary search tree with distinct values, modify it so
# that every node has a new value equal to the sum of the values of the
# original tree that are greater than or equal to node.val.
# 
# As a reminder, a binary search tree is a tree that satisfies these
# constraints:
# 
# 
# The left subtree of a node contains only nodes with keys less than the node's
# key.
# The right subtree of a node contains only nodes with keys greater than the
# node's key.
# Both the left and right subtrees must also be binary search trees.
# 
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
# Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
# 
# 
# 
# 
# 
# 
# Constraints:
# 
# 
# The number of nodes in the tree is between 1 and 100.
# Each node will have value between 0 and 100.
# The given tree is a binary search tree.
# 
# 
# 
# 
# Note: This question is the same as 538:
# https://leetcode.com/problems/convert-bst-to-greater-tree/
# 
# 
# 
#
# The question is asking, for a node N_i in a BST,
# update its value, to the sum of N_j + N_i, where N_j has value larger 
# than N_i.
# For a BST, the the nodes have larger value than N_i means all nodes
# on the right side of the N_i.
# So the new value of N_i = sum(nodes on the right of N_i) + N_i
# For example:
#
#               4
#            /    \
#          1       6
#           \    /  \
#            2  5    7
# 
# For the node 1, the nodes of its right hand side are
# 1. Its right subtree
# 2. Its parent right subtree
#
# We can have its right subtree sum, by propagating the child node values up.
# For the parent right subtree sum, the parent can pass down the current sum 
# of its larger values.
# So for each node, the new value is :
# N_i = (right subtree sum) + (parent right subtree sum) + node value
# 
# Complexity is O(n) since we walk all nodes.
# Space is O(1) as no extra space used.


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def greater_tree(root, larger_sum):
    
    v = root.val
    if root.left == None and root.right == None:
        root.val = larger_sum + root.val
        return v
    
    r = 0
    if root.right:
        r = greater_tree(root.right, larger_sum)
        
    root.val = root.val + larger_sum + r

    l = 0
    if root.left:
        l = greater_tree(root.left, root.val)
        
    return v + r + l

class Solution:
    def bstToGst(self, root: TreeNode) -> TreeNode:
        greater_tree(root, 0)
        
        return root
