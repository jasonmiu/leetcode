#
# @lc app=leetcode id=556 lang=python3
#
# [556] Next Greater Element III
#
# https://leetcode.com/problems/next-greater-element-iii/description/
#
# algorithms
# Medium (30.58%)
# Total Accepted:    34.8K
# Total Submissions: 112.1K
# Testcase Example:  '12'
#
# Given a positive 32-bit integer n, you need to find the smallest 32-bit
# integer which has exactly the same digits existing in the integer n and is
# greater in value than n. If no such positive 32-bit integer exists, you need
# to return -1.
# 
# Example 1:
# 
# 
# Input: 12
# Output: 21
# 
# 
# 
# 
# Example 2:
# 
# 
# Input: 21
# Output: -1
# 
# 
# 
# 
#
# Using the same digits to form a number we can think of finding the permutation.
# Instead of finding ALL permutations of a set of digits, the next greater value
# is actually the next next lexicographical permutation. Like this:
# [1 2 3] -> we should increase the first non-increasing digit from right, ie "2".
# We call this "pivot". And find the first number larger than pivot (ie, 3) from right,
# and swap them.
# Then the first part before pivot (prefix) is minimum for next permutation, we should
# sort the part after pivot (suffix) to get the smallest number. Since suffix was the
# monotic increasing list, and we swapped a smaller value (pivot) so the suffix will still be
# montic increasing. So we can just reverse it.
# Complexity is O(n), for scanning the pivot and the swapping element.
# Space complexity is O(n) for using extra array to do reverse but can be O(1) if we do in-place.

def next_permutation(array):
    pivot = -1
    for i in range(len(array)-2, -1, -1):
        if array[i] < array[i+1]:
            pivot = i
            break

    print(pivot)
    if pivot == -1:
        return None
    
    for i in range(len(array)-1, pivot, -1):
        if array[i] > array[pivot]:
            tmp = array[i]
            array[i] = array[pivot]
            array[pivot] = tmp
            
            ans = array[0:pivot+1] + list(reversed(array[pivot+1::]))
            return ans
        
    return None
        

class Solution:
    def nextGreaterElement(self, n: int) -> int:
        num = list(str(n))
        
        ans = next_permutation(num)
        if ans != None:
            x = int("".join(ans))
            if x <= pow(2, 32)//2 - 1:
                return x
            else:
                return -1
        else:
            return -1
