#
# @lc app=leetcode id=328 lang=python3
#
# [328] Odd Even Linked List
#
# https://leetcode.com/problems/odd-even-linked-list/description/
#
# algorithms
# Medium (50.84%)
# Total Accepted:    200.8K
# Total Submissions: 384.8K
# Testcase Example:  '[1,2,3,4,5]'
#
# Given a singly linked list, group all odd nodes together followed by the even
# nodes. Please note here we are talking about the node number and not the
# value in the nodes.
# 
# You should try to do it in place. The program should run in O(1) space
# complexity and O(nodes) time complexity.
# 
# Example 1:
# 
# 
# Input: 1->2->3->4->5->NULL
# Output: 1->3->5->2->4->NULL
# 
# 
# Example 2:
# 
# 
# Input: 2->1->3->5->6->4->7->NULL
# Output: 2->3->6->7->1->5->4->NULL
# 
# 
# Note:
# 
# 
# The relative order inside both the even and odd groups should remain as it
# was in the input.
# The first node is considered odd, the second node even and so on ...
# 
# 
#
# The idea is using two pointers, 1 for traversing odd nodes and 1 for traversing even nodes.
# For odd traversing, we jump the next node like:
# 1 -> 2 -> 3 -> 4 -> null
# ^- odd pointer
# odd_pointer->next->next is the next odd. Same as in the even traversing case.
# If we only 1 node or no node, which are the special cases, we return only 1 node or zero node.
# If we have 2 nodes which is the simplest case, like:
# 1 -> 2 -> null
# we link the next of odd and even pointer to the next odd and even node, like:
# 1    2 -> null
# +--> null
# So we have two lists, 1 for odd nodes 1 for even nodes.
# We can see we can do this recursivly:
# 1 -> 2 -> 3 -> 4 -> null
# 1 -> 3 -> 4 -> null (odd node link)
#      2 -> 4 -> null (even node link)
# The next odd pointer is 3 and even pointer is 4:
# 3 -> 4 -> null
# which is the base case. Hence we will have
# 1 -> 3 -> null
# 2 -> 4 -> null
# 2 links. Then we find the tail of the odd link, and append the even link to it.
# Complexity O(n) for traversing links. Space is O(1) for in-place.


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def group_list(op, ep):
    if not op or not ep:
        return
    
    if op.next and op.next.next:
        op.next = op.next.next
    else:
        op.next = None
        
    if ep.next and ep.next.next:
        ep.next = ep.next.next
    else:
        ep.next = None
        
    if op.next and ep.next:
        group_list(op.next, ep.next)
        
    return

class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        
        if head:
            odd_head = head
        else:
            return None
        
        if head.next:
            even_head = head.next
        else:
            return odd_head
        
        group_list(odd_head, even_head)
        
        n = odd_head
        while n.next:
            n = n.next
            
        n.next = even_head
        
        return odd_head
        
