// The idea is the same as the python version, which is split the input list to two lists,
// one for odd nodes, one for even nodes. Then appent the even nodes list to the odd nodes list.
// The way to split two list in this implementation is iterative, using two pointers, one start from
// odd node, one start from even node, and join to the next odd or even node, which is one node skip.
// Finally join the lists together and return the odd list head.
// Time: O(n), space: O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {

        if (!head) {
            return nullptr;
        }

        if (head->next == nullptr) {
            return head;
        }

        ListNode* odd_head = head;
        ListNode* even_head = head->next;

        ListNode* odd = odd_head;
        ListNode* even = even_head;

        ListNode* next_odd = nullptr;
        ListNode* next_even = nullptr;
        while (true) {

            if (odd->next) {
                next_odd = odd->next->next;
            } else {
                next_odd = nullptr;
            }

            if (next_odd == nullptr) {
                break;
            }

            next_even = even->next->next;

            odd->next = next_odd;
            even->next = next_even;

            odd = next_odd;
            even = next_even;
        }

        odd->next = even_head;

        return odd_head;
    }
};
