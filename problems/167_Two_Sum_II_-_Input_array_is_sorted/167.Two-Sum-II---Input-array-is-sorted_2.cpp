// Another algor. Make use of the sorted array.
// Since the array is sorted, sum the head and tail elements.
// Let the head side pointer be the smaller element,
// the tail side pointer be the bigger element in the target = smaller + bigger equation
// If the sum is smaller than target, means we like to have a bigger element,
// so move the left pointer to right.
// If the sum is bigger than target, we like to have a smaller element,
// so move the right pointer to left.
// Since the question must has an answer, we can return when we found the target.
// Time: O(n), space: O(1).

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {

        int i = 0;
        int j = numbers.size() - 1;

        int idx_1 = 0;
        int idx_2 = 0;

        // same element cannot be used twice, so when checking
        // [i] + [j], i cannot be equal to j.
        while (i < j) {
            int r = numbers[i] + numbers[j];
            if (r == target) {
                idx_1 = i + 1;
                idx_2 = j + 1;
                break;
            } else if (r < target) {
                // sum too small, try to make it bigger, move i to right
                i = i + 1;
            } else {
                // sum too big, try to make it smaller, move j to left
                j = j - 1;
            }
        }

        return vector{idx_1, idx_2};
    }
};
