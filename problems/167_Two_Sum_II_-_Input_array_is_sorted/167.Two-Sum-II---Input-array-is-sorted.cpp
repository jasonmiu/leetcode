// use the idea, target = X + Y
// For each X, we like to know where is the Y, since the question it must contains
// answer. To find that Y, naive way is to do a double loop, for each X, search Y
// from beginning. But this results a O(n^2).
// So build a hash, the key is the element, the value is the index of that element.
// For each element, check where is the Y in the hash.
// A special case is when the input has duplicated element, like:
// [4, 4, 9], Target = 8
// The hash table value has to be a vector<int>, to contains two indices of the same element,
// And if the Y found same as X, means X == Y, want to return the 2nd index in the hashed value.
//
// Time: O(n), as build hash scan once, check Y scan once. Space: O(n).

#include <unordered_map>

using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        unordered_map<int, vector<int>> counter_map;

        for (int i = 0; i < numbers.size(); i++) {
            counter_map[numbers[i]].push_back(i);
        }

        int idx_1 = 0;
        int idx_2 = 0;

        for (int i = 0; i < numbers.size(); i++) {
            int x = target - numbers[i];

            if (counter_map.find(x) != counter_map.end()) {
                int j = 0;
                if (counter_map[x][0] == i && (counter_map[x].size() > 1)) {
                    j = counter_map[x][1];
                } else {
                    j = counter_map[x][0];
                }

                idx_1 = i + 1;
                idx_2 = j + 1;

                break;
            }
        }

        return vector{idx_1, idx_2};
    }
};
