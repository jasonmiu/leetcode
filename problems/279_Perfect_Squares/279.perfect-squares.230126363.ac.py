# TO EXPLAIN
#
# @lc app=leetcode id=279 lang=python3
#
# [279] Perfect Squares
#
# https://leetcode.com/problems/perfect-squares/description/
#
# algorithms
# Medium (43.34%)
# Total Accepted:    217.9K
# Total Submissions: 502.9K
# Testcase Example:  '12'
#
# Given a positive integer n, find the least number of perfect square numbers
# (for example, 1, 4, 9, 16, ...) which sum to n.
# 
# Example 1:
# 
# 
# Input: n = 12
# Output: 3 
# Explanation: 12 = 4 + 4 + 4.
# 
# Example 2:
# 
# 
# Input: n = 13
# Output: 2
# Explanation: 13 = 4 + 9.
#
class Solution:
    def numSquares(self, n: int) -> int:
        memo = {}
        return self.squtil(n, int(n**0.5), memo)
                
    def squtil(self, n, sn, memo):
        min_cnt = 0
        
        if (n, sn) in memo:
            return memo[(n, sn)]
        
        for num in range(sn, 0, -1):
            sq = num * num
            
            if sq * 4 <= n:
                break
            
            rest = n
            sq_cnt = 0
            
            while rest >= sq:
                rest = rest - sq
                sq_cnt += 1

            cnt = self.squtil(rest, int(rest**0.5), memo) + sq_cnt

            if cnt < min_cnt or min_cnt == 0:
                min_cnt = cnt

        memo[(n, sn)] = min_cnt
        
        return min_cnt
