#
# @lc app=leetcode id=55 lang=python3
#
# [55] Jump Game
#
# https://leetcode.com/problems/jump-game/description/
#
# algorithms
# Medium (32.75%)
# Total Accepted:    372.5K
# Total Submissions: 1.1M
# Testcase Example:  '[2,3,1,1,4]'
#
# Given an array of non-negative integers, you are initially positioned at the
# first index of the array.
# 
# Each element in the array represents your maximum jump length at that
# position.
# 
# Determine if you are able to reach the last index.
# 
# Example 1:
# 
# 
# Input: [2,3,1,1,4]
# Output: true
# Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
# 
# 
# Example 2:
# 
# 
# Input: [3,2,1,0,4]
# Output: false
# Explanation: You will always arrive at index 3 no matter what. Its
# maximum
# jump length is 0, which makes it impossible to reach the last index.
# 
# 
#
# Think this code. This is doing bottom up.
# enum Index {
#     GOOD, BAD, UNKNOWN
# }
#
# public class Solution {
#     public boolean canJump(int[] nums) {
#         Index[] memo = new Index[nums.length];
#         for (int i = 0; i < memo.length; i++) {
#             memo[i] = Index.UNKNOWN;
#         }
#         memo[memo.length - 1] = Index.GOOD;
#
#         for (int i = nums.length - 2; i >= 0; i--) {
#             int furthestJump = Math.min(i + nums[i], nums.length - 1);
#             for (int j = i + 1; j <= furthestJump; j++) {
#                 if (memo[j] == Index.GOOD) {
#                     memo[i] = Index.GOOD;
#                     break;
#                 }
#             }
#         }
#
#         return memo[0] == Index.GOOD;
#     }
# }
# For example:
# 
#  0 1 2 3 4
# [2 3 1 1 4]  <- input
# [U U U U G]  <- memo
#        ^-i
# From current index i, if we can jump to next known GOOD position on the right,
# ie: the range of i + A[i] has a known good position, this position i is good
# too. So from i = 3, we search 3 + 1, from i = 2 we share 2 + 1, from
# i = 1 we search 3 + 1, 3 + 2, 3 + 3. The memo will be:
#  0 1 2 3 4
# [2 3 1 1 4]  <- input
# [U G G G G]  <- memo
#  ^-i
# If from position 0 we can jump to any known good position, then it is good too.
# Otherwise we cannot jump to end. The search of GOOD can be n, and run n times, 
# so the complexity is O(n^2).
# But, for searching the GOOD in memo, we do not need to search from current position
# to the end, since we only need the first encountered GOOD, ie, from the current
# position to the right, the left most GOOD position is enough. We can keep track this
# latest good position, and if i+A[i] >= this latest good position means the jump range
# covers to the next good position, ie. this i position is good too and we can 
# update the latest good position to it.
# This removes the need of good position searching in the memo. The complexity becomes
# O(n).
#
# Here is the top-down memo code, which has O(n^2) too.
# def dfs(nums, i, cache):
#     n = len(nums)
#     if i == n - 1:
#         return True
#     if i >= n:
#         return False
#
#     if i in cache:
#         return cache[i]
#
#     s = nums[i]
#     for j in range(1, s+1):
#         r = dfs(nums, i+j, cache)
#         if r:
#             cache[i] = True
#             return True
#
#     cache[i] = False
#     return False
#
#
# class Solution:
#     def canJump(self, nums: List[int]) -> bool:
#         ans = dfs(nums, 0, dict())
#         return ans
#


class Solution:
    def canJump(self, nums: List[int]) -> bool:
        
        last_can_jump_idx = len(nums) - 1
        for i in range(len(nums)-2, -1, -1):
            if i + nums[i] >= last_can_jump_idx:
                last_can_jump_idx = i
                
        if last_can_jump_idx == 0:
            return True
        else:
            return False
