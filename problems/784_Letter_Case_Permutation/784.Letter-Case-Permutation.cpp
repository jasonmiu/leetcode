// First thing came to mind is the string only have to change
// cases for alphabetic letters. And we have to return ALL possible strings,
// so have to generate all combinations.
// Since the non-alpha chars need not to be transformed, extract all alpha chars first.
// Save their original indices, so when we generate the results, we can refer back to
// the original indices. For example:
// input : a1b2
// alpha_str : ab
// alpha_idxs: 02
//             01 <- index of the alpha_idxs
//
// Then generate all cases combinations of the alpha_str. For each char in the alpha_str,
// we have to make a lower and upper cases for it. For a lower case, move to the next char,
// and make lower and upper case of the next char. For a upper case, same thing, move to
// the next char. So this is a recusive process for spliting two paths for every char.
// When the processing index hit the end of the input string, we know a combination has been
// generated along this path, which is the current string. Save it to a result list.
// for example:
// "a"b
//  |
//  +-- a"b" <-- lower case path
//  |     |
//  |     +-- ab"" -> result
//  |     |
//  |     +-- aB"" -> result
//  |
//  +-- A"b" <-- upper case path
//  |     |
//  |     +-- Ab"" -> result
//  |     |
//  |     +-- AB"" -> result
//
//
// Then we have all alpha combinations, like:
// ab
// aB
// Ab
// AB
//
// For each combination, generate a new string by copying the original
// input s, and replace the char in the current combination with their
// original indcies in alpha_idxs generated before.
//
// Time: O(2^n), since each step of combination generation can give 2 possibilities,
// and run n times. Space is also O(2^n) for saving all results.


void allcases(string& str, int start, vector<string>& results)
{
    if (start >= str.size()) {
        results.push_back(str);
        return;
    }

    string s1(str);
    s1[start] = tolower(s1[start]);
    allcases(s1, start+1, results);

    string s2(str);
    s2[start] = toupper(s2[start]);
    allcases(s2, start+1, results);

    return;
}

class Solution {
public:
    vector<string> letterCasePermutation(string s) {

        vector<int> alpha_idxs;
        string alpha_str;
        for (int i = 0; i < s.size(); i++) {
            if (isalpha(s[i])) {
                alpha_idxs.push_back(i);
                alpha_str.append(1, s[i]);
            }
        }

        vector<string> ans;
        if (alpha_str.size() == 0) {
            ans.push_back(s);
            return ans;
        }

        vector<string> cases;
        allcases(alpha_str, 0, cases);

        for(string& cs : cases) {
            string ps(s);

            for (int i = 0; i < cs.size(); i++) {
                ps[alpha_idxs[i]] = cs[i];
            }

            ans.push_back(ps);
        }

        return ans;

    }
};
