// For example:
//  0  1  2  3
//[-2  3 -2  4]
//
// The easiest way to think is to have a double loop, to find all products
// of the subarrays, like:
//  0  1  2  3
//[-2  3 -2  4]
// (-2), (-2*3 = -6), (-2*3*-2 = 12), (-2 * 3 * -2 * 4 = 48)
//       (3),         (3 * -2 = -6),  (3 * -2 * 4 = -24)
//                    (-2),           (-2 * 4 = -8)
//                                    (-4)
// Then get the max product. This gives a O(n^2) algor.
// This works like a sliding window, and get the combination of the max product.
// But we do not care the combinations, so can we save the sliding window from double loop?
// For product, if all elements are positive, then the max product should always be
// the longest array, which is the input array. But if some elements are negative,
// Then we see a problem, like:
//
//  0  1  2  3
//[-2  3 -2  4]
//     ^
//     | If we propagate the max product so far, what is the max product here should be?
//     | It should start from 3, since if multiply the previous max -2, the current product
//     | will be -6, which is smaller than start the subarray from here, which is 3.
//     | However, if we picked 3 here, the next product will try to mulitply -2,
//     | which gives -6, then at the next element the subarray will be restarted again
//     | from -2. But if we pick the smaller value -6 here, the next element can give a
//     | larger product 12.
//
// So the hard problem is, since we do not know if there are opposit sign elements in
// advance, we do not know which choices at the current element:
// A. multiply with the current product
// B. restart the subarray here and the new product set to be the current value
// is better.
//
// From this point, we can think its a decision tree go to two branchs with A and B above.
// For each element, we can pick two cases, one is multiple the product with the current value,
// and we want to know it will give a max value or min value. Or we can restart the subarray,
// which can also give us the max or min value. So for each element, we keep both max or min for
// the current element so far. The max is of course the final answer we want to have,
// keeping the min is because it can be the very negative, and if we hit a negative element later,
// it will give back a large positive which can be global max too.
//
// Make a array with the size of input, call dp. Which element in dp has two values, the current min
// and current max. Start from the end, since the min and max must be the same input value.
// Go from backward, for each value try multiply the dp min and max after it. Compare with the
// current value. Put the product min or the current value to the current dp min, depends on which
// is smaller. Same for the dp max.
// Since for each time we will find the current max product so far in the current subarray,
// the max value in the dp max will be the answer.
//
// Time: O(n) for scanning once. Space: O(n) for the DP array.

class Solution {
public:
    int maxProduct(vector<int>& nums) {

        int n = nums.size();
        vector<pair<int,int>> dp(n);

        dp[n-1].first = nums[n-1]; // .first is for min product
        dp[n-1].second = nums[n-1]; // .second is for max product
        int ans = nums[n-1];

        for (int i = n-2; i >= 0; i--) {
            int small = nums[i] * dp[i+1].first;
            int big = nums[i] * dp[i+1].second;

            vector<int> vals = {small, big, nums[i]};
            dp[i].first = *(min_element(vals.begin(), vals.end()));
            dp[i].second = *(max_element(vals.begin(), vals.end()));

            int cur_max = dp[i].second;
            ans = max(ans, cur_max);
        }

        return ans;
    }
};
