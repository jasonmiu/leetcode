#
# @lc app=leetcode id=152 lang=python3
#
# [152] Maximum Product Subarray
#
# https://leetcode.com/problems/maximum-product-subarray/description/
#
# algorithms
# Medium (30.28%)
# Total Accepted:    282.2K
# Total Submissions: 918.2K
# Testcase Example:  '[2,3,-2,4]'
#
# Given an integer array nums, find the contiguous subarray within an array
# (containing at least one number) which has the largest product.
# 
# Example 1:
# 
# 
# Input: [2,3,-2,4]
# Output: 6
# Explanation: [2,3] has the largest product 6.
# 
# 
# Example 2:
# 
# 
# Input: [-2,0,-1]
# Output: 0
# Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
# 
#
# The main and hardest idea is to thinking of the array of saving
# the current subarray minimum. We need to have minimum, is because
# the input array can contains -ve values. So if the current subarray 
# has a big +ve max product at i-th element, and the i+1-th element is 
# a -ve value, then the subarray product at i+1 will become a small -ve
# value. However, if i+2-th element is a -ve value again, then the subarray
# product will become a large +ve value. Base on this observation, the
# "max" product is based on the polarity of the current input element.
# For example:
# A = [2, -3, 2]
# dp_max = [2, -3, 2]  <== for i = 0->n, dp_max[i] = A[i]
# dp_min = [2, -3, -12]  <== for i = 0->n, dp_min[i] = A[i] * dp_min[i]
#
# A = [2, -3, -2]
# dp_max = [2, -3, 12] <== for i = 2, dp_max[i] = A[i] * dp_min[i-i]
# dp_min = [2, -6, -2] <== for i = 2, dp_min[i] = A[i]
#
# So we can see when the polarity changes, the DP array for back reference changes too.
# The dp_max[i] is the max product value of the subarray ended at i. So the ans
# will be the max value in the dp_max.
# Time complexity is O(n), space is O(n)

class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        
        dp_max = [0] * len(nums)
        dp_min = [0] * len(nums)
        dp_min[0] = dp_max[0] = nums[0]
        
        for i in range(1, len(nums)):
            a = nums[i]
            
            if a >= 0:
                dp_max[i] = max(a, dp_max[i-1] * a)
                dp_min[i] = min(a, dp_min[i-1] * a)
            else:
                dp_max[i] = max(a, dp_min[i-1] * a)
                dp_min[i] = min(a, dp_max[i-1] * a)
                
        return max(dp_max)
