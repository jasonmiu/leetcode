// For a tree like:
// [3,5,1,6,2,0,8,null,null,7,4], target = 5, k = 2
//
//                            3
//                       /         \
//                      5           1
//                   /  |           |  \
//                  6   2           0   8
//                     / \
//                    7   4
//
// If use 5 as root, from the k distance node from its subtree is easy,
// can be done in DFS search downward.
// The main problem is how to know another side via parent, like this
// example "1" is also the answer. It is because we do not have access
// back to parent in tree. But if we convert it to a undirected graph like:
//
//                         6 -- 5 -- 3 -- 1 -- 0
//                             /           \
//                       7 -- 2             8
//                           /
//                          4
//
// And start from node 5, search outward 1 level by 1 level, the nodes we want
// will be at the K level, since each edge in this graph is 1 distance.
// We can use BFS to find the level by having each level of node in a queue.
// To convert the tree to graph, DFS can do the job by added all nodes and their
// parents to the graph.
//
// Time: O(n) since access all node 2 times. Space is O(n) for the graph.


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void make_graph(TreeNode* root, TreeNode* parent, unordered_map<int, vector<int>>& g)
{
    if (root == nullptr) {
        return;
    }

    if (parent) {
        g[parent->val].push_back(root->val);
        g[root->val].push_back(parent->val);
    }

    if (root->left) {
        make_graph(root->left, root, g);
    }
    if (root->right) {
        make_graph(root->right, root, g);
    }

    return;
}

class Solution {
public:
    vector<int> distanceK(TreeNode* root, TreeNode* target, int k) {

        unordered_map<int, vector<int>> g;
        make_graph(root, nullptr, g);

        queue<int> q;
        q.push(target->val);

        int level = 0;
        unordered_set<int> visited;

        vector<int> ans;

        while (!q.empty()) {

            vector<int> level_vals;
            int qsize = q.size();
            for (int i = 0; i < qsize; i++) {
                int u = q.front();
                q.pop();
                visited.insert(u);
                level_vals.push_back(u);

                const vector<int>& adjs = g[u];
                for (int v : adjs) {
                    if (visited.find(v) == visited.end()) {
                        q.push(v);
                    }
                }
            }

            if (level == k) {
                ans = move(level_vals);
                break;
            } else {
                level ++;
            }
        }
        return ans;
    }
};
