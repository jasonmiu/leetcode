// It is trying to make Cartesian Products of all mapped string of input digits,
// while the elements in a set is the mapped string chars.
// If we know the number of digits (sets) in advance, hard code with
// n for loops is the easiest way. But we do not know the number of input digits, so
// the idea should relate to recursion.
// We can recusively generate the cartesian combinations. If we have only 1 digit,
// then the return is all its mapped char, for example:
// input : [2]
// return: ["a", "b", "c"]
//
// This will be the base case, since for adding 1 more digit, it is appending the
// chars of the next digit to the every strings in the returned sub-combinations.
// Since we build the combinations buttom up, ie. the first returned sub-combination is
// the last digit, so the previous digit prepend of all strings returned from it.
// Then recursively pop up until we back to the first digit.
//
// Time is O(m^n), where the m is the max length of the keymap value (4 in this case).
// It is power n as if we have n digits, we need to make products n times. The space
// is used to store all results so it is also O(m^n).

vector<string> find_comb(const vector<int>& digits, int s, const unordered_map<int, string>& keymap)
{
    vector<string> ret;
    int d = digits[s];

    if (s == digits.size() - 1) {
        string str = keymap.at(d);
        for (char c : str) {
            ret.push_back(string{c});
        }
        return ret;
    }

    string str = keymap.at(d);
    for (char c : str) {
        vector<string> subcomb = find_comb(digits, s+1, keymap);
        for (string ss : subcomb) {
            ret.push_back(string{c} + ss);
        }
    }

    return ret;
}

class Solution {
public:
    vector<string> letterCombinations(string digits) {

        unordered_map<int, string> keymap = {
            {2, "abc"},
            {3, "def"},
            {4, "ghi"},
            {5, "jkl"},
            {6, "mno"},
            {7, "pqrs"},
            {8, "tuv"},
            {9, "wxyz"}
        };

        if (digits.size() == 0) {
            return {};
        }

        vector<int> intd(digits.size());
        transform(digits.begin(), digits.end(), intd.begin(), [](char c) {
            return c - '0';
        });

        vector<string> result = find_comb(intd, 0, keymap);

        return result;
    }
};
