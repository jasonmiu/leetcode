// Using the fast-slow pointers idea.
// For example"
// "God Ding"
//  i
//  j
// pointer i is always pointing to the current non-space char
// pointer j is always >= i, search for the next space char
// "God Ding"
//  i  j
// Once the next space char get found, means we found a word, reverse the content between i and j:
// "doG Ding"
//  i  j
// Then move the i to the next non-space char. Since all words are single space seperated, the next
// non-space char will be j + 1:
// "doG Ding"
//     ji
// Then j search from i again, here is the special case, as no next space any more, but the last index:
// "doG Ding"
//      i   j
// as reverse range is end-exclusive, [i, j), so we have to let j hit the index of size()+1, then do the
// last reverse:
// "doG ginD"
//      i    j
// Let i = j + 1, and since i over the input range, all chars get processed. End the loop.
// Time: O(n), space O(1).

#include <algorithm>

using namespace std;

class Solution {
public:
    string reverseWords(string s) {
        int i = 0;
        int j = 0;

        while (i < s.size()) {
            if (s[i] != ' ') {
                for (j = i; j < s.size() + 1; j++) {
                    if (s[j] == ' ' || j == s.size()) {
                        reverse(&(s[i]), &(s[j]));
                        i = j + 1;
                        break;
                    }
                }
            }
        }

        return s;
    }
};


// another implementation using string::find
/***
#include <algorithm>

using namespace std;

class Solution {
public:
    string reverseWords(string s) {
        int i = 0;
        int j = 0;

        while (i < s.size()) {
            j = s.find(" ", j);

            if (j == string::npos) {
                j = s.size();
            }
            reverse(&(s[i]), &(s[j]));
            i = j + 1;
            j = j + 1;
        }

        return s;
    }
};
***/
