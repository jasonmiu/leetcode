# TO EXPLAIN
#
# @lc app=leetcode id=837 lang=python3
#
# [837] New 21 Game
#
# https://leetcode.com/problems/new-21-game/description/
#
# algorithms
# Medium (32.78%)
# Total Accepted:    12.1K
# Total Submissions: 36.9K
# Testcase Example:  '10\n1\n10'
#
# Alice plays the following game, loosely based on the card game "21".
# 
# Alice starts with 0 points, and draws numbers while she has less than K
# points.  During each draw, she gains an integer number of points randomly
# from the range [1, W], where W is an integer.  Each draw is independent and
# the outcomes have equal probabilities.
# 
# Alice stops drawing numbers when she gets K or more points.  What is the
# probability that she has N or less points?
# 
# Example 1:
# 
# 
# Input: N = 10, K = 1, W = 10
# Output: 1.00000
# Explanation:  Alice gets a single card, then stops.
# 
# 
# Example 2:
# 
# 
# Input: N = 6, K = 1, W = 10
# Output: 0.60000
# Explanation:  Alice gets a single card, then stops.
# In 6 out of W = 10 possibilities, she is at or below N = 6 points.
# 
# 
# Example 3:
# 
# 
# Input: N = 21, K = 17, W = 10
# Output: 0.73278
# 
# Note:
# 
# 
# 0 <= K <= N <= 10000
# 1 <= W <= 10000
# Answers will be accepted as correct if they are within 10^-5 of the correct
# answer.
# The judging time limit has been reduced for this question.
# 
# 
#
class Solution:
    def new21Game(self, N: int, K: int, W: int) -> float:
        
        # alice stop playing if the current point is between (K, K+W-1)
        # If N is larger then the limit (K+W-1), alice must stopped with point <= N
        if (N >= K+W-1):
            return 1.0
        
        # probaility of getting 'x' point. x is the key of the p{}
        p = {}
        
        p[0] = 1.0 # having 0 point, probaility is 1, ie. start the game
        
        prev_prob = 0
        for i in range(1, K+1):
            # i is the current point
            # check the low boundary
            if (i - W - 1) >= 0:
                prev_prob = prev_prob - p[i - W - 1] + p[i-1]
            else:
                prev_prob = prev_prob + p[i-1]
                
            p[i] = prev_prob * (1/W)
            
        ans = p[K]
        # from K + 1, do not need to add p[i-1] as p[i-1] >= K
        for i in range(K+1, N+1):
            if (i - W - 1) >= 0:
                prev_prob = prev_prob - p[i - W - 1]

            p[i] = prev_prob * (1/W)
            
            ans = ans + p[i]
            
        return ans
            
                
            
        
        
