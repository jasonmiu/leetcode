#
# @lc app=leetcode id=1130 lang=python3
#
# [1130] Minimum Cost Tree From Leaf Values
#
# https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/description/
#
# algorithms
# Medium (61.79%)
# Total Accepted:    17.3K
# Total Submissions: 26.8K
# Testcase Example:  '[6,2,4]'
#
# Given an array arr of positive integers, consider all binary trees such
# that:
# 
# 
# Each node has either 0 or 2 children;
# The values of arr correspond to the values of each leaf in an in-order
# traversal of the tree.  (Recall that a node is a leaf if and only if it has 0
# children.)
# The value of each non-leaf node is equal to the product of the largest leaf
# value in its left and right subtree respectively.
# 
# 
# Among all possible binary trees considered, return the smallest possible sum
# of the values of each non-leaf node.  It is guaranteed this sum fits into a
# 32-bit integer.
# 
# 
# Example 1:
# 
# 
# Input: arr = [6,2,4]
# Output: 32
# Explanation:
# There are two possible trees.  The first has non-leaf node sum 36, and the
# second has non-leaf node sum 32.
# 
# ⁠   24            24
# ⁠  /  \          /  \
# ⁠ 12   4        6    8
# ⁠/  \               / \
# 6    2             2   4
# 
# 
# 
# Constraints:
# 
# 
# 2 <= arr.length <= 40
# 1 <= arr[i] <= 15
# It is guaranteed that the answer fits into a 32-bit signed integer (ie. it is
# less than 2^31).
# 
#
# The idea is to try to build all possible trees.
# The main observation is we cut the leaves (input array) in to two
# parts, and start build from two parts. To try all possible trees,
# I should try to cut in all possible points.
# The hard part was I dunno how to find the max leave for a subtree
# to get the middle nodes.
# Base on the main observation, when we cut the array to two part,
# the middle node of that subtree is the max of the first part of 
# leave nodes and the max of the second part of the leave nodes, 
# for eg:
# [6, 2]
# If we cut betwee 6, 2, then
# [6], [2] => middle node is 6 * 2 where is the max of first part ([6])
# and the max of the second part ([2]). Then how about
# [6, 2, 4, 3] if we cut between 6, 2? 
# [6], [2, 4, 3], since a node can has only 0 child or 2 children, it cannot be
#
#              node
#        /     /   \   \
#      6      2     4   3
# That puzzled me. Indeed from the base case with 2 leaves, we see we want to
# have two adjacency nodes to be multiplied. So if we keep scanning the array, 
# and shorten the array recursively, we will cut the last two nodes like: 
# [4], [3]. Then when we see the single node, we cannot go further, so return 0,
# and let the max first and second part to form the middle node of this subtree,
# Thats the "r3" part.
# So we want to try all cutting points, thats the for loop.
# For each cutting points, we like to do the same thing to find the sum of middle nodes
# for both partitions. There are the r1 and r2.
# We sum them up for and get the min sum of middle nodes of this subtree, and propagate up.
# When we cut the array in two parts, we specify the start and end index.
# This cut will be repeated, like:
# 6  2    (0, 1)
# \  /  
#  12   4 (0, 2)
#   \  /
#    24   3 (0, 3)
#     \  /
#      18
#
#  6  2 (0,1)   4  3 (2,3)
#  \ /          \  /
#  12           12 (0, 3)
#   \          /
#        24
# Those repeated (s, e) produce the same middle node values. So we can use DP to cache them.
# Complexity O(n^3), n for for loop, n times find the max in for loop, which is n^2, and recurse
# all input for n * n^2 so O(n^3)
# The space will be O(n^2) for each (s,e) pairs.
#
# Another much faster algor for this problem is using monotonic stack.
# In our problem, we notice that we should place nodes with higher value on to tree’s higher level because 
# if a high-value node is placed on low level, that node will be taken into calculation for more times than 
# when it is placed on a high level.
# For example:
# [99, 1, 2, 3]
#
# 99   1
# \   /
#  99   2
#   \  /
#    99*2   3
#     \    /
#      99*2*3
# 
#       1   2
#       \ /
#        2    3
#        \   /
#   99     6     
#    \    /          
#     99 * 3
#
# The tree 2 has smaller middle nodes sum, so we like to have the larger values places in the higher tree level.
# So, our goal: every time, we find out two smallest element to build a subtree. 
# We can reach this goal by introducing Monotonic Stack.
# class Solution {
#     public int mctFromLeafValues(int[] arr) {
#         Stack<Integer> stack = new Stack();
#         stack.push(16);
#         int ans = 0;
#         for (int i = 0; i < arr.length; i++) {
#             while (!stack.isEmpty() && arr[i] > stack.peek()) {
#                 int x = stack.pop();
#                 ans += x * Math.min(arr[i], stack.peek());
#             }
#             stack.push(arr[i]);
#         }
#         while (stack.size() > 2) {
#             int x = stack.pop();
#             ans += x * stack.peek();
#         }
#         return ans;
#     }
# }
# The complexity and space complexity are O(n)
# Ref: https://huadonghu.com/archives/en/leetcode1130-minimum-cost-tree-from-leaf-values-solution/


def build_tree(arr, s, e, memo):
    # this element is a leave
    if s >= e:
        return 0
    
    if (s, e) in memo:
        return memo[(s, e)]
    
    ans = pow(2, 31)
    for i in range(s+1, e+1):
        r1 = build_tree(arr, s, i-1, memo)
        r2 = build_tree(arr, i, e, memo)
        r3 = max(arr[s:i]) * max(arr[i:e+1])
        ans = min(ans, r1+r2+r3)
        
    memo[(s, e)] = ans
        
    return ans
    

class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        
        memo = {}
        ans = build_tree(arr, 0, len(arr)-1, memo)
        return ans
