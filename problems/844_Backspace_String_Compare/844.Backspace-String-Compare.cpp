// Since when we see a "#" the actual string get backspaced and removed.
// Similar to undo, so we can have a stack to store current char.
// Once it a "#", then pop the stack to remove the latest one.
// Finally the stack will contain the actual string.
// Time O(n), space O(n) for stack.
//
// To get the space O(1), we can start backward of both strings.
// If see a "#", skip the comparing next (left hand side) char,
// If all non-skipped comparsions are correct, then return a true, else false.

string backstr(const string& s)
{
    stack<char> stack;

    for (char c : s) {
        if (c != '#') {
            stack.push(c);
        } else {
            if (!stack.empty()) {
                stack.pop();
            }
        }
    }

    string str;
    while (!stack.empty()) {
        char c = stack.top();
        stack.pop();
        str.append(1, c);
    }

    return str;
}

class Solution {
public:
    bool backspaceCompare(string s, string t) {
        string bs = backstr(s);
        string bt = backstr(t);

        if (bs == bt) {
            return true;
        } else {
            return false;
        }
    }
};
