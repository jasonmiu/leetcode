#
# @lc app=leetcode id=334 lang=python3
#
# [334] Increasing Triplet Subsequence
#
# https://leetcode.com/problems/increasing-triplet-subsequence/description/
#
# algorithms
# Medium (39.68%)
# Total Accepted:    125.3K
# Total Submissions: 314.8K
# Testcase Example:  '[1,2,3,4,5]'
#
# Given an unsorted array return whether an increasing subsequence of length 3
# exists or not in the array.
# 
# Formally the function should:
# 
# Return true if there exists i, j, k 
# such that arr[i] < arr[j] < arr[k] given 0 ≤ i < j < k ≤ n-1 else return
# false.
# 
# Note: Your algorithm should run in O(n) time complexity and O(1) space
# complexity.
# 
# 
# Example 1:
# 
# 
# Input: [1,2,3,4,5]
# Output: true
# 
# 
# 
# Example 2:
# 
# 
# Input: [5,4,3,2,1]
# Output: false
# 
# 
# 
#
# Let the increasing subsequence is (A B C).
# The idea is a greed way to find the first min value,  
# for A. Then scan the next element. If the current element
# is smaller than A, then it will be the new A since the value A
# must be the smallest among the subsequence. If the current
# element is larger than A, but smaller than B, it will be new B
# since we want the C has higher chance to be bigger than B, like:
# [6, 1, 4, 2, 3]
#     A  
#       B'-->B
# If the current element is 2 and current B is B', we update the B
# to the current element.
# If we have a element bigger than A and B, we get the subsequenece.
# One implemetation trick using If-elif-else case is the elif and else
# cases, mean "not all the cases above" and the element only process by 
# 1 case. The commented "continue" version is easier to read.


import math
class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        i = float(math.inf)
        j = float(math.inf)
        
        for n in nums:
            if n <= i:
                i = n
            elif n <= j:
                j = n
            else:
                return True

        return False

'''
import math
class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        i = float(math.inf)
        j = float(math.inf)
        
        for n in nums:
            if n < i:
                i = n
                continue
                
            if n > i and n < j:
                j = n
                continue
                
            if n > j:
                return True
            
        return False
'''
