// This is standard prefix tree algor. The main point is, for a complete word like:
// "apple" get inserted in to the tree, the nodes of this word should be ended with
// a special mark ('#' here). Like:
//
//  Root
//    \
//     a
//      \
//       p
//        \
//         p
//        / \
//       #   l
//            \
//             e
//              \
//               #
//
// So the "apple" and "app" are both whole words that can be found in search().
// String "appl" will not be found in search but startsWith().
//
// One implmentation point for c++ is, the Node struct is actuall a hash of
// char and the node attached to that char. Like:
//
//  Root
//   |
//   a - p - p
//         - l
//     - b - c
//
// Has 3 strings: app, apl, abc. For the first 'a', it contains two hash elements: [p,b].
// But c++ does not allow using un-finished definition, so:
// struct Node {
//    unordered_map<char, Node> children;
// };
// Will *not* work. Hence we need a Node* as the map value here.


struct Node {
public:
    char c;
    unordered_map<char, Node*> children;
};


class Trie {

private:
    Node root;

public:
    Trie() {
        root.c = '@';
    }

    void insert(string word) {
        Node* n = &root;
        for (char c : word) {
            if (n->children.find(c) == n->children.end()) {
                Node* new_node = new Node;
                new_node->c = c;
                n->children[c] = new_node;
                n = n->children[c];
            } else {
                n = n->children[c];
            }
        }

        Node* end_node = new Node;
        end_node->c = '#';
        n->children['#'] = end_node;
    }

    bool search(string word) {
        Node* n = &root;
        for (char c : word) {
            if (n->children.find(c) == n->children.end()) {
                return false;
            } else {
                n = n->children[c];
            }
        }

        if (n->children.find('#') != n->children.end()) {
            return true;
        } else {
            return false;
        }
    }

    bool startsWith(string prefix) {
        Node* n = &root;
        for (char c : prefix) {
            if (n->children.find(c) == n->children.end()) {
                return false;
            } else {
                n = n->children[c];
            }
        }

        return true;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
