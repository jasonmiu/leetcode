// This is like DFS search. For the base case, input array has only 1 element,
// the permutation is itself.
// For 2 elements, the permutation is itself, and swapping two elements, giving
// two permutations.
// For 3 elements, to illustrate:
//
// [ 1 2 3 ]
// permutations:
// [1,2,3], [1,3,2], [3,1,2]
// [2,1,3], [2,3,1], [3,2,1]
//
// Assuming we have only two values, [1 2], the permutation of this 1-lesser element
// array is:
// [ 1 2 ], [ 2 1 ]
// We can see for the current element 3, insert it between every elements of its 1-lesser element
// subarry permutation, we can get the current permutation.
// For n elements, current permutation is added the last element to its n-1 permutation every posistion.
// Time: O(n!), Space: O(n!)

vector<vector<int>> gen_permutation(vector<int>& a)
{
    if (a.size() == 1) {
        return vector<vector<int>>{a};
    }

    // This is indeed no need
    if (a.size() == 2) {
        vector<vector<int>> ret;
        ret.push_back({a[0], a[1]});
        ret.push_back({a[1], a[0]});
        return ret;
    }

    int ele = a.back();
    a.pop_back();
    vector<vector<int>> subs = gen_permutation(a);
    vector<vector<int>> ret;
    for (const auto& v : subs) {
        for (int i = 0; i <= v.size(); i++) {
            vector<int> new_v(v);
            auto it = new_v.begin() + i;
            new_v.insert(it, ele);
            ret.push_back(new_v);
        }
    }

    return ret;
}

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        auto ret = gen_permutation(nums);

        return ret;
    }
};
