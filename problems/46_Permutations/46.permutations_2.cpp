// The "standard" swapping backtrack swapping algor with C++

void swap_permute(vector<int>& a, int start, vector<vector<int>>& result)
{
    if (start == a.size() - 1) {
        result.push_back(a);
        return;
    }

    for (int i = start; i < a.size(); i++) {
        swap(a[start], a[i]);
        swap_permute(a, start+1, result);
        swap(a[i], a[start]);
    }

    return;
}

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> result;

        swap_permute(nums, 0, result);

        return result;
    }
};
