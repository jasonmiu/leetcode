#
# @lc app=leetcode id=46 lang=python3
#
# [46] Permutations
#
# https://leetcode.com/problems/permutations/description/
#
# algorithms
# Medium (58.23%)
# Total Accepted:    524.1K
# Total Submissions: 866.1K
# Testcase Example:  '[1,2,3]'
#
# Given a collection of distinct integers, return all possible permutations.
# 
# Example:
# 
# 
# Input: [1,2,3]
# Output:
# [
# ⁠ [1,2,3],
# ⁠ [1,3,2],
# ⁠ [2,1,3],
# ⁠ [2,3,1],
# ⁠ [3,1,2],
# ⁠ [3,2,1]
# ]
# 
# 
#
# The first idea is to do a DFS-like search.
# for example [1 2 3], we select possible element each time, as our current path:
#                 [ ]
#         /        |       \
#       1          2        3
#     /  \       /   \     /  \
#    2    3     1    3    1    2
#   /     \    /      \   /     \
#  3      2   3       1   2      1
#
# When we used up the available elements, the path will be 1 case of permutation.
# But generate a new set of available elements each recursion the memory usage is like:
# n + (n-1) + (n-2) + .. + 1
# Which is not very good. 
# Using backtrack, for each elements, we see it will be swapped with all other elements once,
# including itself.
# For swapped 1 element, we want to do the same for remaining element, so it is recusive.
# [ 1  2  3 ]
#   ^-s,  i = 0, [ 1 2 3 ]
#                    ^-s, i = 1, [ 1 2 3 ]
#                                      ^-s, return
#                    ^-s, i = 2, [ 1 3 2 ]  
#                                      ^-s, return
#  ^-s,  i = 1, [ 2 1 3 ]
#                   ^-s, i = 1, [ 2 1 3 ]
#                                     ^-s, return
# ... ...
# For each recursion we swap the remaining elements, when we return,
# we swap it back so we can reuse the array.
# Complexity is O(n!), Space for BT is O(n) with DFS will be (n^2).


def p(cur_path, nums, ans):
    n = len(nums)
    if n == 0:
        ans.append(cur_path)
        return
    
    for i in range(0, n):
        c = nums[i]
        s = nums[0:i] + nums[i+1::]
        p(cur_path + [c], s, ans)
        
    return

def pbt(nums, start_idx, ans):
    n = len(nums)
    if start_idx == n-1:
        ans.append(list(nums))
        return
    
    for i in range(start_idx, n):
        t = nums[i]
        nums[i] = nums[start_idx]
        nums[start_idx] = t
        
        pbt(nums, start_idx+1, ans)
        
        t = nums[i]
        nums[i] = nums[start_idx]
        nums[start_idx] = t
        
    return
        

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
#        r = self.permuteDFS(nums)
        r = self.permuteBT(nums)
        return r

    
    def permuteBT(self, nums: List[int]) -> List[List[int]]:
        ans = []
        pbt(nums, 0, ans)
        return ans

    def permuteDFS(self, nums: List[int]) -> List[List[int]]:
        ans = []
        p([], nums, ans)
        
        return ans
