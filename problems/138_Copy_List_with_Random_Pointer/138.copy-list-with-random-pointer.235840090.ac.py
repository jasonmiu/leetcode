# TO EXPLAIN#
# @lc app=leetcode id=138 lang=python3
#
# [138] Copy List with Random Pointer
#
# https://leetcode.com/problems/copy-list-with-random-pointer/description/
#
# algorithms
# Medium (29.88%)
# Total Accepted:    297.2K
# Total Submissions: 994.6K
# Testcase Example:  '{"$id":"1","next":{"$id":"2","next":null,"random":{"$ref":"2"},"val":2},"random":{"$ref":"2"},"val":1}'
#
# A linked list is given such that each node contains an additional random
# pointer which could point to any node in the list or null.
# 
# Return a deep copy of the list.
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input:
# 
# {"$id":"1","next":{"$id":"2","next":null,"random":{"$ref":"2"},"val":2},"random":{"$ref":"2"},"val":1}
# 
# Explanation:
# Node 1's value is 1, both of its next and random pointer points to Node 2.
# Node 2's value is 2, its next pointer points to null and its random pointer
# points to itself.
# 
# 
# 
# 
# Note:
# 
# 
# You must return the copy of the given head as a reference to the cloned
# list.
# 
# 
#
"""
# Definition for a Node.
class Node:
    def __init__(self, val, next, random):
        self.val = val
        self.next = next
        self.random = random
"""
class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        
        new_head = None
        cloned_nodes = {}
        
        n = head
        while(not n == None):
            
            if n not in cloned_nodes:
                node = Node(n.val, n.next, n.random)
                cloned_nodes[n] = node
            else:
                node = cloned_nodes[n]
            
            if not n.next == None:
                if n.next not in cloned_nodes:
                    nn = Node(n.next.val, n.next.next, n.next.random)
                    cloned_nodes[n.next] = nn
                else:
                    nn = cloned_nodes[n.next]
            else:
                nn = None
            
            if not n.random == None:
                if n.random not in cloned_nodes:
                    nr = Node(n.random.val, n.random.next, n.random.random)
                    cloned_nodes[n.random] = nr
                else:
                    nr = cloned_nodes[n.random]
            else:
                nr = None
                    
            node.next = nn
            node.random = nr
            
            if not new_head:
                new_head = node
            
            n = n.next
            
        return new_head
