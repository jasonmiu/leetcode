// The key point is to sort the intervals, with the *earilest ending time* first.
// For example: [1, 5], [2, 4], [2, 3], [4, 5] => [2,3], [2,4], [1,5], [4,5]
// It is because, if we select the interval that has the earilest ending time,
// the time after that ending time has the max capacity to hold other intervals,
// ie. lesser removal.
//
// | 1 2 3 4 5 |
//     ^ ^
//     ^   ^
//   ^       ^
//         ^ ^
//
// The intervals sorted in the example above. If we select the [2,3] first, the
// remaining time capacity has still 4, 5, which is better than other selections.
// If there is an interval [1,3], it doesn't matter sort it first or [2,3] first,
// since it must be an overlap and get a removal count increase.
//
// So we keep find the next earilest ending time interval, which is a greedy algorithm.
// If the current interval overlapped the previous interval, increase the count.
// Since we sorted with ending time, the current interval ending time cannot be eariler than
// the previous ending time. We keep the ending time since it is still the earilest.
// If they are not overlapped, means we have to keep this current interval, hence the ending
// point moved to current interval ending time. Scan thru all intervals.
//
// Finding the max non-overlapping (available) intervals is a classic example of
// Interval Scheduling Algorithm.
//
// Time: O(nlogn) for the sort, space: O(1);


class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {

        if (intervals.size() == 1) {
            return 0;
        }

        sort(intervals.begin(), intervals.end(), [](const vector<int>& a, const vector<int>& b) {
            if (a[1] < b[1]) {
                return true;
            } else {
                return false;
            }
        });

        int e = intervals[0][1];
        int cnt = 0;
        for (int i = 1; i < intervals.size(); i++) {
            if (e > intervals[i][0]) {
                cnt ++;
            } else {
                e = intervals[i][1];
            }
        }

        return cnt;
    }
};
