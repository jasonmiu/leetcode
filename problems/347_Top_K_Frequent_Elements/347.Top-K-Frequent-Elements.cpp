// The idea is to have a counter map, which the keys are the input elements in the
// array, and the values are the frequency (count).
// Then convert them in an array, which use the frequency as the sort key.
// Sort it in decending order.
// time O(mlogm), while the m is the number of the unique  elements.
// space O(n), for the counter map also the sort vector.

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {

        unordered_map<int,int> counts;

        for (int x : nums) {
            counts[x] ++;
        }

        vector<pair<int,int>> count_list;
        for (auto p : counts) {
            count_list.push_back({p.second, p.first});
        }

        sort(count_list.begin(), count_list.end(), std::greater<pair<int,int>>());

        vector<int> ans;
        for (int i = 0; i < k; i++) {
            ans.push_back(count_list[i].second);
        }

        return ans;
    }
};
