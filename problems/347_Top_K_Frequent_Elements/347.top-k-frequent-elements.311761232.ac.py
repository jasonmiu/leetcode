#
# @lc app=leetcode id=347 lang=python3
#
# [347] Top K Frequent Elements
#
# https://leetcode.com/problems/top-k-frequent-elements/description/
#
# algorithms
# Medium (57.24%)
# Total Accepted:    322.4K
# Total Submissions: 545.8K
# Testcase Example:  '[1,1,1,2,2,3]\n2'
#
# Given a non-empty array of integers, return the k most frequent elements.
# 
# Example 1:
# 
# 
# Input: nums = [1,1,1,2,2,3], k = 2
# Output: [1,2]
# 
# 
# 
# Example 2:
# 
# 
# Input: nums = [1], k = 1
# Output: [1]
# 
# 
# Note: 
# 
# 
# You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
# Your algorithm's time complexity must be better than O(n log n), where n is
# the array's size.
# 
# 
#
# The idea is to do something like count sort.
# For example, [1, 1, 1, 2, 2, 3]
# We build a frequency dict first, the value is the freq, like:
# {1: 3, 2: 2, 3: 1}
# We build a counter array, the index is the freq, so we can directly
# put the freq of an element to this counter array. Since index start from
# 0, we -1 to the freq of an element.
#
#  0  1  2
# [3, 2, 1]
# [1, 2, 3] <-- reverse
#
# The the K most freq can be found from the index.
# However, there is a case that multiple elements have a same freq, like:
# [1, 2]
# {1: 1, 2: 1}
# With K=2, While the question does not tell clearly how to handle the tie.
# But the system wants all element in sequence, so answer will be: 
# K=2, [1, 2]
# So instead of the 1D counter array, we should chain up the elements with same freq,
#
#  0 [ [1, 2] ]
# We first access each index (freq) of the counter array, if has a chain in that index,
# we access all elements in that chain as well. For each accessible element, we 
# increase the rank until the rank hits K.
# Complexity build freq dict is O(n), build counter_arr is O(n), accessing the
# counter arr is O(n) so the complexity is O(n).
# The space complexity is also O(n) for the freq dict and counter array.

import collections

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        freq_d = collections.Counter(nums) 
        m = max(freq_d.values())
        counter_arr = [None] * m
        
        for e in freq_d:
            f = freq_d[e]
                
            if counter_arr[f-1] == None:
                counter_arr[f-1] = [e]
            else:
                counter_arr[f-1].append(e)
    
        del freq_d
        
        counter_arr.reverse() 
        ans = []
        i = 0
        rank = 0
        while i < m and rank < k:
            if counter_arr[i] != None:
                for j in range(0, len(counter_arr[i])):
                    ans.append(counter_arr[i][j])
                    rank = rank + 1
                    if rank >= k:
                        break
                        
            i = i + 1

        return ans
