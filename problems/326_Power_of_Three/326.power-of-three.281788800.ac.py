#
# @lc app=leetcode id=326 lang=python3
#
# [326] Power of Three
#
# https://leetcode.com/problems/power-of-three/description/
#
# algorithms
# Easy (41.88%)
# Total Accepted:    214.6K
# Total Submissions: 512.3K
# Testcase Example:  '27'
#
# Given an integer, write a function to determine if it is a power of three.
# 
# Example 1:
# 
# 
# Input: 27
# Output: true
# 
# 
# Example 2:
# 
# 
# Input: 0
# Output: false
# 
# Example 3:
# 
# 
# Input: 9
# Output: true
# 
# Example 4:
# 
# 
# Input: 45
# Output: false
# 
# Follow up:
# Could you do it without using any loop / recursion?
#
# To do this without loop, the idea is using the math log.
# since:
# log_b(n) = y
# base is the base which is 3. The n is the input value.
# If y is power of 3, y is integer.
# The tricky part is how to check y is an integer correctly.
# Since y is a float, it can has representation error, like
# log(243, 3) is 5, but the log() gives 4.9999....
# Use the floating point comparsion to check if y is an integer.
# The complexity depends on the log(), which can be O(logn).
# The space complexity is O(1).

import math

class Solution:
    def isPowerOfThree(self, n: int) -> bool:
        
        # mathematically impossible as no power of any number can be 0 or -ve
        if n <= 0:
            return False
        
        y = math.log(n, 3)
        epsilon = 1e-10
        
        # float % 1 will get the decimal part, like 3.14 % 1 = 0.14
        # Here is checking if y is integer. If y is a float should be treated as an 
        # integer, adding epsilon has 2 cases:
        # 
        #    ^   5.000000000012..                                                                           -  <- added epsilon
        #    e                                                                                              +
        #    V  5.00000000001... <- float should be treated as integer, but larger than interger boundary   +- smaller than 2 epsilons
        #                       <- } lesser than epsilon                                                    +  - <-- added epsilon
        #      5 -------- integer boundary                                                                  -  +  
        #                    <- } } lesser than epsilon                                                        + -- smaller than 2 epsilons
        #     4.9999999999...  <- <- float should be treated as integer, but smaller than interger boundary    -
        #
        # so if the decimal part is smaller than 2 * epsilon, diff b/w y and the integer boundary is
        # smaller than epsilon and should treat them as the same value, ie, y is integer.
        if (y + epsilon) % 1 <= 2 * epsilon:
            return True
        else:
            return False

