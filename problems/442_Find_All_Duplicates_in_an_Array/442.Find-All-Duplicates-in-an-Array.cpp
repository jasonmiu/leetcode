// Few important info in the question:
// 1. The element value is related to the size of the input array, which is 1 - n,
// 2. A duplicated value only appear twice.
//
// A count hash can do this O(n), with space O(n). To full fill the space O(1),
// have to make use of the info above.
//
// The element value only from 1 - n means we can use the input array index, to
// trace the element. Think like a bucket sort. For element i, put it to the index i.
// If all element only appear once, means for array size n, it has values 1, 2, ... n.
// Then the bucket sort will place each element match with its index, ie. A[i] = i.
// If a element is duplicated, a hash collision will happen, when we try to place
// it to the its index that index has already occuplied. Since a value can only appear
// twice, it only collide once. So when we see such collision, save the repeated element.
//
// Problem is, how to do such bucket sort, without extra space? have a extra array and insert
// them is easy which is the same as the count hash. To do with no extra space, we can "push"
// the replaced element to the next slot. For example:
//
// 0 1 2 3 4 (index)
//-----------
// 0 4 2 3 1
//   ^- 4 is going to place in index 4
//
// 0 1 2 3 4 (index)
//-----------
// 0 0 2 3 4
//         ^- 1, get pushed out, and have to find it place
//
// 0 1 2 3 4 (index)
//-----------
// 0 1 2 3 4
//   ^- 1, find it place, and the original value is 0, means it was the starting point of this placement, can stop here.
//
// Once issue is when a element can pushed out, it will go back to the starting point of a placement process,
// from the example above, element 4 moved from idx 1 to idx 4, and then element 1 moved from idx 4 to idx 1, since the
// original "4" was still here, it get pushed again and loop forever. To break this, we need to empty the original slot,
// which is index 1 here. Mark it as 0. 0 is special since all values are started from 1.
//
// To make the indexing easier, expend the input array to size n + 1, so the last index is n. Fill the index 0 with 0,
// and if a element is 0 (a value did not appear in the input array, so a empty slot after bucket sort collision), it will
// be placed to index 0 and return.
//
// Start the placement from all elements, since the placement process will be stopped if a duplication is detected.
// All elements will only be placed once, so time O(n), and space O(1)



void place_loop(vector<int>& a, int idx, int ele, vector<int>& result)
{
    while (true) {
        if (idx != ele) {
            int dest = ele;
            int t = a[dest];
            a[dest] = ele;

            if (t == 0) {
                return;
            }

            if (t == ele) {
                result.push_back(ele);
                return;
            }

            idx = dest;
            ele = t;
        } else {
            a[idx] = ele;
            return;
        }
    }

    return;
}

void place(vector<int>& a, int idx, int ele, vector<int>& result)
{
    if (idx != ele) {
        int dest = ele;
        int t = a[dest];
        a[dest] = ele;

        if (t == 0) {
            return;
        }

        if (t == ele) {
            result.push_back(ele);
            return;
        }

        place(a, dest, t, result);
    } else {
        a[idx] = ele;
    }

    return;
}

class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {

        int n = nums.size();
        nums.push_back(0);
        for (int i = n-1; i >= 0; i--) {
            nums[i+1] = nums[i];
        }
        nums[0] = 0;

        vector<int> result;

        for (int i = 1; i < nums.size(); i++) {
            int e = nums[i];
            nums[i] = 0;
            place_loop(nums, i, e, result);
        }

        return result;
    }
};
