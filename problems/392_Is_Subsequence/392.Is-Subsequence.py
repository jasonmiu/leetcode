# For each char in s, find this char in t. If not found,
# then s must not be subseq.
# If found, we need to next matching possible match char in t
# is later than the current found index. So start the next 
# search from last_hit_idx + 1.
# If all char in s can find a match, the it is a subseq.
# Complexity: O(MN) where is M is len of s and N is len of t.
# Space: O(1).

class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        
        last_hit_idx = -1
        for i in range(0, len(s)):
            hit_idx = None
            for j in range(last_hit_idx+1, len(t)):
                if s[i] == t[j]:
                    hit_idx = j
                    break
                    
            if hit_idx == None:
                return False
            else:
                last_hit_idx = hit_idx
            
            
        return True
                
