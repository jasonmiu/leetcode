#
# @lc app=leetcode id=1010 lang=python3
#
# [1010] Pairs of Songs With Total Durations Divisible by 60
#
# https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/description/
#
# algorithms
# Easy (46.51%)
# Likes:    290
# Dislikes: 26
# Total Accepted:    20.4K
# Total Submissions: 43.8K
# Testcase Example:  '[30,20,150,100,40]'
#
# In a list of songs, the i-th song has a duration of time[i] seconds. 
# 
# Return the number of pairs of songs for which their total duration in seconds
# is divisible by 60.  Formally, we want the number of indices i < j with
# (time[i] + time[j]) % 60 == 0.
# 
# 
# 
# Example 1:
# 
# 
# Input: [30,20,150,100,40]
# Output: 3
# Explanation: Three pairs have a total duration divisible by 60:
# (time[0] = 30, time[2] = 150): total duration 180
# (time[1] = 20, time[3] = 100): total duration 120
# (time[1] = 20, time[4] = 40): total duration 60
# 
# 
# 
# Example 2:
# 
# 
# Input: [60,60,60]
# Output: 3
# Explanation: All three pairs have a total duration of 120, which is divisible
# by 60.
# 
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= time.length <= 60000
# 1 <= time[i] <= 500
# 
#

# @lc code=start
# The time[i] + time[j] = m * 60. The max value of m * 60
# will be max(time) + max(time) = M. 
# m * 60 - time[i]  = time[j], for m = 1 to M
# We place time[j] in a counter dict, and scan the time list,
# if we saw m*60-time[i] before means this value + time[i] must be
# multiples of 60.
# To use the counter dict (multiset) because for the case with 
# duplicated time values, like:
# [150, 150, 30]
# For the 30 (time[2]), it should saw another value twice and create 2 more pairs.
# So the pair counter should add with the seen counter.
# The complexity is O(n)
# The space complexity is O(n) for counter dict
#
# Another cool idea from discussion is:
#
# class Solution:
#     def numPairsDivisibleBy60(self, time):
#         c = collections.Counter()
#         res = 0
#         for t in time:
#             res += c[60 - (t % 60)]
#             if t % 60 == 0:
#                 c[60] += 1
#             else:
#                 c[t % 60] += 1
#         return res
#
# while (t + x) % 60 = 0
# Idea is take x % 60 = 60 - t % 60, which is valid for the most cases.
# But if t % 60 = 0, x % 60 = 0 instead of 60.
# So we handle this case to set c[60] instead of 0.
# The hash values is 1-60. This removed the need of computing the
# multiplyers with the inner loop.


import collections

class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:

        M = ((max(time) * 2) // 60) + 1

        if len(time) == 0 or len(time) == 1:
            return 0
        
        seens = collections.Counter()
        seens[time[0]] = 1
        pair_cnt = 0
        
        for i in range(1, len(time)):
            for m in range(1, M+1):
                r = (60 * m) - time[i]
                if r in seens:
                    pair_cnt += seens[r]

            seens[time[i]] += 1

        return pair_cnt
        
        

# @lc code=end
