# use bitwise to test if we have only 1 bit set.
# Need check n == 0 since 0 & any value will be
# 0 hence need to check.

class Solution:
    def isPowerOfTwo(self, n: int) -> bool:
        
        if n == 0:
            return False
        
        if ((n - 1) & n) == 0:
            return True
        else:
            return False

