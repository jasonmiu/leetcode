#
# @lc app=leetcode id=235 lang=python3
#
# [235] Lowest Common Ancestor of a Binary Search Tree
#
# https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/
#
# algorithms
# Easy (46.77%)
# Likes:    1362
# Dislikes: 90
# Total Accepted:    329.3K
# Total Submissions: 702.2K
# Testcase Example:  '[6,2,8,0,4,7,9,null,null,3,5]\n2\n8'
#
# Given a binary search tree (BST), find the lowest common ancestor (LCA) of
# two given nodes in the BST.
# 
# According to the definition of LCA on Wikipedia: “The lowest common ancestor
# is defined between two nodes p and q as the lowest node in T that has both p
# and q as descendants (where we allow a node to be a descendant of itself).”
# 
# Given binary search tree:  root = [6,2,8,0,4,7,9,null,null,3,5]
# 
# 
# 
# Example 1:
# 
# 
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
# Output: 6
# Explanation: The LCA of nodes 2 and 8 is 6.
# 
# 
# Example 2:
# 
# 
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
# Output: 2
# Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant
# of itself according to the LCA definition.
# 
# 
# 
# 
# Note:
# 
# 
# All of the nodes' values will be unique.
# p and q are different and both values will exist in the BST.
# 
# 
#

# @lc code=start
# The idea is to find the paths to node p and q.
# Then from the paths, find their common nodes, as the command ancestor must appear in both paths.
# To search the lowest common ancestor, means find the deepest common node in both paths.
# We search from the deepest path, as it must pass thru the command ancestor when we search back up.
# Use hash table to store the shorter path for comparasion. Search the longer path from bottom to upper,
# if the current node appear in the shorter path hash table, we found the lowest ancestor.
# Complexity is O(logn), which is the height of the tree.
# Space complexity is O(logn), for storing the paths, which are the height of the tree too.
#
# Better solution from discussion, with space complexity O(1)
# The idea is based on the common ancestor of p and q is when the tree node split on both sides.
#def lowestCommonAncestor(self, root, p, q):
#    while root:
#        if root.val > p.val and root.val > q.val:
#            root = root.left
#        elif root.val < p.val and root.val < q.val:
#            root = root.right
#        else:
#            return root


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def tree_path(root, x, path):
    path.append(root)
    if root.val == x:
        return path
    
    if x > root.val:
        return tree_path(root.right, x, path)
    elif x < root.val:
        return tree_path(root.left, x, path)

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        
        path_q = None
        path_q = tree_path(root, q.val, [])
        
        path_p = None
        path_p = tree_path(root, p.val, [])
        
        a = b = None
        if len(path_p) >= len(path_q):
            a = path_p
            b = path_q
        else:
            a = path_q
            b = path_p
            
        set_b = set([n.val for n in b])
                
        for i in range(len(a)-1, -1, -1):
            if a[i].val in set_b:
                return a[i]
            
        return None
                
        
       
# @lc code=end
