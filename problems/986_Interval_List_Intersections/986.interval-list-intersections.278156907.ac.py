#
# @lc app=leetcode id=986 lang=python3
#
# [986] Interval List Intersections
#
# https://leetcode.com/problems/interval-list-intersections/description/
#
# algorithms
# Medium (64.30%)
# Likes:    552
# Dislikes: 24
# Total Accepted:    40.8K
# Total Submissions: 63.2K
# Testcase Example:  '[[0,2],[5,10],[13,23],[24,25]]\n[[1,5],[8,12],[15,24],[25,26]]'
#
# Given two lists of closed intervals, each list of intervals is pairwise
# disjoint and in sorted order.
# 
# Return the intersection of these two interval lists.
# 
# (Formally, a closed interval [a, b] (with a <= b) denotes the set of real
# numbers x with a <= x <= b.  The intersection of two closed intervals is a
# set of real numbers that is either empty, or can be represented as a closed
# interval.  For example, the intersection of [1, 3] and [2, 4] is [2,
# 3].)
# 
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: A = [[0,2],[5,10],[13,23],[24,25]], B = [[1,5],[8,12],[15,24],[25,26]]
# Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]
# Reminder: The inputs and the desired output are lists of Interval objects,
# and not arrays or lists.
# 
# 
# 
# 
# Note:
# 
# 
# 0 <= A.length < 1000
# 0 <= B.length < 1000
# 0 <= A[i].start, A[i].end, B[i].start, B[i].end < 10^9
# 
# 
# NOTE: input types have been changed on April 15, 2019. Please reset to
# default code definition to get new method signature.
# 
# 
#

# @lc code=start
# To think this problem, we start with the simplest case, with 1 interval on each list:
#    a_i        a_j
# A [0 . . . . 5]
# B  [1 . . . . . 7]
#    b_i          b_j
# Case 1: a_j >= b_j, interval is b_i to a_j
#
# A [0 . . . . 5]
# B   [1 . . 4]
# Case 2: a_j < b_j, interval is b_i to b_j
#
# A [0 . . . . 5]
# B              . . [8 . . 11]
# Case 3: a_j < b_i, no interval
#
# We need to make sure the list A is that one closer to the left, ie, we want a_i <= b_i.
# It does not matter which list is A or B, so we let A is that one always closer to left.
#
# For each turn, we pop the first element of A and B, call them 'a' and 'b', then compare them as the cases above.
# In case 1, 'a' will not has any further intersection, so it can be removed. 'b' will be updated
# with the "chopped" section. eg:
# A [0 . . . . 5]        => []
# B  [1 . . . . . 7]     => [5 . 7]
# The updated section will be placed back to the list for next comparison.
#
# In case 2, 'a' covers 'b' totally. The intersection is the whole b, so 'b' can be removed but 'a' will be 
# placed back to the list, since this 'a' can still cover the later intervals. For eg:
# A [0 . . . . . . . . . . . . 13]
# B   [1 . 3]   [6 . . 9]  [11 . . . 15]
# The long 'a' interval will be placed to compare again
#
# In case 3, a and b have no overlap. Since a is on the left of b, a has no more intersection with other intervals,
# so it can be removed.
#
# Do this recursivly until A or B is empty, since no more intersection can be made. We return the accumulated intersections.
#
# The complexity is O(n) since we compare the elements in the lists one by one.
# The time complexity is O(1), since we only use the input lists space.


def find_inter(A, B, intersections):
    
    if len(A) == 0 or len(B) == 0:
        return intersections
    
    if A[0][0] > B[0][0]:
        A, B = B, A
        
    a = A.pop(0)
    b = B.pop(0)
    
    if a[1] < b[0]:
        B.insert(0, b)
    elif a[1] <= b[1]:
        intersections.append([b[0], a[1]])
        b[0] = a[1]
        B.insert(0, b)
    else:
        intersections.append([b[0], b[1]])
        A.insert(0, a)
        
    return find_inter(A, B, intersections)
        

class Solution:
    def intervalIntersection(self, A: List[List[int]], B: List[List[int]]) -> List[List[int]]:
        inter = find_inter(A, B, [])
        
        return inter
# @lc code=end
