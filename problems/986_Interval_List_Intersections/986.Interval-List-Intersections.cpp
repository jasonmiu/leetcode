// The algor is bascially same as the python version.
// The main point is, when comparing two intervals, there are
// few cases:

//
// Two same intervals:
// [ . . . . ]
// [ . . . . ]
//
// First tail overlap second head
// [ . . . . ]
//       [ . . . .]
//
// First head overlap second tail
//       [ . . . . ]
// [ . . . . ]
//
// First fully overlap second
// [ . . . . . . . . . ]
//       [ . . . . ]
//
// Second fully overlap first
//      [ . . . .]
// [ . . . . . . . . . ]
//
// First end first and no overlap
// [ . . . . ]
//               [ . . . . ]
//
// Second end first and no overlap
//               [ . . . . ]
// [ . . . . ]
//
// The idea is to remove the overlapped interval away from it list.
// In this implementation it is to find the overlapped interval which
// the end time finished eariler, which means that interval get processed
// fully and we can move the index forward of that list.
//
// Repeatly checking all overlapped intervals until one list is end.
//
// Time: O(n). Space: O(1).

vector<int> check_overlap(int s1, int s2, int e1, int e2, int& movelist)
{
    int inter_s = 0;
    int inter_e = 0;

    if (s1 <= s2 && e1 >= s2 && e1 <= e2) {
        inter_s = s2;
        inter_e = e1;
        movelist = 1;
        return {inter_s, inter_e};
    }

    if (s1 >= s2 && s1 <= e2 && e1 >= e2) {
        inter_s = s1;
        inter_e = e2;
        movelist = 2;
        return {inter_s, inter_e};
    }

    if (s1 <= s2 && s1 <= e2 && e1 >= s2 && e1 >= e2) {
        inter_s = s2;
        inter_e = e2;
        movelist = 2;
        return {inter_s, inter_e};
    }

    if (s1 >= s2 && s1 <= e2 && e1 >= s2 && e1 <= e2) {
        inter_s = s1;
        inter_e = e1;
        movelist = 1;
        return {inter_s, inter_e};
    }

    if (e1 < s2) {
        movelist = 1;
        return {-1, -1};
    }

    if (s1 > e2) {
        movelist = 2;
        return {-1, -1};
    }

    return {-1, -1};
}


void over(vector<vector<int>>& l1, vector<vector<int>>& l2, int l1_idx, int l2_idx,
          vector<vector<int>>& overlaps)
{
    if (l1_idx >= l1.size() || l2_idx >= l2.size()) {
        return;
    }

    int s1 = 0, s2 = 0, e1 = 0, e2 = 0;

    int head_list = 1;

    s1 = l1[l1_idx][0];
    e1 = l1[l1_idx][1];
    s2 = l2[l2_idx][0];
    e2 = l2[l2_idx][1];

    vector<int> overlap = check_overlap(s1, s2, e1, e2, head_list);
    if (overlap != vector<int>{-1, -1}) {
        overlaps.push_back(overlap);
    }

    if (head_list == 1) {
        over(l1, l2, l1_idx + 1, l2_idx, overlaps);
    } else if (head_list == 2) {
        over(l1, l2, l1_idx, l2_idx + 1, overlaps);
    }

    return;
}

class Solution {
public:
    vector<vector<int>> intervalIntersection(vector<vector<int>>& firstList, vector<vector<int>>& secondList) {

        vector<vector<int>> overlaps;

        over(firstList, secondList, 0, 0, overlaps);

        return overlaps;
    }
};
