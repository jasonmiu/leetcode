// Scan the linked list using for next loop.
// Find the length of the list first.
// Mark the index of the mid-point.
// If the length is even number, for example, 6,
// divide it by 2 will give the 2nd middle node if
// index start from 0.
// If length is odd, then mid-point index is middle
// by dividing 2.
// Scan the list again until we hit the mid-point index.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        int len = 0;
        for (ListNode* n = head; n != nullptr; n = n->next) {
            len ++;
        }

        int m = 0;
        m = len / 2;

        int i = 0;
        for (ListNode* n = head; n != nullptr; n = n->next) {
            if (i == m) {
                return n;
            }
            i++;
        }

        return nullptr;
    }
};
