#
# @lc app=leetcode id=876 lang=python3
#
# [876] Middle of the Linked List
#
# https://leetcode.com/problems/middle-of-the-linked-list/description/
#
# algorithms
# Easy (65.28%)
# Likes:    677
# Dislikes: 46
# Total Accepted:    91.1K
# Total Submissions: 139.5K
# Testcase Example:  '[1,2,3,4,5]'
#
# Given a non-empty, singly linked list with head node head, return a middle
# node of linked list.
# 
# If there are two middle nodes, return the second middle node.
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: [1,2,3,4,5]
# Output: Node 3 from this list (Serialization: [3,4,5])
# The returned node has value 3.  (The judge's serialization of this node is
# [3,4,5]).
# Note that we returned a ListNode object ans, such that:
# ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next
# = NULL.
# 
# 
# 
# Example 2:
# 
# 
# Input: [1,2,3,4,5,6]
# Output: Node 4 from this list (Serialization: [4,5,6])
# Since the list has two middle nodes with values 3 and 4, we return the second
# one.
# 
# 
# 
# 
# Note:
# 
# 
# The number of nodes in the given list will be between 1 and 100.
# 
# 
# 
# 
#

# @lc code=start
# This is a simple list traverse. First find the lenght of the list,
# and find the mid point.
# The point is remember the mid point is an INDEX, and start with 0 
# it is 1 lesser than length. And when compute the length,
# If we have a head node the length is start with 1 as we have 1 element
# already. The length // 2 will give the mid index of the list if the lenght
# is odd. Will give the 2nd mid index if the length is even.
# length = 5 , mid = index 2, [0, 1, 2, 3, 4]
#                                    ^- mid
# length = 6, mid = index 3, [0, 1, 2, 3, 4, 5]
#                                      ^- mid
# Complexity is O(n)
# Space complexity is O(1)

# A cool algor from suggested solution
#Approach 2: Fast and Slow Pointer
#
#Intuition and Algorithm
#
#When traversing the list with a pointer slow, make another pointer fast that traverses twice as fast. When fast reaches the end of the list, slow must be in the middle.
#class Solution(object):
#    def middleNode(self, head):
#        slow = fast = head
#        while fast and fast.next:
#            slow = slow.next
#            fast = fast.next.next
#        return slow

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        
        n = head
        length = 1
        while n.next:
            length += 1
            n = n.next
        
        mid = length // 2 
        
        n = head
        i = 0
        while i < mid:
            i += 1
            n = n.next
                       
        return n
        
# @lc code=end
