// For example:
// [10,5,2,6], k = 100
// Subarrays are:
// [10], [10, 5], [10, 5, 2], [10, 5, 2, 6]
//           [5],     [5, 2],     [5, 2, 6]
//                       [2],        [2, 6]
//                                      [6]
//
// The brute force way is to generate all subarrays,
// and scan evey one for product but this is O(n^3), and
// the biggest issue is, the product can be very big
// and overflow (since it can be max_value^length).
//
// If think in sliding window way, which scan the input
// with different window size, and for each window,
// multipy the new element and divide the old element.
// This reduce to O(n^2) but still not solveing the big
// product overflow issue.
//
// So to prevent overflow, we must not multipy all values
// in a subarray, but just stop the multipication when the
// product is larger than k. This makes the "process new value,
// remove old value" sliding window technique useless.
//
// If start from the smallest 1 size window from left,
// the product is itself (nums[0]), then expend the window size to right,
// For example:
// [10,5,2,6]
//
// (10), product = 10
// (10, 5), product = 50
// with (10, 5) subarray, if its product is < K, all sub-subarrays of this subarray
// has the products < K, since by removing any elements in it, means removing any item
// in a multipication, for all +ve values, their product must also smaller than K.
// This is the most important observation.
// From the table above, the number of subarrays is related to the array size.
// If the array size is n, number of subarray also has product in range is n (as in columns).
// The previous elements are already counted since we start from left.
//
// Keep expending the window, until the product is no longer smaller than K:
//
// (10, 5, 2), product = 100
//
// So try to reduce the product, by shrinking the windows from left:
// (5, 2), product = 10, until it is smaller than K again.
// Also count all subarrays using the window size, like the columns of the table above.
//
// By this, it like we skipped counting those product >= K subarrys:
//
// [10], [10, 5], [--------], [-----------]
//           [5],     [5, 2],     [5, 2, 6]
//                       [2],        [2, 6]
//                                      [6]
//
// Since only have to scan the input once, it is O(n), space is O(1).


class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        int i = 0;
        int j = 0;

        int prod = 1;
        int cnt = 0;

        while (j < nums.size()) {

            prod = prod * nums[j];
            if (prod < k) {
                cnt = cnt + (j - i + 1);
            } else {
                while (i <= j && prod >=k) {
                    prod = prod / nums[i];
                    i++;
                }
                cnt = cnt + (j - i + 1);
            }

            j++;
        }

        return cnt;
    }
};
