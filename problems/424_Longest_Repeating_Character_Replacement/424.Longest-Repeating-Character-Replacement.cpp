// The question is asking, for a string, how many characters in that has to be
// changed to make it a longest string with same character?
// So if string AAAX, the best way is to preserve the most occurrence character,
// since if keep the string with the same size, keep the most occurrence character
// but change other characters will give the smallest chars to change.
// Then the real question is to find, what is the longest length of substring,
// that has single type of charactor, with the change lesser than input k?
// So we like to use up the change chance k, and find the current most occurrence
// character in the current testing substring. For the current substring,
// the max len will be the "most occurrence char's count + k". Therefore we can use
// a sliding window, start from 0, keep extending the size to left. For each extension,
// find the max occurrence in the current window. Since string contains only uppercase
// letters, it will be A-Z search space is constance 26. After getting the most occurrence
// char's count in this window 'major count', we check if the window size - that 'major count'
// is still less than k. If less than k, means we can still extend the window. Otherwise, we
// have to shrink the window until the window size - 'major count' is smaller or equal to k.
// Repeat this until the window hit the end of the string input. The main idea is to keep the
// window contains only the number of chars has to be changed smaller than k. The max size of
// those windows is the answer we want. For example:
//
// k = 1
// "AABABBA"
//  ^
//  |- start
//  |- end
//
// The current windows has substring "A". Put it to the current count dictionary:
//
// "AABABBA"    count: {A:1}
//  ^
//  |- start
//  |- end
//
// In the count dict, check which char has the max occurrence. Now it is "A", with
// occurrance 1. Window size is 1, the number of chars needed to be changed is
// Window size - max occurrance = 1 - 1 = 0, which is <= k (1). So extend the window:
//
// "AABABBA"    count: {A:2}
//  ^^
//  se
//
// Now the max occurrance is 2, and the number of chars needed to be changed is
// window size - max occurance = 2 - 2 which is still <= k. Keep extend the window:
//
// "AABABBA"    count: {A:2, B:1}
//  ^ ^
//  s e
//
// The max occurrance is still 2 which the major char is A. But the number of char needed
// to be changed become 1 (window size = 3, max occurance = 2). It is still <= k. Extend
// the window:
//
// "AABABBA"    count: {A:3, B:1}
//  ^  ^
//  s  e
//
// The max occurance is 3, the number of char needed to be changed is , <= k.
//
// "AABABBA"    count: {A:3, B:2}
//  ^   ^
//  s   e
//
// While the major char in this window is still A, and its max occurrance is 3, but the window
// size is now 5, and number of char needed to be changed is 2 > k now. So we save the the current
// window size - 1 (4, as dun need the latest char now), and shrink the window from right. Also
// decrease the count of the current right most char too.
//
// "AABABBA"    count: {A:2, B:2}
//   ^  ^
//   s  e
//
// The major char in this window is A (or B, since their max occurrances are the same), and
// the max occurrance is now 2. The window size is 4, so the char need to be changed is 2, thats > k.
// So shrink window again and save the window size:
//
// "AABABBA"    count: {A:1, B:2}
//    ^ ^
//    s e
//
// Now the major char i this window is B, and max occurrance is 2. Window size became 3, so the char needed to
// be changed is 1, <= k, so extend:
//
// "AABABBA"    count: {A:1, B:3}
//    ^  ^
//    s  e
//
// Max occurance is 3, window size is 4, char to be changed is 1 <= k, extend:
//
// "AABABBA"    count: {A:2, B:3}
//    ^   ^
//    s   e
//
// Max occurance is 3, window size is 5, char to be changed is 2 > k, shrink:
//
// "AABABBA"    count: {A:2, B:2}
//     ^  ^
//     s  e
//
// Max occurance is 2, window size is 4, char to be changed is 2 > k, shrink:
//
// "AABABBA"    count: {A:1, B:2}
//      ^ ^
//      s e
//
// Max occurance is 2, window size is 3, char to be changed is 1, extend:
//
// "AABABBA"    count: {A:1, B:2}
//      ^  ^
//      s  e
//
// Window now hit the end of the input. This is the last window, and since we keep all sliding windows containing
// substrings that need change lesser than k, check this last window size if the longest.
// Finally, we return the longest window size we encountered. Since the max occurance finding in a constance search
// space, it is O(1) even it has to search thru the whole count dict. And has to do this n times for each char in the
// input string. So the time is O(n), and the space is O(1) for the count dict.
//
// Extra optimization: Need not to find the max occurance by going thru the count dict everytime, since we do not need
// to actually shrink the window but just keep the longest one. Just save the over all global max occurance in a window
// so far. Every time the window extend, increase the count of the new char. If it is higer than the global max
// occurrance in history, means the current window can contain a longer substring. If not, just slide the whole window
// with the *current longest size* to the left. Decrease the count of the right side that fallen out from the window.
// Hence we need not the correct max occurance for each window, but just keep the longest window, do not have to
// loop thru the count dict which even faster.



int diff_chars(const unordered_map<char,int>& counts, char& major_char)
{
    int total_cnt = 0;
    int max_cnt = INT_MIN;

    for (auto itr = counts.begin(); itr != counts.end(); itr++) {
        char c = itr->first;
        int cnt = itr->second;

        total_cnt += cnt;

        if (cnt >= max_cnt) {
            max_cnt = cnt;
            major_char = c;
        }
    }

    int mc_cnt = counts.find(major_char)->second;
    return total_cnt - mc_cnt;
}

class Solution {
public:
    int characterReplacement(string s, int k) {

        unordered_map<char,int> counts;
        int start = 0;
        int end = 0;
        int max_len = INT_MIN;

        char c = s[end];
        counts[c] ++;
        while (end < s.size()) {

            char major_char;
            int can_change = diff_chars(counts, major_char);

            if (can_change > k) {

                int win_size = end - start;
                max_len = max(max_len, win_size);

                char left_char = s[start];
                start ++;
                counts[left_char] --;
            } else {
                end ++;
                if (end < s.size()) {
                    c = s[end];
                    counts[c] ++;
                }
            }
        }

        max_len = max(max_len, (end-start));

        return max_len;
    }
};
