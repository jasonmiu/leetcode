// Same algor as the previous one, but not finding the max occurance for each window.
// Instead, keep the longest one and sliding thru the input. A window can contains invalid
// substring, for example:
//  0123456
// "AABABBA"    k = 1
//   ^  ^
//   s  e
//   |__|- window size 4, and having ABAB which needs two change but k = 1.
//  It works because, the window is currently 4 is because the max occurance in the history
// so far is 3 (3 As in 0, 1, 3 that proccessed). Added the current "B" at 4 did not increase
// the max occurance. So it moves forward:
//  0123456
// "AABABBA"    k = 1
//   ^   ^
//   s   e
//
// Now window is 5, but the max occurance is still 3, so it has to slide the window:
//  0123456
// "AABABBA"    k = 1
//    ^  ^
//    s  e
// And keep the window size as 4 this round. So if a new char does not increase the max overall
// occurrance, the window will just slide as the max size. If a new char does increase the max
// overall occurrance, then the window can expend.


class Solution {
public:
    int characterReplacement(string s, int k) {

        int start = 0;
        int end = 0;
        int max_occ = INT_MIN;
        int max_len = INT_MIN;

        unordered_map<char,int> counts;

        while (end < s.size()) {

            char end_c = s[end];
            counts[end_c] ++;

            max_occ = max(counts[end_c], max_occ);

            // window size is end index - start index + 1, because
            // both are indices, for example, end = start = 0,
            // which means the substring is size 1 pointing at the
            // beginning of input string. So have to + 1 for the
            // substring size.
            int win_size = end - start + 1;

            if (win_size - max_occ > k) {

                // Not shrink the window. But just slide the window
                // to keep the biggest size we know so far.
                char start_c = s[start];
                counts[start_c] --;
                start ++;
                win_size = end - start + 1;
            }
            max_len = max(max_len, win_size);

            end++;
        }

        return max_len;
    }
};
