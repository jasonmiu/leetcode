#
# @lc app=leetcode id=1221 lang=python3
#
# [1221] Split a String in Balanced Strings
#
# https://leetcode.com/problems/split-a-string-in-balanced-strings/description/
#
# algorithms
# Easy (78.42%)
# Total Accepted:    27.1K
# Total Submissions: 34.1K
# Testcase Example:  '"RLRRLLRLRL"'
#
# Balanced strings are those who have equal quantity of 'L' and 'R'
# characters.
# 
# Given a balanced string s split it in the maximum amount of balanced
# strings.
# 
# Return the maximum amount of splitted balanced strings.
# 
# 
# Example 1:
# 
# 
# Input: s = "RLRRLLRLRL"
# Output: 4
# Explanation: s can be split into "RL", "RRLL", "RL", "RL", each substring
# contains same number of 'L' and 'R'.
# 
# 
# Example 2:
# 
# 
# Input: s = "RLLLLRRRLR"
# Output: 3
# Explanation: s can be split into "RL", "LLLRRR", "LR", each substring
# contains same number of 'L' and 'R'.
# 
# 
# Example 3:
# 
# 
# Input: s = "LLLLRRRR"
# Output: 1
# Explanation: s can be split into "LLLLRRRR".
# 
# 
# Example 4:
# 
# 
# Input: s = "RLRRRLLRLL"
# Output: 2
# Explanation: s can be split into "RL", "RRRLLRLL", since each substring
# contains an equal number of 'L' and 'R'
# 
# 
# 
# Constraints:
# 
# 
# 1 <= s.length <= 1000
# s[i] = 'L' or 'R'
# 
# 
#
# The idea is to use a stack to keep track the number of
# 'R' and 'L'. If the current char in str s is not equal
# to the top of stack, pop the stack. If the stack is empty,
# we have a balance substr, can split here.
# Since we want to split as much as possible, split
# greedly once we see a balance substr.
# Complexity is O(n), for scanning the str s.
# Space complexity is O(n), for the stack

class Solution:
    def balancedStringSplit(self, s: str) -> int:
        stack = []
        cnt = 0
        stack.append(s[0])
        
        for i in range(1, len(s)):
            if len(stack) == 0 or s[i] == stack[-1]:
                stack.append(s[i])
            else:
                stack.pop()
                if len(stack) == 0:
                    cnt += 1
                    
        return cnt
