#
# @lc app=leetcode id=1186 lang=python3
#
# [1186] Maximum Subarray Sum with One Deletion
#
# https://leetcode.com/problems/maximum-subarray-sum-with-one-deletion/description/
#
# algorithms
# Medium (33.80%)
# Total Accepted:    9.5K
# Total Submissions: 27K
# Testcase Example:  '[1,-2,0,3]'
#
# Given an array of integers, return the maximum sum for a non-empty subarray
# (contiguous elements) with at most one element deletion. In other words, you
# want to choose a subarray and optionally delete one element from it so that
# there is still at least one element left and the sum of the remaining
# elements is maximum possible.
# 
# Note that the subarray needs to be non-empty after deleting one element.
# 
# 
# Example 1:
# 
# 
# Input: arr = [1,-2,0,3]
# Output: 4
# Explanation: Because we can choose [1, -2, 0, 3] and drop -2, thus the
# subarray [1, 0, 3] becomes the maximum value.
# 
# Example 2:
# 
# 
# Input: arr = [1,-2,-2,3]
# Output: 3
# Explanation: We just choose [3] and it's the maximum sum.
# 
# 
# Example 3:
# 
# 
# Input: arr = [-1,-1,-1,-1]
# Output: -1
# Explanation: The final subarray needs to be non-empty. You can't choose [-1]
# and delete -1 from it, then get an empty subarray to make the sum equals to
# 0.
# 
# 
# 
# Constraints:
# 
# 
# 1 <= arr.length <= 10^5
# -10^4 <= arr[i] <= 10^4
# 
#
# This is similar to the question [53] Maximum Subarray, but more hard to think about,
# the definition of "can has deletion in subarray".
# The idea is, starting with the max subarray, without deletion, we can create an array submax1
# to save the current max value of subarrays up to index i, by using DP.
# With possible deletion, we can have another subarray that can have 1 deletion during counting subarry values,
# call it submax0. When we find the max subarray value of submax0, 3 cases can happen:
# 1. current subarray with delete max value + current arr[i]
# 2. excluding the current arr[i], which is the continous subarray max value 
# 3. starting subarray from arr[i]
# We pick the max amount 3.
# Then we need also update the continous subarray max value, only two cases,
# a. current continous subarray + current arr[i]
# b. starting subarray from arr[i]
# finally we find the max value in the subarray with deletion.
# The complexity is O(n), space is also O(n) but can down to O(1) if not using submax arrays but just var to store
# the previous value.


class Solution:
    def maximumSum(self, arr: List[int]) -> int:
        
        submax0 = [0] * len(arr)
        submax0[0] = arr[0]
        submax1 = [0] * len(arr)
        submax1[0] = arr[0]
        
        ans = arr[0]
        
        for i in range(1, len(arr)):
            submax0[i] = max(submax0[i-1] + arr[i], # subarry max with deletion up to now including arr[i]
                             submax1[i-1], # submax1 is the max sum if we didn't exclude arr[j] in middle, just keep extend in the subarry or start a new one
                             arr[i] # starting with arr[i]                        
                           )
            submax1[i] = max(submax1[i-1] + arr[i], arr[i])
            ans = max(ans, submax0[i])
            
        return ans
