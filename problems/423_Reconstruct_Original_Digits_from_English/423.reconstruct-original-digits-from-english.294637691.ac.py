#
# @lc app=leetcode id=423 lang=python3
#
# [423] Reconstruct Original Digits from English
#
# https://leetcode.com/problems/reconstruct-original-digits-from-english/description/
#
# algorithms
# Medium (46.35%)
# Total Accepted:    21.8K
# Total Submissions: 47K
# Testcase Example:  '"owoztneoer"'
#
# Given a non-empty string containing an out-of-order English representation of
# digits 0-9, output the digits in ascending order.
# 
# Note:
# 
# Input contains only lowercase English letters.
# Input is guaranteed to be valid and can be transformed to its original
# digits. That means invalid inputs such as "abc" or "zerone" are not
# permitted.
# Input length is less than 50,000.
# 
# 
# 
# Example 1:
# 
# Input: "owoztneoer"
# 
# Output: "012"
# 
# 
# 
# Example 2:
# 
# Input: "fviefuro"
# 
# Output: "45"
# 
# 
#
### Want to rewrite with my own code

'''
For each digits, I print the number it has... And I found:
First, only 0 has 'z', only 6 has 'x', only 2 has 'w', only 4 has 'u'.
Then, I removed 0, 2, 4, 6, and for the left: only 7 has 's', only 1 has 'o', only 8 has 'g', only 5 has 'f'
And so on...
So I wrote the solution
'''

class Solution:
    def originalDigits(self, s: str) -> str:
        # z -> 0
        # x -> 6
        # w -> 2
        # u -> 4
        
        
        # s -> 7
        # o -> 1
        # g -> 8
        # f -> 5
        
        # h -> 3
        # i -> 9
        pattern = [[0 for _ in range(26)] for _ in range(10)]
        digits = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
        for i in range(10):
            for d in digits[i]:
                pattern[i][ord(d) - ord('a')] += 1
        num = [0 for _ in range(10)]
        total = [0 for _ in range(26)]
        for c in s:
            total[ord(c) - ord('a')] += 1
        #for i in range(10):
        #    if i not in (0, 6, 2, 4):
        #        print(i, pattern[i])
        #return
            
        def identify(i, char):
            num[i] = total[ord(char)-ord('a')]
            for idx, p in enumerate(pattern[i]):
                total[idx] -= p * num[i]
        
        identify(0, 'z')
        identify(6, 'x')
        identify(2, 'w')
        identify(4, 'u')
        
        identify(7, 's')
        identify(1, 'o')
        identify(8, 'g')
        identify(5, 'f')
        identify(3, 'h')
        identify(9, 'i')
        ret = ""
        for i in range(10):
            ret += str(i) * num[i]
        return ret
            

