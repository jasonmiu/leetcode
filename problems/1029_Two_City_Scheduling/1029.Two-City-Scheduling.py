# The idea is assuming sending all people to city A.
# Then try send a person, i, to city B, and see how much we can save
# for sending person i to B. Find the largest saving person i first.
# Then find the next person j who can save the next largest by going
# to city B. Keep finding such person until we can n/2 persons going
# to city B.
# We can find the all cost differeces for each person, and sort
# from largest difference (most -ve).
# I used a wrong approach to generate all possible A/B city
# combinations. We do not need all combinations as we can find a
# max saving (min cost) by finding the cost diff directly.
# Complexity O(nlogn) for sorting.
# Space O(n) for saving the cost diff.

class Solution:
    def twoCitySchedCost(self, costs: List[List[int]]) -> int:
        n = len(costs)
        
        city_a_costs = []
        for i in range(0, n):
            city_a_costs.append(costs[i][0])
            
        cost_diff = []
        for i in range(0, n):
            cost_diff.append(((costs[i][1] - city_a_costs[i]), i))
            
        sorted_cost_diff = sorted(cost_diff, key=lambda d: d[0])
        
        for i in range(0, n//2):
            (d, idx) = sorted_cost_diff[i]
            city_a_costs[idx] += d
            
        return sum(city_a_costs)
            
        
