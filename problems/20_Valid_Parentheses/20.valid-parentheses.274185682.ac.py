#
# @lc app=leetcode id=20 lang=python3
#
# [20] Valid Parentheses
#
# https://leetcode.com/problems/valid-parentheses/description/
#
# algorithms
# Easy (37.46%)
# Total Accepted:    737.2K
# Total Submissions: 2M
# Testcase Example:  '"()"'
#
# Given a string containing just the characters '(', ')', '{', '}', '[' and
# ']', determine if the input string is valid.
# 
# An input string is valid if:
# 
# 
# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.
# 
# 
# Note that an empty string is also considered valid.
# 
# Example 1:
# 
# 
# Input: "()"
# Output: true
# 
# 
# Example 2:
# 
# 
# Input: "()[]{}"
# Output: true
# 
# 
# Example 3:
# 
# 
# Input: "(]"
# Output: false
# 
# 
# Example 4:
# 
# 
# Input: "([)]"
# Output: false
# 
# 
# Example 5:
# 
# 
# Input: "{[]}"
# Output: true
# 
# 
#
# Using stack to match the parentheses. For a open parenthese, 
# push it to stack. For a close parenthese, pop the stack.
# The popped out element should be the open parenthese of the 
# current char. If they are not match, the parentheses are not
# balance hence False. After scanning whole input sting and 
# stack is empty, means parentheses are balance, hence return True.
# To match the diff types of parenthese pairs, use a dict to map 
# a parenthese pair to the same value, so we can compare the mapped 
# values for a parenthese pair.
# The complexity is O(n) for scanning whole string.
# Space complexity is O(n) for storing the string to the stack.

class Solution:
    def isValid(self, s: str) -> bool:
        
        stack = []
        pdict = {"(": 1, 
                 "{": 2,
                 "[": 3,
                 ")": 1,
                 "}": 2,
                 "]": 3}
        
        for p in s:
            if p == '(' or p == '{' or p == '[':
                stack.append(p)
            elif p == ')' or p == '}' or p == ']':
                if len(stack) == 0:
                    return False
                else:
                    q = stack.pop()
                    if not pdict[p] == pdict[q]:                    
                        return False
                    
        if len(stack) == 0:
            return True
        
        return False
