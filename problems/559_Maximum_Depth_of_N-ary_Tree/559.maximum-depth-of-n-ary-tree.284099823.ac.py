#
# @lc app=leetcode id=559 lang=python3
#
# [559] Maximum Depth of N-ary Tree
#
# https://leetcode.com/problems/maximum-depth-of-n-ary-tree/description/
#
# algorithms
# Easy (66.21%)
# Total Accepted:    76.9K
# Total Submissions: 115.4K
# Testcase Example:  '[1,null,3,2,4,null,5,6]\r'
#
# Given a n-ary tree, find its maximum depth.
# 
# The maximum depth is the number of nodes along the longest path from the root
# node down to the farthest leaf node.
# 
# Nary-Tree input serialization is represented in their level order traversal,
# each group of children is separated by the null value (See examples).
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: root = [1,null,3,2,4,null,5,6]
# Output: 3
# 
# 
# Example 2:
# 
# 
# 
# 
# Input: root =
# [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: 5
# 
# 
# 
# Constraints:
# 
# 
# The depth of the n-ary tree is less than or equal to 1000.
# The total number of nodes is between [0, 10^4].
# 
# 
#
# Find the longest path using Depth First Search. 
# For a node, the longest path of its subtrees must be
# its longest path too, as the depth of the current node 
# and its subtree is 1 + subtree_depth. If subtree_depth D
# is the longest amount the subtree depths, the longest path
# of the current node will 1 + D. Hence we can search the
# longest path of the subtrees recursivly.
# The complexity is O(log_b(n)), n is number of nodes and the b is
# the fan outs of nodes.
# The space complexity is O(log_b(n)) for the call stack

"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""

def dfs(root, depth):
    if root == None:
        return 0
    
    if len(root.children) == 0:
        return depth + 1
    else:
        depths = []
        for c in root.children:
            depths.append(dfs(c, depth+1))
            
        return max(depths)
            

class Solution:
    def maxDepth(self, root: 'Node') -> int:
        d = dfs(root, 0)
        return d
