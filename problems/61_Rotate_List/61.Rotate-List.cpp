// From the given examples, 1 rotation means moving the tail node to the head,
// like:
// 1 -> 2 -> 3 -> 4 -> 5, rotate 1
// 5 -> 1 -> 2 -> 3 -> 4
//
// So we need 3 pointers for 1 rotation, head, tail, and the previous node of tail.
// Since we need to do k times, how to find the previous node of tail? If scan from head
// everytime it takes k*n times. So can build a map that having each node map to its previous
// node. For number of rotations, if rotate 0, means no rotation. If rotate n times, means no rotation
// too. So the actual number of rotations is k mod n times.
// Then we move the tail to head k times, like:
//
// k = 7, number of rotations = k mod n = 2
// 1 -> 2 -> 3 -> 4 -> 5
// H              P    T
//
// 5 -> 1 -> 2 -> 3 -> 4
// H              P    T
//
// 4 -> 5 -> 1 -> 2 -> 3
// H              P    T
//
// Time is O(n) for scaning the list, and max k can only b n-1. Space is O(n) for the pointer map.
//
// Another way is to link the tail node to head as a cycle, and cut the node as the new tail at the k
// position (start from 0). This needs no hash table.


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {

        unordered_map<ListNode*, ListNode*> prev_nodes;

        int list_size = 0;
        ListNode* prev = nullptr;
        ListNode* n = head;
        ListNode* tail = nullptr;
        while (n != nullptr) {
            prev_nodes[n] = prev;
            prev = n;
            if (n->next == nullptr) {
                tail = n;
            }
            n = n->next;
            list_size ++;
        }

        if (list_size == 0) {
            return nullptr;
        }

        k = k % list_size;

        ListNode* h = head;
        ListNode* t = tail;
        ListNode* p = prev_nodes[t];
        for (int i = 0; i < k; i++) {
            t->next = h;
            p->next = nullptr;
            h = t;
            t = p;
            p = prev_nodes[t];
        }

        return h;
    }
};
