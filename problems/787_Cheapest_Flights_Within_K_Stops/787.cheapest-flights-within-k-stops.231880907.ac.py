# TO EXPLAIN
#
# @lc app=leetcode id=787 lang=python3
#
# [787] Cheapest Flights Within K Stops
#
# https://leetcode.com/problems/cheapest-flights-within-k-stops/description/
#
# algorithms
# Medium (36.50%)
# Total Accepted:    56.4K
# Total Submissions: 154.4K
# Testcase Example:  '3\n[[0,1,100],[1,2,100],[0,2,500]]\n0\n2\n1'
#
# There are n cities connected by m flights. Each fight starts from city u and
# arrives at v with a price w.
# 
# Now given all the cities and flights, together with starting city src and the
# destination dst, your task is to find the cheapest price from src to dst with
# up to k stops. If there is no such route, output -1.
# 
# 
# Example 1:
# Input: 
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 1
# Output: 200
# Explanation: 
# The graph looks like this:
# 
# 
# The cheapest price from city 0 to city 2 with at most 1 stop costs 200, as
# marked red in the picture.
# 
# 
# Example 2:
# Input: 
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 0
# Output: 500
# Explanation: 
# The graph looks like this:
# 
# 
# The cheapest price from city 0 to city 2 with at most 0 stop costs 500, as
# marked blue in the picture.
# 
# Note:
# 
# 
# The number of nodes n will be in range [1, 100], with nodes labeled from 0 to
# n - 1.
# The size of flights will be in range [0, n * (n - 1) / 2].
# The format of each flight will be (src, dst, price).
# The price of each flight will be in the range [1, 10000].
# k is in the range of [0, n - 1].
# There will not be any duplicated flights or self cycles.
# 
# 
#
# The idea is to find all paths from src to dst. Looks like a shortest path which can use dijkstra,
# but dijkstra only gives us the smallest cost (shortest in the term of cost) path, but cannot select
# other paths with number of stops (number of nodes). To count nodes, BFS is a better idea.
# For each level advancing of BFS, we count it as 1 stop. If the current number of stops is higher than
# the input k, we stop the BFS and check if we have visited the dst. If we have not visited the dst,
# no such path has been found.
# If we visited the dst, we want to know what is the min cost to arrive this dst node so far. So the 
# problem is how to track the cost of different path. For example, a graph:
#             A  ---- 500 ----> C
#             |                 ^
#             |                100
#             +-- 100 --> B ---+
# Src = A, Dst = C
# At the first stop, A -> B, and A -> C
# Since we visited the dst, the cost to dst is now 500.
# At the 2nd stop, B -> C, the *total cost* should be 200, not just B->C 100.
# So when we put the next node to the queue of BFS, we save the min cost to that node as
# (node, min cost from src). 
# When we visit the dst, we check if the path from the current popped node from queue has the
# (cost + cost from n -> dst) smaller than the total min cost. If so, we found the min total cost so far.
# Complexity: O(n) since need to visit all nodes
# Space: O(n) for the queue. Since we can have a graph like: 
# node_0 -> node_1 , node_0 -> node_2 ... node_0 -> node_n , ie, single node to all other nodes.

import queue

class Solution:
    
    cost_graph = None
    
    def build_graph(self, flights):
        g = {}
        for f in flights:
            u = f[0]
            v = f[1]
            cost = f[2]
            if not u in g:
                g[u] = [(v, cost)]
            else:
                g[u].append((v, cost))
            
        return g
        
    
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, K: int) -> int:
        
        LARGEST = 99999999999999
        min_cost = LARGEST
        
        self.cost_graph = self.build_graph(flights)
        q = queue.Queue()
        
        stops = -1
        # pass the (next node, current accumlated cost)
        q.put((src, 0))  # (node, cost)
        
        while(not q.empty()):
            # do 1 tree level
            current_level_node_nr = q.qsize()
            while(current_level_node_nr > 0):
                # we arrived at node v, with the accumlated cost so far
                (v , cost) = q.get()
#                print("v:" , v)

                # Get the path from src to dst, if the current accumulated cost
                # is smaller than the min_cost, update min_cost
                if (v == dst):
                    min_cost = min(min_cost, cost)

                if v in self.cost_graph:
                    for n in self.cost_graph[v]:
#                        print("next cost:", n[1], "current cost", cost, "min_cost", min_cost)
                        # n[1] is the cost to the next node. If cost to next node plus current accumlated is 
                        # larger than current min_cost, then no point to fly to next node since the cost will only be
                        # increased
                        if (n[1] + cost) > min_cost:
                            continue
                        
                        
                        # visit next node, n[0], and accumalate the cost with the next flight distance  
                        q.put((n[0], cost + n[1]))
            
                current_level_node_nr = current_level_node_nr - 1
                
            # one BFS tree level is 1 stop
            stops = stops + 1

            if stops > K:
                break
                
        if min_cost < LARGEST:
            return min_cost
        else:
            return -1
                
            
        
