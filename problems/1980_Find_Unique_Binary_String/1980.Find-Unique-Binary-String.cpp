// Using the fact from the question that the max n is 16.
// That means the max possible value is (2^16)-1, within n bits.
// So convert all string to to int, save them in a set, and
// count from 0 - ((2^n)-1) to see if a value is not appear in the saved set.
// If so, convert it back to binary string.
//
// Another way is to create a binary string which differs from first binary at 1st position,
// second binary at 2nd position, third binary at 3rd position,...and so on.
// This will make sure that the binary we have made is unique (as it differs from all others at atleast one position).
// This is from the Cantor's Diagonalization. This can deal the case if the n is infinity.

int bin_to_int(const string& bs)
{
    int a = 0;
    for (int i = 0; i < bs.size(); i++) {
        if (bs[i] == '1') {
            a = a | 0x01;
        }
        a = a << 1;
    }
    a = a >> 1; // The last loop will shift it 1 more time than we
                // actually need, since the loop is:
                // - condition check
                // - shift
                // - condition check to False
                // So before the last condition check to quite loop,
                // it shifted the number already. So shift it back.
    return a;
}

string int_to_bin(int a, int n)
{
    string s(n, '0');
    for (int i = s.size()-1; i >=0; i--) {
        int b = a & 0x01;
        if (b == 1) {
            s[i] = '1';
        }
        a = a >> 1;
    }

    return s;
}

class Solution {
public:
    string findDifferentBinaryString(vector<string>& nums) {

        unordered_set<int> has_nums;
        for (const string& s : nums) {
            int a = bin_to_int(s);
            has_nums.insert(a);
        }

        int n = nums.size();
        for (int i = 0; i <= pow(2, n) - 1; i++) {
            if (has_nums.find(i) == has_nums.end()) {
                string s = int_to_bin(i, n);
                return s;
            }
        }

        return "";
    }
};
