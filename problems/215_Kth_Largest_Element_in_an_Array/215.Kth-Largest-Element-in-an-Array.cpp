// This is a quick selection algor, which similar to quick sort.
// First just like quick sort, we do a partition on the input array. For example:
// [3,2,1,5,6,4] => partition
// [5,6,4,3,2,1]
//      ^
//      | pivot index, d
//
// Since question is asking for largest element, so we partition the array
// that the elements which are larger or equal to pivot to left,
// and the smaller elements to right.
//
// Then what we want to search is actually the index. The returned pivot index, d, from the
// partition, is actually the d-th largest element. Since all the indices of its left
// are used by the elements larger than it, so the index d is actually the d-th largest
// element count from 0.
//
// Then the actual quick selection algor is, firstly do a partition and see if the
// current returned pivot index d, equal to the input k. If so, it is the k-th largest element
// we want. Otherwise, if the d is bigger than k, means the k-th largest element is on the
// left of the partitioned array, so search left recusively. Same as of the d is smaller than k,
// the k-th largest element is on the right on the partitioned array, so search right recusively.
//
// Since we cut half of the partition space every time, and has to do logn time, and the average time
// of partition is O(n), so time is O(n), and space is O(1).

int rev_partition(vector<int>& a, int start, int end)
{
    int p = a[end];

    int i = start;
    int left_pivot = start;
    while (i <= end-1) {

        if (a[i] >= p) {
            swap(a[i], a[left_pivot]);

            left_pivot ++;
        }

        i++;
    }

    swap(a[i], a[left_pivot]);

    return left_pivot;
}

int k_largest(vector<int>& a, int k, int start, int end)
{

    int p_idx = rev_partition(a, start, end);
    if (p_idx == k) {
        return a[p_idx];
    }

    if (k < p_idx) {
        return k_largest(a, k, start, p_idx-1);
    } else {
        return k_largest(a, k, p_idx+1, end);
    }

    return 0;
}

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {

        int ans = k_largest(nums, k-1, 0, nums.size()-1);
        return ans;
    }
};
