#
# @lc app=leetcode id=215 lang=python3
#
# [215] Kth Largest Element in an Array
#
# https://leetcode.com/problems/kth-largest-element-in-an-array/description/
#
# algorithms
# Medium (50.53%)
# Total Accepted:    460.1K
# Total Submissions: 910.4K
# Testcase Example:  '[3,2,1,5,6,4]\n2'
#
# Find the kth largest element in an unsorted array. Note that it is the kth
# largest element in the sorted order, not the kth distinct element.
# 
# Example 1:
# 
# 
# Input: [3,2,1,5,6,4] and k = 2
# Output: 5
# 
# 
# Example 2:
# 
# 
# Input: [3,2,3,1,2,4,5,5,6] and k = 4
# Output: 4
# 
# Note: 
# You may assume k is always valid, 1 ≤ k ≤ array's length.
# 
#
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        
        # Idea is create an array, which the index is the nums values,
        # and the values of array is the counts of that nums value.
        # To find the K-th lagest nums value, we accumulate the counter values
        # in the array, until the accumulated sum >= k, that array index is the answer
        
        # Trap here. If the nums values have netgative numbers, using array index does
        # not work. So if the smallest value is a netgative number, we offset all nums
        # values with that smallest value netgative number, to ensure all values are 
        # postive so we can use array index. After we get the answser we have to 
        # "shift back" the offset
        
        min_val = min(nums)
        
        if min_val < 0:
            for i in range(0, len(nums)):
                nums[i] += -min_val

        max_val = max(nums)
        val_cnts = [0] * (max_val + 1)
        
        for i in nums:
            val_cnts[i] += 1
        
        sum = 0
        ans = -1
        for i in range(len(val_cnts)-1, -1, -1):
            sum = sum + val_cnts[i]
            if sum >= k:
                ans = i
                break
                
        if min_val < 0:
            ans -= -min_val
            
        return ans
