#
# @lc app=leetcode id=204 lang=python3
#
# [204] Count Primes
#
# https://leetcode.com/problems/count-primes/description/
#
# algorithms
# Easy (30.05%)
# Total Accepted:    279.3K
# Total Submissions: 929.5K
# Testcase Example:  '10'
#
# Count the number of prime numbers less than a non-negative number, n.
# 
# Example:
# 
# 
# Input: 10
# Output: 4
# Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
# 
# 
#
# this is the Sieve of Eratosthenes algor.
# There are two optimations
# The inner loop "while j < n", is started from i**2,
# since as all the smaller multiples of i will have already been marked at that point
# The outter loop can stopped at sqrt(n), since sqrt(n) ^ 2 == n is a multiple of sqrt(n)
# hence the range will be covered by inner loop for finding multiples of known primes.
# Trick is after converting sqrt(n) to int(), it is a round-down operation we want to +1 for range()
# which is < sqrt(n). For example sqrt(10) is 3.16 int(3.16) becames 3. We actaully want to cover '3' hence
# range(2, 3+1)

class Solution:
    def countPrimes(self, n: int) -> int:
        
        primes = [True] * (n)
        primes_cnt = 0        
        for i in range(2, int(pow(n, 0.5))+1):
            
            if primes[i]:
            
                k = 0
                j = (i ** 2) + (i * k)
                while j < n:
                    primes[j] = False
                    k = k + 1
                    j = (i ** 2) + (i * k)

                
        for i in range(2, n):
            if primes[i]:
                primes_cnt += 1
                
        return primes_cnt
                

