// same algor but use
// STL lower_bound for searching row.

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {

        int n = matrix[0].size();

        auto it = lower_bound(matrix.begin(), matrix.end(), vector<int>{target},
                              [](const vector<int>& a, const vector<int>& b) {
                                  return a[0] < b[0];
                              });
        int search_row = it - matrix.begin();
        if (search_row >= matrix.size()) {
            search_row --;
        } else if (search_row >= 1 && target < matrix[search_row][0]) {
            search_row --;
        }
        return binary_search(&(matrix[search_row][0]), &(matrix[search_row][n]), target);
    }
}

