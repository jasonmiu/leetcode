// Since the input matrix is sorted from left to right,
// and for each row, the starting element is bigger than
// the end of the last row.
// So it is sorted as a zag-zap:
//
// -------------->
// [1  2   3  4  5] |
//  +------------+  |
//  V               |
// [6  7   8  9 10] |
// [11 12 13 14 15] V
//
// Also since each row must be larger than last row, the matrix is also column sorted.
// If we know a row has a range that cover the target, then we can use binary search
// on that row. Easiest way is to do a linear search for each row, and check its range,
// like:
//
////     bool searchMatrix(vector<vector<int>>& matrix, int target) {
////         int n = matrix[0].size();
////         bool found = false;
////         for (int i = 0; i < matrix.size(); i++) {
////             if (matrix[i][0] <= target && target <= matrix[i][n-1]) {
////                 found = binary_search(&(matrix[i][0]), &(matrix[i][n]), target);
////                 return found;
////             }
////         }
////         return false;
////     }
//
// Which gives O(m + logn). But since the column is sorted too, we can also use the
// binary search to find the row. Issue is, for checking the first element of each row,
// to see if the row is covering the target, it is not likely the first element equals
// to the target directly. We like to find the biggest element that is smaller than the
// target. That can use a bisection left (lower_bound()) to find. bisection left find the
// position where the target should be inserted. So if the bisection left found the target
// it will give the row of target, otherwise, it will give the next row (as bisection left
// give [index] >= target), so the actual row we want to search will be the returned row - 1.
// If the returned row is 0, handle this exception.
//
// The time becomes O(logm + logn), Space is O(1).


int col_bisect_left(vector<vector<int>>& matrix, int target)
{
    int n = matrix[0].size();

    int i = 0;
    int j = matrix.size() - 1;
    int m = 0;

    while (i < j) {
        m = i + ((j-i) / 2);
        if (matrix[m][0] >= target) {
            j = m;
        } else {
            i = m + 1;
        }
    }

    return i;
}

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {

        int n = matrix[0].size();

        int search_row = col_bisect_left(matrix, target);

        if (search_row >= matrix.size()) {
            return false;
        } else {
            if (search_row >= 1 && target < matrix[search_row][0]) {
                search_row --;
            }
            return binary_search(&(matrix[search_row][0]), &(matrix[search_row][n]), target);
        }
    }
};
