# The idea is to have two pointers, one starts from left to right (i),
# one starts from right to left (j). pointer i should always point to the first
# element in nums that equals to val, while the pointer j should always point to the
# first element in nums that is NON val from right. For example:
# [ 3 2 2 3 ], val = 3
#   i     j  (0, 3)
#   i   j    (0, 2)
# then contents of two pointers should be swapped
# [ 2 2 3 3 ]
#   i   j
# then repeat this process until the i > j, which means all elements have been processed.
# As its like buring candle from both sizes when they meet the candle is all burnt.
# since we only scan once, the time is O(n), and all works in place, so space O(1).

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:

        i = 0;
        j = len(nums) - 1

        while i <= j:
            # i will point to first val from left
            while i < len(nums) and nums[i] != val:
                i = i + 1

            # j will point to first non val from right
            while nums[j] == val and j >= 0:
                j = j - 1

            if i > j:
                break

            t = nums[j]
            nums[j] = nums[i]
            nums[i] = t

        return i


