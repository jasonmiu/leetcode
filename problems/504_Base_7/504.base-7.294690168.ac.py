#
# @lc app=leetcode id=504 lang=python3
#
# [504] Base 7
#
# https://leetcode.com/problems/base-7/description/
#
# algorithms
# Easy (45.33%)
# Total Accepted:    48.8K
# Total Submissions: 107.2K
# Testcase Example:  '100'
#
# Given an integer, return its base 7 string representation.
# 
# Example 1:
# 
# Input: 100
# Output: "202"
# 
# 
# 
# Example 2:
# 
# Input: -7
# Output: "-10"
# 
# 
# 
# Note:
# The input will be in range of [-1e7, 1e7].
# 
#
# Basic base convertion. Need to be careful to handle special cases:
# num < 0
# num == 0
# Always check the input if vaild for the first conditional statment,
# like, if(), while().
class Solution:
    def convertToBase7(self, num: int) -> str:
        
        a = []
        netgative = False
        if num < 0:
            netgative = True
            num = - num
            
        if num == 0:
            return "0"
        
        while num > 0:
            r = num % 7
            num = num // 7
            a.append(str(r))
            
        if netgative:
            a.append('-')
        a.reverse()
        return "".join(a)
