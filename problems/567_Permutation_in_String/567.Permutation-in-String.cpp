// The most simplest way is to find all substrings in s2,
// and check if a substring is a permutation of s1.
// To check if a string is a permutation of another,
// just check if all chars are the same between two strings.
// This a case for hash table. Can generate the substrings of s2,
// by sliding window with size as s1, check the set of chars in
// this window if they are the same as s1, we found a match.
// A pitfall is multiple same chars, like:
// s1: hellooo
// s2: XXXlelohXXX
// while the window of leloh is NOT the permutation of s1, if we just use set,
// s1 set is helo, while the substring leloh set also "helo", and hence a failure.
// So should use multiset, which can contains multiple same keys.
// But generate a new set of each window leads a overtime, since we re-hash a lot of
// same chars again, like:
//
// 0123456789A
// XXXlelohXXX
// |  ----| <- first window
//  | ---- | <_ 2nd window
//   |----  | <- 3rd window
// "----" is the re-hashed part, and they are not needed.
// Can hash only the new char entering the window (right hand side), and
// delete the char moved out from the window (left hand side).
// but for C++ multiset.erase(key) function it will erase *all* keys
// so if like:
// s1: abcb
// s2: acccbbbaca
//         ^|  | <-- new window
//         |
//         char moved out from window
// while the new window should contain "bbac" which is a match,
// but since the "b" on the most left hand side left the window,
// if using multiset.erase(key), the set of window will become:
// "ac", which is a failure. Right way is to use multiset.find(key),
// get the iterator for *only 1* key, and use multiset.erase(iterator)
// to remove only 1 key from the set.
//
// Time: O(m*n), m is the size of s1, n is the size of s2, since we
// scan the whole s2 (n), for each time in this n, compare the set with size m.
// Space: O(m+n), for two hash tables.


#include <unordered_set>
#include <algorithm>
#include <iterator>

using namespace std;

class Solution {
public:
    bool checkInclusion(string s1, string s2) {
        int i = 0;
        int j = s1.size() - 1;

        unordered_multiset<char> s1_set(s1.begin(), s1.end());

        bool first_char = true;
        unordered_multiset<char> s2_set;
        char last_char = 0;
        while (j < s2.size()) {
            if (first_char) {
                auto it = s2_set.begin();
                copy(&(s2[i]), &(s2[j+1]), inserter(s2_set, it));
                first_char = false;
            } else {
                auto it = s2_set.find(last_char);
                s2_set.erase(it);
                s2_set.insert(s2[j]);
            }

            if (s2_set == s1_set) {
                return true;
            }
            last_char = s2[i];
            i++;
            j++;
        }

        return false;
    }
};
