#
# @lc app=leetcode id=43 lang=python3
#
# [43] Multiply Strings
#
# https://leetcode.com/problems/multiply-strings/description/
#
# algorithms
# Medium (31.88%)
# Total Accepted:    234.9K
# Total Submissions: 736.6K
# Testcase Example:  '"2"\n"3"'
#
# Given two non-negative integers num1 and num2 represented as strings, return
# the product of num1 and num2, also represented as a string.
# 
# Example 1:
# 
# 
# Input: num1 = "2", num2 = "3"
# Output: "6"
# 
# Example 2:
# 
# 
# Input: num1 = "123", num2 = "456"
# Output: "56088"
# 
# 
# Note:
# 
# 
# The length of both num1 and num2 is < 110.
# Both num1 and num2 contain only digits 0-9.
# Both num1 and num2 do not contain any leading zero, except the number 0
# itself.
# You must not use any built-in BigInteger library or convert the inputs to
# integer directly.
# 
# 
#
# The idea is to simulate the multiplation with
# handwriting, but we do not have to do the 
# sumation for the each multiplated results with
# string digits adding. Instead, we save each
# multiplated results in a list. The index for 
# putting those digits are i+j, for the i in the
# current index of num1 and j is the index
# of num2. For example:

# Index         10 
#          -------
#              34  <-- i
#            * 56  <-- j
# ----------------
#              24  # i = 0, j = 0, num1[0] = 4, num2[0] = 6, d[0] += 24
#             20   # i = 0, j = 1, num1[0] = 4, num2[1] = 5, d[1] += 20
#             18   # i = 1, j = 0, num1[1] = 3, num2[0] = 6, d[1] += 18
#            15    # i = 1, j = 1, num1[1] = 3, num2[1] = 5, d[2] += 15
# The for each index in d, accumate the results of that digits.
# d[0] = 24, d[1] = 20 + 18 = 38, d[2] = 15
# [24, 38, 15]
# Then for each digits we propagate the carries to next digit
# [4 -> 2, 38, 15] -> [4, 40, 15] -> [4, 0 -> 4, 15] -> [4, 0, 19] -> [4, 0, 9 -> 1] -> [4, 0, 9, 1]
# Note that the least significant digit, index 0, is on the list left most but the result string right most,
# So we need to reverse it to [1, 9, 0, 4]
# We then skip all leading zeros and return the remaining part as a string.
# This is a O(n^2) algor.

class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        
        # The length of the result of 2 numbers, must be
        # smaller than the length of num1 + length of num2
        mul_digits = [0] * (len(num1) + len(num2))
        
        # Tricky index converting. Since the indices of a string
        # and a list are in reverse way. We convert all strings
        # to list, with the least significant digit with index 0.
        # The remaining code will use list for computations, so we 
        # can keep the index format consistent.
        num1 = list(reversed([int(x) for x in num1]))
        num2 = list(reversed([int(x) for x in num2]))
        
        for i in range(len(num1)-1, -1, -1):
            for j in range(len(num2)-1, -1, -1):
                mul_digits[i+j] += num1[i] * num2[j]
                
        #print(mul_digits)
        for i in range(0, len(mul_digits)-1):
            mul_digits[i+1] += mul_digits[i] // 10
            mul_digits[i] = mul_digits[i] % 10 
            
        #print(mul_digits)
            
        
        mul_digits.reverse()
        #print(mul_digits)

        for i in range(0, len(mul_digits)):
            if mul_digits[i] != 0:
                return "".join([str(x) for x in mul_digits[i::]])
        
        # Special case if the loop above didn't return anything,
        # the mul_digits[] must has all 0s
        
        return "0"

