# Do a binary search
# Since the last version 'n' must be a bad version,  the first bad version is bewteen
# 1 to n, while 'n' can be the first bad version.
# assume 1 is good version, test the middle version 'm' = (1 + n) // 2 if bad or not.
# if it is already bad, the first bad version has to be 1 to m, while m can be the first bad version.
# including the "m" as the lower bound of the search window is important.
# if m is a good version, then search the right, from m to n.
# The start version s, and end version e, has a invariant then the s has to be < e.
# When we finished all search, s is pointing to the first bad version.
# It is because s is m + 1, when m is a good version. So (good version + 1) is a bad version, this is a first bad version.
# And we know if e <= s, all bad versions (right hand side) got searched, since versions smaller than s are all good version,
# versions >= s are all bad version, s must be the first bad version now.

# The isBadVersion API is already defined for you.
# @param version, an integer
# @return an integer
# def isBadVersion(version):

class Solution:
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """

        s = 1
        e = n
        while s < e:
            m = (s + e) // 2
            if not isBadVersion(m):
                s = m + 1
            else:
                e = m

        return s
