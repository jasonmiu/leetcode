#
# @lc app=leetcode id=1311 lang=python3
#
# [1311] Get Watched Videos by Your Friends
#
# https://leetcode.com/problems/get-watched-videos-by-your-friends/description/
#
# algorithms
# Medium (41.57%)
# Total Accepted:    5.5K
# Total Submissions: 13.1K
# Testcase Example:  '[["A","B"],["C"],["B","C"],["D"]]\n[[1,2],[0,3],[0,3],[1,2]]\n0\n1'
#
# There are n people, each person has a unique id between 0 and n-1. Given the
# arrays watchedVideos and friends, where watchedVideos[i] and friends[i]
# contain the list of watched videos and the list of friends respectively for
# the person with id = i.
# 
# Level 1 of videos are all watched videos by your friends, level 2 of videos
# are all watched videos by the friends of your friends and so on. In general,
# the level k of videos are all watched videos by people with the shortest path
# equal to k with you. Given your id and the level of videos, return the list
# of videos ordered by their frequencies (increasing). For videos with the same
# frequency order them alphabetically from least to greatest. 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: watchedVideos = [["A","B"],["C"],["B","C"],["D"]], friends =
# [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 1
# Output: ["B","C"] 
# Explanation: 
# You have id = 0 (green color in the figure) and your friends are (yellow
# color in the figure):
# Person with id = 1 -> watchedVideos = ["C"] 
# Person with id = 2 -> watchedVideos = ["B","C"] 
# The frequencies of watchedVideos by your friends are: 
# B -> 1 
# C -> 2
# 
# 
# Example 2:
# 
# 
# 
# 
# Input: watchedVideos = [["A","B"],["C"],["B","C"],["D"]], friends =
# [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 2
# Output: ["D"]
# Explanation: 
# You have id = 0 (green color in the figure) and the only friend of your
# friends is the person with id = 3 (yellow color in the figure).
# 
# 
# 
# Constraints:
# 
# 
# n == watchedVideos.length == friends.length
# 2 <= n <= 100
# 1 <= watchedVideos[i].length <= 100
# 1 <= watchedVideos[i][j].length <= 8
# 0 <= friends[i].length < n
# 0 <= friends[i][j] < n
# 0 <= id < n
# 1 <= level < n
# if friends[i] contains j, then friends[j] contains i
# 
#
# The question is asking to find a list of videos at the level k, of a friend list graph.
# The friend list is a graph, not a tree, which can loop back to previous node.
# So we need to two things:
# 1. find the friends at level k
# 2. count the videos from friends at level k and sort
# To find the friends at level k, we can use BFS.
# One issue is the friends can loop back like:
# Friends list: [[1,2,4],[0,3],[0,4],[1], [0, 2]]
#
# 0 ------ 1 -- 3
# |
# +----+---+
# |        |
# |        2
# |        |
# +----+-- 4
#
# If id = 0, k = 1, the friends at level 1 is {1, 2, 4}, 
# If id = 0, k = 2, the friends at level 2 is {3}, not including 4
# So when we add the node to the queue of BFS, we check if we added it before
# when we add it, instead of check the 'visited' during the beginning of the
# loop like dealing with tree.
# 
# After getting the friends at level, we use a hash counter to count the freq of
# videos, then sort with the freq.
# Complexity for bfs is O(n) in case of a node is fully connected to other nodes,
# then we need to visit them n times. The counting hence is O(n) for scanning n friends
# then the sort takes O(nlogn), so the overall complexity is O(nlogn). The
# space complexity is O(n) for the friends at level list.


def bfs(root_idx, fds, k):
    q = []
    q.append(root_idx)    
    q.append(None) # Use None as the indicator of end of a level
    lv = 0
    queued = set()
    queued.add(root_idx)
    
    while len(q) > 0 and lv < k:
        n = q.pop(0)
        
        if n == None:
            lv += 1
            continue

        for c in fds[n]:
            if c not in queued:
                q.append(c)
                queued.add(c)
            
        q.append(None)
        
        
    f = set()
    for n in q:
        if (n not in f) and n != None:
            f.add(n)
            
    return f
    

import collections

class Solution:
    def watchedVideosByFriends(self, watchedVideos: List[List[str]], friends: List[List[int]], id: int, level: int) -> List[str]:
        
        lv_frds = bfs(id, friends, level)
        
#        print(lv_frds)
        
        cnt = collections.Counter()
        for i in lv_frds:
            for v in watchedVideos[i]:
                cnt[v] += 1
                
        freq = list(zip(cnt.values(), cnt.keys()))
        
        freq.sort()
        return [ele[1] for ele in freq]
