#
# @lc app=leetcode id=518 lang=python3
#
# [518] Coin Change 2
#
# https://leetcode.com/problems/coin-change-2/description/
#
# algorithms
# Medium (44.73%)
# Total Accepted:    70.1K
# Total Submissions: 153.2K
# Testcase Example:  '5\n[1,2,5]'
#
# You are given coins of different denominations and a total amount of money.
# Write a function to compute the number of combinations that make up that
# amount. You may assume that you have infinite number of each kind of
# coin.
# 
# 
# 
# 
# 
# 
# Example 1:
# 
# 
# Input: amount = 5, coins = [1, 2, 5]
# Output: 4
# Explanation: there are four ways to make up the amount:
# 5=5
# 5=2+2+1
# 5=2+1+1+1
# 5=1+1+1+1+1
# 
# 
# Example 2:
# 
# 
# Input: amount = 3, coins = [2]
# Output: 0
# Explanation: the amount of 3 cannot be made up just with coins of 2.
# 
# 
# Example 3:
# 
# 
# Input: amount = 10, coins = [10] 
# Output: 1
# 
# 
# 
# 
# Note:
# 
# You can assume that
# 
# 
# 0 <= amount <= 5000
# 1 <= coin <= 5000
# the number of coins is less than 500
# the answer is guaranteed to fit into signed 32-bit integer
# 
# 
#
# This question looks like the "322 Coin change"
# https://leetcode.com/problems/coin-change/description/
# But the approach is very different since this time we are counting the combination but not permutation
# DFS recursion thinking is hard to deal with this since the DFS count the different coins permutation 
# along the path, like:
# amount 5, using coins [1, 2]
# DFS will treat 1 1 1 2 and 2 1 1 1 two different ways to exchange the coins, but we want conbination 
# indeed so 1112 and 2111 should be counted as 1.
# But this problem still has the self similar property and we can find the sub-problem with the thinking:
# For Ci in coins, example coins = [1 2 5], C0 = 1, C1 = 2, C2 = 5, i=1,2,3,
# if W ways to make up amount j with Ci, it can be using first 0 to i-1 coins, to make j - Ci amount.
# If we have zero coins, we have no way to make any amount. If we need to make 
# 0 amount, then we always has 1 way to make by using no coin. 
# Make a table that, the each is by chosing the Ci coin, it is the number of ways to make amount j 
# For example amount = 5, coins = [1, 2, 5], The table is initialized as:
#            0 1 2 3 4 5
# []         1 0 0 0 0 0
# [1]        1
# [1 2]      1
# [1 2 5]    1
# The row 0 is when we have no coin, we make zero way for the amounts, except 0 amount which has 1 way
# (do nothing). The column 0 means the way we can make 0 amount, so they are 1.
# Each row i is asking, by looking at coin i-1, we can select it or not in to our combination.
# By selecting it, we look from current amount j - (coin i-1 value) 
# By no selecting it, it is the way of not including this coin. So the formular is:
# dp[i][i] = dp[i][j-Ci] + dp[i-1][j]
# fill the table
#            0 1 2 3 4 5
# []         1 0 0 0 0 0
# [1]        1 1 1 1 1 1
# [1 2]      1 1 2 2 3 3
# [1 2 5]    1 1 2 2 3 4
# The Complexity is O(Amount*N), while N is number of coins. The space complexity is also O(Amount*N)


class Solution:
    def change(self, amount: int, coins: List[int]) -> int:
        
        dp = []
        for i in range(0, len(coins) + 1):
            dp.append([0] * (amount + 1))
            
        for i in range(0, len(coins) + 1):
            dp[i][0] = 1
            
        for i in range(1, len(coins) + 1):
            for j in range(1, amount + 1):
                if coins[i-1] <= j:
                    dp[i][j] = dp[i][j - coins[i-1]] + dp[i-1][j]
                else:
                    dp[i][j] = dp[i-1][j]
                    
                #print("dp[{}][{}]:{}".format(i, j, dp[i][j]))
                    
        return dp[len(coins)][amount]
