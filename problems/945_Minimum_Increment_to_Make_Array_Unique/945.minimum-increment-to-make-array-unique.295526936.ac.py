#
# @lc app=leetcode id=945 lang=python3
#
# [945] Minimum Increment to Make Array Unique
#
# https://leetcode.com/problems/minimum-increment-to-make-array-unique/description/
#
# algorithms
# Medium (43.96%)
# Total Accepted:    18.1K
# Total Submissions: 40.4K
# Testcase Example:  '[1,2,2]'
#
# Given an array of integers A, a move consists of choosing any A[i], and
# incrementing it by 1.
# 
# Return the least number of moves to make every value in A unique.
# 
# 
# 
# Example 1:
# 
# 
# Input: [1,2,2]
# Output: 1
# Explanation:  After 1 move, the array could be [1, 2, 3].
# 
# 
# 
# Example 2:
# 
# 
# Input: [3,2,1,2,1,7]
# Output: 6
# Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
# It can be shown with 5 or less moves that it is impossible for the array to
# have all unique values.
# 
# 
# 
# 
# 
# Note:
# 
# 
# 0 <= A.length <= 40000
# 0 <= A[i] < 40000
# 
# 
# 
# 
# 
#
# The idea is to find the next "hole" in the array, like
# [ 3 2 1 2 1 7 ]
# For 1, the next "hole" is 4, as:
# [1 2 3 x x x 7]
# We can use a counter map, to find the repeated numbers, and do incremention until it became
# unique. But this can be slow. We want to find the next possible unique fast.
# We can sort the list, like:
# [1 1 2 2 3 7]
# If we can mantain the order, then we can find the next 'hole' when we scan to the right.
# We can make the sort propagate like carry in summation. Like:
# [1 1+1 2 2 3 7]
# [1 2 2+1 2 3 7]
# [1 2 3 3+1 3 7]
# [1 2 3 4 4+1 7]
# [1 2 3 4 5 7]
# To mantain the order, the current number A[i] need to be larger than previous number A[i-1],
# so if the previous is larger then current, the current need to be updated to A[i-1] + 1, and the move
# of this step will be new value - old value + 1.
# Complexity O(nlogn), space O(1)

class Solution:
    def minIncrementForUnique(self, A: List[int]) -> int:
        A.sort()
        move = 0
        for i in range(1, len(A)):
            pre = A[i-1]
            a = A[i]
            if (pre >= a):
                move += pre - a + 1
                A[i] = pre + 1
        return move
