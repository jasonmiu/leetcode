#
# @lc app=leetcode id=234 lang=python3
#
# [234] Palindrome Linked List
#
# https://leetcode.com/problems/palindrome-linked-list/description/
#
# algorithms
# Easy (37.28%)
# Total Accepted:    354.2K
# Total Submissions: 932.6K
# Testcase Example:  '[1,2]'
#
# Given a singly linked list, determine if it is a palindrome.
# 
# Example 1:
# 
# 
# Input: 1->2
# Output: false
# 
# Example 2:
# 
# 
# Input: 1->2->2->1
# Output: true
# 
# Follow up:
# Could you do it in O(n) time and O(1) space?
# 
#
# The main point is how to access from tail reversely.
# Add a 'prev' pointer for making it a doublely linked link.
# A better way for saving 'prev' pointers is find the minpoint,
# by using fast-slow pointer. The fast pointer need run doublely
# fast as the slow pointer, so when the fast pointer hits the end,
# the slow pointer will be at the middle.
# Then reverse the 2nd half by pointing the next pointer to the
# prev node. Finally compare the reversed 2nd half and first half.
# Complexity O(n)
# Space O(1)
"""
def isPalindrome(self, head):
    fast = slow = head
    # find the mid node
    while fast and fast.next:
        fast = fast.next.next
        slow = slow.next
    # reverse the second half
    node = None
    while slow:
        nxt = slow.next
        slow.next = node
        node = slow
        slow = nxt
    # compare the first and second half nodes
    while node: # while node and head:
        if node.val != head.val:
            return False
        node = node.next
        head = head.next
    return True
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        
        if head == None:
            return True
        
        i = head
        j = head.next
        
        if j == None:
            return True
        
        n = 1
        while j != None:
            j.prev = i
            i = i.next
            j = j.next
            n += 1
            
        hp = head
        tp = i
            
        i = 0
        j = n - 1
        while  i <= j:
            if hp.val != tp.val:
                return False
            
            i += 1
            j -= 1
            hp = hp.next
            tp = tp.prev
            
        return True
            
        
        
        
