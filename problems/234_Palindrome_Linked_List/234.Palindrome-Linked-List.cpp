// To do in the time of O(n) and no extra space using extra data structure,
// need to understand the properties of the palindrome first. For palindrome,
// reading from left and reading from right are the same, so the link is actually
// two parts, cut them in the middle and reading the first part from left and reading
// the second part from right they will be the same.
// So first find the middle point. The middle point can be found by the fast-slow pointer.
// The fast slow pointer should start from head together, which is the main implemenation point.
// Then from the middle node, reverse the second part of the list. Think the reverse operation
// of each node involve 3 pointers, the previous node, the current node, and the next node.
// Since the current node next pointer will be updated, we need to save the next node first.
// Then compare the first part of the list and the reversed second part of the list,
// and check if they are the same. Since we cut them in half, if the original list length is
// even, both sub-lists will have the same length. If the original list length is odd,
// the second list will be longer with 1 node. It is ok since this extra 1 node is the middle node,
// which is "shared" by two lists. If just need to scan still any list end, and if any element is
// different in between, the the original list is not palindrome. Otherwise, they are palindrome.
// Time: O(n), Space: O(1).

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

ListNode* find_mid(ListNode* head)
{
    ListNode* slow = head;
    ListNode* fast = head;

    while (true) {
        if (fast == nullptr || fast->next == nullptr) {
            return slow;
        } else {
            slow = slow->next;
            fast = fast->next->next;
        }
    }

    return nullptr;
}

ListNode* rev(ListNode* head)
{
    if (head == nullptr) {
        return nullptr;
    }

    ListNode* prev = nullptr;
    ListNode* cur = head;
    ListNode* next = cur->next;
    while (cur != nullptr) {
        next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }

    return prev;
}

class Solution {
public:
    bool isPalindrome(ListNode* head) {

        ListNode* mid = find_mid(head);
        ListNode* rev_from_mid = rev(mid);

        while (head != nullptr && rev_from_mid != nullptr) {

            if (head->val != rev_from_mid->val) {
                return false;
            }
            head = head->next;
            rev_from_mid = rev_from_mid->next;
        }
        return true;
    }
};
