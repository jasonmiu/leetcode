# Sort by the hight of people, tallest people in the front.
# Since no one is taller than the tallest people, it will be the
# current queue front. If a person has same hight of the current 
# tallest people, its k value must be 1 as he has 1 people (tallest)
# in front of him. So just insert to the queue with the index as his
# k value. Then process to the next tallest group.
# Complexity: O(nlogn) for sorting the input.
# Space: O(n) for extra queue.

class Solution:
    def reconstructQueue(self, people):
        people.sort(key=lambda p: (-p[0], p[1]))
        queue = []
        for p in people:
            queue.insert(p[1], p)
        return queue

