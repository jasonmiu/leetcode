#
# @lc app=leetcode id=1021 lang=python3
#
# [1021] Remove Outermost Parentheses
#
# https://leetcode.com/problems/remove-outermost-parentheses/description/
#
# algorithms
# Easy (75.47%)
# Total Accepted:    60.2K
# Total Submissions: 79.2K
# Testcase Example:  '"(()())(())"'
#
# A valid parentheses string is either empty (""), "(" + A + ")", or A + B,
# where A and B are valid parentheses strings, and + represents string
# concatenation.  For example, "", "()", "(())()", and "(()(()))" are all valid
# parentheses strings.
# 
# A valid parentheses string S is primitive if it is nonempty, and there does
# not exist a way to split it into S = A+B, with A and B nonempty valid
# parentheses strings.
# 
# Given a valid parentheses string S, consider its primitive decomposition: S =
# P_1 + P_2 + ... + P_k, where P_i are primitive valid parentheses strings.
# 
# Return S after removing the outermost parentheses of every primitive string
# in the primitive decomposition of S.
# 
# 
# 
# Example 1:
# 
# 
# Input: "(()())(())"
# Output: "()()()"
# Explanation: 
# The input string is "(()())(())", with primitive decomposition "(()())" +
# "(())".
# After removing outer parentheses of each part, this is "()()" + "()" =
# "()()()".
# 
# 
# 
# Example 2:
# 
# 
# Input: "(()())(())(()(()))"
# Output: "()()()()(())"
# Explanation: 
# The input string is "(()())(())(()(()))", with primitive decomposition
# "(()())" + "(())" + "(()(()))".
# After removing outer parentheses of each part, this is "()()" + "()" +
# "()(())" = "()()()()(())".
# 
# 
# 
# Example 3:
# 
# 
# Input: "()()"
# Output: ""
# Explanation: 
# The input string is "()()", with primitive decomposition "()" + "()".
# After removing outer parentheses of each part, this is "" + "" = "".
# 
# 
# 
# 
# 
# 
# Note:
# 
# 
# S.length <= 10000
# S[i] is "(" or ")"
# S is a valid parentheses string
# 
# 
# 
# 
# 
# 
# 
#
# The idea is use a stack like counter, cnt, to keep track the 
# open / close parentheses '(' and ')'. For '(', cnt increase 1,
# for ')', cnt decrease 1. If cnt is 0, the parenthese is totally
# balanced. But we want to remove the outter most parentheses,
# so if we see '(' and cnt is larger than 0, means we are not at outter most
# parenthese, so append the current paraenthese to output.
# similarly, if we see ')' and cnt is larger than 1, means we are not at
# outter most parenthese (somethings have already in stack), so append the
# current paraenthese to output.
# Complexity is O(n) for scanning the input once.
# Space Complexity is O(n) for the output

class Solution:
    def removeOuterParentheses(self, S: str) -> str:
        
        cnt = 0
        out = ""
        
        for p in S:
            if p == '(':
                if cnt > 0:
                    out = out + p
                cnt += 1
            elif p == ')':
                if cnt > 1:
                    out = out + p
                cnt -= 1
                    
        return out
        
