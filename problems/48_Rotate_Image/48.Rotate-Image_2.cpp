// The idea is to think rotate only the outter boundary
// arrays, start from the first row.
//
//   0 1 2 3 4
//  ----------
// 0|A B C D E
// 1|F G H I J
// 2|K L M N O
// 3|P Q R S T
// 4|U V W X Y
//
// It is easy to see, the first row, is going to be the last column:
//
//   0 1 2 3 4
//  ----------
// 0|A B C D E
// 1|
// 2|
// 3|
// 4|
//
//
//   0 1 2 3 4
//  ----------
// 0|        A
// 1|        B
// 2|        C
// 3|        D
// 4|        E
//
// If counting the first row from left to right,
// after rotate this row is counting the last column up to down.
//
//   0 1 2 3 4
//  ----------
// 0|        E
// 1|        J
// 2|        O
// 3|        T
// 4|        Y
//
// For last column, if counting from up to down, it is going to the
// last row from right to left:
//   0 1 2 3 4
//  ----------
// 0|
// 1|
// 2|
// 3|
// 4|Y T O J E
//
// For the last row, if count from left, it will go to the first column,
// from top to down:
//
//   0 1 2 3 4
//  ----------
// 0|
// 1|
// 2|
// 3|
// 4|U V W X Y
//
//   0 1 2 3 4
//  ----------
// 0|U
// 1|V
// 2|W
// 3|X
// 4|Y
//
// It is easier to think as only involve 4 lines, first row, last column, last row, first column.
// After finished the outter lines, recursively move inward. Every time, reduce the matrix size by 2,
// as removing first row, last row, first column, last column.
// If the size is 1, means no need rotate, or 0, nothing to rotate, we can stop.
// Using a loop that the counter is a offset index of the original matrix, and think that counter count
// the first row from left.




void rotate_boundary(vector<vector<int>>& a, int start, int n)
{
    if (n <= 1) {
        return;
    }

    int first_row = start;
    int first_col = start;
    int last_row = a.size() - start - 1;
    int last_col = a.size() - start - 1;

    for (int j = 0; j < n-1; j++) {

        int temp = a[last_row-j][first_col];

        a[last_row-j][first_col] = a[last_row][last_col-j];
        a[last_row][last_col-j] = a[first_row+j][last_col];
        a[first_row+j][last_col] = a[first_row][first_col+j];
        a[first_row][first_col+j] = temp;
    }

    rotate_boundary(a, start+1, n - 2);

    return;
}

class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        rotate_boundary(matrix, 0, matrix.size());
    }
};
