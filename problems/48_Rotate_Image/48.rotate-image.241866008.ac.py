# TO EXPLAIN 
#
# @lc app=leetcode id=48 lang=python3
#
# [48] Rotate Image
#
# https://leetcode.com/problems/rotate-image/description/
#
# algorithms
# Medium (51.36%)
# Total Accepted:    303.6K
# Total Submissions: 591.1K
# Testcase Example:  '[[1,2,3],[4,5,6],[7,8,9]]'
#
# You are given an n x n 2D matrix representing an image.
# 
# Rotate the image by 90 degrees (clockwise).
# 
# Note:
# 
# You have to rotate the image in-place, which means you have to modify the
# input 2D matrix directly. DO NOT allocate another 2D matrix and do the
# rotation.
# 
# Example 1:
# 
# 
# Given input matrix = 
# [
# ⁠ [1,2,3],
# ⁠ [4,5,6],
# ⁠ [7,8,9]
# ],
# 
# rotate the input matrix in-place such that it becomes:
# [
# ⁠ [7,4,1],
# ⁠ [8,5,2],
# ⁠ [9,6,3]
# ]
# 
# 
# Example 2:
# 
# 
# Given input matrix =
# [
# ⁠ [ 5, 1, 9,11],
# ⁠ [ 2, 4, 8,10],
# ⁠ [13, 3, 6, 7],
# ⁠ [15,14,12,16]
# ], 
# 
# rotate the input matrix in-place such that it becomes:
# [
# ⁠ [15,13, 2, 5],
# ⁠ [14, 3, 4, 1],
# ⁠ [12, 6, 8, 9],
# ⁠ [16, 7,10,11]
# ]
# 
# 
#
class Solution:
    
    def rot_edge(self, matrix, offset, n):
        steps = n - 1 
        
        for x in range(0, steps):
            pos = [
                    (offset, x+offset),
                    (x+offset, offset+n-1),
                    (offset+n-1, offset+n-1-x),
                    (offset+n-1-x, offset)]
            
            last_m = None
            for i in range(0, 4):
                current_p = pos[i]
                current_row = current_p[0]
                current_col = current_p[1]
                #print("current_p", current_p)
                
                next_p = pos[(i+1)%4]
                next_row = next_p[0]
                next_col = next_p[1]
               # print("next_p", next_p)
                
                if last_m == None:
                    m = matrix[current_row][current_col]
                    last_m = m
                
                m2 = matrix[next_row][next_col]
                matrix[next_row][next_col] = last_m
                last_m = m2
                
                
    
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix[0])
        original_n = n
        
        offset = 0
        while offset < (original_n//2):
            #print("offset", offset)
            self.rot_edge(matrix, offset, n)
            offset += 1
            n = n - 2
        
        #print(matrix)
        
