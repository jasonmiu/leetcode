// The hardest part is how to figure out the correct update index sequence.
// If think as the coner rotating:
//
//    0 1 2
//  +------
// 0| 1 2 3
// 1| 4 5 6
// 2| 7 8 9
//
//    0 1 2
//  +------
// 0| 7   1
// 1|
// 2| 9   3
//
// (0,0) -> (0,2)
// (0,2) -> (2,2)
// (2,2) -> (2,0)
// (2,0) -> (0,0)
//
// Treat every cell as a group of 4.
// i, as the row, go from 0 to the middle of the matrix.
// If number of rows is even, i is at the index of boundary
// of the upper part, like:
//
// 0
// 1 <- i < 2 end here, if number of row = 4
// 2
// 3
//
// If number of rows is odd, i is at the middle row:
//
//
// 0
// 1
// 2 <- i < 3 end here, if number of row = 5
// 3
// 4
//
// while j as column index always at the lower bound of half.
// for a 5x5 matrix, the starting positions are
// i = 0, 1, 2
// j = 0, 1
//
// (0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)
//
//   0 1 2 3 4
//  ----------
// 0|A B
// 1|C D
// 2|E F
// 3|
// 4|
//
// To rotate to right, it becomes:
//   0 1 2 3 4
//  ----------
// 0|    E C A
// 1|    F D B
// 2|
// 3|
// 4|
//
// From this, from couting from upper left to lower right in rows,
// it become counting from upper right in columns.
// The main point is identify how the counting direction change.
// In a 2D array, always thinking:
// Counting row first, or column first?
// Count from start, or count from end?
// Start from counting in 1 pattern, see how the counting pattern change
// affects the result.


class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        vector<vector<int>>& a = matrix;
        int n = a.size();

        int mid = 0;
        if (n % 2 == 0) {
            mid = n / 2;
        } else {
            mid = n / 2 + 1;
        }

        for (int i = 0; i < mid; i++) {
            for (int j = 0; j < n / 2; j++) {
                int temp = a[n-1-j][i];
                a[n-1-j][i] = a[n-1-i][n-1-j];
                a[n-1-i][n-1-j] = a[j][n-1-i];
                a[j][n-1-i] = a[i][j];
                a[i][j] = temp;
            }
        }

        return;

    }
};
