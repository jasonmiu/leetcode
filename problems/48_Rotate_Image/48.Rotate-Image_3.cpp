// Easier implmentation of the 90 rotation.
// rotation clockwise 90 = transpose + horizontal reverse
// The actual matrix transpose is for a matrix M*N, the transpose T has dimension N*M.
// For this question it is a N*N matrix so the transpose is switching the row and col
// of a cell.
// One point is we only has to switch the upper part along the diagonal, otherwise
// the lower part will flip back and the net effect is no change.
// 180 rotation = horizontal reverse + vertical reverse
// 270 rotation = transpose + vertical reverse


void transpose(vector<vector<int>>& a)
{
    for (int i = 0; i < a.size(); i++) {
        for (int j = i; j < a[0].size(); j++) {
            int t = a[i][j];
            a[i][j] = a[j][i];
            a[j][i] = t;
        }
    }

    return;
}

void reverse(vector<vector<int>>& a)
{
    for (int i = 0; i < a.size(); i++) {
        vector<int>& row = a[i];
        reverse(row.begin(), row.end());
    }

    return;
}

class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        transpose(matrix);
        reverse(matrix);
    }
};
