#
# @lc app=leetcode id=994 lang=python3
#
# [994] Rotting Oranges
#
# https://leetcode.com/problems/rotting-oranges/description/
#
# algorithms
# Easy (46.37%)
# Total Accepted:    56.7K
# Total Submissions: 120.2K
# Testcase Example:  '[[2,1,1],[1,1,0],[0,1,1]]'
#
# In a given grid, each cell can have one of three values:
# 
# 
# the value 0 representing an empty cell;
# the value 1 representing a fresh orange;
# the value 2 representing a rotten orange.
# 
# 
# Every minute, any fresh orange that is adjacent (4-directionally) to a rotten
# orange becomes rotten.
# 
# Return the minimum number of minutes that must elapse until no cell has a
# fresh orange.  If this is impossible, return -1 instead.
# 
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: [[2,1,1],[1,1,0],[0,1,1]]
# Output: 4
# 
# 
# 
# Example 2:
# 
# 
# Input: [[2,1,1],[0,1,1],[1,0,1]]
# Output: -1
# Explanation:  The orange in the bottom left corner (row 2, column 0) is never
# rotten, because rotting only happens 4-directionally.
# 
# 
# 
# Example 3:
# 
# 
# Input: [[0,2]]
# Output: 0
# Explanation:  Since there are already no fresh oranges at minute 0, the
# answer is just 0.
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= grid.length <= 10
# 1 <= grid[0].length <= 10
# grid[i][j] is only 0, 1, or 2.
# 
# 
# 
# 
#
# For each step, scan the grid and find the current rotten oranges.
# Then rot the adjacent oranges. Mark the current orange as 3 
# so will not be visited again.
# Complexity is O((mn)^2). Since we can have (mn) oranges.
# Each orange we scan the mn cells.
# Space is the O(mn) for saving the positions of rotten oranges.
# We can also use a BFS for putting the rotten oranges to the
# queue which we need not to mark the visited cells. And can get O(mn).

class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        time = -1
        m = len(grid)
        n = len(grid[0])
        
        has_rot = True
        can_rot = True
        while has_rot and can_rot:
            rottens = []
            for i in range(0, m):
                for j in range(0, n):
                    if grid[i][j] == 2:
                        rottens.append((i, j))
                        
            if len(rottens) == 0:
                has_rot = False
            else:
                can_rot = False
                for (i, j) in rottens:
                    if i-1 >= 0 and grid[i-1][j] == 1:
                        grid[i-1][j] = 2
                        can_rot = True
                        
                    if i+1 < m and grid[i+1][j] == 1:
                        grid[i+1][j] = 2
                        can_rot = True
                        
                    if j-1 >= 0 and grid[i][j-1] == 1:
                        grid[i][j-1] = 2
                        can_rot = True
                        
                    if j+1 < n and grid[i][j+1] == 1:
                        grid[i][j+1] = 2
                        can_rot = True
                        
                    grid[i][j] = 3
                        
            time += 1
                    
                    
        for i in range(0, m):
            for j in range(0, n):
                if grid[i][j] == 1:
                    return -1
                
        return time
