// This is quite similar to the 542 "01 Matrix".
// The idea is similar, to think the rotten oranges as
// seeds, and use BFS to propagate the inflections.
// If we found fresh orange can be inflected, we set it
// rotten and put it to the BFS queue, increase the time,
// which means the distance of a fresh orange to its nearest
// rotten orange. If no orange get inflected, no need to
// increase time as no distance increased.
// Finally if no more item in the BFS queue, means no more
// orange got inflected. The search can end.
// Check all oranges if any fresh one left. If so it cannot be
// inflected means no path connect to any rotten orange.
// Return -1 if this is the case, otherwise return the
// time, which is the step need to inflect all oranges.
// Time: O(n), n as the number of cells. As we put a cell
// to the queue once. Space: O(1).

class Solution {
public:
    int orangesRotting(vector<vector<int>>& grid) {

        queue<pair<int,int>> q;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 2) {
                    q.emplace(make_pair(i, j));
                }
            }
        }

        int time = 0;
        while (!q.empty()) {
            int qsize = q.size();
            bool is_inflected = false;
            for (int i = 0; i < qsize; i++) {
                pair<int, int> pos = q.front();
                q.pop();

                vector<pair<int,int>> dirs = {
                    make_pair(pos.first - 1, pos.second),
                    make_pair(pos.first, pos.second + 1),
                    make_pair(pos.first + 1, pos.second),
                    make_pair(pos.first, pos.second - 1)
                };

                for (const auto& d : dirs) {
                    if (d.first >= 0 && d.first < grid.size() &&
                        d.second >= 0 && d.second < grid[0].size()) {
                        int cell = grid[d.first][d.second];
                        if (cell == 1) {
                            grid[d.first][d.second] = 2;
                            q.push(d);
                            is_inflected = true;
                        }
                    }
                }
            }
            if (is_inflected) {
                time ++;
            }
        }

        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 1) {
                    return -1;
                }
            }
        }

        return time;
    }
};
