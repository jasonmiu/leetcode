# When manually work on the pow() calculation, we can see
# a lot of input values will be repeated, for example:
# x = 12
# 12, 3, 10, 5, 16, 8, 4, 2, 1
# x = 13
# 13, 40, 20, 10, 5, 16, 8, 4, 2, 1
# The values 12 and 13 repeated their calculation at "10".
# The steps (pow) of value 10 are the same for both "12" and "13".
# So if we saw this input value before, we can find it from 
# the cache.
# The base is when x == 1, then no step needed, so pow(1) = 0.
# For the current value, the steps needed is the steps of its 
# sub-value + 1.
# Complexity: O(n) where n is the max number of steps in [lo, hi]
# Space: O(n) for the cache.

def pow(x, memo):

    if x in memo:
        return memo[x]
    
    if x == 1:
        return 0
    
    if (x % 2) == 0:
        steps = pow(x / 2, memo) + 1
    else:
        steps = pow((3*x)+1, memo) + 1
        
    memo[x] = steps
    return steps


class Solution:
    def getKth(self, lo: int, hi: int, k: int) -> int:
        
        memo = dict()
        s = list()
        
        for x in range(lo, hi+1):
            p = pow(x, memo)
            s.append((p, x))
            
        s.sort()

        return s[k-1][1]
