// The algor is bascially same as the PY version,
// which start from each cell of the matrix,
// and do a DFS from it. For the DFS, search all 4 directions,
// and of the current visiting cell is not matching the current char
// in the input word, that path is a false path. If can forward the
// char in the input word along with the DFS, until the last matching
// char, we found a match, so can return true.
// Instead using a set to store the visited positions of the current DFS path,
// mark the visited cell a special char '#'. Once we return from the current DFS
// level, restore the char.
//
// Time: O((nm)^2)), since for each nm cell, we need to access all nm cells.
// Space: O(1).

bool dfs(vector<vector<char>>& a, const string& w, int start, int row, int col)
{
    if (a[row][col] == w[start]) {
        if (start == w.size() - 1) {
            return true;
        }

        char cur = a[row][col];
        a[row][col] = '#';

        vector<pair<int,int>> dirs = {
            {row - 1, col},
            {row, col + 1},
            {row + 1, col},
            {row, col - 1}
        };

        for (auto d : dirs) {
            if (d.first >= 0 && d.first < a.size() &&
                d.second >= 0 && d.second < a[0].size() &&
                a[d.first][d.second] != '#') {
                bool ret = dfs(a, w, start + 1, d.first, d.second);
                if (ret) {
                    return true;
                }
            }
        }

        a[row][col] = cur;
    }

    return false;
}

class Solution {
public:
    bool exist(vector<vector<char>>& board, string word) {
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if (board[i][j] == word[0]) {
                    if (dfs(board, word, 0, i, j)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
};
