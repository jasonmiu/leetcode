#
# @lc app=leetcode id=79 lang=python3
#
# [79] Word Search
#
# https://leetcode.com/problems/word-search/description/
#
# algorithms
# Medium (32.77%)
# Total Accepted:    400.5K
# Total Submissions: 1.2M
# Testcase Example:  '[["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]]\n"ABCCED"'
#
# Given a 2D board and a word, find if the word exists in the grid.
# 
# The word can be constructed from letters of sequentially adjacent cell, where
# "adjacent" cells are those horizontally or vertically neighboring. The same
# letter cell may not be used more than once.
# 
# Example:
# 
# 
# board =
# [
# ⁠ ['A','B','C','E'],
# ⁠ ['S','F','C','S'],
# ⁠ ['A','D','E','E']
# ]
# 
# Given word = "ABCCED", return true.
# Given word = "SEE", return true.
# Given word = "ABCB", return false.
# 
# 
#
# We are looking for a path starting from any cell,
# by moving to 4 adjacent cells if we can form a path that
# all visited cells match each char of the given word, then 
# we found the word, return True, else, start from next cell
# and search again. If started from all cells but no such matching
# path is found, return False.
# When form a path, we do not want it to move back, for example:
# [['A', 'B']], we move A to B, when we are in B we do not want to 
# move back to A otherwise it will loop forever. So use a set to mark
# the visited cells for this path.
# Given this example:
# [["A","B","C","E"],
#  ["S","F","E","S"],
#  ["A","D","E","E"]]
# "ABCESEEEFS"
# 
# Assume we visited AB, and now at C (0, 2)
#   [[@"A",@"B", *"C", "E"],
#    ["S", "F",  "E", "S"],
#   ["A", "D",  "E", "E"]]
# We can go two Es, (0, 3) and (1, 2). 
#
#   [[@"A",@"B",  @"C", "E"],
#    ["S", "F",  0"E", "S"],
#   ["A", "D",   "E", "E"]]
# Let move downward, then go S
#   [[@"A",@"B",  @"C",  "E"],
#    ["S", "F",  0"E",  1"S"],
#   ["A", "D",   3"E",  2"E"]]
#
# We are at (2, 2) E. No more cell to go, and should go back to C and try again.
#   [[@"A",@"B",  @"C", 0"E"],
#    ["S", "F",  *"E",  *"S"],
#   ["A", "D",   *"E", *"E"]]
# But if the S and 3 *-marked Es are added to visited set,
# when we are at (0, 3) E we have no way to go. Since we have gone back to C, 
# we should remove those cells from visited set for backtracking.
#    [[@"A",  @"B", @"C", 0"E"],
#    [6"S",  5"F",  4"E", 1"S"],
#    ["A", "  D",   3"E", 2"E"]]
# Complexity O((mn)^2), for each cell we need to visit all cells, so O(mn) and do this mn times.
# The space is O(mn), but can use marker on the board instead of a set, then the space becames O(1)


def dfs(board, r, c, visited, word, i):
    
    if (r, c) in visited:
        return False
    if i >= len(word):
        return False
    if board[r][c] != word[i]:
        return False
    
    if board[r][c] == word[i] and i == (len(word) - 1):
        return True
    
    m = len(board)
    n = len(board[0])
    
    visited.add((r, c))
    for (off_r, off_c) in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        new_r = r + off_r
        new_c = c + off_c
        if new_r >= 0 and new_r < m and new_c >= 0 and new_c < n:
            ret = dfs(board, new_r, new_c, visited, word, i+1)
            if ret:
                return ret
    visited.remove((r, c))
    return False

class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        for i in range(0, len(board)):
            for j in range(0, len(board[0])):
                ret = dfs(board, i, j, set(), word, 0)
                if ret:
                    return True
                
        return False
        
