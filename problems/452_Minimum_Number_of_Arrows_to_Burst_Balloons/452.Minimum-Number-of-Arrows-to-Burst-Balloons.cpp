// The question is asking, if there are some intervals (ballons diameters) placing
// on a horizontal bar, if we draw some vertical lines along this bar, what is the minimum
// vertical lines we can use to intersect all intervals? Example:
//
// input:
// [[10,16],[2,8],[1,6],[7,12]]
//
// 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
// --------------------------------------------------
// |---------|
//   |-----------|
//         ^   |------------|
//         |         |------------------|
//         |            ^
//         |            |
//
// Two vertical lines to intersect all intervals.
//
// So we can see this as the interval overlapping problem. We want to find the minimum numbers
// of the interval overlaps that cover all intervals.
//
// If we have only 1 interval, then we must need 1 shot. If two intervals, we like to see
// if any overlap between them. To check if two intervals have overlap, can sort them by their
// starting point, so we can check if the ending point of the current overlapped part is covering
// the next interval. If two adjacent intervals are overlapped, then we need only 1 shot, and update
// the current overlapping parts. Few case:
//
// 1. |------| <-- current overlap
//        |------| <-- current interval
//
// 2. |------|
//      |--|
//
// 3. |-----|
//            |---|
//
// case 1, the current interval has larger end, so the new overlapped part
// is the current interval start to current overlap end.
// case 2, the current interval has smaller end. So the new overlapped part
// is the current interval start to current interval end.
// case 3, no overlap. The new overlapped part is the current interval start and end,
// also need to increase the shot count.
//
// Update the overlapped part for each interval. If an interval is still having overlapping
// port of the current overlap, means we can still use the original shot to interect this interval.
// If not, we need a new shot. This is a greedy way to try to cover as much as intervals with
// a overlap. Since each overlap can cover the most intervals, the final overlaps (shots) we need
// will be the minimal.
// Time: O(nlogn) for sorting. Space: O(1).



class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {

        if (points.size() == 1) {
            return 1;
        }

        sort(points.begin(), points.end(), [](const vector<int>& a, const vector<int>& b) {
            if (a[0] < b[0]) {
                return true;
            } else {
                return false;
            }
        });

        int overlap_s = points[0][0];
        int overlap_e = points[0][1];
        int shot = 1;

        for (int i = 1; i < points.size(); i++) {

            const vector<int>& cur = points[i];

            if (overlap_e >= cur[0]) {
                overlap_s = cur[0];

                if (cur[1] <= overlap_e) {
                    overlap_e = cur[1];
                }

            } else {
                shot ++;

                overlap_s = cur[0];
                overlap_e = cur[1];
            }
        }

        return shot;
    }
};
