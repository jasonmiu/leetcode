// For a matrix, it has 4 boundaries:
//
//  [1, 2, 2, 3, 5]
//  [3, 2, 3, 4, 4]
//  [2, 4, 5, 3, 1]
//  [6, 7, 1, 4, 5]
//  [5, 1, 1, 2, 4]
//
// In this question, the top and left boundaries are called Pacific,
// and the bottom and right boundaries are called Atlantic.
// And we want to find all the cells that can reach both pacific and atlantic
// sides, for tracing the cell values from high to low.
// The first idea is to start from each cell, use DFS to see if it can both reach
// pacific and atlantic sides. For visiting each neighboring cell by following the
// values, if the path search the pacific or atlantic side, then propagate till
// the end of the path. But since each cell has to trace N other cells to find the path,
// this becomes a O(N^2) algor with N equals to number of cells.
// What if using a visited check? If a cell is visited, then we do visit it again
// but just return the cached result. Issue is we may need to re-visit a cell for
// going two different sides. Example:
//
// 1 2 9 9
// 9 3 6 9
// 9 4 7 9
// 9 9 5 9
//
// If we scan from top left to bottom right, when we are visiting (1,1) "3", it traces
// via DFS to the Pacific side:
//
// A A 9 9
// 9 A 6 9
// 9 4 7 9
// 9 9 5 9

// Letters are the visited cells. While then the (1,2) "6" goes no where, the next (2,1) "4"
// goes no where since its upper cell was visited:
// A A 9 9
// 9 A B 9
// 9 C 7 9
// 9 9 5 9
//
// Then the (2,2) "7", it can only go down since other were visited:
// A A 9 9
// 9 A B 9
// 9 C D 9
// 9 9 D 9
//
// Hence in this case, no cell can go both sides since we marked it as visited, even we may want
// to test from different sides, the correct anwser is (2,2)"7".
//
// From this we see we cannot has only 1 visited set. Another way to think is reverse the search
// from the boundaries. Start from pacific side, and save all the accessible cells from pacific side
// to the pacific path set. In test case, for DFS tracing a path, all cells on this path are accessible
// from pacific side. Then when we start from another cell, if its path hit the pacific path set that means
// from here we have explored that path and we know it is accessible, hence do not need to search again.
// Same case for atlantic side. Since we explore twice, those cells appear in both path sets are the
// cells that can be reachable from both sides.
// Time: O(n), n = number of cells. Space: O(n) for the sets.


struct pair_hash
{
    const size_t operator() (const pair<int,int>& p) const {
        return p.first * 31 + p.second * 7;
    }

};

void flow(const vector<vector<int>>& g, int r, int c, unordered_set<pair<int,int>, pair_hash>& visited)
{
    pair<int,int> pos = {r, c};
    int m = g.size();
    int n = g[0].size();

    if (visited.find(pos) != visited.end()) {
        return;
    }
    visited.insert(pos);

    vector<pair<int,int>> dirs = {
        {r+1, c},
        {r, c+1},
        {r-1, c},
        {r, c-1}
    };

    int cur_h = g[r][c];
    for (auto d : dirs) {
        if (d.first >= 0 && d.first < m && d.second >= 0 && d.second < n) {

            int next_h = g[d.first][d.second];
            if (next_h >= cur_h) {
                flow(g, d.first, d.second, visited);
            }
        }
    }

    return;
}

class Solution {
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        int m = heights.size();
        int n = heights[0].size();

        unordered_set<pair<int,int>, pair_hash> pac_visited;
        unordered_set<pair<int,int>, pair_hash> atl_visited;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    flow(heights, i, j, pac_visited);
                }
                if (i == (m-1) || j == (n-1)) {
                    flow(heights, i, j, atl_visited);
                }
            }
        }

        vector<vector<int>> ans;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                pair<int,int> pos = {i,j};
                if ((pac_visited.find(pos) != pac_visited.end()) &&
                    (atl_visited.find(pos) != atl_visited.end()))
                {
                    vector<int> ele = {i,j};
                    ans.push_back(ele);
                }
            }
        }

        return ans;
    }
};
