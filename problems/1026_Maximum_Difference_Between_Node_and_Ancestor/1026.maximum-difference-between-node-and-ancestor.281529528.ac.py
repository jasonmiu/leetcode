#
# @lc app=leetcode id=1026 lang=python3
#
# [1026] Maximum Difference Between Node and Ancestor
#
# https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/description/
#
# algorithms
# Medium (61.68%)
# Total Accepted:    17.5K
# Total Submissions: 28.3K
# Testcase Example:  '[8,3,10,1,6,null,14,null,null,4,7,13]'
#
# Given the root of a binary tree, find the maximum value V for which there
# exists different nodes A and B where V = |A.val - B.val| and A is an ancestor
# of B.
# 
# (A node A is an ancestor of B if either: any child of A is equal to B, or any
# child of A is an ancestor of B.)
# 
# 
# 
# Example 1:
# 
# 
# 
# 
# Input: [8,3,10,1,6,null,14,null,null,4,7,13]
# Output: 7
# Explanation: 
# We have various ancestor-node differences, some of which are given below :
# |8 - 3| = 5
# |3 - 7| = 4
# |8 - 1| = 7
# |10 - 13| = 3
# Among all possible differences, the maximum value of 7 is obtained by |8 - 1|
# = 7.
# 
# 
# 
# 
# Note:
# 
# 
# The number of nodes in the tree is between 2 and 5000.
# Each node will have value between 0 and 100000.
# 
# 
#
# The max diff can be happened in any nodes pair along the tree path, like:
#     8
#    /\
#   3   4
#  /\   /\
# 2  5 11 7
# The max diff is 11-4 = 7. The brute force way is for each node, do a 
# depth first search to find the diff of each pairs along the path. This 
# leads a O(nlogn) complexity.
# The max diff bewteen node and ancestor can be thought as the 
# diff between the min and max nodes along a path.
# Simplest case is 2 nodes, like:
# 8
#  \
#   4
# Then the max diff is 4. The max node along the path is 8, the min node along the path is 4.
# When we are at the leave node 4, we know the max and min along its path already. We can
# find the max diff by path_max - path_min.
# We can do this recursively, like:
# 8
#  \
#   4
#    \
#     7
# At leave node 7, we know the max and min nodes (8, 4), so the max diff between node and ancestor 
# of this path is 4 between the nodes 4 and 8.
# We pass the max diff of a path back up. And compare the max diffs of the left and right subtree:
# 8
#  \
#   4
#  / \
# 11  7
# At node 4, the max diff of left is 11-4=7, the max diff of right is 8-4=4. So pass the larger value up.
# One tricky part is we need to eval the current node value first to see if it is the min or max of the current
# path.
# The complexity is O(n), for visiting all nodes.
# The space complexity is O(logn), for recusivly record a path.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import math

def max_diff(root, cur_max, cur_min):
    if root == None:
        return 0
    
    new_max = root.val if root.val > cur_max else cur_max
    new_min = root.val if root.val < cur_min else cur_min
    
    if root.left == None and root.right == None:
        return (new_max - new_min)
    
    ret_r = ret_l = -(math.inf)
    if root.left:
        ret_l = max_diff(root.left, new_max, new_min)
    if root.right:
        ret_r = max_diff(root.right, new_max, new_min)
    
    return max(ret_l, ret_r)
        

class Solution:
    def maxAncestorDiff(self, root: TreeNode) -> int:
        return max_diff(root, -math.inf, math.inf)
