#
# @lc app=leetcode id=380 lang=python3
#
# [380] Insert Delete GetRandom O(1)
#
# https://leetcode.com/problems/insert-delete-getrandom-o1/description/
#
# algorithms
# Medium (44.22%)
# Total Accepted:    169.9K
# Total Submissions: 374.7K
# Testcase Example:  '["RandomizedSet","insert","remove","insert","getRandom","remove","insert","getRandom"]\n[[],[1],[2],[2],[],[1],[2],[]]'
#
# Design a data structure that supports all following operations in average
# O(1) time.
# 
# 
# 
# insert(val): Inserts an item val to the set if not already present.
# remove(val): Removes an item val from the set if present.
# getRandom: Returns a random element from current set of elements. Each
# element must have the same probability of being returned.
# 
# 
# 
# Example:
# 
# // Init an empty set.
# RandomizedSet randomSet = new RandomizedSet();
# 
# // Inserts 1 to the set. Returns true as 1 was inserted successfully.
# randomSet.insert(1);
# 
# // Returns false as 2 does not exist in the set.
# randomSet.remove(2);
# 
# // Inserts 2 to the set, returns true. Set now contains [1,2].
# randomSet.insert(2);
# 
# // getRandom should return either 1 or 2 randomly.
# randomSet.getRandom();
# 
# // Removes 1 from the set, returns true. Set now contains [2].
# randomSet.remove(1);
# 
# // 2 was already in the set, so return false.
# randomSet.insert(2);
# 
# // Since 2 is the only number in the set, getRandom always return 2.
# randomSet.getRandom();
# 
# 
#
# O(1) requirment is easy to relete to the hash. 
# The problem is how to do a getRandom() in O(1).
# To have genRandom() in O(1), we need to be able to 
# access the size and the index of the values.
# If using linked list, we can do remove and insert in O(1),
# but it is hard to maintain the indices for getRandom().
# We can use array, which has properties:
# 1. get at O(1)
# 2. swap two elements at O(1)
# 3. Remove head or tail at O(1)
# 4. Insert at tail at O(1)
# So for removing an element, we can swap it to the end of an array,
# then remove the last element.
# And hash table is keep tracking hash[val] -> index of val in array
# When we insert, we update the hash[val] with the last index of array since 
# we do an append.
# When we delete, we swap the last value to the index of the target value,
# and we update the hash[orginial last_val] with the index of target value.


import random

class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.vals = []
        self.index_dict = {} # index_dict[val] = idx of vals
        

    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        
        if val in self.index_dict:
            return False
        else:
            self.vals.append(val)
            idx = len(self.vals) - 1
            self.index_dict[val] = idx
            return True
        

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val not in self.index_dict:
            return False
        else:
            val_idx = self.index_dict[val]
            last_val = self.vals[-1]
            self.vals[val_idx] = last_val
            self.index_dict[last_val] = val_idx
            del self.index_dict[val]
            self.vals.pop()
            return True
        

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        ridx = random.randint(0, len(self.vals)-1)
        return self.vals[ridx]
        


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
