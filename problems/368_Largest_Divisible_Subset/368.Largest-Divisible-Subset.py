# For a divisible set, a > b > c > d > 1
# while a mod b == 0 and
# b mod c == 0 and
# c mod d == 0, then
# a mod d is also == 0.
# So we want to make a longest divisible chain like this.
# Another fact is, for a number 'x' has 'y' divisible numbers,
# such as 4 > 2 > 1, 1 has 1 divisible numbers (1), 2 has
# 2 divisible numbers (1, 2), and 4 has 3 divisible numbers (4, 2, 1),
# the larger number k, where k > x, if k mod x == 0 (k and x is divisible),
# the number of divisibles of k must including number of divisibles of x.
# So first to count how many divisibles can be for a number.
# For example
# [1, 4, 5, 8, 9, 12]
# init a divisible count like:
# [1, 1, 1, 1, 1, 1], as all number can be divisibles by itself.
# For each number i, we look for the numbers that is smaller than i,
# and if they are divisible, check if the divisble count of the smaller number + 1
# can be bigger than the current divisible count of number i. If so, include this
# smaller number as the divisible chain.
# The divisible count will be:
# [1, 2, 2, 3, 2, 3]
# So the longest chain can be started from index 3 (number 8) or index 5 (12).
# Then reconstruct this divisible chain. 
# The first one is the largest number, with highest divisible count,
# So it is index 3 (8). Then find the next largest number that is divisible with it,
# so it is index 1 (4), the current largest number become 4, and again find the next 
# largest number that is divisible with it, so next is index 0 (1), and we
# finished to reconstruct the chain.
# Complexity O(n^2) for the double loop in the longest chain finding.
# Space O(n) for the counter.
# The longest chain part is similar the problem 300. Longest Increasing Subsequence


class Solution:
    def largestDivisibleSubset(self, nums: List[int]) -> List[int]:
        n = len(nums)
        if n == 0:
            return []

        count = [1] * n
        nums.sort()
        
        for i in range(1, n):
            for j in range(i - 1, -1, -1):
                if nums[i] % nums[j] == 0:
                    count[i] = max(count[i], count[j] + 1)
                    
        max_cnt = 0
        max_cnt_idx = 0
        for i in range(0, n):
            if count[i] > max_cnt:
                max_cnt = count[i]
                max_cnt_idx = i
                
        ans = []
        e = nums[max_cnt_idx]
        cnt = count[max_cnt_idx]
        for i in range(max_cnt_idx, -1, -1):
            if e % nums[i] == 0 and count[i] == cnt:
                ans.append(nums[i])
                e = nums[i]
                cnt = cnt - 1
                
        return ans
