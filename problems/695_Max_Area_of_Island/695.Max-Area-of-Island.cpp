// The basic idea is to flood fill an island, and find out the size of
// each island, then find the biggest one.
// So for a set of connected cells, how many cells are value 1?
// We can loop for each cell and flood fill it, and check if it is a island
// cell (value 1) during the flood. Question is, how do we know if this is
// a new island during the flood.
// When looping each cell, we only flood if this cell is a *unvisited* island
// cell. When we flood, we color the visited island cell as "2", then when we
// visit the same cell again which is "2", in the outter loop of flood filling,
// we know we have visited it and can skip.
// For flood filling, we do a DFS like to count all cells. If we hit a 0 (water)
// or 2 (visited), then we return without counting that cell.
// So from the outter loop, once we found a unvisited island cell, we can find its
// area by connecting all its island cells. Then we compare it with the current
// max island size.
// Time: O(n), since visit all cells once. Space: O(n), worst case whole map
// is a single island, then the call stack will grow for each cell hence N.

#include <algorithm>


int island_size(vector<vector<int>>& grid, int r, int c)
{
    if (r < 0 || r >= grid.size() || c < 0 || c >= grid[0].size()) {
        return 0;
    }

    int cell = grid[r][c];

    if (cell == 2 || cell == 0) {
        return 0;
    }

    if (cell == 1) {
        int size = 1;
        grid[r][c] = 2;
        size += island_size(grid, r - 1, c);
        size += island_size(grid, r, c + 1);
        size += island_size(grid, r + 1, c);
        size += island_size(grid, r, c - 1);
        return size;
    }

    return 0;
}


class Solution {
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {

        int max_size = 0;

        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 1) {
                    int s = island_size(grid, i, j);
                    max_size = std::max(s, max_size);
                }
            }
        }

        return max_size;
    }
};
