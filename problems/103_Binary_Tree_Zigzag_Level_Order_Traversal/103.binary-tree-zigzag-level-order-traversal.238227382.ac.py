#
# @lc app=leetcode id=103 lang=python3
#
# [103] Binary Tree Zigzag Level Order Traversal
#
# https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/
#
# algorithms
# Medium (43.78%)
# Total Accepted:    266.7K
# Total Submissions: 609.3K
# Testcase Example:  '[3,9,20,null,null,15,7]'
#
# Given a binary tree, return the zigzag level order traversal of its nodes'
# values. (ie, from left to right, then right to left for the next level and
# alternate between).
# 
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
# ⁠   3
# ⁠  / \
# ⁠ 9  20
# ⁠   /  \
# ⁠  15   7
# 
# 
# 
# return its zigzag level order traversal as:
# 
# [
# ⁠ [3],
# ⁠ [20,9],
# ⁠ [15,7]
# ]
# 
# 
#
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque

class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        
        if root == None:
            return []
        
        order = 1 # 1 is right to left, -1 is left to right
        ans = []
        
        # The idea is to use the double ended queue, then do a BFS
        #          3
        #         / \
        #        9   20
        #       /   / \
        #      10  15  7
        #         /
        #        23
        # 
        # order 1, q = [3], pop 3, queue L->R, q = [20, 9] (3)
        # order -1, q = [20, 9], Lpop 20, queue R->L, q = [9, 7, 15], Lpop 9, q = [7, 15, 10] (20, 9)
        # order 1, q = [7, 15, 10], pop 10, queue L->R, q = [7, 15], pop 15, queue L->R, q = [7, 23], pop 7, queue L->R, q = [23] (10, 15, 7)
        # order -1, q = [23], rpop 23 (23)
        
        q = deque()
        
        q.append(root)
        
        while(not len(q) == 0):
            level = []
            
            for i in range(0, len(q)):
                if order > 0:
                    n = q.pop()
                else:
                    n = q.popleft()
                                
#                print(n.val, " ", end="")
                level.append(n.val)
                
                if order > 0:
                    if n.left:
                        q.appendleft(n.left)
                    if n.right:
                        q.appendleft(n.right)
                else:
                    if n.right:
                        q.append(n.right)
                    if n.left:
                        q.append(n.left)
                        
            order = -order
            ans.append(level)
#            print("")                
        return ans
                
                
                
