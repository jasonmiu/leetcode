#
# @lc app=leetcode id=414 lang=python3
#
# [414] Third Maximum Number
#
# https://leetcode.com/problems/third-maximum-number/description/
#
# algorithms
# Easy (29.55%)
# Total Accepted:    108.6K
# Total Submissions: 367.4K
# Testcase Example:  '[3,2,1]'
#
# Given a non-empty array of integers, return the third maximum number in this
# array. If it does not exist, return the maximum number. The time complexity
# must be in O(n).
# 
# Example 1:
# 
# Input: [3, 2, 1]
# 
# Output: 1
# 
# Explanation: The third maximum is 1.
# 
# 
# 
# Example 2:
# 
# Input: [1, 2]
# 
# Output: 2
# 
# Explanation: The third maximum does not exist, so the maximum (2) is returned
# instead.
# 
# 
# 
# Example 3:
# 
# Input: [2, 2, 3, 1]
# 
# Output: 1
# 
# Explanation: Note that the third maximum here means the third maximum
# distinct number.
# Both numbers with value 2 are both considered as second maximum.
# 
# 
#
# We use a list (rank_list) of 3 elements to store max, 2nd max, and 3rd max numbers.
# For each number in nums, compare it to the elements in the rank_list.
# To keep the rank_list ordered, if the current number is larger than any element in rank_list,
# we shift the elements to right (smaller end) and replace the original element with current num.
# rank_list is a constance size list so this operation is O(1).
# If rank_list[2] get updated, then we have 3rd max element to return. Else, return the rank_list[0]
# which is the max.
# Since we scan the nums once and each operation in the loop is constance, the overall complexity is
# O(n). We only use a static list for rank_list so the space complexity is O(1).

class Solution:
    def thirdMax(self, nums: List[int]) -> int:
        
        rank_list = [float("-inf"), float("-inf"), float("-inf")]
        
        repeated = set()
        for n in nums:
            
            if n in repeated:
                continue
            else:
                repeated.add(n)
            
            if n > rank_list[0]:
                rank_list[2] = rank_list[1]
                rank_list[1] = rank_list[0]
                rank_list[0] = n
                continue
                
            if n > rank_list[1]:
                rank_list[2] = rank_list[1]
                rank_list[1] = n
                continue
                
            if n > rank_list[2]:
                rank_list[2] = n
                continue
        
        #print("rank_list", rank_list)
        
        if rank_list[2] > float("-inf"):
            return rank_list[2]
        else:
            return rank_list[0]
