# Simple swap reverse from head and tail until we meet at the middle
# For string with odd length size, the middle index is the center 
# element which needs not swap. For string with even length size,
# the middle index is the first element of the 2nd half.
# So we swap till the head pointer smaller then middle index

class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        
        n = len(s)
        m = n // 2
        
        for i in range(0, m):
            t = s[i]
            s[i] = s[n-1-i]
            s[n-1-i] = t
            
            
