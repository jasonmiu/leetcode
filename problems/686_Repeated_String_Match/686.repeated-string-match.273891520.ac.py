#
# @lc app=leetcode id=686 lang=python3
#
# [686] Repeated String Match
#
# https://leetcode.com/problems/repeated-string-match/description/
#
# algorithms
# Easy (31.81%)
# Total Accepted:    77.2K
# Total Submissions: 242.7K
# Testcase Example:  '"abcd"\n"cdabcdab"'
#
# Given two strings A and B, find the minimum number of times A has to be
# repeated such that B is a substring of it. If no such solution, return -1.
# 
# For example, with A = "abcd" and B = "cdabcdab".
# 
# Return 3, because by repeating A three times (“abcdabcdabcd”), B is a
# substring of it; and B is not a substring of A repeated two times
# ("abcdabcd").
# 
# Note:
# The length of A and B will be between 1 and 10000.
# 
#
# If B can be a substring of A after repeating A, 
# B and A must has a overlapping part. For eg,
# A = "abcdd"
# B = "dda"
# The overlapping part is "dd", which means we want
# to find the first index in A that match the B.
# "abcdd"
#    "dda"
# The index is 3. This is what align_strings() does.
# To repeat A and have substring B, the repeated string
# C must longer or equal to B. So the len of C has two cases:
# 1. same as B
# 2. longer than B by 1 more repeat
# Next is to find the len of C. Let the length of right side of the aligned 
# string as R, ie:
# "abcdd_"
#     dda"
#       ^-- R = 1
# as the prefix_idx is the shifted places of B, so R is:
# R = len(B) + prefix_idx - len(A)
# Find find how many repeat of A can cover the R, 
# do roundup(R / len(A))
# First test the first case see if B is already a substring.
# If it failed, repeat A one more time for case 2. 
# There is a special case, while the B is already a substr of A.
# This handled at the beginning.
# In align_string(), for each char of A, we scan it thru B, B length 
# is a constance, so align_string() is O(n). The repeatedString()
# we scanned B with constance operations. So overall complexity is
# O(n), while n is len(A).
# The space complexity is O(1) as we do not need extra space for A

class Solution:
    def align_strings(self, a, b):
        prefix_idx = -1
        
        for i in range(0, len(a)):
            if a[i] == b[0]:
                if a[i::] == b[0:len(a)-i]:
                    prefix_idx = i
                    break
                    
        return prefix_idx

        
    def repeatedStringMatch(self, A: str, B: str) -> int:
        
        if B in A:
            return 1
        
        prefix_idx = self.align_strings(A, B)
        #print(prefix_idx)

        if prefix_idx >= 0:
            #print(A)
            #print(B.rjust(prefix_idx+len(B)))
        
            repeat_num = ((len(B) + prefix_idx - len(A)) // len(A)) + 1
            
            #print(prefix_idx)
            
            C = A * repeat_num
            if B in C:
                return repeat_num
            else:
                repeat_num += 1
                C = A * repeat_num
                if B in C:
                    return repeat_num
                else:
                    return -1
            
        else:
            return -1
