#
# @lc app=leetcode id=1139 lang=python3
#
# [1139] Largest 1-Bordered Square
#
# https://leetcode.com/problems/largest-1-bordered-square/description/
#
# algorithms
# Medium (45.64%)
# Total Accepted:    6.6K
# Total Submissions: 14.4K
# Testcase Example:  '[[1,1,1],[1,0,1],[1,1,1]]'
#
# Given a 2D grid of 0s and 1s, return the number of elements in the largest
# square subgrid that has all 1s on its border, or 0 if such a subgrid doesn't
# exist in the grid.
# 
# 
# Example 1:
# 
# 
# Input: grid = [[1,1,1],[1,0,1],[1,1,1]]
# Output: 9
# 
# 
# Example 2:
# 
# 
# Input: grid = [[1,1,0,0]]
# Output: 1
# 
# 
# 
# Constraints:
# 
# 
# 1 <= grid.length <= 100
# 1 <= grid[0].length <= 100
# grid[i][j] is 0 or 1
# 
#
# The idea is for every grid, if it is 1, we create a square there, 
# and match it with the grid to see if they are a match, if not,
# expend the square.
#
#+------------------------+
#|                        |
#|  1-----+-+-+           |
#|  |     | | |           |
#|  |     | | |           |
#|  +-----a | |           |
#|  |       | |           |
#|  +-------b |           |
#|  |         |           |
#|  +---------c           |
#|                        |
#|                        |
#|                        |
#|                        |
#|                        |
#+------------------------+
# Visit each cell is O(n^2) and for each cell we create a square need O(n) by
# expending the point from a b to c. Matching the square and our board takes
# O(n^2) for brute force. We try to make this matching O(1).
# 
# We need to figure out if a line is continous. We can use 2 DP tables to save
# for each cell, what is the the max consecutive length of 1 for the col and row axis.
# When we match we can check if the current side length, is equal to the consecutive 1s
# at that cell. If four sides are all match then we found a square.
#
# for a board like:
# 1 1 1
# 1 0 1
# 1 1 1
#
# DP row will be:
# 0 1 2
# 0 0 0
# 0 1 2
#
# DP col will be:
# 0 0 0
# 1 0 1
# 2 0 2
# Hence the complexity is O(n^3). The space complexity is O(n^2), for n is the size of the board side.
# ref: https://leetcode.com/problems/largest-1-bordered-square/discuss/444750/A-O(n3)-JavaScript-solution-with-explanation.

def check(row1, row2, col1, col2, dp_row, dp_col):
    
    if ((dp_col[row1][col1] - dp_col[row2][col1]) == (col1 - col2) and \
        (dp_col[row1][col2] - dp_col[row2][col2]) == (col1 - col2) and \
        (dp_row[row1][col1] - dp_row[row1][col2]) == (row1 - row2) and \
        (dp_row[row2][col1] - dp_row[row2][col2]) == (row1 - row2)):
    
        return True
    else:
        return False

class Solution:
    def largest1BorderedSquare(self, grid: List[List[int]]) -> int:
        
        h = len(grid)
        w = len(grid[0])
        
        dp_row = [[0 for x in range(0, w)] for y in range(0, h)]
        dp_col = [[0 for x in range(0, w)] for y in range(0, h)]
        
        for i in range(0, h):
            cnt = 0
            for j in range(0, w):
                if grid[i][j] == 1:
                    dp_row[i][j] = cnt
                    cnt += 1
                else:
                    cnt = 0
                    
        for j in range(0, w):
            cnt = 0
            for i in range(0, h):
                if grid[i][j] == 1:
                    dp_col[i][j] = cnt
                    cnt += 1
                else:
                    cnt = 0
        
#        print("DP_ROW", dp_row)
#        print("DP_COL", dp_col)

        ans = 0
        for i in range(0, h):
            for j in range(0, w):
                if grid[i][j] == 1:
                    
                    # ex row and col is the right bottom corner
                    ex_row = i
                    ex_col = j
                    
                    while (ex_row < h) and (ex_col < w):
                        if check(i, ex_row, j, ex_col, dp_row, dp_col):
#                            print("i", i, "j", j, "ex_row", ex_row, "ex_col", ex_col)
                            s = (ex_col - j + 1) * (ex_row - i + 1)
#                            print("S", s)
                            ans = max(ans, s)
                            
                        
                        ex_row += 1
                        ex_col += 1
                        
        return ans
    
                        
