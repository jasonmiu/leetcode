#
# @lc app=leetcode id=49 lang=python3
#
# [49] Group Anagrams
#
# https://leetcode.com/problems/group-anagrams/description/
#
# algorithms
# Medium (50.30%)
# Total Accepted:    495.7K
# Total Submissions: 939.7K
# Testcase Example:  '["eat","tea","tan","ate","nat","bat"]'
#
# Given an array of strings, group anagrams together.
# 
# Example:
# 
# 
# Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
# Output:
# [
# ⁠ ["ate","eat","tea"],
# ⁠ ["nat","tan"],
# ⁠ ["bat"]
# ]
# 
# Note:
# 
# 
# All inputs will be in lowercase.
# The order of your output does not matter.
# 
# 
#
# Anagrams means for two words, they are:
# 1. same in length
# 2. same in the chars
# To represent a anagram, we can use a counter hash, so for example:
# eat = {a:1, e:1, t:1}
# tea = {a:1, e:1, t:1}
# If both counter hashs are the same, they are anagrams. 
# But need to compare the counters of each word for every word, this leads
# O(n^2) n == number of words.
# The problem is comparing the representative of a word contributes to the
# complexity. We like to use hash for this comparision since check a element
# in a hash or not takes O(1) and do this for all words will become O(n).
# We can sort each word, if length of each word is m then complexity is O(n + nmlogm).
# But since we have the fixed size of the input source, ie. the 26 lower case alphabets,
# we can use a 26 size vector to store the counts of each char of a word, and hash
# this vector as the representative. Making a vector takes O(m) and for all words
# it takes O(nm), when we group all n words with their hashed representatives, 
# the complexity is O(n+nm).
# The space is O(n) for the hash.

def str_to_tuple(s):
    a = [0] * 26
    for c in s:
        a[ord(c) - ord('a')] += 1
        
    return tuple(a)


import collections
class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        
        d = collections.defaultdict(list)
        for s in strs:
            t = str_to_tuple(s)
            d[t].append(s)
            
        ans = []
        for k in d:
            ans.append(d[k])
            
        return ans
