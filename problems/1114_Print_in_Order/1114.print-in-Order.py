# Using threading.Lock to sync each thread:
# 
# Thread 2 3 need to wait for the "done_lock"
# to be released by it previous threads.

import threading

class Foo:
    def __init__(self):
        self.first_done_lock = threading.Lock()
        self.second_done_lock = threading.Lock()
        
        self.first_done_lock.acquire()
        self.second_done_lock.acquire()


    def first(self, printFirst: 'Callable[[], None]') -> None:
        
        # printFirst() outputs "first". Do not change or remove this line.
        printFirst()
        self.first_done_lock.release()


    def second(self, printSecond: 'Callable[[], None]') -> None:
        self.first_done_lock.acquire()
        
        # printSecond() outputs "second". Do not change or remove this line.
        printSecond()
        
        self.second_done_lock.release()


    def third(self, printThird: 'Callable[[], None]') -> None:

        self.second_done_lock.acquire()
        
        # printThird() outputs "third". Do not change or remove this line.
        
        printThird()
