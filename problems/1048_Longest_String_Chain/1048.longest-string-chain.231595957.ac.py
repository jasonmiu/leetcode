# TO EXPLAIN
# @lc app=leetcode id=1048 lang=python3
#
# [1048] Longest String Chain
#
# https://leetcode.com/problems/longest-string-chain/description/
#
# algorithms
# Medium (51.07%)
# Total Accepted:    18.1K
# Total Submissions: 35.4K
# Testcase Example:  '["a","b","ba","bca","bda","bdca"]'
#
# Given a list of words, each word consists of English lowercase letters.
# 
# Let's say word1 is a predecessor of word2 if and only if we can add exactly
# one letter anywhere in word1 to make it equal to word2.  For example, "abc"
# is a predecessor of "abac".
# 
# A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >=
# 1, where word_1 is a predecessor of word_2, word_2 is a predecessor of
# word_3, and so on.
# 
# Return the longest possible length of a word chain with words chosen from the
# given list of words.
# 
# 
# 
# Example 1:
# 
# 
# Input: ["a","b","ba","bca","bda","bdca"]
# Output: 4
# Explanation: one of the longest word chain is "a","ba","bda","bdca".
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= words.length <= 1000
# 1 <= words[i].length <= 16
# words[i] only consists of English lowercase letters.
# 
# 
# 
# 
# 
#
class Solution:
    
    word_len_dict = None # need init
    cache = {}
    
    def build_word_len_dict(self, words):
        for w in words:
            n = len(w)
            if not n in self.word_len_dict:
                self.word_len_dict[n] = [w]
            else:
                self.word_len_dict[n].append(w)
    
        
    def is_predecessor(self, w1, w2):
        if not (len(w2) - len(w1)) == 1:
            return False
        
        for w in w1:
            if not w in w2:
                return False
        
        return True
        
    def word_clain(self, word, word_len):
        
        #print("wc: word:", word, "len:", word_len)
        if len(word) == 1:
            return 1
        
        if len(word) == 0:
            return 0
        
        len_wcs = []
        max_word_chain_len = 0
        
        if (word_len - 1) in self.word_len_dict:
            for p in self.word_len_dict[word_len-1]:
                if self.is_predecessor(p, word):
                    if p in self.cache:
                        len_wcs.append(self.cache[p])
                    else:
                        len_wcs.append(self.word_clain(p, word_len-1))            
                    
            if len(len_wcs) > 0:
                max_word_chain_len = max(len_wcs) + 1
            else:
                max_word_chain_len = 1
        else:
            max_word_chain_len = 1
            
        return max_word_chain_len
                    
     
    
    def longestStrChain(self, words: List[str]) -> int:
        
        self.word_len_dict = {}
        self.cache = {}
        
        if len(words) == 0:
            return 1
            
        self.build_word_len_dict(words)
        
        print(self.word_len_dict)
       
        max_len = max(self.word_len_dict.keys())
        min_len = min(self.word_len_dict.keys())
        
        max_wc_len = -1
        for i in range(max_len, min_len-1, -1):
            if i in self.word_len_dict:
                for w in self.word_len_dict[i]:
                    
                    if w in self.cache:
                        wc_len = self.cache[w]
                    else:
                        wc_len = self.word_clain(w, i)
                        self.cache[w] = wc_len
                        
                    if wc_len >= max_wc_len:
                        max_wc_len = wc_len
            else:
                wc_len = 1
                if wc_len >= max_wc_len:
                    mac_wc_len = wc_len

                    
        return max_wc_len
            
            
            
            
            
            
            
