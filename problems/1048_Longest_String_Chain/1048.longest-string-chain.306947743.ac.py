#
# @lc app=leetcode id=1048 lang=python3
#
# [1048] Longest String Chain
#
# https://leetcode.com/problems/longest-string-chain/description/
#
# algorithms
# Medium (51.13%)
# Total Accepted:    35.2K
# Total Submissions: 66.5K
# Testcase Example:  '["a","b","ba","bca","bda","bdca"]'
#
# Given a list of words, each word consists of English lowercase letters.
# 
# Let's say word1 is a predecessor of word2 if and only if we can add exactly
# one letter anywhere in word1 to make it equal to word2.  For example, "abc"
# is a predecessor of "abac".
# 
# A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >=
# 1, where word_1 is a predecessor of word_2, word_2 is a predecessor of
# word_3, and so on.
# 
# Return the longest possible length of a word chain with words chosen from the
# given list of words.
# 
# 
# 
# Example 1:
# 
# 
# Input: ["a","b","ba","bca","bda","bdca"]
# Output: 4
# Explanation: one of the longest word chain is "a","ba","bda","bdca".
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= words.length <= 1000
# 1 <= words[i].length <= 16
# words[i] only consists of English lowercase letters.
# 
# 
# 
# 
# 
#
# The idea is sort the words with lengths, and start from the
# longest word, the longest words must have the longest possible
# word chain = 1. We cache this length.
# We decrease the length, and find the predecessor. If we found a 
# predecessor, the longest possible word chain of this predecessor
# is the cached longest possible word chain of succeeder + 1.
# We build up and find the longest possible word chain saved in the cache.

import collections

def build_len_dict(words):
    d = collections.defaultdict(list)
    for w in words:
        d[len(w)].append(w)
        
    return d

def is_pred(w1, w2):
    if set(w1).issubset(set(w2)) and (len(w2) - len(w1) == 1):
        return True
    else:
        return False


class Solution:
    def longestStrChain(self, words: List[str]) -> int:
        
        len_dict = build_len_dict(words)
        cache = {}
        for w in words:
            cache[w] = 1
            
        for i in range(max(len_dict.keys())-1, -1, -1):
            if i not in len_dict:
                continue
                
            for w in len_dict[i]:
                sub_chain_lens = [0]
                for w2 in len_dict[i+1]:
                    if is_pred(w, w2):
                        sub_chain_lens.append(cache[w2])
                cache[w] = max(sub_chain_lens) + 1
                        
        return max(cache.values())
        
        
