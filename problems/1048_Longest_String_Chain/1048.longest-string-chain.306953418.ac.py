#
# @lc app=leetcode id=1048 lang=python3
#
# [1048] Longest String Chain
#
# https://leetcode.com/problems/longest-string-chain/description/
#
# algorithms
# Medium (51.13%)
# Total Accepted:    35.2K
# Total Submissions: 66.5K
# Testcase Example:  '["a","b","ba","bca","bda","bdca"]'
#
# Given a list of words, each word consists of English lowercase letters.
# 
# Let's say word1 is a predecessor of word2 if and only if we can add exactly
# one letter anywhere in word1 to make it equal to word2.  For example, "abc"
# is a predecessor of "abac".
# 
# A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >=
# 1, where word_1 is a predecessor of word_2, word_2 is a predecessor of
# word_3, and so on.
# 
# Return the longest possible length of a word chain with words chosen from the
# given list of words.
# 
# 
# 
# Example 1:
# 
# 
# Input: ["a","b","ba","bca","bda","bdca"]
# Output: 4
# Explanation: one of the longest word chain is "a","ba","bda","bdca".
# 
# 
# 
# 
# Note:
# 
# 
# 1 <= words.length <= 1000
# 1 <= words[i].length <= 16
# words[i] only consists of English lowercase letters.
# 
# 
# 
# 
# 
#
# This is another idea start from shortest length of words.
# The shortest words have the longest chain 1. Then we start from
# next words, by finding the predecessor by removing 1 char, and see
# if we can find such predecessor in the cache. If so, we find the 
# longest chain of the predecessor, and + 1 for the current chain of 
# the word.
# Complexity O(nlogn)
# Space O(n) for cache.

class Solution:
    def longestStrChain(self, words: List[str]) -> int:
        dp = {}
        result = 1

        words_sort_len = sorted(words, key=len)
        
        for word in words_sort_len:
            dp[word] = 1

            for i in range(len(word)):
                
                prev = word[0:i] + word[i+1::]

                if prev in dp:
                    dp[word] = max(dp[prev] + 1, dp[word])

        
        return max(dp.values())

