# The idea is the thread zero must go first, and the even and odd
# threads go after. The first number must be 0, so at the beginning
# we acquired even and odd locks, when zero lock open, so the thread zero
# will go first. The number of zeros must be equal to self.n, use 
# a counter to keep track. If the current x is odd, we release the 
# odd lock and let odd thread to run. If the current x is even,
# we release the even lock and let the even thread to run.
# Any even or odd thread done with printing, it will release the 
# zero lock so the thread zero will run next.
# One issue is how to stop all 3 threads after printing all values.
# Since either thread even or odd must be waiting at the moment,
# when thread zero finds we have enough digits, it break and 
# release all locks to give them a chance to return.
# However, both the even or odd thread may have the last chance to 
# print the current value together since at the last moment
# both of their locks are released. We know the last digit must be the
# self.n, so if the odd or even thread knows it is the last digit it
# checks the self.n to see if it is odd or even and print if it match
# the thread.
# Having 1 thread (thread zero) to managing the return sync is hard,
# since we know which digits to print for each thread, using a range()
# loop for each thread is a better idea like:
# 
# class ZeroEvenOdd:
#     def __init__(self, n):
#         self.n = n
#         self.ct = 0
#         self.gates = [Lock(),Lock(),Lock()]
#         self.gates[0].acquire()
#         self.gates[1].acquire()
#
#     def zero(self, printNumber):
#         for _ in range(self.n):
#             self.gates[2].acquire()
#             printNumber(0)
#             self.ct += 1
#             self.gates[self.ct % 2].release()
#
#     def even(self, printNumber):
#         for _ in range(self.n//2):
#             self.gates[0].acquire()
#             printNumber(self.ct)
#             self.gates[2].release()
#
#     def odd(self, printNumber):
#         for _ in range((self.n+1)//2):
#             self.gates[1].acquire()
#             printNumber(self.ct)
#             self.gates[2].release()

import threading

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        
        self.x = 1
        self.cnt = 0

        self.zero_lock = threading.Lock()
        self.even_lock = threading.Lock()
        self.odd_lock = threading.Lock()
        
        self.even_lock.acquire()
        self.odd_lock.acquire()
        
	# printNumber(x) outputs "x", where x is an integer.
    def zero(self, printNumber: 'Callable[[int], None]') -> None:
        
        while True:
            self.zero_lock.acquire()
            
            printNumber(0)
            
            if (self.x % 2) == 0:
                self.even_lock.release()
            else:
                self.odd_lock.release()
        
            self.cnt += 1
            if self.cnt >= self.n:
                break

        if self.even_lock.locked():
            self.even_lock.release()
            
        if self.odd_lock.locked():
            self.odd_lock.release()
        
    def even(self, printNumber: 'Callable[[int], None]') -> None:
        
        while True:
            self.even_lock.acquire()
            
            if self.cnt >= self.n:
                if self.n % 2 == 0:
                    printNumber(self.x)
            else:
                if self.x % 2 == 0:
                    printNumber(self.x)
                    self.x += 1
            
            if self.zero_lock.locked():
                self.zero_lock.release()
            
            if self.cnt >= self.n:
                break
        
    def odd(self, printNumber: 'Callable[[int], None]') -> None:
        while True:
            self.odd_lock.acquire()
            
            if self.cnt >= self.n:
                if self.n % 2 != 0:
                    printNumber(self.x)
            else:
                if self.x % 2 != 0:
                    printNumber(self.x)
                    self.x += 1

            if self.zero_lock.locked():
                self.zero_lock.release()
            
            if self.cnt >= self.n:
                break


