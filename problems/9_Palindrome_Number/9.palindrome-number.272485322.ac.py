#
# @lc app=leetcode id=9 lang=python3
#
# [9] Palindrome Number
#
# https://leetcode.com/problems/palindrome-number/description/
#
# algorithms
# Easy (45.40%)
# Total Accepted:    712.9K
# Total Submissions: 1.6M
# Testcase Example:  '121'
#
# Determine whether an integer is a palindrome. An integer is a palindrome when
# it reads the same backward as forward.
# 
# Example 1:
# 
# 
# Input: 121
# Output: true
# 
# 
# Example 2:
# 
# 
# Input: -121
# Output: false
# Explanation: From left to right, it reads -121. From right to left, it
# becomes 121-. Therefore it is not a palindrome.
# 
# 
# Example 3:
# 
# 
# Input: 10
# Output: false
# Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
# 
# 
# Follow up:
# 
# Coud you solve it without converting the integer to a string?
# 
#
# We can convert the int to string, and search the string from both sides. But the question
# challanges to not to do convertion.
# This algor still search the digits from both sides, but instead of string search, we use math
# to find each digit.
# First we need to know the lenght (number of digits, num_len) of the input x, by keep dividing 10 until 
# the quotient becomes 0. If the original 'x' is -ve, this will not work since the init condition
# will fail. However if x is -ve it will not be palindrome so we return False at beginning.
# We start seach from both sides, i being the index of most significant figure (num_len - 1) and j being the
# index of least significant figure (0). 
#     121
#   i-^ ^-j
# To get the digit at i, since we start from left hand side and want the right most digit, we use:
# x / (10^i) % 10
# eg: for x = 121, i = 2, x / (10^2) = 1, 1 % 10 = 1
# eg: for x = 121, i = 1, x / (10^1) = 12, 12 % 10 = 2

# To get the digit at j, since we start from right hand side and want the left most digit, we use:
# x % (10^(j+1)) / (10^j)
# eg: for x = 121, j = 0, x % (10^1) = 1, 1 / (10^0) = 1
# eg: for x = 121, j = 1, x % (10^2) = 21, 21 / (10^1) = 2
# We stop to search then i and j cross each other. Two cases:
# num_len is odd, stop when i == j, the digit in the middle does not need to compare
# num_len is even, stop when i < j
# x[i] and x[j] should always be the same. If not, it is not a palindrome and we return False.
# Complexity is O(n) and space complexity is O(1).


class Solution:
    def isPalindrome(self, x: int) -> bool:
        
        num_len = 0
        if x < 0:
            return False
        
        n = x
        if n == 0:
            num_len = 1
        else:
            while n > 0:
                n = n // 10
                num_len = num_len + 1
                
        i = num_len - 1
        j = 0
        
        while i > j:
            l = (x // pow(10, i))  % 10
            r = (x % pow(10, j+1)) // pow(10, j)
            i = i - 1
            j = j + 1
            if l != r:
                return False
            
        return True
                
